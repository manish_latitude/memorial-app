<?php
namespace App\Library;
class General {
    // function to generate random string
    public static function getRandomString($length = 8)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;   
    }

    //get time array
    public static function getTimeData() {
        return  ["00:00" => "12:00 AM","00:30" => "12:30 AM",
                    "01:00" => "01:00 AM","01:30" => "01:30 AM",
                    "02:00" => "02:00 AM","02:30" => "02:30 AM",
                    "03:00" => "03:00 AM","03:30" => "03:30 AM",
                    "04:00" => "04:00 AM","04:30" => "04:30 AM",
                    "05:00" => "05:00 AM","05:30" => "05:30 AM",
                    "06:00" => "06:00 AM","06:30" => "06:30 AM",
                    "07:00" => "07:00 AM","07:30" => "07:30 AM",
                    "08:00" => "08:00 AM","08:30" => "08:30 AM",
                    "09:00" => "09:00 AM","09:30" => "09:30 AM",
                    "10:00" => "10:00 AM","10:30" => "10:30 AM",
                    "11:00" => "11:00 AM","11:30" => "11:30 AM",
                    "12:00" => "12:00 PM","12:30" => "12:30 PM",
                    "13:00" => "01:00 PM","13:30" => "01:30 PM",
                    "14:00" => "02:00 PM","14:30" => "02:30 PM",
                    "15:00" => "03:00 PM","15:30" => "03:30 PM",
                    "16:00" => "04:00 PM","16:30" => "04:30 PM",
                    "17:00" => "05:00 PM","17:30" => "05:30 PM",
                    "18:00" => "06:00 PM","18:30" => "06:30 PM",
                    "19:00" => "07:00 PM","19:30" => "07:30 PM",
                    "20:00" => "08:00 PM","20:30" => "08:30 PM",
                    "21:00" => "09:00 PM","21:30" => "09:30 PM",
                    "22:00" => "10:00 PM","22:30" => "10:30 PM",
                    "23:00" => "11:00 PM","23:30" => "11:30 PM"
                ];
        return  ["1" => "12:00 AM","2" => "12:30 AM",
                    "3" => "01:00 AM","4" => "01:30 AM",
                    "5" => "02:00 AM","6" => "02:30 AM",
                    "7" => "03:00 AM","8" => "03:30 AM",
                    "9" => "04:00 AM","10" => "04:30 AM",
                    "11" => "05:00 AM","12" => "05:30 AM",
                    "13" => "06:00 AM","14" => "06:30 AM",
                    "15" => "07:00 AM","16" => "07:30 AM",
                    "17" => "08:00 AM","18" => "08:30 AM",
                    "19" => "09:00 AM","20" => "09:30 AM",
                    "20" => "10:00 AM","21" => "10:30 AM",
                    "22" => "11:00 AM","23" => "11:30 AM",
                    "24" => "12:00 PM","25" => "12:30 PM",
                    "26" => "01:00 PM","27" => "01:30 PM",
                    "28" => "02:00 PM","29" => "02:30 PM",
                    "30" => "03:00 PM","31" => "03:30 PM",
                    "32" => "04:00 PM","33" => "04:30 PM",
                    "34" => "05:00 PM","35" => "05:30 PM",
                    "36" => "06:00 PM","37" => "06:30 PM",
                    "38" => "07:00 PM","39" => "07:30 PM",
                    "40" => "08:00 PM","41" => "08:30 PM",
                    "42" => "09:00 PM","43" => "09:30 PM",
                    "44" => "10:00 PM","45" => "10:30 PM",
                    "46" => "11:00 PM","47" => "11:30 PM"
                ];        
    }

    //get reminder time array
    public static function reminderData() {
        return ["1" => "Minutes","2" => "Hours","3" => "Days","4" => "Weeks"];
    }

    //address type
    public static function addressTypeArr() {
        return ['1'=>'Office', '2' => 'Residential'];   
    }

    //importance arr
    public static function importanceArr() {
        return ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'];
    }

    //causion arr
    public static function causionArr() {
        return ['1' => 'Caution' , '2' => 'No caution', '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Risk'];
    }

    //sensitivity arr
    public static function sensitivityArr() {
        return ['1' => 'Low' , '2' => 'Medium', '3' => 'High'];
    }
}
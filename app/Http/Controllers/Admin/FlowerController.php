<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\Flower;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;

class FlowerController extends Controller
{
    //
    public function __construct(){

    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function index()
    {
    	$pager = 2;
    	$flower = Flower::get();

    	return view('Admin.flower.list',['date' => $flower]);
    }

    public function perfomaction()
    {
        if(Input::get('action') == 'delete'){
            foreach (Input::get('ids') as $id) {
                $img = Flower::where('id',$id)->first()->image;
                File::delete(public_path('/images/flower/') . $img);
                $flower = Flower::find($id);
                $flower->delete();
            }
            return "true";
        }else{
            foreach (Input::get('ids') as $id) {
                $flower = Flower::find($id);
                $st = Input::get('action') == 'active' ? '1' : '0';
                $flower->status = $st;
                $flower->updated_at = date('Y-m-d H:i:s');

                $flower->save();
            }
            return "true";
        }
    }

    public function arrayData(Datatables $datatables)
    {
    	$builder = Flower::query()->orderBy('position')->select('id','image','name','price','url','status');

    	return $datatables->eloquent($builder)
                ->addColumn('check', function ($flower) {
                    return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=" . $flower->id . " name=\"uid[]\"><span class=\"checkmark\"></span> </label>";
                })
                ->addColumn('order', function($flower){
                            return "<img width=\"70px\" height=\"70px\" src='" . url('public/images/dnd.png') . "' onerror=this.src='" . url('public/images/avatar.png') . "'>";
                        })
                ->editColumn('name',function($flower){
    				return $flower->name;
    			})
    			->editColumn('image',function($flower){
    				return "<img width=\"100px\" height=\"70px\" src='".url('public/images/flower/')."/".$flower->image."'onerror=this.src='".url('public/images/avatar.png')."'>";
    			})
    			->editColumn('price', function($flower){
    				return $flower->price;
    			})
    			->editColumn('url', function($flower){
    				return $flower->url;
    			})
                ->editColumn('status',function($flower){
                    if($flower->status == 1){
                         return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $flower->id . " checked=\"true\" data-off-text=\"Inactive\">
";
                    }else if($flower->status == 0){
                        return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$flower->id."checked=\"true\" data-off-text=\"Inactive\">";
                    }else{
                        return '<span class="badge bg-yellow">Delete</span>';
                    }
                })
				->addColumn('action', function($flower){
					 return "<a href=" . url('admin/flower/edit/' . $flower->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>				 	

					 	<a href=" . url('admin/flower/destroy/' . $flower->id) . " class=\"btn btn-danger btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
				})
				->rawColumns(['id','name','price','url','action','image','status','check','order'])
				->toJson();
    }

    public function create()
    {
    	return view('Admin.flower.add');
    }

    public function store(Request $request)
    {
    	$rules = [
    			'name' => 'required',
    			'price' => 'required|regex:/^([0-9])(?=.*([$%])).+$/',
                'url' => 'required|url'
    		];

    	$message = [
    			'name.required' => 'Name is Required',
    			'price.required' => 'Price is Required',
                'price.regex' => 'Enter valid value',
                'url.required' => 'Url is Required',
                'url.url' => 'Enter Valid URL'
    		];

    	$validator = Validator::make($request->all(),$rules, $message);

    	if($validator->fails()){
    		if(Input::get('id') != null && Input::get('id') > 0){
    			return Redirect::to('admin/flower/edit/'.Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
    		}else{
    			return Redirect::to('admin/flower/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
    		}
    	}else{
    		if($request->file('image')){
	            $image = time().'.'.$request->file('image')->getClientOriginalExtension();
	            $request->file('image')->move(public_path('images/flower'), $image);
	        }
	        
            if($request->status == '' || $flower['status'] == 1){
                $flower['status'] = 1;
            }else{
                $flower['status'] = $request->status;
            }
	        $flower['name'] = $request->name;
			$flower['price'] = $request->price;
			$flower['description'] = $request->description;
			if($request->file('image')){
                $flower['image'] = $image;    
            }else{
                $flower['image'] = $request->old_image;
            }
			$flower['url'] = $request->url;
			$flower['updated_at'] = date('Y-m-d H:i:s');

    		if(isset($request->id)){
    			if($request->file('image')){
	    			$img = Flower::where('id',$request->id)->first()->image;
	                        File::delete(public_path('/images/flower/') . $img);
    			}
    			Flower::where('id',$request->id)->update($flower);
    			return Redirect('admin/flower')->with('success','Flower Updated SuccessFully');
    		}else{
    			$flower1 = new Flower;
    			$flower1->insert($flower);
    			return Redirect('admin/flower')->with('success','Flower Created SuccessFully');
    		}
    	}
    }

    public function edit($id)
    {
    	if($id > 0){
    		$flower = Flower::find($id);

    		if($flower){
    			return View::make('Admin.flower.edit')->with('data',$flower);
    		}else{
    			return View::make('Admin.error.404');
    		}
    	}
    }

    public function updateImage(Request $request)
    {
        $img = Flower::where('id',$request->id)->first()->image;
        File::delete(public_path('/images/flower/').$img);

        $set['image'] = null;
        $set['updated_at'] = date('Y-m-d H:i:s');
        if(Flower::where('id',$request->id)->update($set)){
            return "true";
        }
    }

    public function destroy($id)
    {
    	$flower = Flower::find($id);
    	if($flower){
    		$img = Flower::where('id',$id)->first()->image;
	        File::delete(public_path('/images/flower/') . $img);
    		if(Flower::where('id',$id)->delete()){
    			return Redirect('admin/flower')->with('success','Flower deleted successfully');
    		}
    	
    	}else{
    		return "false";
    	}
    }

    public function updateOrder(Request $request)
    {
        foreach ($request->value as $key => $value) {
            $data['position'] = $key+1;
            $data['updated_at'] = date('Y-m-d H:i:s');
            Flower::where('id',$value)->update($data);
        }
    }
}

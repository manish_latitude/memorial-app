<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;

class CountryController extends Controller
{
    //
    public function __construct(){

    }

    public function index()
    {
    	$pager = 2;
    	$country = Country::get();

    	return view('Admin.country.list',['data' => $country]);
    }

    public function perfomaction()
    {
        if(Input::get('action') == 'delete'){
            foreach (Input::get('ids') as $id) {
                $state = Country::find($id);
                $state->delete();
            }
            return "true";
        }else{
            foreach (Input::get('ids') as $id) {
                $state = Country::find($id);
                $st = Input::get('action') == 'active' ? '1' : '0';
                $state->status = $st;
                $state->updated_at = date('Y-m-d H:i:s');

                $state->save();
            }
            return "true";
        }
    }

    public function arrayData(Datatables $datatables)
    {
    	$builder = Country::query()->select('id','country','status');

    	return $datatables->eloquent($builder)
    			->addColumn('check', function ($country) {
                    return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=" . $country->id . " name=\"uid[]\"><span class=\"checkmark\"></span> </label>";
                })
                ->editColumn('id',function($country){
                	return $country->id;
                })
                ->editColumn('country',function($country){
                	return $country->country;
                })
                ->editColumn('status',function($country){
                    if($country->status == 1){
                         return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $country->id . " checked=\"true\" data-off-text=\"Inactive\">
";
                    }else if($country->status == 0){
                        return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$country->id."checked=\"true\" data-off-text=\"Inactive\">";
                    }else{
                        return '<span class="badge bg-yellow">Delete</span>';
                    }
                })
                ->addColumn('action', function($country){
                     return "<a href=" . url('admin/country/edit/' . $country->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>                 

                        <a href=" . url('admin/country/destroy/' . $country->id) . " class=\"btn btn-danger btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>
                            ";
                })
                ->rawColumns(['id','country','status','action','check'])
                ->toJson();

    }

    public function create()
    {
    	return view('Admin.country.add');
    }

    public function store(Request $request)
    {
    	// if(isset($request->state1) && $request->state1 == 'add'){
    	// 	if($request->state != ''){
    	// 		$state = new State;
    	// 		$state->country_id = $request->id;
    	// 		$state->state = $request->state;
    	// 		$state->created_at = date('Y-m-d H:i:s');
    	// 		$state->save();
    	// 	}
    	// 	return Redirect('/admin/country/state/'.$request->id)->with('success','State added successfully');
    	// }if(isset($request->city1) && $request->city1 == 'add'){
     //        if($request->city != ''){
     //            $city = new City;
     //            $city->state_id = $request->id;
     //            $city->city = $request->city;
     //            $city->created_at = date('Y-m-d H:i:s');
     //            $city->save();
     //        }
     //        return Redirect('/admin/country/city/'.$request->id)->with('success','City added successfully');
     //    }else{
	    	$rules = [
	    			'country' => 'required'
	    		];

	    	$message = [
	    			'country.required' => 'State is required'
	    		];

	    	$validator = Validator::make($request->all(),$rules,$message);

	    	if($validator->fails()){
	    		if(Input::get('id') != null && Input::get('id') > 0){
	                return Redirect::to('admin/country/edit/'.Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
	            }else{
	                return Redirect::to('admin/country/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
	            }
	    	}else{
	    		$country['status'] = $request->status;
	            $country['country'] = $request->country;
	            $country['updated_at'] = date('Y-m-d H:i:s');

	            if(isset($request->id)){
	            	Country::where('id',$request->id)->update($country);
	            	return Redirect('admin/country')->with('success','Country Updated Successfully');
	            }else{
	            	$country1 = new Country;
	            	$country1->insert($country);
	            	return Redirect('admin/country')->with('success','Country Created Successfully');
	            }
	    	}
    	// }
    }

    public function edit($id)
    {
    	if($id > 0){
    		$country = Country::find($id);

    		if($country){
    			return View::make('Admin.country.edit')->with(['data' => $country]);
    		}else{
    			return View::make('Admin.error.404');
    		}
    	}
    }

    public function destroy($id)
    {
    	$country = Country::find($id);
        if($country){
            if(Country::where('id',$id)->delete()){
                return Redirect('admin/country')->with('success','Country deleted successfully');
            }
        
        }else{
            return "false";
        }	
    }

}

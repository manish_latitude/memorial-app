<?php 

namespace App\Http\Controllers\Admin;
/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Library\General;
use App\Models\Faqs;
use Illuminate\Support\Facades\Hash;
use App\Models\Appsettings;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Auth;
use DB;
use Session;

class FaqController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      $pager = 2;
      $faq = Faqs::get();
      return view('Admin.faq.list',['data' => $faq]);
    }

    public function perfomaction()
    {
        if(Input::get('action') == 'delete'){
            foreach (Input::get('ids') as $id) {
                $cms = Faqs::find($id);
                $cms->delete();
            }
            return "true";
        }else{
            foreach (Input::get('ids') as $id) {
                $cms = Faqs::find($id);
                $st = Input::get('action') == 'active' ? '1' : '0';
                $cms->status = $st;
                $cms->updated_at = date('Y-m-d H:i:s');

                $cms->save();
            }
            return "true";
        }
    }

   public function arrayData(Datatables $datatables) {  
        $builder = Faqs::query()->select('id','title','status');  
		
        return $datatables->eloquent($builder)
		      ->addColumn('check', function ($faq) {  
              return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=".$faq->id." name=\"sid[]\"><span class=\"checkmark\"></span> </label>";
          })		
          ->editColumn('status', function($faq) { 
							if ($faq->status == 1) {
                return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$faq->id." checked=\"true\" data-off-text=\"Inactive\">";
              }else{
                return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$faq->id." data-off-text=\"Inactive\">";
              }
          })		                 
          ->addColumn('action', function($faq) {
               return "<a href=" . url('admin/faq/edit/' . $faq->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>         

                <a href=" . url('admin/faq/destroy/' . $faq->id) . " class=\"btn btn-danger btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
          })
          ->rawColumns(['check','id','title','status','action'])
          ->toJson();
    }  

    public function create()
    {
      return view('Admin.faq.add');
    }

    public function store(Request $request)
    {
      $rules = [
              'title' => 'required'
            ];

      $message = [
              'title.required' => 'Title is required'
            ];

      $validator = Validator::make($request->all(),$rules,$message);

      if($validator->fails()){
        if(Input::get('id') != null && Input::get('id') > 0){
            return Redirect::to('admin/faq/edit/'.Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
        }else{
            return Redirect::to('admin/faq/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
        }
      }else{
        $faq['title'] = $request->title;
        $faq['description'] = $request->description;
        $faq['status'] = $request->status;
        $faq['updated_at'] = date('Y-m-d H:i:s');

        if(isset($request->id)){
            Faqs::where('id',$request->id)->update($faq);
            return Redirect('admin/faq')->with('success','Faq Updated Successfully');
        }else{
            $faqObj = new Faqs;
            $faqObj->insert($faq);
            return Redirect('admin/faq')->with('success','Faq Created Successfully');
        }
      }
    }

    public function edit($id)
    {
      $faq = Faqs::where('id',$id)->first();

      return view('Admin.faq.edit',['data' => $faq]);
    }

    public function destroy($id)
    {
        $faq = Faqs::find($id);
        if($faq){
            if(Faqs::where('id',$id)->delete()){
                return Redirect('admin/faq')->with('success','Faq deleted successfully');
            }
        }else{
            return "false";
        }
    }
}
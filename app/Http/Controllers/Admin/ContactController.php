<?php

namespace App\Http\Controllers\Admin;
/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;

class ContactController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('Admin.Contact-us.contact-us');
         
    }

    /*
        return add view language keyword
    */
    public function add(Request $request) {
        
    }

    /*
        return edit view language keyword
    */
    public function edit(Request $request,$id = 0) {
 
    }

    /*
        add and edit conatct
    */
    public function store(Request $request) {
 
    }

    /*
        add and edit spouse contact
    */
    
}

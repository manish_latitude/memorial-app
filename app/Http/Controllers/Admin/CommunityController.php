<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Community;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;

class CommunityController extends Controller
{
    //
    public function __construct(){

    }

    public function index()
    {
    	$pager = 2;
    	$community = Community::get();

    	return view('Admin.community.list',['data' => $community]);
    }

    public function perfomaction()
    {
        if(Input::get('action') == 'delete'){
            foreach (Input::get('ids') as $id) {
                $com = Community::find($id);
                $com->delete();
            }
            return "true";
        }else{
            foreach (Input::get('ids') as $id) {
                $com = Community::find($id);
                $c = Input::get('action') == 'active' ? '1' : '0';
                $com->status = $c;
                $com->updated_at = date('Y-m-d H:i:s');

                $com->save();
            }
            return "true";
        }
    }

    public function arrayData(Datatables $datatables)
    {
    	$builder = Community::query()->select('id','community','status');

    	return $datatables->eloquent($builder)
    			->addColumn('check', function ($community) {
                    return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=" . $community->id . " name=\"uid[]\"><span class=\"checkmark\"></span> </label>";
                })
                ->editColumn('id',function($community){
                	return $community->id;
                })
                ->editColumn('community',function($community){
                	return $community->community;
                })
                ->editColumn('status',function($community){
                    if($community->status == 1){
                         return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $community->id . " checked=\"true\" data-off-text=\"Inactive\">
";
                    }else if($community->status == 0){
                        return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$community->id."checked=\"true\" data-off-text=\"Inactive\">";
                    }else{
                        return '<span class="badge bg-yellow">Delete</span>';
                    }
                })
                ->addColumn('action', function($community){
                     return "<a href=" . url('admin/community/edit/' . $community->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>                 

                        <a href=" . url('admin/community/destroy/' . $community->id) . " class=\"btn btn-danger btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>
                            ";
                })
                ->rawColumns(['id','community','status','action','check'])
                ->toJson();

    }

    public function create()
    {
    	return view('Admin.community.add');
    }

    public function store(Request $request)
    {
    	$rules = [
    			'community' => 'required'
            ];

    	$message = [
    			'community.required' => 'Community is required'
    		];

    	$validator = Validator::make($request->all(),$rules,$message);

    	if($validator->fails()){
    		if(Input::get('id') != null && Input::get('id') > 0){
                return Redirect::to('admin/community/edit/'.Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
            }else{
                return Redirect::to('admin/community/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
            }
    	}else{
    		$community['status'] = $request->status;
            if(Community::where('community',$request->community)->exists()){
                return Redirect('admin/community')->with('error','Community Already Exists');
            }else{
                $community['community'] = $request->community;
            }
            $community['updated_at'] = date('Y-m-d H:i:s');

            if(isset($request->id)){
            	Community::where('id',$request->id)->update($community);
                return Redirect('admin/community')->with('success','Community Updated Successfully');
            }else{
            	$community1 = new Community;
            	$community1->insert($community);
            	return Redirect('admin/community')->with('success','Community Created Successfully');
            }
    	}
    }

    public function edit($id)
    {
    	if($id > 0){
    		$community = Community::find($id);

    		if($community){
    			return View::make('Admin.community.edit')->with(['data' => $community]);
    		}else{
    			return View::make('Admin.error.404');
    		}
    	}
    }

    public function destroy($id)
    {
    	$community = Community::find($id);
        if($community){
            if(Community::where('id',$id)->delete()){
                return Redirect('admin/community')->with('success','Community deleted successfully');
            }
        
        }else{
            return "false";
        }	
    }

}

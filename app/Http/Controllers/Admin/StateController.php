<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;

class StateController extends Controller
{
    //
    public function __construct(){

    }

    public function index()
    {
    	$pager = 2;
    	$state = State::get();

    	return view('Admin.state.list',['data' => $state]);
    }

    public function perfomaction()
    {
        if(Input::get('action') == 'delete'){
            foreach (Input::get('ids') as $id) {
                $state = State::find($id);
                $state->delete();
            }
            return "true";
        }else{
            foreach (Input::get('ids') as $id) {
                $state = State::find($id);
                $st = Input::get('action') == 'active' ? '1' : '0';
                $state->status = $st;
                $state->updated_at = date('Y-m-d H:i:s');

                $state->save();
            }
            return "true";
        }
    }

    public function arrayData(Datatables $datatables)
    {
    	$builder = State::query()->select('id','state','status');

    	return $datatables->eloquent($builder)
    			->addColumn('check', function ($state) {
                    return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=" . $state->id . " name=\"uid[]\"><span class=\"checkmark\"></span> </label>";
                })
                ->editColumn('id',function($state){
                	return $state->id;
                })
                ->editColumn('state',function($state){
                	return $state->state;
                })
                ->editColumn('status',function($state){
                    if($state->status == 1){
                         return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $state->id . " checked=\"true\" data-off-text=\"Inactive\">
";
                    }else if($state->status == 0){
                        return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$state->id."checked=\"true\" data-off-text=\"Inactive\">";
                    }else{
                        return '<span class="badge bg-yellow">Delete</span>';
                    }
                })
                ->addColumn('action', function($state){
                     return "<a href=" . url('admin/state/edit/' . $state->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>                 

                        <a href=" . url('admin/state/destroy/' . $state->id) . " class=\"btn btn-danger btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>
                        <a href=".url('admin/state/city/' . $state->id)." class=\"btn btn-primary submit\" title='City'>City</a>
                            ";
                })
                ->rawColumns(['id','state','status','action','check'])
                ->toJson();

    }

    public function create()
    {
        $data['country'] = Country::orderBy('id')->pluck('country','id');
    	return view('Admin.state.add',$data);
    }

    public function store(Request $request)
    {
    	if(isset($request->city1) && $request->city1 == 'add'){
    		if($request->city != ''){
    			$city = new City;
    			$city->state_id = $request->id;
    			$city->city = $request->city;
    			$city->created_at = date('Y-m-d H:i:s');
    			$city->save();
    		}
    		return Redirect('/admin/state/city/'.$request->id)->with('success','City added successfully');
    	}else{
	    	$rules = [
	    			'state' => 'required'
	    		];

	    	$message = [
	    			'state.required' => 'State is required'
	    		];

	    	$validator = Validator::make($request->all(),$rules,$message);

	    	if($validator->fails()){
	    		if(Input::get('id') != null && Input::get('id') > 0){
	                return Redirect::to('admin/state/edit/'.Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
	            }else{
	                return Redirect::to('admin/state/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
	            }
	    	}else{
                $state['country_id'] = $request->country;
	    		$state['status'] = $request->status;
	            $state['state'] = $request->state;
	            $state['updated_at'] = date('Y-m-d H:i:s');

	            if(isset($request->id)){
	            	State::where('id',$request->id)->update($state);
	            	return Redirect('admin/state')->with('success','State Updated Successfully');
	            }else{
	            	$state1 = new State;
	            	$state1->insert($state);
	            	return Redirect('admin/state')->with('success','State Created Successfully');
	            }
	    	}
    	}
    }

    public function edit($id)
    {
    	if($id > 0){
            $data['country'] = Country::orderBy('id')->pluck('country','id');
    		$data['state'] = State::find($id);

    		if($data){
    			return View::make('Admin.state.edit')->with($data);
    		}else{
    			return View::make('Admin.error.404');
    		}
    	}
    }

    public function destroy($id)
    {
    	$state = State::find($id);
        if($state){
            if(State::where('id',$id)->delete()){
                return Redirect('admin/state')->with('success','State deleted successfully');
            }
        
        }else{
            return "false";
        }	
    }

    public function city(Request $request,$id=0)
    {
    	$state = State::find($id);
    	$city = City::where('state_id',$id)->get();
    	return view('Admin.state.city')->with('state',$state)->with('City',$city);
    }

    public function cityUpdate(Request $request)
    {
    	$state = State::find($request->id);

    	if(count($request->city) > 0){
    		foreach ($request->city as $id => $city) {
    			foreach ($city as $cid => $value) {
    				$city1 = City::find($id);
    				$city1->state_id = $state->id;
    				$city1->city = $value;
    				$city1->updated_at = date('Y-m-d H:i:s');
    				$city1->save();
    			}
    		}
    		return Redirect('/admin/state/city/'.$request->id)->with('success','City Updated Successfully');
    	}else{
    		return Redirect('/admin/state/city/'.$request->id)->with('error','Something went wrong');
    	}
    }

    public function cityDelete($id)
    {
    	$city = City::find($id);
    	if($city->delete()){
    		return "true";
    	}else{
    		return redirect()->back()->with('Record Not Found');
    	}
    }

}

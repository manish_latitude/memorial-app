<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\Condolence;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;

class CondolenceController extends Controller {

    //
    public function __construct() {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $pager = 2;
        $condolence = Condolence::get();

        return view('Admin.condolence.list', ['data' => $condolence]);
    }

    public function perfomaction() {
        if (Input::get('action') == 'delete') {
            $img = Condolence::where('id', $id)->first()->image;
            File::delete(public_path('/images/condolence/') . $img);
            foreach (Input::get('ids') as $id) {
                $con = Condolence::find($id);
                $con->delete();
            }
            return "true";      
        } else {
            foreach (Input::get('ids') as $id) {
                $con = Condolence::find($id);
                $st = Input::get('action') == 'active' ? '1' : '0';
                $con->status = $st;
                $con->updated_at = date('Y-m-d H:i:s');

                $con->save();
            }
            return "true";
        }
    }

    public function arrayData(Datatables $datatables) {
        $builder = Condolence::query()->orderBy('position','asc')->select('id', 'image', 'name', 'price', 'url', 'status');

        return $datatables->eloquent($builder)
                        ->addColumn('check', function ($con) {
                            return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=" . $con->id . " name=\"uid[]\"><span class=\"checkmark\"></span> </label>";
                        })
                        ->addColumn('order', function($con){
                            return "<img width=\"70px\" height=\"70px\" src='" . url('public/images/dnd.png') . "' onerror=this.src='" . url('public/images/avatar.png') . "'>";
                        })
                        ->editColumn('name', function($con) {
                            return $con->name;
                        })
                        ->editColumn('image', function($con) {
                            return "<img width=\"100px\" height=\"70px\" src='" . url('public/images/condolence/') . "/" . $con->image . "' onerror=this.src='" . url('public/images/avatar.png') . "'>";
                        })
                        ->editColumn('price', function($con) {
                            return $con->price;
                        })
                        ->editColumn('url', function($con) {
                            return $con->url;
                        })
                        ->editColumn('status', function($con) {
                            if ($con->status == 1) {
                                return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $con->id . " checked=\"true\" data-off-text=\"Inactive\">
";
                            } else if ($con->status == 0) {
                                return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $con->id . "checked=\"true\" data-off-text=\"Inactive\">";
                            } else {
                                return '<span class="badge bg-yellow">Delete</span>';
                            }
                        })
                        ->addColumn('action', function($con) {
                            return "<a href=" . url('admin/condolence/edit/' . $con->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>				 	

					 	<a href=" . url('admin/condolence/destroy/' . $con->id) . " class=\"btn btn-danger btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
                        })
                        ->rawColumns(['id', 'name', 'price', 'url', 'action', 'image', 'status', 'check','order'])
                        ->toJson();
    }

    public function create() {
        return view('Admin.condolence.add');
    }

    public function store(Request $request) {
        $rules = [
            'name' => 'required',
            'price' => 'required|regex:/^([0-9])(?=.*([$%])).+$/',
            'url' => 'required|url'
        ];

        $message = [
            'name.required' => 'Name is Required',
            'price.required' => 'Price is Required',
            'price.regex' => 'Enter valid value',
            'url.required' => 'Url is Required',
            'url.url' => 'Enter Valid Url'
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            if (Input::get('id') != null && Input::get('id') > 0) {
                return Redirect::to('admin/condolence/edit/' . Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
            } else {
                return Redirect::to('admin/condolence/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
            }
        } else {
            if ($request->file('image')) {
                $image = time() . '.' . $request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(public_path('images/condolence'), $image);
            } 

            if ($request->status == '' || $con['status'] == 1) {
                $con['status'] = 1;
            } else {
                $con['status'] = $request->status;
            }
            $con['name'] = $request->name;
            $con['price'] = $request->price;
            $con['description'] = $request->description;
            if($request->file('image')){
                $con['image'] = $image;    
            }else{
                $con['image'] = $request->old_image;
            }
            $con['url'] = $request->url;
            $con['updated_at'] = date('Y-m-d H:i:s');
            
            if (isset($request->id)) {
                if ($request->file('image')) {
                    $img = Condolence::where('id', $request->id)->first()->image;
                    File::delete(public_path('/images/condolence/') . $img);
                }
                Condolence::where('id', $request->id)->update($con);
                return Redirect('admin/condolence')->with('success', 'Condolence Updated SuccessFully');
            } else {
                $con1 = new Condolence;
                $con1->insert($con);
                return Redirect('admin/condolence')->with('success', 'Condolence Created SuccessFully');
            }
        }
    }

    public function edit($id) {
        if ($id > 0) {
            $con = Condolence::find($id);

            if ($con) {
                return View::make('Admin.condolence.edit')->with('data', $con);
            } else {
                return View::make('Admin.error.404');
            }
        }
    }

    public function updateImage(Request $request) {
        $img = Condolence::where('id', $request->id)->first()->image;

        File::delete(public_path('/images/condolence/') . $img);

        $set['image'] = null;
        $set['updated_at'] = date('Y-m-d H:i:s');

        if (Condolence::where('id', $request->id)->update($set)) {
            return "true";
        }
    }

    public function destroy($id) {
        $con = Condolence::find($id);
        if ($con) {
            $img = Condolence::where('id', $id)->first()->image;
            File::delete(public_path('/images/condolence/') . $img);
            if (Condolence::where('id', $id)->delete()) {

                return Redirect('admin/condolence')->with('success', 'Condolence deleted successfully');
            }
        } else {
            return "false";
        }
    }

    public function updateOrder(Request $request)
    {
        foreach ($request->value as $key => $value) {
            $data['position'] = $key+1;
            $data['updated_at'] = date('Y-m-d H:i:s');
            Condolence::where('id',$value)->update($data);
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Library\General;
use App\Models\Appsettings;
use App\Models\AppAdmob;
use App\Models\Admob;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Auth;
use DB;
use Session;

class AdsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // echo $request->id;die;
        return view('Admin.ads.list');
        // image content youtube url wesite url
    }

    public function create() {
        $appdata = Appsettings::orderBy('id')->pluck('app_name', 'id');
		$already_app = AppAdmob::orderBy('app_id')->pluck('app_id'); 
		foreach($already_app as $app){
		  $ap=explode(",",$app);
		  foreach($ap as $a){
		    $app_exist[]=$a;
		  }
		}
        return view('Admin.ads.add',["appdata"=>$appdata,'appalready'=>$app_exist]);
    }

    public function store(Request $request) {
        
        $rules = [
            'app_id' => 'required',
            'admob_title' => 'required|max:255',
			
			
           // 'content' => 'required',
        ];

        $message = [
            "admob_title.required" => "Title is required",
            "admob_title.max" => "Title must be less than 255 characters",
            "app_id.required"=> "App is Required",
			
			
            // "app_slug.max"          => "Slug must be less than 255 characters",
            //"content.required" => "content is required",
        ];
		for($i=0;$i<=Input::get('totalrow');$i++){
		 if(Input::get('ads_key')[$i] == ''){
		 $rules['ads_key[]']='required';
		$message['ads_key[].required']="Content is required";
		 }
		  if(Input::get('ads_value')[$i] == ''){
		 $rules['ads_value[]']='required';
		$message['ads_value[].required']="value is required";
		 }
		  if(Input::get('ads_type')[$i] == ''){
		 $rules['ads_type[]']='required';
		$message['ads_type[].required']="type is required";
		 }
		 if(Input::get('ads_des')[$i] == ''){
		 $rules['ads_des[]']='required';
		$message['ads_des[].required']="type is required";
		 }	
		}
		// print_r(Input::get('app_id')); exit;
        // run the validation rules on the inputs from the form 
        $validator = Validator::make(Input::all(), $rules, $message);
        // process the login
        if ($validator->fails()) {
            if (Input::get('id') != null && Input::get('id') > 0) {
                return Redirect::to('ads/edit/' . Input::get('id'))->withErrors($validator);
            } else {
                return Redirect::to('ads/create/')->withErrors($validator);
            }
        }

      
        $dataSettingObj = "";
        if (isset($request->id)) {
            $dataSettingObj = AppAdmob::find($request->id);
             $dataSettingObj->updated_at = date('Y-m-d H:i:s');
			 $dataSettingObj->updated_by = Auth::user()->id;
        } else {
            $dataSettingObj = new AppAdmob();
            $dataSettingObj->created_at = date('Y-m-d H:i:s');
			$dataSettingObj->created_by = Auth::user()->id;
            $dataSettingObj->updated_by = Auth::user()->id;
        }
$appid=implode(",",Input::get('app_id'));
        $dataSettingObj->app_id = $appid;
        $dataSettingObj->admob_title = Input::get('admob_title');
        
        $dataSettingObj->status = Input::get('status');


        if ($dataSettingObj->save()) {
            if (Input::get('id') != null && Input::get('id') > 0) {
			 $dataadObj = "";
			 
			   $oldids=explode(",",Input::get('oldids'));
			 
				for($i=0;$i<=Input::get('totalrow');$i++){
				if(isset(Input::get('adid')[$i])){
				$dataadObj = Admob::find(Input::get('adid')[$i]);
				$dataadObj->updated_at = date('Y-m-d H:i:s');
				$oldads[]=Input::get('adid')[$i];
						}else{
						$dataadObj = new Admob();
						$dataadObj->created_at = date('Y-m-d H:i:s');
						}	
			               
			$dataadObj->admob_id = $request->id;
            $dataadObj->ad_key = Input::get('ads_key')[$i];
			$dataadObj->ad_value = Input::get('ads_value')[$i];
			$dataadObj->ad_type = Input::get('ads_type')[$i];
			$dataadObj->description = Input::get('ads_des')[$i];
			$dataadObj->save();
				 }
				
				 $diff=array_diff($oldids,$oldads);
				 
				 foreach($diff as $df){ 
				   $Admob    = Admob::find($df);
					if (count($Admob) > 0) {
			               $Admob->delete(); }
							          }
									  
                return redirect('/ads')->with('success', "Data updated successfully.");
            } else {
			  
			    $dataadObj = "";
				for($i=0;$i<=Input::get('totalrow');$i++){
						  
								$dataadObj = new Admob();
								$dataadObj->created_at = date('Y-m-d H:i:s');
							
			               
			$dataadObj->admob_id = $dataSettingObj->id;
            $dataadObj->ad_key = Input::get('ads_key')[$i];
			$dataadObj->ad_value = Input::get('ads_value')[$i];
			$dataadObj->ad_type = Input::get('ads_type')[$i];
			$dataadObj->description = Input::get('ads_des')[$i];
			$dataadObj->save();
				 }
                return redirect('/ads')->with('success', "Data created successfully.");
            }
        } else {
            return redirect('/ads')->with('success', "something went wrong to save setting title please try later.");
        }
    }

    public function edit($id) { //echo $id;
        if ($id > 0) {
            $appdata = Appsettings::orderBy('id')->pluck('app_name', 'id');
            $dataSettingDetail = AppAdmob::find($id); 
			$dataDetail = Admob::where('admob_id',$id)->get(); 
			$totalrow= count($dataDetail)-1; 
			$already_app = AppAdmob::orderBy('app_id')->pluck('app_id')->where("deleted_at",NULL); 
		foreach($already_app as $app){
		  $ap=explode(",",$app);
		  foreach($ap as $a){
		    $app_exist[]=$a;
		  }
		}
            //echo "<pre>";print_r($dataSettingDetail);
            if (count($dataSettingDetail) > 0) {
                return view('Admin.ads.edit', ["data" => $dataSettingDetail,"ad"=>$dataDetail,"totalrow"=>$totalrow, 'appdata' => $appdata,'appalready'=>$app_exist]);
            }
            return View::make('Admin.error.404');
        }
    }

    public function arrayData(Datatables $datatables) {

        $builder = AppAdmob::query()->select('id', 'app_id', 'admob_title','status', 'created_at');
        return $datatables->eloquent($builder)
		 ->addColumn('check', function ($user) {  
                                return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=".$user->id." name=\"sid[]\"><span class=\"checkmark\"></span> </label>";
                            })
                        ->editColumn('app_id', function ($user) { $appdata = Appsettings::find(explode(",",$user->app_id)); $apname=array(); foreach($appdata as $ap){ $apname[]=$ap->app_name; } $apname=implode(",",$apname);
                            return $apname;
                        })
                        ->editColumn('admob_title', function ($user) {
                            return $user->admob_title;
                        })
						 ->editColumn('created_at', function ($user) {
                            return $user->created_at;
                        })
                        ->editColumn('status', function($user) {
                            if ($user->status == 'active') {
                                return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$user->id." checked=\"true\" data-off-text=\"Inactive\">";
                            } else if ($user->status == 'inactive') {
                                return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$user->id." data-off-text=\"Inactive\">";
                            } else {
                                return '<span class="badge bg-yellow">Delete</span>';
                            }
                        })
                        ->addColumn('action', function($user) {
                            return "<a href=" . url('ads/edit/' . $user->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>  
                                             <a href=\"#myModal\" data-toggle=\"modal\"  onclick=\"$('#deleteurl').attr('href','".url('ads/delete/' . $user->id)."');\" class=\"btn btn-danger delete-btn btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
                        })
                        ->rawColumns(['check','app_id','admob_title', 'created_at', 'status', 'action'])
                        ->toJson();
    }

    public function perfomaction(){
	   if(Input::get('action') == 'delete'){
	     foreach(Input::get('ids') as $id){
		    $AppAdmob    = AppAdmob::find($id);
			$AppAdmob->delete();
		 }
		 return "true";
	   }
	   else{
	     foreach(Input::get('ids') as $id){
		    $AppAdmob    = AppAdmob::find($id);
			$st=Input::get('action') == 'active'?'active':'inactive';
			$AppAdmob->status  = $st;
			$AppAdmob->updated_at = date('Y-m-d H:i:s');
			 
            $AppAdmob->updated_by = Auth::user()->id;
			$AppAdmob->save();
		 }
		 return "true";
	   }
	}


public function delete(Request $request, $id = 0) {

        $AppAdmob    = AppAdmob::find($id);
        if (count($AppAdmob) > 0) {

            if($AppAdmob->delete()) {
			
			 $Admob    = Admob::where('admob_id',$id)->delete();
        

           
                return redirect('/ads')->with('success', "Content deleted successfully.");
				
				
				
            } else {
                return redirect('/ads')->with('error', 'Something went wrong please try later');
            }
        }
        return redirect('/ads')->with('error', trans('Record not found'));
    }
	
	}
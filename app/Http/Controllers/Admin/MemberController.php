<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\Funeral;
use App\Models\Member;
use App\Models\AppUser;
use App\Models\MemberImage;
use App\Models\OrderOfService;
use App\Models\Template;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\TemplateImage;
use App\Models\Community;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Auth;
use DB;
use Excel;
use Mail;

class MemberController extends Controller
{
    //
    public function __construct(){

    }

    public function index()
    {
    	$pager = 2;
    	$member = Member::get();

    	return view('Admin.member.list',['data' => $member]);
    }

    public function export()
    {
        $data = Member::join('funeral','member.funeral_id','=','funeral.id')
                        ->select('member.id','funeral.home_name','member.name','member.dateOfbirth','member.dateOfpass','member.created_at')
                        ->get();
        Excel::create('Member Details', function($excel) use($data) {
        $excel->sheet('Member Details', function($sheet) use($data) {
            $sheet->fromArray($data);
        });
        })->export('xls');
    }

    public function perfomaction()
    {
    	if(Input::get('action') == 'delete'){
    		foreach (Input::get('ids') as $id) {
                $pro = Member::where('id',$id)->first()->image;
                File::delete(public_path('/images/member-profile/').$pro);
    			$member = Member::find($id);
    			$member->delete();

                if(MemberImage::where('member_id',$id)->exists()){
                    $img = MemberImage::where('member_id',$id)->first()->image_video;
                        File::delete(public_path('/images/member/') . $img);
                    MemberImage::where('member_id',$id)->delete();
                }

                if(OrderOfService::where('member_id',$id)->exists()){
                    $order = OrderOfService::where('member_id',$id)->get();
                    File::delete(public_path('/images/OrderOfService/').$order);
                    OrderOfService::where('member_id',$id)->delete();   
                }
    		}
            return "true";
    	}else if(Input::get('action') == 'approve'){
            foreach (Input::get('ids') as $id) {
                $update['approval'] = 1;
                $update['status'] = 1;
                $update['updated_at'] = date('Y-m-d H:i:s');

                $data = Member::where('id',$id)->update($update);
            }
            return "true";
        }else if(Input::get('action') == 'reject'){
            foreach (Input::get('ids') as $id) {
                $update['approval'] = 2;
                $update['status'] = 0;
                $update['updated_at'] = date('Y-m-d H:i:s');

                $data = Member::where('id',$id)->update($update);
            }
            return "true";
        }else{
    		foreach (Input::get('ids') as $id) {
    			$member = Member::find($id);
    			$st = Input::get('action') == 'active' ? '1' : '0';
    			$member->status = $st;
    			$member->updated_at = date('Y-m-d H:i:s');
    			$member->save();
    		}
    		return "true";
    	}
    }

    public function arrayData(Datatables $datatables)
    {
    	// $builder = Member::query()->select('id','funeral_id','name','dateOfbirth','dateOfpass','status');

        $builder = DB::table('member')
                    ->leftjoin('funeral','funeral.id','=','member.funeral_id')
                    ->leftjoin('community','community.id','=','member.community_id')
                    ->select('member.*')
                    ->orderBy('approval','ASC')
                    ->orderBy('id','DESC');

    	// return $datatables->eloquent($builder)
        return Datatables::of($builder)
            ->addColumn('check', function ($funeral) {
                return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=" . $funeral->id . " name=\"uid[]\"><span class=\"checkmark\"></span> </label>";
            })
            ->editColumn('id',function($member){
                return $member->id;
            })
    		->editColumn('funeral_id',function($member){
                if($member->funeral_id == 0){
                    return 'None';   
                }else{
    			     return $name = Funeral::where('id',$member->funeral_id)->pluck('home_name')->toArray();
                }
    		})
            ->editColumn('community_id',function($member){
                return $com = Community::where('id',$member->community_id)->pluck('community')->toArray();
            })
    		->editColumn('name',function($member){
    			return $member->name;
    		})
    		->editColumn('dateOfbirth',function($member){
    			return $member->dateOfbirth;
    		})
    		->editColumn('dateOfpass',function($member){
    			return $member->dateOfpass;
    		})
    		->editColumn('approval',function($funeral){
                    if($funeral->approval == 0){
                        return " <button type=\"button\" class=\"btn btn-primary submit\" title='Pending' data-toggle='modal' data-id=".$funeral->id." data-target='#approvalModal'>Pending</button>";
                    }else if($funeral->approval == 1){
                        if($funeral->status == 1){
                            return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $funeral->id . " checked=\"true\" data-off-text=\"Inactive\">";
                        }else if($funeral->status == 0){
                            return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$funeral->id."checked=\"true\" data-off-text=\"Inactive\">";
                        }else{
                             return '<span class="badge bg-yellow">Delete</span>';
                        }    
                    }else if($funeral->approval == 2){
                        return "Rejected";
                    }
                })
			->addColumn('action',function($member){
				return "<a href=" . url('admin/member/edit/' . $member->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>				 	

				 	<a href=" . url('admin/member/destroy/' . $member->id) . " class=\"btn btn-danger btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
			})
			->rawColumns(['funeral_id','name','dateOfpass','dateOfbirth','approval','status','action','check','community_id'])
			->toJson();
    }

    public function create()
    {
        // $data['country'] = Country::orderBy('id')->pluck('country','id');
        $data['country'] = Country::get();
    	$data['funeral'] = Funeral::where('home_name','!=',null)->where('approval',1)->where('status',1)->orderBy('id','desc')->pluck('home_name','id');
        $data['community'] = Community::orderBy('id')->pluck('community','id');
    	return view('Admin.member.add',$data);
    }

    public function store(Request $request)
    {
    	$rules = [
    			'funeral_home' => 'required',
    			'member_name' => 'required',
    			'dateOfbirth' => 'required|date',
    			'dateOfpass' => 'required|date|after_or_equal:dateOfbirth',
                'country' => 'required',
                // 'state' => 'required',
                // 'city' => 'required',
                // 'community' => 'required_without:other',
                // 'other' => 'required_without:community|unique:community,community'
    		];

    	$message = [
    			'funeral_home.required' => 'Select Funeral Home',
    			'member_name.required' => 'Enter Deseased Member name',
    			'dateOfbirth.required' => 'Select Birthdate',
    			'dateOfbirth.date' => 'Select Valid Date',
    			'dateOfpass.required' => 'Select Passing Date',
    			'dateOfpass.date' => 'Select Valid Date',
                'dateOfpass.after_or_equal' => 'Passing date not before birthdate',
                'country.required' => 'Select Country',
                // 'state.required' => 'Select State',
                // 'city.required' => 'Select City',
                // 'community.required_without:other' => 'Please Select or Add Community',
                // 'other.required_without:community' => 'Please Select or Add Community',
                // 'communityText.unique:community' => 'Community Already Added'
    		];

    	$validator = Validator::make($request->all(),$rules,$message);

    	if($validator->fails()){
    		return Redirect::to('admin/member/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
    	}else{
    		$image = $request->hasfile('image');
    		$image1 = $request->hasfile('order');
    		$video = Input::get('video');
			//$video = explode(',', $v);

	       	if($request->hasfile('image')){
	            foreach($request->file('image') as $img){
			        $filename = $img->getClientOriginalName();
			        $extension = $img->getClientOriginalExtension();
			        $picture[] = date('His').$filename;
			        $img->move(public_path('images/member'), date('His').$filename);
			    }
	        }else{
	            $image = "";
	        }

	        if($request->hasfile('order')){
	            foreach($request->file('order') as $img){
			        $filename = $img->getClientOriginalName();
			        $extension = $img->getClientOriginalExtension();
			        $pictures[] = date('His').$filename;
			        $img->move(public_path('images/OrderOfService'), date('His').$filename);
			    }
	        }else{
	            $image1 = "";
	        }

	        $member = new Member;

	        $member->user_id = Auth::user()->id;
	        $member->funeral_id = $request->funeral_home;
            if($request->community == 'Other'){
                $com = new Community;
                $com->community = $request->other;
                if($com->save()){
                    $member->community_id = $com->id;
                }
            }else{
                $member->community_id = $request->community; 
            }
	        $member->name = $request->member_name;
            $member->country_id = $request->country;
            // $member->state_id = $request->state;
            // $member->city_id = $request->city;
	        $member->dateOfbirth = date("Y-m-d", strtotime($request->dateOfbirth));
	        $member->dateOfpass = date("Y-m-d", strtotime($request->dateOfpass));
	       	$member->about = $request->about;
	        // $member->area = $request->area;
	        $member->created_at = date('Y-m-d H:i:s');
	        $member->updated_at = date('Y-m-d H:i:s');
	        $member->save();

	        if($request->hasfile('image')){
                foreach ($picture as $value) {
                    $file = new MemberImage;

                    $file->member_id = $member->id;
                    $file->image_video = $value;
                    $file->type = 1;

                    $file->save();
                }
			}

			if($video){
				foreach ($video as $value) {
					$file = new MemberImage;

					$file->member_id = $member->id;
					$file->image_video = $value;
					$file->type = 2;

					$file->save();
				}
			}

			if($request->hasfile('order')){
                foreach ($pictures as $value) {
                    $file = new OrderOfService;

                    $file->member_id = $member->id;
                    $file->image = $value;

                    $file->save();
                }
			}

			if($member->save()){
				return Redirect('admin/member')->with('success','Member Created SuccessFully');
			}
    	}
    }

    public function edit($id)
    {
    	if ($id > 0) {
		 	$data['funeral'] = Funeral::orderBy('id')->pluck('home_name', 'id');

            $data['country'] = Country::orderBy('id')->pluck('country','id');
            // $data['country'] = Country::get();
            $data['state'] = State::pluck('state','id');
            $data['city'] = City::pluck('city','id');

            $data['community'] = Community::get();

		 	$data['member'] = member::find($id);

		 	$img[] = MemberImage::where('member_id',$id)->where('type',1)->get();
    		$data['mimg'] = $img;

    		$video[] = MemberImage::where('member_id',$id)->where('type',2)->get();
    		$data['mv'] = $video;

    		$order[] = OrderOfService::where('member_id',$id)->get();
    		$data['order'] = $order;

    		if($data){
    			return view('Admin.member.edit',$data);
    		}else{
    			return View::make('Admin.error.404');
    		}
		}
    }

    public function updateImage(Request $request)
    {
    	if(MemberImage::where('id',$request->id)->delete()){
    		return "success";
    	}
    }

    public function updateOrder(Request $request)
    {
    	if(OrderOfService::where('id',$request->id)->delete()){
    		return "success";
    	}
    }

    public function update(Request $request)
    {
    	$rules = [
    			'funeral_home' => 'required',
    			'member_name' => 'required',
    			'dateOfbirth' => 'required|date',
    			'dateOfpass' => 'required|date|after_or_equal:dateOfbirth',
                'country' => 'required',
                // 'state' => 'required',
                // 'city' => 'required',
                'community' => 'required_without:other',
                'other' => 'required_without:community|unique:community,community'
    		];

    	$message = [
    			'funeral_home.required' => 'Select Funeral Home',
    			'member_name.required' => 'Enter Deseased Member name',
    			'dateOfbirth.required' => 'Select Birthdate',
    			'dateOfbirth.date' => 'Select Valid Date',
    			'dateOfpass.required' => 'Select Passing Date',
    			'dateOfpass.date' => 'Select Valid Date',
                'dateOfpass.after_or_equal' => 'Passing date not before birthdate',
                'country.required' => 'Select Country',
                // 'state.required' => 'Select State',
                // 'city.required' => 'Select City',
                'community.required_without:other' => 'Please Select or Add Community',
                'other.required_without:community' => 'Please Select or Add Community',
                'communityText.unique:community' => 'Community Already Added'
    		];

    	$validator = Validator::make($request->all(),$rules,$message);

    	if($validator->fails()){
    		return Redirect::to('admin/member/edit/'.Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
    	}else{
    		$image = $request->hasfile('image');
    		$image1 = $request->hasfile('order');
    		$video = Input::get('video');

	        if($request->hasfile('image')){
	            foreach($request->file('image') as $img){
			        $filename = $img->getClientOriginalName();
			        $extension = $img->getClientOriginalExtension();
			        $picture[] = date('His').$filename;
			        $img->move(public_path('images/member'), date('His').$filename);
			    }
	        }else{
	            $image = "";
	        }

	        if($request->hasfile('order')){
	            foreach($request->file('order') as $img){
			        $filename = $img->getClientOriginalName();
			        $extension = $img->getClientOriginalExtension();
			        $pictures[] = date('His').$filename;
			        $img->move(public_path('images/OrderOfService'), date('His').$filename);
			    }
	        }else{
	            $image1 = "";
	        }

	        $member['user_id'] = Auth::user()->id;
	        $member['funeral_id'] = $request->funeral_home;
            if($request->community == 'Other'){
                $com = new Community;
                $com->community = $request->other;
                if($com->save()){
                    $member['community_id'] = $com->id;
                }
            }else{
                $member['community_id'] = $request->community; 
            }
	        $member['name'] = $request->member_name;
            $member['country_id'] = $request->country;
            // $member['state_id'] = $request->state;
            // $member['city_id'] = $request->city;
	        $member['dateOfbirth'] = date("Y-m-d" , strtotime($request->dateOfbirth));
	        $member['dateOfpass'] = date("Y-m-d", strtotime($request->dateOfpass));
	       	$member['about'] = $request->about;
	        // $member['area'] = $request->area;
	        $member['updated_at'] = date('Y-m-d H:i:s');

            Member::where('id',$request->id)->update($member);

            if($request->hasfile('image')){
                foreach ($picture as $value) {
                    $file = new MemberImage;

                    $file->member_id = $request->id;
                    $file->image_video = $value;
                    $file->type = 1;

                    $file->save();
                }
			}

			if($video){
				MemberImage::where('member_id',$request->id)->where('type',2)->delete();
				foreach ($video as $value) {
					$file = new MemberImage;

					$file->member_id = $request->id;
					$file->image_video = $value;
					$file->type = 2;

					$file->save();
				}
			}

			if($request->hasfile('order')){
                foreach ($pictures as $value) {
                    $file = new OrderOfService;

                    $file->member_id = $request->id;
                    $file->image = $value;

                    $file->save();
                }
			}

			return Redirect('admin/member')->with('success','Member Updated SuccessFully'); 
	    }
    }

    public function destroy($id)
    {
    	$pro = Member::where('id',$id)->first()->image;
    	File::delete(public_path('/images/member-profile/').$pro);
    	$delete = Member::where('id',$id)->delete();
        if($delete){
            if(MemberImage::where('member_id',$id)->exists()){
                $img = MemberImage::where('member_id',$id)->first()->image_video;
                    File::delete(public_path('/images/member/') . $img);
                MemberImage::where('member_id',$id)->delete();
            }

            if(OrderOfService::where('member_id',$id)->exists()){
            	$order = OrderOfService::where('member_id',$id)->get();
            	File::delete(public_path('/images/OrderOfService/').$order);
                OrderOfService::where('member_id',$id)->delete();   
            }
        return Redirect('admin/member')->with('success','Funeral deleted successfully');
    	}else{
    		return "false";
    	}	
    }

    public function stateList(Request $request)
    {
        $state = State::where('country_id',$request->country_id)->pluck('state','id');

        return json_encode($state);
    }

    public function cityList(Request $request)
    {
        $city = City::where('state_id',$request->state_id)->pluck('city','id');

        return json_encode($city);
    }

    public function orderOfService(Request $request)
    {
        if($request->tmp_id == 1){
            return view('web.templates.first.templateFirst',['user_id' => $request->user_id]);
        }if($request->tmp_id == 3){
            return view('web.templates.first.templateSecond',['user_id' => $request->user_id]);
        }if($request->tmp_id == 5){
            return view('web.templates.first.templateThird',['user_id' => $request->user_id]);
        }if($request->tmp_id == 2){
            return view('web.templates.second.templateFirst',['user_id' => $request->user_id]);
        }if($request->tmp_id == 4){
            return view('web.templates.second.templateSecond',['user_id' => $request->user_id]);
        }if($request->tmp_id == 6){
            return view('web.templates.second.templateThird',['user_id' => $request->user_id]);
        }
    }

    public function imageStore(Request $request)
    {
        $image_parts = explode(";base64,", $request->src);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $img = uniqid().'.png';
        $file = public_path('images/orderTemplate/').$img;
        file_put_contents($file, $image_base64);

        $order = new TemplateImage;
        $order->user_id = $request->user_id;
        $order->image = $img;
        if($order->save()){    
            // return json_encode($order);
            $return["json"] = json_encode($order);
            echo json_encode($return);
        }
    }

    public function imageJson(Request $request)
    {
        echo $request->json;
    }

    public function getId(Request $request)
    {
        $funeral = Member::where('id', $request->id)
                                ->get()
                                ->first();
        return $funeral;
    }

    public function approval(Request $request)
    {
        $member = Member::where('id',$request->id)->first();

        if($member['flag'] == 1){
            $email = AppUser::where('id',$member['user_id'])->first()->email;
        }else if($member['flag'] == 2){
            $email = Funeral::where('id',$member['user_id'])->first()->email;
        }
        if($request->val == 'Approve'){
            $update['approval'] = 1;
            $update['updated_at'] = date('Y-m-d H:i:s');

            $data = Member::where('id',$request->id)->update($update);
            if($data){
                $subject = 'Memorial Approval';
                $body = "Your request approved successfully";

                $data = array('message' => $body,"to" => $email, "subject" => $subject, "from" => 'info@keeptheirmemoryalive.com', "emailLabel" => 'Memorial Approval');

                $res =Mail::send('emails.approvalMail',['data'=>$data],function($message)use ($data){
                    
                      $message->from($data['from'],$data['emailLabel']);
                      $message->to($data['to'],'Name');
                      $message->subject($data['subject']);
                });
                return 1;
            }
        }else if($request->val == 'Reject'){
            $update['approval'] = 2;
            $update['updated_at'] = date('Y-m-d H:i:s');

            $data = Member::where('id',$request->id)->update($update);
            if($data){
                $subject = 'Memorial Rejection';
                $body = "Your request approved successfully";

                $data = array('message' => $body,"to" => $member['email'], "subject" => $subject, "from" => 'info@keeptheirmemoryalive.com', "emailLabel" => 'Memorial Rejection');

                $res =Mail::send('emails.rejectMail',['data'=>$data],function($message)use ($data){
                    
                      $message->from($data['from'],$data['emailLabel']);
                      $message->to($data['to'],'Name');
                      $message->subject($data['subject']);
                });
                return 2;
            }
        }
    }
}

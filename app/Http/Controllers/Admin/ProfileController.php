<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Country;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;

use Excel;
class ProfileController extends Controller
{
    public function __construct() {
        // echo "HEREr";die;
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $countryArr = Country::pluck('country', 'id')->toArray();
        return View::make('Admin.profile.index')->with('country',$countryArr);
    }

    public function store(Request $request) {
        $rules = [
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'email'         => "required|email|unique:user,email,".Auth::user()->id,'id',
            'phone_number'  => 'required|numeric|min:10',
            //'user_role'     => 'required',
        ];

        // if(!isset($request->id)) {
        //     $rules['conf_password'] = 'required|same:password';
        // }

        $message = [
            "first_name.required"   => "First name is required",
            "first_name.max"        => "First name must be less than 255 characters",
            "last_name.required"    => "Last name is required",
            "last_name.max"         => "Last name must be less than 255 characters",
//            "username.required"     => "User name is required",
//            "username.max"          => "User name must be less than 255 characters",
            "email.required"        => "Email is required",
            "email.email"           => "Invalid e-mail address",
            "email.unique"          => "E-mail address is already exist",
            "phone_number.required" => "Phone Number is required",
            "phone_number.numeric"  => "Phone should be numeric",
            "phone_number.min"      => "Minimum 10 characters",
            "phone_number.max"      => "Maximum 12 characters",
            //"user_role.required"    => "Please select the user role",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules,$message);
        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/profile')->withErrors($validator)->withInput(Input::except('laravel_password'));
        }
         
        $userObj = User::find(Auth::user()->id);
        $userObj->updated_at = date('Y-m-d H:i:s');

        $userObj->first_name    = Input::get('first_name');
        $userObj->last_name     = Input::get('last_name');
        $userObj->email         = Input::get('email');
        $userObj->phone_number  = Input::get('phone_number');
        if(Input::get('password'))
        {
            $userObj->password      = bcrypt(Input::get('password'));
        }
      
        if($userObj->save()) {
            return redirect('/admin/profile')->with('success', "Profile updated successfully.");
        } else {
            return redirect('/admin/profile')->with('success', "something went wrong please try later.");
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AppUser;
use App\Models\Funeral;
use App\Models\Member;
use DB;
use Mail;
use Redirect;
use Auth;
use App\Model\User;
use Response;

class DashboardController extends Controller {

	// retun dashboard view
    public function index() {
    	$data['appUser'] = AppUser::count('id');
    	$data['funeral'] = Funeral::count('id');
    	$data['member'] = Member::count('id');
    	return view('Admin.dashboard',$data);
    }

    //return add form test
    public function add() {
    	return view('Admin.Admin.add');	
    }
}
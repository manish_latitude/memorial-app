<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\MemorialTag;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;

class MemorialTagController extends Controller
{
    //
    public function __construct(){

    }

    public function index()
    {
    	$pager = 2;
    	$tag = MemorialTag::get();

    	return view('Admin.memorial-tag.list',['data' => $tag]);
    }

    public function perfomaction()
    {
        if(Input::get('action') == 'delete'){
            foreach (Input::get('ids') as $id) {
                $img = MemorialTag::where('id',$id)->first()->image;
                File::delete(public_path('/images/memorial-tag/') . $img);
                $flower = MemorialTag::find($id);
                $flower->delete();
            }
            return "true";
        }else{
            foreach (Input::get('ids') as $id) {
                $flower = MemorialTag::find($id);
                $st = Input::get('action') == 'active' ? '1' : '0';
                $flower->status = $st;
                $flower->updated_at = date('Y-m-d H:i:s');

                $flower->save();
            }
            return "true";
        }
    }

    public function arrayData(Datatables $datatables)
    {
    	$builder = MemorialTag::query()->orderBy('position')->select('id','image','name','price','url','status');

    	return $datatables->eloquent($builder)
                ->addColumn('check', function ($tag) {
                    return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=" . $tag->id . " name=\"uid[]\"><span class=\"checkmark\"></span> </label>";
                })
                ->addColumn('order', function($tag){
                    return "<img width=\"70px\" height=\"70px\" src='" . url('public/images/dnd.png') . "' onerror=this.src='" . url('public/images/avatar.png') . "'>";
                })
                ->editColumn('name',function($tag){
    				return $tag->name;
    			})
    			->editColumn('image',function($tag){
    				return "<img width=\"100px\" height=\"70px\" src='".url('public/images/memorial-tag/')."/".$tag->image."'onerror=this.src='".url('public/images/avatar.png')."'>";
    			})
    			->editColumn('price', function($tag){
    				return $tag->price;
    			})
    			->editColumn('url', function($tag){
    				return $tag->url;
    			})
                ->editColumn('status',function($tag){
                    if($tag->status == 1){
                         return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $tag->id . " checked=\"true\" data-off-text=\"Inactive\">
";
                    }else if($tag->status == 0){
                        return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$tag->id."checked=\"true\" data-off-text=\"Inactive\">";
                    }else{
                        return '<span class="badge bg-yellow">Delete</span>';
                    }
                })
				->addColumn('action', function($tag){
					 return "<a href=" . url('admin/memorial-tag/edit/' . $tag->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>				 	

					 	<a href=" . url('admin/memorial-tag/destroy/' . $tag->id) . " class=\"btn btn-danger btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
				})
				->rawColumns(['id','name','price','url','action','image','status','check','order'])
				->toJson();
    }

    public function create()
    {
    	return view('Admin.memorial-tag.add');
    }

    public function store(Request $request)
    {
    	$rules = [
    			'name' => 'required',
    			'price' => 'required|regex:/^([0-9])(?=.*([$%])).+$/',
                'url' => 'required|url'
    		];

    	$message = [
    			'name.required' => 'Name is Required',
    			'price.required' => 'Price is Required',
                'price.regex' => 'Enter valid value',
                'url.required' => 'Url is Required',
                'url.url' => 'Enter Valid URL'
    		];

    	$validator = Validator::make($request->all(),$rules, $message);

    	if($validator->fails()){
    		if(Input::get('id') != null && Input::get('id') > 0){
    			return Redirect::to('admin/memorial-tag/edit/'.Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
    		}else{
    			return Redirect::to('admin/memorial-tag/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
    		}
    	}else{
    		if($request->file('image')){
	            $image = time().'.'.$request->file('image')->getClientOriginalExtension();
	            $request->file('image')->move(public_path('images/memorial-tag'), $image);
	        }
	        
            if($request->status == '' || $tag['status'] == 1){
                $tag['status'] = 1;
            }else{
                $tag['status'] = $request->status;
            }
	        $tag['name'] = $request->name;
			$tag['price'] = $request->price;
			$tag['description'] = $request->description;
			if($request->file('image')){
                $tag['image'] = $image;    
            }else{
                $tag['image'] = $request->old_image;
            }
			$tag['url'] = $request->url;
			$tag['updated_at'] = date('Y-m-d H:i:s');

    		if(isset($request->id)){
    			if($request->file('image')){
	    			$img = MemorialTag::where('id',$request->id)->first()->image;
	                        File::delete(public_path('/images/memorial-tag/') . $img);
    			}
    			MemorialTag::where('id',$request->id)->update($tag);
    			return Redirect('admin/memorial-tag')->with('success','Tag Updated SuccessFully');
    		}else{
    			$tag1 = new MemorialTag;
    			$tag1->insert($tag);
    			return Redirect('admin/memorial-tag')->with('success','Tag Created SuccessFully');
    		}
    	}
    }

    public function edit($id)
    {
    	if($id > 0){
    		$tag = MemorialTag::find($id);

    		if($tag){
    			return View::make('Admin.memorial-tag.edit')->with('data',$tag);
    		}else{
    			return View::make('Admin.error.404');
    		}
    	}
    }

    public function updateImage(Request $request)
    {
        $img = MemorialTag::where('id',$request->id)->first()->image;

        File::delete(public_path('/images/memorial-tag/').$img);

        $set['image'] = null;
        $set['updated_at'] = date('Y-m-d H:i:s');

        if(MemorialTag::where('id',$request->id)->update($set)){
            return "true";
        }
    }

    public function destroy($id)
    {
    	$tag = MemorialTag::find($id);
    	if($tag){
    		$img = MemorialTag::where('id',$id)->first()->image;
	        File::delete(public_path('/images/memorial-tag/') . $img);
    		if(MemorialTag::where('id',$id)->delete()){
    			return Redirect('admin/memorial-tag')->with('success','Tag deleted successfully');
    		}
    	
    	}else{
    		return "false";
    	}
    }

    public function updateOrder(Request $request)
    {
        foreach ($request->value as $key => $value) {
            $data['position'] = $key+1;
            $data['updated_at'] = date('Y-m-d H:i:s');
            MemorialTag::where('id',$value)->update($data);
        }
    }   
}

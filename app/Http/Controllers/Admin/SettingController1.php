<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Library\General;
use App\Models\DataProperty;
use App\Models\DataPropertyChild;
use App\Models\DataPropertySubChild;
use App\Models\Language;
use App\Models\User;

use App\Models\Contact;
use App\Models\ContactAddress;
use App\Models\ContactJob;
use App\Models\ContactNumber;
use App\Models\ContactCommunication;
use App\Models\ContactBusiness;
use App\Models\ContactPoliticalPosition;
use App\Models\ContactRepresentative;
use App\Models\ContactReferral;
use App\Models\ContactSpouse;
use App\Models\ContactDependent;
use App\Models\ContactEducation;
use App\Models\ContactGreetingsLetter;

use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Auth;

class SettingController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        // echo $request->id;die;
        if(isset($request->id) && !empty($request->id)) {
            $dataTitle = DataProperty::where('deleted_on',null)->orderBy('title','asc')->pluck('title','id');
            return view('Admin.setting.add_multi_lang_data',['dataTitle'=>$dataTitle,'titleId'=>$request->id]);
        } else {
            $dataTitle = DataProperty::where('deleted_on',null)->orderBy('title','asc')->pluck('title','id');
            return view('Admin.setting.add_multi_lang_data',['dataTitle'=>$dataTitle,'titleId'=>'']);
        }
    }

    public function create() {
        return view('Admin.setting.add');
    }

    public function store(Request $request) {
        // echo "<pre>";print_r($request->all());die;
        $rules = [
            'title' => 'required|max:255',
            'slug'  => 'required|max:255',
            // 'notes' => 'required',
        ];

        $message = [
            "title.required"    => "Title is required",
            "title.max"         => "Title must be less than 255 characters",
            "slug.required"     => "Slug is required",
            "slug.max"          => "Slug must be less than 255 characters",
            // "notes.required"    => "Notes is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules,$message);
        // process the login
        if ($validator->fails()) {
            if(Input::get('id') != null && Input::get('id') > 0){
                return Redirect::to('data-setting/edit/'.Input::get('id'))->withErrors($validator);
            } else {
                return Redirect::to('data-setting/create/')->withErrors($validator);
            }
        }

        $dataSettingObj = "";
        if(isset($request->id)) {
            $dataSettingObj = DataProperty::find($request->id);
            $dataSettingObj->updated_on = date('Y-m-d H:i:s');
            $dataSettingObj->updated_by = Auth::user()->id;
        } else {
            $dataSettingObj             = new DataProperty();
            $dataSettingObj->created_at = date('Y-m-d H:i:s');
            $dataSettingObj->created_by = Auth::user()->id;
        }

        $dataSettingObj->title  = Input::get('title');
        $dataSettingObj->notes  = Input::get('notes');
        $dataSettingObj->slug   = Input::get('slug');

        if($dataSettingObj->save()) {
            if (Input::get('id') != null && Input::get('id') > 0) {
                return redirect('/data-setting')->with('success', "Data updated successfully.");
            } else {
                return redirect('/data-setting')->with('success', "Data created successfully.");
            }
        } else {
            return redirect('/data-setting')->with('success', "something went wrong to save setting title please try later.");
        }
    }

    public function edit($id) {
        if ($id > 0) {
            $dataSettingDetail = DataProperty::find($id);
            if (count($dataSettingDetail) > 0) {
                return View::make('Admin.setting.edit')->with('data', $dataSettingDetail);
            }
            return View::make('Admin.error.404');
        }
    }

    public function dataMultiLangStore(Request $request) {

        // echo "<pre>";print_r($request->all());die;
        // echo "<pre>";
        $isSave = false;
        $childDataSettingIds = [];
        if(count($request->lang) > 0) {

            foreach ($request->default_text_option as $id => $dataTitle) {
                foreach ($dataTitle as $childId => $title) {
                
                    $childDataSettingObj = DataPropertyChild::find($childId);
                    // echo "<pre>";print_r($childDataSettingObj);die;
                    if(count($childDataSettingObj) > 0){
                        $childDataSettingObj->updated_by = Auth::user()->id;
                        $childDataSettingObj->updated_on = date('Y-m-d H:i:s');
                    } else {
                        $childDataSettingObj = new DataPropertyChild();
                        $childDataSettingObj->created_by = Auth::user()->id;
                        $childDataSettingObj->created_at = date('Y-m-d H:i:s');
                    }

                    $childDataSettingObj->data_property_id  = $request->data_property_id;
                    $childDataSettingObj->title             = $title;
                    $childDataSettingObj->save();
                    
                    $childDataSettingIds[$id] = $childDataSettingObj->id;
                }
            }
                
            $parentId = "";
            foreach ($request->lang as $kId => $langTitle) {
                $parentId = $childDataSettingIds[$kId];
                foreach ($langTitle as $langId => $titleArr) {
                    foreach ($titleArr as $key => $title) {
                        
                        $subChildDataSettingObj = DataPropertySubChild::where('id',$key)
                                                    ->where('language_id',$langId)
                                                    ->first();

                        if(count($subChildDataSettingObj) > 0) {
                            $subChildDataSettingObj->updated_on = date('Y-m-d H:i:s');
                            $subChildDataSettingObj->updated_by = Auth::user()->id;
                        } else {
                            $subChildDataSettingObj             = new DataPropertySubChild();
                            $subChildDataSettingObj->created_at = date('Y-m-d H:i:s');
                            $subChildDataSettingObj->created_by = Auth::user()->id;
                        }

                        $subChildDataSettingObj->data_property_child_id = $parentId;
                        $subChildDataSettingObj->language_id            = $langId;
                        $subChildDataSettingObj->multilang_title        = $title;   
                        if($subChildDataSettingObj->save()) {
                            $isSave = true;
                        }
                    }
                }
            }

        }
        
        if($isSave) {
            return redirect("/data-setting/$request->data_property_id")->with('success', "Data saved successfully.");
        } else {
            return redirect("/data-setting/$request->data_property_id")->with('success', "something went wrong to save setting multi language title. please try later.");
        }
    }

    public function checkSlug(Request $request) {
        $dataSettingObj = DataProperty::where('slug','LIKE',"%$request->slug%")->get();
        if(count($dataSettingObj) > 0) {
            return "true";
        } else {
            return "false";
        }
    }

    public function addMultiLangFields(Request $request) {
        
        $checkDefaultValue = DataPropertyChild::where('data_property_id',$request->selTitleId)->where('title',$request->defaultText)->first();
        if(count($checkDefaultValue) > 0){
            return "false";
        }
        // echo "<pre>";print_r($request->all());die;
        $languages = Language::getAllLanguages();

        $dataPropertyChildObj = new DataPropertyChild();
        $dataPropertyChildObj->data_property_id = $request->selTitleId;
        $dataPropertyChildObj->title            = $request->defaultText;
        $dataPropertyChildObj->created_by       = Auth::user()->id;
        $dataPropertyChildObj->created_at       = date('Y-m-d H:i:s');
        $dataPropertyChildObj->save();

        $dataPropertySubChildArr = [];
        foreach ($languages as $key => $language) {
            $dataPropertySubChildObj = new DataPropertySubChild();
            $dataPropertySubChildObj->data_property_child_id    = $dataPropertyChildObj->id;
            $dataPropertySubChildObj->language_id               = $language['id'];
            $dataPropertySubChildObj->multilang_title           = $request->defaultText;
            $dataPropertySubChildObj->created_by                = Auth::user()->id;
            $dataPropertySubChildObj->created_at                = date('Y-m-d H:i:s');
            $dataPropertySubChildObj->save();
            $dataPropertySubChildArr[] = $dataPropertySubChildObj;
        }
        // echo "<pre>";print_r($dataPropertySubChildArr);die;
        return view('Admin.setting.add_multi_lang_fields',['divClass'=>$request->divClass,'titleId'=>$request->selTitleId, 'titleText'=>$request->titleText,'defaultText'=>$request->defaultText,'languages'=>$languages,'dataPropertyChildObj'=>$dataPropertyChildObj,'dataPropertySubChildArr'=>$dataPropertySubChildArr]);
    }

    public function addMultiLangData(Request $request) {

        $languages  = Language::getAllLanguages();

        $dataProperty       = DataProperty::select('title')->where('id',$request->titleId)->first();
        $childData          = DataPropertyChild::where('data_property_id',$request->titleId)
                                ->where('deleted_on',null)
                                ->get()
                                ->toArray();

        $childIds           = [];
        $subChildData       = [];
        $childDefaultText   = [];

        if(count($childData) > 0) {
            $childIds           = array_column($childData, 'id');

            foreach ($childData as $key => $childProperty) {
                $childDefaultText[$childProperty['id']] = $childProperty['title'];
                $childDefaultText['id']                 = $childProperty['id'];
            }
            $subChildData       = DataPropertySubChild::select('id','data_property_child_id','language_id','multilang_title','created_by')
                                    ->whereIn('data_property_child_id',$childIds)
                                    ->with(['language'=>function($query) {
                                            $query->select('id','title','slug');
                                    }])
                                    ->get()
                                    ->toArray();
        }

        $childLangDataArr = [];

        if(count($subChildData) > 0) {
            foreach ($subChildData as $key => $childDataArr) {
                $childLangDataArr[$childDataArr['data_property_child_id']][] = $childDataArr;
            }
        }

        if(count($childLangDataArr) > 0) {
            return view('Admin.setting.get_multi_lang_data',['dataProperty'=>$dataProperty,'childData'=>$childData,'childDefaultText'=>$childDefaultText,'subChildData'=>$childLangDataArr]);
        }
    }

    public function destroyMultiLangRow($id) {
        
        $contactRes = DataProperty::checkDataSetting($id);

        if($contactRes == 1) {
            return "false";
        }
        
        $subChildObj    = DataPropertySubChild::where('data_property_child_id',$id)->get();

        foreach ($subChildObj as $keyId => $subchildData) {
            $subchildData->deleted_on = date('Y-m-d H:i:s');
            $subchildData->save();
        }

        $childObj       = DataPropertyChild::where('id',$id)->first();
        $childObj->deleted_on = date('Y-m-d H:i:s');
        $childObj->save();

        echo "true";
    }
}
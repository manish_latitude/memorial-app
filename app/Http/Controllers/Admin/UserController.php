<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use Excel;

class UserController extends Controller {

    public function __construct() {
       // echo "HEREr";die;
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() { 
        $pager = 2;
        $userList = User::get();
        // $userList = User::orderBy('id', 'asc')->paginate($pager);
        // echo "<pre>";print_r($userList);die;
        return view('Admin.user.list', ['data' => $userList]);
        // return view('');
        // return view('web.user.index');
    }

    public function create() {
        $userRolesArr = Role::where('id', "!=", 2)->pluck('name', 'id')->toArray();
        return view('Admin.user.add', ['user_role' => $userRolesArr]);
    }

    public function userConfirm($token) {
        $userObj = User::where('token', $token)->first();

        if (count($userObj) > 0) {
            $userObj->is_verified = 1;
            $userObj->status = 1;
            $userObj->verified_at = date('Y-m-d H:i:s');
            $userObj->save();
            return view('Admin.user.setPassword', ['token' => $token]);
        }
    }

    public function userAccountConfirm() {  
        return view('Admin.user.accountConfirm');
    }

    public function savePassword(Request $request) {
    
        $rules = [
            'password' => 'required|min:6',
            'conf_password' => 'required|same:password'
        ];
        $message = [
            "password.required" => "Password is required",
            "conf_password.required" => "Confirm Password is required",
            "password.min" => "Password should be six characters long",
            "conf_password.same" => "Confirmation Password should be like password",
        ];
        $validator = Validator::make(Input::all(), $rules, $message);
        // process the login

        if ($validator->fails()) { 
            return Redirect::to('admin/user/confirm/' . $request->token)->withErrors($validator);
        }

        $userObj = User::where('token', $request->token)->first();
        if (count($userObj) > 0) {
            $userObj->is_verified = 1;
            $userObj->status = 1;
            $userObj->verified_at = date('Y-m-d H:i:s');
            $userObj->password = bcrypt($request->password);
            $userObj->save();
            return Redirect::to('admin/user/account/confirm');
        }
    }

    public function store(Request $request) {
        // echo "<pre>";print_r($request->all());die;
        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            //'username'      => 'required|max:255',
            'email' => "required|email|unique:user,email," . $request->id, 'id',
            'phone_number' => 'required|numeric|min:10',
            'user_role' => 'required',
        ];

        // if(!isset($request->id)) {
        //     $rules['conf_password'] = 'required|same:password';
        // }

        $message = [
            "first_name.required" => "First name is required",
            "first_name.max" => "First name must be less than 255 characters",
            "last_name.required" => "Last name is required",
            "last_name.max" => "Last name must be less than 255 characters",
//            "username.required"     => "User name is required",
//            "username.max"          => "User name must be less than 255 characters",
            "email.required" => "Email is required",
            "email.email" => "Invalid e-mail address",
            "email.unique" => "E-mail address is already exist",
            "phone_number.required" => "Phone Number is required",
            "phone_number.numeric" => "Phone should be numeric",
            "phone_number.min" => "Minimum 10 characters",
            "phone_number.max" => "Maximum 12 characters",
            "user_role.required" => "Please select the user role",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);
        // process the login
        if ($validator->fails()) {
            if (Input::get('id') != null && Input::get('id') > 0) {
                return Redirect::to('admin/user/edit/' . Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
            } else {
                return Redirect::to('admin/user/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
            }
        }

        $userObj = "";
        if (isset($request->id)) {
            $userObj = User::find($request->id);
            $userObj->status = $request->status;
            $userObj->updated_at = date('Y-m-d H:i:s');
        } else {
            $userObj = new User();

            //generate unique token - start
            $firstNameLetter = substr($request->first_name, 0, 1);
            $lastNameLetter = substr($request->last_name, 0, 1);
            $uniquetoken = User::generateToken();
            $uniqueString = $firstNameLetter . $lastNameLetter . $uniquetoken;
            //generate unique token - end

            $userObj->token = $uniqueString;
            $userObj->status = 0;
            $userObj->created_at = date('Y-m-d H:i:s');
        }

        $userObj->first_name = Input::get('first_name');
        $userObj->last_name = Input::get('last_name');
        //$userObj->username      = Input::get('username');
        $userObj->email = Input::get('email');
        $userObj->phone_number = Input::get('phone_number');
        if (Input::get('password')) {
            $userObj->password = bcrypt(Input::get('password'));
        }
        $userObj->user_role = $request['user_role'][0];
        if ($userObj->save()) {
            //section for new user
            if (!isset($request->id)) {

                //save the user role in role user table - start
                $roleUserObj = new RoleUser();
                $roleUserObj->user_id = $userObj->id;
                $roleUserObj->role_id = $request['user_role'][0];
                $roleUserObj->save();
                //save the user role in role user table - end
                //generating confirmation link
                $confirm_link = url('admin/user/confirm') . '/' . $userObj->token;
                $tomail = $request->email;
                $subject = 'User Registration';
                $body = "User created successfully";

                $data = array("name" => $request->first_name, 'confirm_link' => $confirm_link, 'message' => $body, "to" => $tomail, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'));
//print_r( $data); exit;
                 $res =Mail::send('emails.signup',['data'=>$data],function($message) use ($data){
                  $message->from($data['from'],$data['emailLabel']);
                  $message->to($data['to'],$data['name']);
                  $message->subject($data['subject']);
                  }); 
            }
           // print_r($res); exit;
            if (Input::get('id') != null && Input::get('id') > 0) {
                return redirect('admin/user')->with('success', "User updated successfully.");
            } else {
                return redirect('admin/user')->with('success', "User created successfully.");
            }
        } else {
            return redirect('admin/user')->with('success', "something went wrong please try later.");
        }
    }

    public function edit($id) {
        // echo $id;die;
        if ($id > 0) {
            $userRolesArr = Role::where('id', "!=", 2)->pluck('name', 'id')->toArray();
            $user = User::find($id);
            if (count($user) > 0) {
                return View::make('Admin.user.edit')->with('data', $user)->with('user_role', $userRolesArr);
            }
            return View::make('Admin.error.404');
        }
    }

    public function perfomaction() {
        if (Input::get('action') == 'delete') {
            foreach (Input::get('ids') as $id) {
                $User = User::find($id);
                $User->delete();
            }
            return "true";
        } else {
            foreach (Input::get('ids') as $id) {
                $User = User::find($id);
                $st = Input::get('action') == 'active' ? '1' : '0';
                $User->status = $st;
                $User->updated_at = date('Y-m-d H:i:s');

                //$User->updated_by = Auth::user()->id;
                $User->save();
            }
            return "true";
        }
    }

    public function editConfirmPass(Request $request) {
        // echo \Auth::User()->id;die;
        $userObj = User::find(\Auth::User()->id);
        if (Hash::check($request->password, $userObj->password)) {
            return "true";
        } else {
            return "false";
        }
    }

    public function sendResetPasswordLink(Request $request) {

        $userObj = User::find(\Auth::User()->id);
        if (Hash::check($request->password, $userObj->password)) {
            $userObj = User::find($request->user_id);
            if (count($userObj) > 0) {
                //generate unique token - start
                $firstNameLetter = substr($userObj->first_name, 0, 1);
                $lastNameLetter = substr($userObj->last_name, 0, 1);
                $uniquetoken = User::generateToken();
                $uniqueString = $firstNameLetter . $lastNameLetter . $uniquetoken;
                //generate unique token - end
                //save new token - start
                $userObj->token = $uniqueString;
                $userObj->save();
                //save new token - end
                $reset_link = url('admin/user/reset-password') . '/' . $userObj->token;
                $tomail = $userObj->email;
                $subject = 'Reset Passowrd';
                $body = "To reset your password click to below link";

                $data = array("user_name" => $userObj->first_name . " " . $userObj->last_name, 'reset_link' => $reset_link, 'message' => $body, "to" => $tomail, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'));
//print_r($data); exit;
                $res = Mail::send('emails.resetPass', ['data' => $data], function($message) use ($data) {
                            $message->from($data['from'], $data['emailLabel']);
                            $message->to($data['to'], $data['user_name']);
                            $message->subject($data['subject']);
                        });

                return "true";
            }
        } else {
            return "false";
        }
    }

    public function resetPassword($token) {
        $userObj = User::where('token', $token)->where('is_verified', 1)->first();
        if (count($userObj) > 0) {
            return view('web.user.resetPassword', ['user' => $userObj]);
        } else {
            return Redirect::to('admin/login');
        }
    }

    public function saveResetPassword(Request $request) {

        $rules = [
            'password' => 'required|min:6',
            'conf_password' => 'required|same:password'
        ];
        $message = [
            "password.required" => "Password is required",
            "conf_password.required" => "Confirm Password is required",
            "password.min" => "Password should be six characters long",
            "conf_password.same" => "Confirm Password and Password do not match.",
        ];
        $validator = Validator::make(Input::all(), $rules, $message);
        // process the login

        if ($validator->fails()) {
            return Redirect::to('admin/user/reset-password/' . $request->token)->withErrors($validator);
        }

        $userObj = User::where('id', $request->user_id)->where('token', $request->token)->where('is_verified', 1)->first();
        if (count($userObj) > 0) {
            $userObj->password = bcrypt($request->password);
            $userObj->save();
            return Redirect::to('admin/user/reset-password-confirm');
        } else {
            return Redirect::to('admin/login');
        }
    }

    public function resetConfirm() {
        return view('web.user.resetConfirm');
    }

    public function destroy(Request $request) {
        dD($request->all());
        $userObj = User::find(\Auth::User()->id);
        if (Hash::check($request->password, $userObj->password)) {
            $user = User::find($request->id);
            if (count($user) > 0) {
                $user->deleted_at = date('Y-m-d H:i:s');
                $user->save();
                return "true";
            }
        } else {
            return "false";
        }
    }

    public function arrayData(Datatables $datatables) {
        $builder = User::query()->select('id', 'first_name', 'last_name', 'email', 'phone_number', 'user_role', 'status', 'created_at', 'updated_at')->where('user_role', "!=", 2)->where('deleted_at','=',null);

        return $datatables->eloquent($builder)
                        ->editColumn('email', function ($user) {
                            return $user->email;
                        })
//                            ->editColumn('username', function ($user) {
//                                return $user->username;
//                            })
                        ->editColumn('first_name', function ($user) {
                            return $user->first_name;
                        })
                        ->editColumn('last_name', function ($user) {
                            return $user->last_name;
                        })
                        ->editColumn('phone_number', function ($user) {
                            return $user->phone_number;
                        })
                        ->editColumn('user_role', function ($user) {  
                             $Roles = Role::find($user->user_role);
                                return $Roles->name;
                            })
//                        ->editColumn('user_role', function($user) {
//                            return $user->user_role == 1 ? "Admin" : "User";
//                        })
                        ->editColumn('status', function($user) {
                            if ($user->id != 1) {
                                if ($user->status == 1) {
                                    return "
    <input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $user->id . " checked=\"true\" data-off-text=\"Inactive\">
";
                                } else if ($user->status == 0) {
                                    return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $user->id . " data-off-text=\"Inactive\">
";
                                } else {
                                    return '<span class="badge bg-yellow">Delete</span>';
                                }
                            } else {
                                return "";
                            }
                        })
                        ->editColumn('created_at', function ($user) {
                            if ($user->created_at !== null) {
                                return date("j-M-y g:m A", strtotime($user->created_at));
                            }
                            return '';
                        })
                        ->editColumn('updated_at', function ($user) {
                            if ($user->updated_at !== null) {
                                return date("j-M-y g:m A", strtotime($user->updated_at));
                            }
                            return '';
                        })
                        ->addColumn('action', function($user) {
                            if ($user->id != 1) {
                                return "<a href=" . url('admin/user/edit/' . $user->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>  
                                              <button type=\"button\" class=\"btn btn-danger delete-btn\"  data-id = '" . $user->id . "'  data-toggle=\"modal\" data-target=\".bs-example-modal-delete\" style=\"font-size: 13px;width: 31px;padding-left: 9px;height: 29px;\"><i class=\"fa fa-trash\"></i></button>";
                            } else {

                                return "<a href=" . url('admin/user/edit/' . $user->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>  ";
                            }
                        })
                        ->rawColumns(['email', 'first_name', 'last_name', 'phone_number', 'user_role', 'action', 'status'])
                        ->toJson();
    }

    

}

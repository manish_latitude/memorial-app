<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\Setting;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;

class GeneralSettingController extends Controller
{
    public function __construct() {
    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $data = Setting::get();
        return View::make('Admin.general-setting.add')->with('data',$data);
    }

    public function store(Request $request)
    {
        foreach ($request->setting as $key => $value) {
            $setting = Setting::find($key);
            foreach ($value as $k => $v) {
                $setting->value = $v;
                $setting->updated_at = date('Y-m-d H:i:s');

                $add = $setting->save();
            }
        }

        if($add){
            //Setting::where('id',$request->id)->update($setting);
            return Redirect('admin/setting')->with('success','Settings Updated Successfully');    
        }else{
            return Redirect('admin/setting')->with('success','Something went wrong');
        }

		
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\WillTable;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;

class WillController extends Controller
{
    //
    public function __construct(){

    }

    public function index()
    {
    	$pager = 2;
    	$bereavement = WillTable::get();

    	return view('Admin.will-plan.list',['data' => $bereavement]);
    }

    public function perfomaction()
    {
        if(Input::get('action') == 'delete'){
            foreach (Input::get('ids') as $id) {
                $img = WillTable::where('id',$id)->first()->image;
                File::delete(public_path('/images/willplan/') . $img);
                $con = WillTable::find($id);
                $con->delete();
            }
            return "true";
        }else{
            foreach (Input::get('ids') as $id) {
                $con = WillTable::find($id);
                $st = Input::get('action') == 'active' ? '1' : '0';
                $con->status = $st;
                $con->updated_at = date('Y-m-d H:i:s');

                $con->save();
            }
            return "true";
        }
    }

    public function arrayData(Datatables $datatables)
    {
    	$builder = WillTable::query()->orderBy('position')->select('id','image','name','url','status');

    	return $datatables->eloquent($builder)
                ->addColumn('check', function ($will) {
                    return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=" . $will->id . " name=\"uid[]\"><span class=\"checkmark\"></span> </label>";
                })
                ->addColumn('order', function($will){
                    return "<img width=\"70px\" height=\"70px\" src='" . url('public/images/dnd.png') . "' onerror=this.src='" . url('public/images/avatar.png') . "'>";
                })
    			->editColumn('name',function($will){
    				return $will->name;
    			})
    			->editColumn('image',function($will){
    				return "<img width=\"100px\" height=\"70px\" src='".url('public/images/willplan/')."/".$will->image."'onerror=this.src='".url('public/images/avatar.png')."'>";
    			})
    			->editColumn('url', function($will){
    				return $will->url;
    			})
    			->editColumn('status',function($will){
                    if($will->status == 1){
                         return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $will->id . " checked=\"true\" data-off-text=\"Inactive\">
";
                    }else if($will->status == 0){
                        return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$will->id."checked=\"true\" data-off-text=\"Inactive\">";
                    }else{
                        return '<span class="badge bg-yellow">Delete</span>';
                    }
                })
                ->addColumn('action', function($will){
					 return "<a href=" . url('admin/will-plan/edit/' . $will->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>				 	

					 	<a href=" . url('admin/will-plan/destroy/' . $will->id) . " class=\"btn btn-danger btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
				})
				->rawColumns(['id','name','url','action','image','status','check','order'])
				->toJson();
    }

    public function create()
    {
    	return view('Admin.will-plan.add');
    }

    public function store(Request $request)
    {
    	$rules = [
    			'name' => 'required',
                'url' => 'required|url'
    		];

    	$message = [
    			'name.required' => 'Name is Required',
                'url.required' => 'URL is Required',
                'url.url' => 'Enter Valid URL'
    		];

    	$validator = Validator::make($request->all(),$rules, $message);

    	if($validator->fails()){
    		if(Input::get('id') != null && Input::get('id') > 0){
    			return Redirect::to('admin/will-plan/edit/'.Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
    		}else{
    			return Redirect::to('admin/will-plan/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
    		}
    	}else{
    		if($request->file('image')){
	            $image = time().'.'.$request->file('image')->getClientOriginalExtension();
	            $request->file('image')->move(public_path('images/willplan'), $image);
	        }

            if($request->status == '' || $will['status'] == 1){
                $will['status'] = 1;
            }else{
                $will['status'] = $request->status;
            }
	        $will['name'] = $request->name;
			if($request->file('image')){
                $will['image'] = $image;    
            }else{
                $will['image'] = $request->old_image;
            }
			$will['description'] = $request->description;
			$will['url'] = $request->url;
			$will['updated_at'] = date('Y-m-d H:i:s');
			
    		if(isset($request->id)){
    			if($request->file('image')){
	    			$img = WillTable::where('id',$request->id)->first()->image;
	               File::delete(public_path('/images/willplan/') . $img);
    			}
    			WillTable::where('id',$request->id)->update($will);
    			return Redirect('admin/will-plan')->with('success','Will Updated SuccessFully');
    		}else{
    			$will1 = new WillTable;
    			$will1->insert($will);
    			return Redirect('admin/will-plan')->with('success','Will Created SuccessFully');
    		}
    	}
    }

    public function edit($id)
    {
    	if($id > 0){
    		$will = WillTable::find($id);

    		if($will){
    			return View::make('Admin.will-plan.edit')->with('data',$will);
    		}else{
    			return View::make('Admin.error.404');
    		}
    	}
    }

    public function updateImage(Request $request)
    {
        $img = WillTable::where('id',$request->id)->first()->image;

        File::delete(public_path('/images/willplan/').$img);

        $set['image'] = null;
        $set['updated_at'] = date('Y-m-d H:i:s');

        if(WillTable::where('id',$request->id)->update($set)){
            return "true";
        }
    }

	public function destroy($id)
    {
    	$will = WillTable::find($id);
    	if($will){
    		$img = WillTable::where('id',$id)->first()->image;
	        File::delete(public_path('/images/willplan/') . $img);
    		if(WillTable::where('id',$id)->delete()){
    			return Redirect::to('admin/will-plan')->with('success','Plan deleted successfully');
    		}
    	}else{
    		return "false";
    	}
    }	

    public function updateOrder(Request $request)
    {
        foreach ($request->value as $key => $value) {
            $data['position'] = $key+1;
            $data['updated_at'] = date('Y-m-d H:i:s');
            WillTable::where('id',$value)->update($data);
        }
    }   
}

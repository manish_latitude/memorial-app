<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Input;
use Validator;
use Redirect;
use View;
use Auth;
use Hash;

class ChangePassword extends Controller
{
    //
    public function __construct(){

    }

    public function index(){
    	$id = Auth::user()->id;

    	return View::make('Admin.change-password.add')->with(['data' => $id]);
    }

    public function store(Request $request){
    	$rules = [
                'old_password' => 'required',
    			'new_password' => 'required|min:6',
    			'conf_password' => 'required|same:new_password',
    		];

    	$message = [
                'old_password.required' => 'Current password required',
    			'new_password.required' => 'Password is required',
    			'new_password.min' => 'Password should be 6 charactor long',
    			'conf_password.required' => 'Confirm password is required',
    			'conf_password.same' => 'Confirm password same as password' 
    		];

    	$validator = Validator::make($request->all(),$rules,$message);

    	if($validator->fails()){
    		return Redirect::to('admin/change-password')->withErrors($validator)->withInput(Input::except('laravel_password'));
    	}
        
        $user = User::where('id',Auth::user()->id)->first();
        
        $pwd = $user->password;

        if(Hash::check(Input::get('old_password'),$pwd)){
        	$change_pwd['password'] = bcrypt(Input::get('new_password'));
        	$change_pwd['updated_at'] = date('Y-m-d H:i:s');

            if(User::where('id',Auth::user()->id)->update($change_pwd)){
            	return Redirect::to('admin/change-password')->with('success','Password Changed Successfully');
            }else{
            	return Redirect::to('admin/change-password')->with('error','Password Not Change yet');
            }
        }else{
            return Redirect::to('admin/change-password')->with('error','Current password not match');
        }
    }
}

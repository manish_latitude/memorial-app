<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\Bereavement;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;

class BereavementController extends Controller
{
    //
    public function __construct(){

    }

    public function index()
    {
    	$pager = 2;
    	$bereavement = Bereavement::get();

    	return view('Admin.bereavement.list',['data' => $bereavement]);
    }

    public function perfomaction()
    {
        if(Input::get('action') == 'delete'){
            foreach (Input::get('ids') as $id) {
                $img = Bereavement::where('id',$id)->first()->image;
                File::delete(public_path('/images/bereavement/') . $img);
                $con = Bereavement::find($id);
                $con->delete();
            }
            return "true";
        }else{
            foreach (Input::get('ids') as $id) {
                $con = Bereavement::find($id);
                $st = Input::get('action') == 'active' ? '1' : '0';
                $con->status = $st;
                $con->updated_at = date('Y-m-d H:i:s');

                $con->save();
            }
            return "true";
        }
    }

    public function arrayData(Datatables $datatables)
    {
    	$builder = Bereavement::query()->orderBY('position')->select('id','image','name','description','url','status');

    	return $datatables->eloquent($builder)
                ->addColumn('check', function ($ber) {
                    return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=" . $ber->id . " name=\"uid[]\"><span class=\"checkmark\"></span> </label>";
                })
                ->addColumn('order', function($ber){
                            return "<img width=\"70px\" height=\"70px\" src='" . url('public/images/dnd.png') . "' onerror=this.src='" . url('public/images/avatar.png') . "'>";
                        })
    			->editColumn('name',function($ber){
    				return $ber->name;
    			})
    			->editColumn('image',function($ber){
    				return "<img width=\"100px\" height=\"70px\" src='".url('public/images/bereavement/')."/".$ber->image."'onerror=this.src='".url('public/images/avatar.png')."'>";
    			})
    			->editColumn('url', function($ber){
    				return $ber->url;
    			})
    			->editColumn('status',function($ber){
                    if($ber->status == 1){
                         return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $ber->id . " checked=\"true\" data-off-text=\"Inactive\">
";
                    }else if($ber->status == 0){
                        return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$ber->id."checked=\"true\" data-off-text=\"Inactive\">";
                    }else{
                        return '<span class="badge bg-yellow">Delete</span>';
                    }
                })
                ->addColumn('action', function($ber){
					 return "<a href=" . url('admin/bereavement/edit/' . $ber->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>				 	

					 	<a href=" . url('admin/bereavement/destroy/' . $ber->id) . " class=\"btn btn-danger btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
				})
				->rawColumns(['id','name','url','action','image','status','check','order'])
				->toJson();
    }

    public function create()
    {
    	return view('Admin.bereavement.add');
    }

    public function store(Request $request)
    {
    	$rules = [
    			'name' => 'required',
                'url' => 'required|url'
    		];

    	$message = [
    			'name.required' => 'Name is Required',
                'url.required' => 'Url is Required',
                'url.url' => 'Enter Valid Url'
    		];

    	$validator = Validator::make($request->all(),$rules, $message);

    	if($validator->fails()){
    		if(Input::get('id') != null && Input::get('id') > 0){
    			return Redirect::to('admin/bereavement/edit/'.Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
    		}else{
    			return Redirect::to('admin/bereavement/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
    		}
    	}else{
    		if($request->file('image')){
	            $image = time().'.'.$request->file('image')->getClientOriginalExtension();
	            $request->file('image')->move(public_path('images/bereavement'), $image);
	        }
            if($request->status == '' || $ber['status'] == 1){
                $ber['status'] = 1;
            }else{
                $ber['status'] = $request->status;
            }
	        $ber['name'] = $request->name;
			$ber['description'] = $request->description;
			if($request->file('image')){
                $ber['image'] = $image;    
            }else{
                $ber['image'] = $request->old_image;
            }
			$ber['url'] = $request->url;
			$ber['updated_at'] = date('Y-m-d H:i:s');
			
    		if(isset($request->id)){
    			if($request->file('image')){
	    			$img = Bereavement::where('id',$request->id)->first()->image;
	                        File::delete(public_path('/images/bereavement/') . $img);
    			}
    			Bereavement::where('id',$request->id)->update($ber);
    			return Redirect('admin/bereavement')->with('success','Bereavement Updated SuccessFully');
    		}else{
    			$ber1 = new Bereavement;
    			$ber1->insert($ber);
    			return Redirect('admin/bereavement')->with('success','Bereavement Created SuccessFully');
    		}
    	}
    }

    public function edit($id)
    {
    	if($id > 0){
    		$ber = Bereavement::find($id);

    		if($ber){
    			return View::make('Admin.bereavement.edit')->with('data',$ber);
    		}else{
    			return View::make('Admin.error.404');
    		}
    	}
    }

    public function updateImage(Request $request)
    {
        $img = Bereavement::where('id',$request->id)->first()->image;

        File::delete(public_path('/images/bereavement/').$img);

        $set['image'] = null;
        $set['updated_at'] = date('Y-m-d H:i:s');

        if(Bereavement::where('id',$request->id)->update($set)){
            dd('njfs');
            return "true";
        }
    }

	public function destroy($id)
    {
    	$ber = Bereavement::find($id);
    	if($ber){
    		$img = Bereavement::where('id',$id)->first()->image;
	        File::delete(public_path('/images/bereavement/') . $img);
    		if(Bereavement::where('id',$id)->delete()){
    			return Redirect::to('admin/bereavement')->with('success','Bereavement deleted successfully');
    		}
    	}else{
    		return "false";
    	}
    }	

    public function updateOrder(Request $request)
    {
        foreach ($request->value as $key => $value) {
            $data['position'] = $key+1;
            $data['updated_at'] = date('Y-m-d H:i:s');
            Bereavement::where('id',$value)->update($data);
        }
    }
}

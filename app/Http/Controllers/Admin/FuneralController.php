<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\Funeral;
use App\Models\FuneralImage;
use App\Models\FuneralProfile;
use App\Models\Member;
use App\Models\Country;
// use App\Models\State;
// use App\Models\City;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Auth;
use DB;
use Excel;
use Mail;

class FuneralController extends Controller
{
    //
    public function __construct(){

    }

    public function index()
    {
    	$pager = 2;
    	$funeral = Funeral::where('user_type',1)->get();
    	return view('Admin.funeral.list',['data' => $funeral]);
    }

    public function export()
    {
        $data = Funeral::where('user_type',1)->select('id','home_name','person_name','email','phone','dateOfbirth','created_at')->where('user_type',1)->get();
        Excel::create('Funeral Details', function($excel) use($data) {
        $excel->sheet('Funeral Details', function($sheet) use($data) {
            $sheet->fromArray($data,null,'A1',false,false);
            $sheet->row(1,'ID');
        });
        })->export('xls');
    }

    public function perfomaction()
    {
    	if(Input::get('action') == 'delete'){
    		foreach (Input::get('ids') as $id) {
                $pro = Funeral::where('id',$id)->first()->image;
                File::delete(public_path('/images/funeral-profile/').$pro);
    			$funeral = Funeral::find($id);
    			$funeral->delete();

                if(FuneralImage::where('funeral_id',$id)->exists()){
                    $img = FuneralImage::where('id',$id)->get();
                        File::delete(public_path('/images/funeral/') . $img);
                    FuneralImage::where('funeral_id',$id)->delete();
                }

                if(FuneralProfile::where('funeral_id',$id)->exists()){
                    FuneralProfile::where('funeral_id',$id)->delete();   
                }
    		}
            return "true";
    	}else if(Input::get('action') == 'approve'){
            foreach (Input::get('ids') as $id) {
                $update['approval'] = 1;
                $update['status'] = 1;
                $update['updated_at'] = date('Y-m-d H:i:s');

                $data = Funeral::where('id',$id)->update($update);
            }
            return "true";
        }else if(Input::get('action') == 'reject'){
            foreach (Input::get('ids') as $id) {
                $update['approval'] = 2;
                $update['status'] = 0;
                $update['updated_at'] = date('Y-m-d H:i:s');

                $data = Funeral::where('id',$id)->update($update);
            }
            return "true";
        }else{
    		foreach (Input::get('ids') as $id) {
    			$funeral = Funeral::find($id);
    			$st = Input::get('action') == 'active' ? '1' : '0';
    			$funeral->status = $st;
    			$funeral->updated_at = date('Y-m-d H:i:s');
    			$funeral->save();
    		}
    		return "true";
    	}
    }

    public function arrayData()
    {
        // $builder = DB::table('funeral')
        //             ->leftjoin('country','country.id','=','funeral.country')
        //             ->select('funeral.*')
        //             ->orderBy('approval','ASC')
        //             ->orderBy('id','DESC');

        $builder = Funeral::leftjoin('country',function($join){
            $join->on('funeral.country','=','country.id');
        })->where('funeral.id','!=',1)->select('funeral.*')->orderBy('approval','ASC')->orderBy('id','DESC');

       return Datatables::of($builder)
                ->addColumn('check', function ($funeral) {
                    return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=" . $funeral->id . " name=\"uid[]\"><span class=\"checkmark\"></span> </label>";
                })
                ->editColumn('id', function($funeral){
                    return $funeral->id;
                })
    			->editColumn('image',function($funeral){
    				return "<img width=\"100px\" height=\"70px\" src='".url('public/images/funeral-profile/')."/".$funeral->image."' onerror=this.src='".url('public/images/avatar.png')."'>";
    			})
    			->editColumn('home_name',function($funeral){
    				return $funeral->home_name;
    			})
    			->editColumn('person_name',function($funeral){
    				return $funeral->person_name;
    			})
                ->editColumn('country',function($funeral){
                    return $country = Country::where('id',$funeral->country)->pluck('country')->toArray();
                })
    			// ->editColumn('state',function($funeral){
       //              if($funeral->state == null){
       //                  return $funeral->state;
       //              }else{
       //                  return $state = State::where('id',$funeral->state)->pluck('state')->toArray();
       //              }
    			// })
    			// ->editColumn('city',function($funeral){
       //              if($funeral->city == null){
       //                  return $funeral->city;
       //              }else{
    			// 	    return $city = City::where('id',$funeral->city)->pluck('city')->toArray();
       //              }
    			// })
    			->editColumn('phone',function($funeral){
    				return $funeral->phone;
    			})
    			->editColumn('email',function($funeral){
    				return $funeral->email;
    			})
    			->editColumn('website',function($funeral){
    				return $funeral->website;
    			})
                ->editColumn('approval',function($funeral){
                    if($funeral->approval == 0){
                        return " <button type=\"button\" class=\"btn btn-primary submit\" title='Pending' data-toggle='modal' data-id=".$funeral->id." data-target='#approvalModal'>Pending</button>";
                    }else if($funeral->approval == 1){
                        if($funeral->status == 1){
                            return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $funeral->id . " checked=\"true\" data-off-text=\"Inactive\">";
                        }else if($funeral->status == 0){
                            return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$funeral->id."checked=\"true\" data-off-text=\"Inactive\">";
                        }else{
                             return '<span class="badge bg-yellow">Delete</span>';
                        }    
                    }else if($funeral->approval == 2){
                        return "Rejected";
                    }
                })
    			->addColumn('action',function($funeral){
    				return "<a href=" . url('admin/funeral/edit/' . $funeral->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>				 	

					 	<a href=" . url('admin/funeral/destroy/' . $funeral->id) . " class=\"btn btn-danger btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
    			})
    			->rawColumns(['id','home_name','person_name','image','country','phone','email','website','action','approval','check'])
    			->toJson();
    }

    public function create()
    {
        //$data['state'] = State::orderBy('id')->pluck('state','id');
        $data['country'] = Country::get();
        // $data['state'] = State::get();
    	return view('Admin.funeral.add',$data);
    }

    public function store(Request $request)
    {
    	$rules = [
    			'home_name'     => 'required|string',
    			'person_name'   => 'required|string',
    			'address'       => 'required',
                'country'       => 'required',
    			'phone_number'  => 'required|numeric|min:10',
    			'zip'           => 'required',
    			'email'         => 'required|email|unique:funeral,email,'.$request->id,'id',
    			'facebook'      => 'url|nullable',
    			'twitter'       => 'url|nullable',
    			'linkedin'      => 'url|nullable'
    		];

    	$message = [
    			'home_name.required'       => 'Enter funeral home name',
    			'person_name.required'     => 'Enter contact person name',
    			'address.required' 		   => 'Enter address',
                'country.required'         => 'Select Country',
    			'phone_number.required'    => 'Enter Mobile number',
    			'phone_number.numeric'     => "Mobilre Number should be numeric",
	            'phone_number.min'         => "Minimum 10 characters",
    			'zip.required'             => 'Enter zip code',
    			// 'zip.numeric'              => "Zip code should be numeric",
	      //       'zip.min'                  => "Minimum 6 characters",
	            'email.required'           => "Email is Required",
    			'email.email'              => "Invalide Email Address",
    			'email.unique'             => "Email Address Already Exist",
    			'facebook.url'             => 'Enter Valid URL',
    			'twitter.url'              => 'Enter Valid URL',
    			'linkedin.url'             => 'Enter Valid URL'
    		];
    	$validator = Validator::make($request->all(),$rules,$message);

    	if($validator->fails()){
    		return Redirect::to('admin/funeral/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
    	}else{
    		$img = $request->file('profile_image');
    		$image = $request->hasfile('image');
    		$video = Input::get('video');
			//$video = explode(',', $v);

    		if($img){
	            $imageName = time().'.'.$img->getClientOriginalExtension();
	            $img->move(public_path('images/funeral-profile'),$imageName);
	        }else{
	            $imageName = "";
	        }

	        if($request->hasfile('image')){
	            foreach($request->file('image') as $img){
			        $filename = $img->getClientOriginalName();
			        $extension = $img->getClientOriginalExtension();
			        $picture[] = date('His').$filename;
			        $img->move(public_path('images/funeral'), date('His').$filename);
			    }
	        }else{
	            $image = "";
	        }
	     
            $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $token = substr(str_shuffle(str_repeat($pool, 5)), 0, 30);  

	        $funeral = new Funeral;

	        $funeral->home_name = $request->home_name;
	        $funeral->person_name = $request->person_name;
	        $funeral->image = $imageName;
	        $funeral->address = $request->address;
            $funeral->country = $request->country;
	        $funeral->phone = $request->phone_number;
	        $funeral->zip = $request->zip;
	        $funeral->email = $request->email;
	        $funeral->website = $request->website;
	        $funeral->facebook = $request->facebook;
	        $funeral->twitter = $request->twitter;
	        $funeral->linkedin = $request->linkedin;
            $funeral->token = $token;
	        $funeral->created_at = date('Y-m-d H:i:s');
	        $funeral->updated_at = date('Y-m-d H:i:s');
	        
	        $funeral->save();

	        if($request->title && $request->description){
		        $description = $request->description;
		        foreach ($request->title as $key => $value) {
		        	$FuneralProfile = new FuneralProfile;
		        	$FuneralProfile->funeral_id = $funeral->id;
		        	$FuneralProfile->title = $value;
		        	$FuneralProfile->description = $description[$key];
		        	$FuneralProfile->save();
	        	}
	    	}

	        if($request->hasfile('image')){
                foreach ($picture as $value) {
                    $file = new FuneralImage;

                    $file->funeral_id = $funeral->id;
                    $file->image_video = $value;
                    $file->type = 1;

                    $file->save();
                }
			}

			if($video){
				foreach ($video as $value) {
					$file = new FuneralImage;

					$file->funeral_id = $funeral->id;
					$file->image_video = $value;
					$file->type = 2;

					$file->save();
				}
			}

			if($funeral->save()){

                $confirm_link = url('admin/funeral/resetPassword') . '/' . $funeral->token;

                    $tomail = $request->email;
                    $subject = 'Memorial Password Reset';
                    $body = "Password reset successfully";
                    $data = array("name" => $request->person_name, 'reset_link' => $confirm_link, 'message' => $body, "to" => $tomail, "subject" => $subject, "from" => 'info@keeptheirmemoryalive.com', "emailLabel" => 'Password Reset',"email" => $request->email);
                    $res =Mail::send('emails.resetPass',['data'=>$data],function($message) use ($data){
                        $message->from($data['from'],$data['emailLabel']);
                        $message->to($data['to'],$data['name']);
                        $message->subject($data['subject']);
                    }); 
				return Redirect('admin/funeral')->with('success','Funeral Created SuccessFully');
			}
    	}
    }

    public function edit($id)
    {
    	if($id > 0){
    		$data['funeral'] = Funeral::find($id);

           $data['country'] = Country::orderBy('id')->pluck('country','id');
            // $data['country'] = Country::get();
            // $data['state'] = State::pluck('state','id');
            // $data['city'] = City::pluck('city','id');

    		$pro[] = FuneralProfile::where('funeral_id',$id)->get();
    		$data['profile'] = $pro;

    		$img[] = FuneralImage::where('funeral_id',$id)->where('type',1)->get();
    		$data['fimg'] = $img;
    	
    		$video[] = FuneralImage::where('funeral_id',$id)->where('type',2)->get();
    		$data['fv'] = $video;

    		if($data){
    			return view('Admin.funeral.edit',$data);
    		}else{
    			return View::make('Admin.error.404');
    		}
    	}
    }

    public function update(Request $request)
    {
    	$rules = [
    			'home_name'     => 'required|string',
    			'person_name'   => 'required|string',
    			'address'       => 'required',
                'country'       => 'required',
    			'phone_number'  => 'required|numeric|min:10',
    			'zip'           => 'required',
    			'email'         => 'required|email|unique:funeral,email,'.$request->id,'id',
    			'facebook'      => 'url|nullable',
    			'twitter'       => 'url|nullable',
    			'linkedin'      => 'url|nullable',
    		];

    	$message = [
    			'home_name.required'       => 'Enter funeral home name',
    			'person_name.required'     => 'Enter contact person name',
    			'address.required' 		   => 'Enter address',
                'country.required'         => 'Please Select Country',
    			'phone_number.required'    => 'Enter Mobile number',
    			'phone_number.numeric'     => "Mobilr Number should be numeric",
	            'phone_number.min'         => "Minimum 10 characters",
    			'zip.required'             => 'Enter zip code',
    			// 'zip.numeric'              => "Zip code should be numeric",
	      //       'zip.min'                  => "Minimum 6 characters",
	            'email.required'           => "Email is Required",
    			'email.email'              => "Invalide Email Address",
    			'email.unique'             => "Email Address Already Exist",
    			'facebook.url'             => 'Enter Valid URL',
    			'twitter.url'              => 'Enter Valid URL',
    			'linkedin.url'             => 'Enter Valid URL'
    		];

    	$validator = Validator::make($request->all(),$rules,$message);

    	if($validator->fails()){
    		return Redirect::to('admin/funeral/edit/'.Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
    	}else{
    		$img = $request->file('profile_image');
    		$image = $request->hasfile('image');
    		$video = Input::get('video');

			if($img){
	            $imageName = time().'.'.$img->getClientOriginalExtension();
	            $img->move(public_path('images/funeral-profile'),$imageName);
	        }else{
	            $imageName = Funeral::where('id',$request->id)->first()->image;
	        }

	        if($request->hasfile('image')){
	            foreach($request->file('image') as $img){
			        $filename = $img->getClientOriginalName();
			        $extension = $img->getClientOriginalExtension();
			        $picture[] = date('His').$filename;
			        $img->move(public_path('images/funeral'), date('His').$filename);
			    }
	        }else{
	            $image = "";
	        }

	       	$funeral['home_name'] = $request->home_name;
	        $funeral['person_name'] = $request->person_name;
	        $funeral['image'] = $imageName;
	        $funeral['address'] = $request->address;
            $funeral['country'] = $request->country;
	        $funeral['phone'] = $request->phone_number;
	        $funeral['zip'] = $request->zip;
	        $funeral['email'] = $request->email;
	        $funeral['website'] = $request->website;
	        $funeral['facebook'] = $request->facebook;
	        $funeral['twitter'] = $request->twitter;
	        $funeral['linkedin'] = $request->linkedin;
	        $funeral['updated_at'] = date('Y-m-d H:i:s');

	        if($request->file('profile_image')){
                $img = Funeral::where('id',$request->id)->first()->image;
               File::delete(public_path('/images/funeral-profile/') . $img);
            }
	     	
	     	Funeral::where('id',$request->id)->update($funeral);

	     	if($request->title && $request->description){
	     		FuneralProfile::where('funeral_id',$request->id)->delete();
		        $description = $request->description;
		        foreach ($request->title as $key => $value) {
		        	$FuneralProfile = new FuneralProfile;
		        	$FuneralProfile->funeral_id = $request->id;
		        	$FuneralProfile->title = $value;
		        	$FuneralProfile->description = $description[$key];
		        	$FuneralProfile->save();
	        	}
	    	}

	    	if($request->hasfile('image')){
                foreach ($picture as $value) {
                    $file = new FuneralImage;

                    $file->funeral_id = $request->id;
                    $file->image_video = $value;
                    $file->type = 1;

                    $file->save();
                }
			}

			if($video){
                FuneralImage::where('funeral_id',$request->id)->where('type',2)->delete();
				foreach ($video as $value) {
					$file = new FuneralImage;

					$file->funeral_id = $request->id;
					$file->image_video = $value;
					$file->type = 2;

					$file->save();
				}
			}

			return Redirect('admin/funeral')->with('success','Funeral Updated SuccessFully'); 
    	}
    }

    public function updateImage(Request $request)
    {
    	if(FuneralImage::where('id',$request->id)->delete()){
    		return "success";
    	}
    }

    public function updateProfile(Request $request)
    {
        $img = Funeral::where('id',$request->id)->first()->image;

        File::delete(public_path('/images/funeral-profile/').$img);

        $set['image'] = null;
        $set['updated_at'] = date('Y-m-d H:i:s');

        if(Funeral::where('id',$request->id)->update($set)){
            return "true";
        }
    }

    public function destroy($id)
    {
    	$pro = Funeral::where('id',$id)->first()->image;
    	File::delete(public_path('/images/funeral-profile/').$pro);
    	$delete = Funeral::where('id',$id)->delete();
        if($delete){
            if(Member::where('funeral_id',$id)->exists()){
                $member = Member::where('funeral_id',$id)->delete();
            }

            if(FuneralImage::where('funeral_id',$id)->exists()){
                $img = FuneralImage::where('id',$id)->get();
                    File::delete(public_path('/images/funeral/') . $img);
                FuneralImage::where('funeral_id',$id)->delete();
            }

            if(FuneralProfile::where('funeral_id',$id)->exists()){
                FuneralProfile::where('funeral_id',$id)->delete();   
            }
        return Redirect('admin/funeral')->with('success','Funeral deleted successfully');
    	}else{
    		return "false";
    	}	
    }

    public function stateList(Request $request)
    {
        $state = State::where('country_id',$request->country_id)->pluck('state','id');

        return json_encode($state);
    }

    public function cityList(Request $request)
    {
        $city = City::where('state_id',$request->state_id)->pluck('city','id');

        return json_encode($city);
    }

    public function getId(Request $request)
    {
        $funeral = Funeral::where('id', $request->id)
                                ->get()
                                ->first();
        return $funeral;
    }

    public function approval(Request $request)
    {
        $funeral = Funeral::where('id',$request->id)->first();
        if($request->val == 'Approve'){
            $update['approval'] = 1;
            $update['updated_at'] = date('Y-m-d H:i:s');
            $data = Funeral::where('id',$request->id)->update($update);
            if($data){
                $subject = 'Memorial Approval';
                $body = "Your request approved successfully";

                $data = array('message' => $body,"to" => $funeral['email'], "subject" => $subject, "from" => 'info@keeptheirmemoryalive.com', "emailLabel" => 'Memorial Approval');

                $res =Mail::send('emails.approvalMail',['data'=>$data],function($message)use ($data){
                    
                      $message->from($data['from'],$data['emailLabel']);
                      $message->to($data['to'],'Name');
                      $message->subject($data['subject']);
                });
                return 1;
            }
        }else if($request->val == 'Reject'){
            $update['approval'] = 2;
            $update['updated_at'] = date('Y-m-d H:i:s');
            
            $data = Funeral::where('id',$request->id)->update($update);
            if($data){
                $subject = 'Memorial Rejection';
                $body = "Your request approved successfully";

                $data = array('message' => $body,"to" => $funeral['email'], "subject" => $subject, "from" => 'info@keeptheirmemoryalive.com', "emailLabel" => 'Memorial Rejection');

                $res =Mail::send('emails.rejectMail',['data'=>$data],function($message)use ($data){
                    
                      $message->from($data['from'],$data['emailLabel']);
                      $message->to($data['to'],'Name');
                      $message->subject($data['subject']);
                });
                return 2;
            }
        }
    }

    public function resetPasswordView($token)
    {
        return View::make('Admin.funeral.resetPassword')->with('token',$token);
    }

    public function resetPassword(Request $request)
    {
        $rules = [
                'password' => 'required|min:6',
                'conf_password' => 'required|same:password'
            ];

        $message = [
                "password.required" => "Password is required",
                "conf_password.required" => "Confirm Password is required",
                "password.min" => "Password should be six characters long",
                "conf_password.same" => "Confirmation Password should be like password"                
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if($validator->fails()){
            return Redirect::to('admin/funeral/resetPassword/'.$request->token)->withErrors($validator)->withInput(Input::except('laravel_password'));
        }else{
            $userObj = Funeral::where('token', $request->token)->first();
            if (count($userObj) > 0) {
                $userObj->password = bcrypt($request->password);
                $userObj->is_verified = 1;
                if($userObj->save()){
                    $tomail = $userObj->email;
                    $subject = 'Memorial Password Reset';
                    $body = "Password reset successfully";

                    $data = array("name" => $userObj->person_name, 'message' => $body, "to" => $tomail, "subject" => $subject, "from" => 'info@keeptheirmemoryalive.com', "emailLabel" => 'Password Reset');

                    $res =Mail::send('emails.resetPassSuccess',['data'=>$data],function($message) use ($data){
                        $message->from($data['from'],$data['emailLabel']);
                        $message->to($data['to'],$data['name']);
                        $message->subject($data['subject']);
                    }); 
                }

                return Redirect('admin/funeral/resetPassword/'.$request->token)->with('success','Password reset successfully');
            } else {
                return Redirect('admin/funeral/resetPassword/'.$request->token)->with('success','Password reset fail');
            }
        }
    }
}

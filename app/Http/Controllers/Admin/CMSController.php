<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\CMS;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;

class CMSController extends Controller {

    public function __construct() {
    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $pager = 2;
        $cms = CMS::get(); 
        return view('Admin.cms.list',['data' => $cms]);
    }

    public function perfomaction()
    {
        if(Input::get('action') == 'delete'){
            foreach (Input::get('ids') as $id) {
                $cms = CMS::find($id);
                $cms->delete();
            }
            return "true";
        }else{
            foreach (Input::get('ids') as $id) {
                $cms = CMS::find($id);
                $st = Input::get('action') == 'active' ? '1' : '0';
                $cms->status = $st;
                $cms->updated_at = date('Y-m-d H:i:s');

                $cms->save();
            }
            return "true";
        }
    }

    public function arrayData(Datatables $datatables)
    {
        $builder = CMS::query()->select('id','title','slug','status');

        return $datatables->eloquent($builder)
                ->addColumn('check', function ($cms) {
                    return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=" . $cms->id . " name=\"uid[]\"><span class=\"checkmark\"></span> </label>";
                })
                ->editColumn('title',function($cms){
                    return $cms->title;
                })
                ->editColumn('slug',function($cms){
                    return $cms->slug;
                })
//                 ->editColumn('status',function($cms){
//                     if($cms->status == 1){
//                          return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $cms->id . " checked=\"true\" data-off-text=\"Inactive\">
// ";
//                     }else if($cms->status == 0){
//                         return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$cms->id."checked=\"true\" data-off-text=\"Inactive\">";
//                     }else{
//                         return '<span class="badge bg-yellow">Delete</span>';
//                     }
//                 })
                ->addColumn('action', function($cms){
                     return "<a href=" . url('admin/cms/edit/' . $cms->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>";
                })
                ->rawColumns(['title','slug','action','status','check'])
                ->toJson();
    }

    public function create()
    {
        return view('Admin.cms.add');
    }

    public function store(Request $request)
    {
        $rules = [  
                'title' => 'required',
            ];

        $message = [
                'title.required' => 'Title is required',
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if($validator->fails()){
            if(Input::get('id') != null && Input::get('id') > 0){
                return Redirect::to('admin/cms/edit/'.Input::get('id'))->withErrors($validator);
            }else{
                return Redirect::to('admin/cms/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
            }
        }else{
            $cms['title'] = $request->title;
            $cms['description'] = $request->description;
            $cms['updated_at'] = date('Y-m-d H:i:s');

            if(isset($request->id)){
                CMS::where('id',$request->id)->update($cms);
                return Redirect('admin/cms')->with('success','CMS Updated Successfully');
            }else{
                $cms1 = new CMS;
                $cms1->insert($cms);
                return Redirect('admin/cms')->with('success','CMS Created Successfully');
            }
        }
    }

    public function edit($id)
    {
        if($id > 0){
            $cms = CMS::find($id);

            if($cms){
                return View::make('Admin.cms.edit')->with('data',$cms);
            }else{
                return View::make('Admin.error.404');
            }
        }
    }

    public function destroy($id)
    {
        $cms = CMS::find($id);
        if($cms){
            if(CMS::where('id',$id)->delete()){
                return Redirect('admin/cms')->with('success','CMS deleted successfully');
            }
        }else{
            return "false";
        }
    }

}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\FuneralPlan;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;

class FuneralPlanController extends Controller
{
    //
    public function __construct(){

    }

    public function index()
    {
    	$pager = 2;
    	$bereavement = FuneralPlan::get();

    	return view('Admin.funeral-plan.list',['data' => $bereavement]);
    }

    public function perfomaction()
    {
        if(Input::get('action') == 'delete'){
            foreach (Input::get('ids') as $id) {
                $img = FuneralPlan::where('id',$id)->first()->image;
                File::delete(public_path('/images/funeralplan/') . $img);   
                $con = FuneralPlan::find($id);
                $con->delete();
            }
            return "true";
        }else{
            foreach (Input::get('ids') as $id) {
                $con = FuneralPlan::find($id);
                $st = Input::get('action') == 'active' ? '1' : '0';
                $con->status = $st;
                $con->updated_at = date('Y-m-d H:i:s');

                $con->save();
            }
            return "true";
        }
    }

    public function arrayData(Datatables $datatables)
    {
    	$builder = FuneralPlan::query()->orderBy('position')->select('id','image','name','location','status');

    	return $datatables->eloquent($builder)
                ->addColumn('check', function ($plan) {
                    return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=" . $plan->id . " name=\"uid[]\"><span class=\"checkmark\"></span> </label>";
                })
                ->addColumn('order', function($plan){
                    return "<img width=\"70px\" height=\"70px\" src='" . url('public/images/dnd.png') . "' onerror=this.src='" . url('public/images/avatar.png') . "'>";
                })
    			->editColumn('name',function($plan){
    				return $plan->name;
    			})
    			->editColumn('image',function($plan){
    				return "<img width=\"100px\" height=\"70px\" src='".url('public/images/funeralplan/')."/".$plan->image."'onerror=this.src='".url('public/images/avatar.png')."'>";
    			})
    			->editColumn('location', function($plan){
    				return $plan->location;
    			})
    			->editColumn('status',function($plan){
                    if($plan->status == 1){
                         return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $plan->id . " checked=\"true\" data-off-text=\"Inactive\">
";
                    }else if($plan->status == 0){
                        return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$plan->id."checked=\"true\" data-off-text=\"Inactive\">";
                    }else{
                        return '<span class="badge bg-yellow">Delete</span>';
                    }
                })
                ->addColumn('action', function($plan){
					 return "<a href=" . url('admin/funeral-plan/edit/' . $plan->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>				 	

					 	<a href=" . url('admin/funeral-plan/destroy/' . $plan->id) . " class=\"btn btn-danger btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
				})
				->rawColumns(['id','name','location','action','image','status','check','order'])
				->toJson();
    }

    public function create()
    {
    	return view('Admin.funeral-plan.add');
    }

    public function store(Request $request)
    {
        
    	$rules = [
    			'name' => 'required',
                'location' => 'required'
    		];

    	$message = [
    			'name.required' => 'Name is Required',
                'location.required' => 'Location is Required'
    		];

    	$validator = Validator::make($request->all(),$rules, $message);

    	if($validator->fails()){
    		if(Input::get('id') != null && Input::get('id') > 0){
    			return Redirect::to('admin/funeral-plan/edit/'.Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
    		}else{
    			return Redirect::to('admin/funeral-plan/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
    		}
    	}else{
    		if($request->file('image')){
	            $image = time().'.'.$request->file('image')->getClientOriginalExtension();
	            $request->file('image')->move(public_path('images/funeralplan'), $image);
	        }

            if($request->status == '' || $plan['status'] == 1){
                $plan['status'] = 1;
            }else{
                $plan['status'] = $request->status;
            }
	        $plan['name'] = $request->name;
			if($request->file('image')){
                $plan['image'] = $image;    
            }else{
                $plan['image'] = $request->old_image;
            }
			$plan['location'] = $request->location;
			$plan['updated_at'] = date('Y-m-d H:i:s');
			
    		if(isset($request->id)){
    			if($request->file('image')){
	    			$img = FuneralPlan::where('id',$request->id)->first()->image;
	                        File::delete(public_path('/images/funeralplan/') . $img);
    			}
    			FuneralPlan::where('id',$request->id)->update($plan);
    			return Redirect('admin/funeral-plan')->with('success','Plan Updated SuccessFully');
    		}else{
    			$plan1 = new FuneralPlan;
    			$plan1->insert($plan);
    			return Redirect('admin/funeral-plan')->with('success','Plan Created SuccessFully');
    		}
    	}
    }

    public function edit($id)
    {
    	if($id > 0){
    		$ber = FuneralPlan::find($id);

    		if($ber){
    			return View::make('Admin.funeral-plan.edit')->with('data',$ber);
    		}else{
    			return View::make('Admin.error.404');
    		}
    	}
    }

    public function updateImage(Request $request)
    {
        $img = FuneralPlan::where('id',$request->id)->first()->image;

        File::delete(public_path('/images/funeral-plan/').$img);

        $set['image'] = null;
        $set['updated_at'] = date('Y-m-d H:i:s');

        if(FuneralPlan::where('id',$request->id)->update($set)){
            return "true";
        }
    }

	public function destroy($id)
    {
    	$ber = FuneralPlan::find($id);
    	if($ber){
    		$img = FuneralPlan::where('id',$id)->first()->image;
	        File::delete(public_path('/images/funeralplan/') . $img);
    		if(FuneralPlan::where('id',$id)->delete()){
    			return Redirect::to('admin/funeral-plan')->with('success','Plan deleted successfully');
    		}
    	}else{
    		return "false";
    	}
    }

    public function updateOrder(Request $request)
    {
        foreach ($request->value as $key => $value) {
            $data['position'] = $key+1;
            $data['updated_at'] = date('Y-m-d H:i:s');
            FuneralPlan::where('id',$value)->update($data);
        }
    }	
}

<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Library\General;

use App\Models\User;
use App\Models\EmailUser;
use App\Models\Appsettings;
use App\Models\setconfig;


use Illuminate\Support\Facades\Hash;
use Html;
use DB;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Auth;

class SettingController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
       
       $email_user = EmailUser::find(1);
       // dd($email_user);
        return view('Admin.setting.add',compact('email_user'));
    }

 //    public function create() {
	// $dataTitle = Appsettings::orderBy('id')->pluck('app_name', 'id');;
 //        return view('Admin.setting.add',['dataTitle'=>$dataTitle,'titleId'=>'']);
 //    }
        public function InsertUser(Request $req){
           $id = DB::table('setting_user')->find(1);
           if($id->id == 1){
            // dd($id);
            $this->validate($req, [
            'institutes_amount' => 'required',
            'student_amount' => 'required',
            'email' => 'required|string|email|max:255',
            
           ]);
            
            DB::table('setting_user')->where('id',1)->update([
                                            'email' => $req->email,
                                            'institutes_amount' => $req->institutes_amount,
                                            'student_amount' => $req->student_amount,
                                            'password' => bcrypt($req->password),
                                            'updated_at' => date('Y-m-d H:i:s'),
                                            'updated_by' => Auth::user()->id,
                                            'updated_ip' => \Request::ip(),
                                        ]);
            return back()->with('message','Update successfully');
           
           }else{
           
             $this->validate($req, [
            'institutes_amount' => 'required|max:255',
            'student_amount' => 'required|max:255',
            'email' => 'required|string|email|max:255|unique:setting_user',
            // 'password' => 'required|string|min:6',
           ]);
             DB::table('setting_user')->insert([
                                            'email' => $req->email,
                                            'institutes_amount' => $req->institutes_amount,
                                            'student_amount' => $req->student_amount,
                                            'password' => bcrypt($req->password),
                                            'created_at' => date('Y-m-d H:i:s'),
                                            'created_by' => Auth::user()->id,
                                            'created_ip' => \Request::ip(),
                                        ]);
            return back();
           }
        }
   

    public function edit($id) {
        if ($id > 0) {
            $dataSettingDetail = DataProperty::find($id);
            if (count($dataSettingDetail) > 0) {
                return View::make('Admin.setting.edit')->with('data', $dataSettingDetail);
            }
            return View::make('Admin.error.404');
        }
    }

    public function dataMultiLangStore(Request $request) {

        // echo "<pre>";print_r($request->all());die;
        // echo "<pre>";
        $isSave = false;
		
				for($i=0;$i<Input::get('total');$i++){
				
				$dataadObj = setconfig::find(Input::get('cid')[$i]);
				$dataadObj->updated_at = date('Y-m-d H:i:s');
				$dataadObj->updated_by = Auth::user()->id;
			
            $dataadObj->key = Input::get('key')[$i];
			$dataadObj->value = Input::get('value')[$i];
			$dataadObj->description = Input::get('description')[$i];
			$dataadObj->save();
			$isSave = true;
				 }
        
        if($isSave) {
            return redirect("/data-setting/$request->data_property_id")->with('success', "Data saved successfully.");
        } else {
            return redirect("/data-setting/$request->data_property_id")->with('success', "something went wrong to save setting multi language title. please try later.");
        }
    }

    public function checkSlug(Request $request) {
        $dataSettingObj = DataProperty::where('slug','LIKE',"%$request->slug%")->get();
        if(count($dataSettingObj) > 0) {
            return "true";
        } else {
            return "false";
        }
    }

    public function addMultiLangFields(Request $request) {
        $dataTitle = Appsettings::orderBy('id')->pluck('app_name', 'id');
        $checkDefaultValue = setconfig::where('app_id',$request->selTitleId)->where('key',$request->key)->first();
		echo $request->key;
		print_r($checkDefaultValue);
        if(count($checkDefaultValue) > 0){
            return "false";
        }
        // echo "<pre>";print_r($request->all());die;
       $setconfig = new setconfig();
        $setconfig->created_at = date('Y-m-d H:i:s'); 
		$setconfig->created_by = Auth::user()->id;
		 $setconfig->app_id  = $request->selTitleId;
        $setconfig->key  = Input::get('key');
        $setconfig->value   = Input::get('value');
        $setconfig->description  = Input::get('desc');
       
        $setconfig->save();
		
        // echo "<pre>";print_r($dataPropertySubChildArr);die;
        return view('Admin.setting.add',['divClass'=>$request->divClass,'titleId'=>$request->selTitleId, 'titleText'=>$request->titleText,'key'=>$request->key,'value'=>$request->value,'desc'=>$request->desc,'dataTitle'=>$dataTitle]);
    }

    public function addMultiLangData(Request $request) {
	
        $configkey = setconfig::where('app_id',$request->titleId)->where('deleted_at',NULL)->get()->toArray();
        $data_app=Appsettings::where('id',$request->titleId)->first();

            return view('Admin.setting.get_multi_lang_data',['configkeys'=>$configkey,'data_app'=>$data_app]);
        
    
        
    }

    public function destroyMultiLangRow($id) {
       
        $setconfig    = setconfig::find($id);
        if (count($setconfig) > 0) {

            $setconfig->delete(); echo "true";
			}
			else{
			  echo "false";
			}

        
    }
}
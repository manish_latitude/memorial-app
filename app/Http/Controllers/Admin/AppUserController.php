<?php



namespace App\Http\Controllers\Admin;



use Illuminate\Http\Request;

use Yajra\Datatables\Datatables;

use App\Http\Controllers\Controller;

use Collective\Html\Eloquent\FormAccessible;

use App\Models\AppUser;

use Illuminate\Support\Facades\Hash;

use Html;

use File;

use Input;

use Validator;

use Redirect;

use View;

use Storage;

use Auth;

use Mail;

use Excel;



class AppUserController extends Controller

{

    public function __construct(){



    }



    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

    	$pager = 2;

    	$user = AppUser::get();



    	return view('Admin.app-user.list',['data' => $user]);

    }

    public function export()
    {
        
        $data = AppUser::select('id','name','email','phone_number','dateOfbirth','created_at')->get();

        Excel::create('App User Details', function($excel) use($data) {

        $excel->sheet('App User Details', function($sheet) use($data) {

            $sheet->fromArray($data);

        });

        })->export('xls');
    }

    public function perfomaction()

    {
    	if(Input::get('action') == 'delete'){
        
    		foreach (Input::get('ids') as $id) {
                
    			$user = AppUser::find($id);

    			$user->delete();
                
    		}
            return "true";
    	}else{

    		foreach (Input::get('ids') as $id) {

    			$user = AppUser::find($id);

    			$st = Input::get('action') == 'active' ? '1' : '0';

    			$user->status = $st;

    			$user->updated_at = date('Y-m-d H:i:s');



    			$user->save();

    		}

    		return "true";

    	}

    }



    public function arrayData(Datatables $datatables)

    {

    	$builder = AppUser::query()->select('id','name','email','phone_number','register_type','social_id','status','created_at','updated_at')->where('deleted_at',null);



    	return $datatables->eloquent($builder)
                ->addColumn('check', function ($user) {
                    return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=" . $user->id . " name=\"uid[]\"><span class=\"checkmark\"></span> </label>";
                })
                ->editColumn('id',function($user){

                    return $user->id;

                })

				->editColumn('email',function($user){

					return $user->email;

				})

				->editColumn('name',function($user){

					return $user->name;

				})

				->editColumn('phone_number',function($user){

					return $user->phone_number;

				})

				->editColumn('register_type', function($user){

                    if($user->register_type == 0){

                        return "Normal";

                    }else if($user->register_type == 1){

                        return "Facebook";

                    }else if($user->register_type == 2){

                        return "GPlus";

                    }

				})

                ->editColumn('social_id', function($user){

                    return $user->social_id;

                })

				->editColumn('status',function($user){

					if($user->status == 1){

						 return "

    <input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $user->id . " checked=\"true\" data-off-text=\"Inactive\">

";

					}else if($user->status == 0){

						return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$user->id."checked=\"true\" data-off-text=\"Inactive\">";

					}else{

						return '<span class="badge bg-yellow">Delete</span>';

					}

				})

				->editColumn('created_at',function($user){

					if($user->created_at !== null){

						return date("Y-m-d H:i:s", strtotime($user->created_at));

					}

					return '';

				})

				->editColumn('updated_at',function($user){

					if($user->updated_at !== null){

						return date("Y-m-d H:i:s", strtotime($user->updated_at));

					}

					return '';

				})

				->addColumn('action', function($user){

					 return "<a href=" . url('admin/app-user/edit/' . $user->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>				 	



					 	<a href=" . url('admin/app-user/destroy/' . $user->id) . " class=\"btn btn-danger btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>";

				})

				->rawColumns(['id','name','email','phone_number','social_id','register_type','created_at','updated_at','status','action','check'])

				->toJson();

    }



    public function create()

    {

    	return view('Admin.app-user.add');

    }



    public function store(Request $request)

    {

    	$rules = [

    			'name' => 'required|max:255',

    			'email' => 'required|email|unique:app_user,email,'.$request->id,'id',

    			'phone_number' => 'required|min:10|numeric',

                // 'password' => 'required|min:6',

                // 'confirm_password' => 'required|same:password'

    		];



    	$message = [

    			'name.required' => "Name is Required",

    			'name.max' => "Name must be less than 255 characters",

    			'email.required' => "Email is Required",

    			'email.email' => "Invalide Email Address",

    			'email.unique' => "Email Address Already Exist",

    			'phone_number.required' => "Mobile Number is required",

	            'phone_number.numeric' => "Mobile should be numeric",

	            'phone_number.min' => "Minimum 10 characters",

	            // 'phone_number.max' => "Maximum 12 characters",

                // "password.required" => "Password is required",

                // "confirm_password.required" => "Confirm Password is required",

                // "password.min" => "Password should be six characters long",
                
                // "confirm_password.same" => "Confirmation Password should be like password"

    		];



    	$validator = Validator::make($request->all(),$rules,$message);



    	if($validator->fails()){

    		if(Input::get('id') != null && Input::get('id') > 0){

    			return Redirect::to('admin/app-user/edit/'.Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));

    		}else{

    			return Redirect::to('admin/app-user/create')->withErrors($validator)->withInput(Input::except('laravel_password'));

    		}

    	}else{

    		$user = "";



    		if(isset($request->id)){

    			$user = AppUser::find($request->id);

                if($user->status == 1){

                    $user->status = 1;

                }else{

                    $user->status = $request->status;

                }

            	$user->updated_at = date('Y-m-d H:i:s');

    		}else{

    			$user = new AppUser;



    			$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $token = substr(str_shuffle(str_repeat($pool, 5)), 0, 30);  



    			$user->token = $token;

    			$user->status = 1;

    			$user->created_at = date('Y-m-d H:i:s');

    		}

            $user->social_id = Input::get('social_id');

    		$user->name = Input::get('name');

    		$user->email = Input::get('email');

    		$user->phone_number = Input::get('phone_number');

    		$user->dateOfbirth = date("Y-m-d", strtotime($request->dateOfbirth));

            if(Input::get('user_type')){
    		  $user->register_type = Input::get('user_type');
            }else{
                $user->register_type = 0;
            }


    		if($user->save()){

    			if(!isset($request->id)){

    				$confirm_link = url('admin/app-user/resetPassword') . '/' . $user->token;

	                $tomail = $request->email;
                    $subject = 'Memorial Password Reset';
                    $body = "Password reset successfully";

	                $data = array("name" => $request->name, 'reset_link' => $confirm_link, 'message' => $body, "to" => $tomail, "subject" => $subject, "from" => 'info@keeptheirmemoryalive.com', "emailLabel" => 'Password Reset',"email" => $request->email);

	                $res =Mail::send('emails.resetPass',['data'=>$data],function($message) use ($data){

		                $message->from($data['from'],$data['emailLabel']);

		                $message->to($data['to'],$data['name']);

		                $message->subject($data['subject']);

	                }); 

    			}

    			if(Input::get('id') != null && Input::get('id') > 0){

    				return Redirect('admin/app-user')->with('success','User Updated Successfully');

    			}else{

    				return Redirect('admin/app-user')->with('success','User Created Successfully');

    			}

    		}else{

    			return redirect('admin/app-user')->with('success', "something went wrong please try later.");

    		}

    	}

    }



    public function edit($id)

    {

    	if($id > 0){



    		$user = AppUser::find($id);

    		

    		if($user){

    			return View::make('Admin.app-user.edit')->with('data',$user);

    		}

    		return View::make('Admin.error.404');

    	}

    }



    public function destroy($id)

    {

    	$user = AppUser::find($id);

    	if($user){

    		if(AppUser::where('id',$id)->delete()){

    			return Redirect('admin/app-user')->with('success','User deleted successfully');

    		}

    	}else{

    		return "false";

    	}

    }

    public function resetPasswordView($token)
    {
        return View::make('Admin.app-user.resetPassword')->with('token',$token);
    }

    public function resetPassword(Request $request)
    {
        $rules = [
                 'password' => 'required|min:6',

                'conf_password' => 'required|same:password'
            ];

        $message = [
                "password.required" => "Password is required",

                "conf_password.required" => "Confirm Password is required",

                "password.min" => "Password should be six characters long",
                
                "conf_password.same" => "Confirmation Password should be like password"                
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if($validator->fails()){
            return Redirect::to('admin/app-user/resetPassword/'.$request->token)->withErrors($validator)->withInput(Input::except('laravel_password'));
        }else{
            $userObj = AppUser::where('token', $request->token)->first();
            if (count($userObj) > 0) {
                $userObj->password = bcrypt($request->password);
                $userObj->is_verified = 1;
                if($userObj->save()){
                    $tomail = $userObj->email;
                    $subject = 'Memorial Password Reset';
                    $body = "Password reset successfully";

                    $data = array("name" => $userObj->name, 'message' => $body, "to" => $tomail, "subject" => $subject, "from" => 'info@keeptheirmemoryalive.com', "emailLabel" => 'Password Reset');

                    $res =Mail::send('emails.resetPassSuccess',['data'=>$data],function($message) use ($data){

                        $message->from($data['from'],$data['emailLabel']);

                        $message->to($data['to'],$data['name']);

                        $message->subject($data['subject']);

                    }); 
                }

                return Redirect('admin/app-user/resetPassword/'.$request->token)->with('success','Password reset successfully');
            } else {
                return Redirect('admin/app-user/resetPassword/'.$request->token)->with('success','Password reset fail');
            }
        }
    }

}


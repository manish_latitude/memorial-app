<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\AboutUs;
use App\Models\Funeral;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;

class AboutUsController extends Controller
{
    //
    public function __consturct(){

    } 

    public function index()
    {
    	$pager = 2;
    	$about = AboutUs::get();

    	return view('Admin.about-us.list',['data' => $about]);
    }

    public function perfomaction()
    {
    	if(Input::get('action') == 'delete'){
    		foreach (Input::get('ids') as $id) {
    			$about = AboutUs::find($id);
    			$about->delete();
    			return "true";
    		}
    	}else{
    		foreach (Input::get('ids') as $id) {
    			$about = AboutUs::find($id);
    			$st = Input::get('action') == 'active' ? '1' : '0';
    			$about->status = $st;
    			$about->updated_at = date('Y-m-d H:i:s');
    			$about->save();
    		}
    		return "true";
    	}
    }

    public function arrayData(Datatables $datatables)
    {
    	$builder = AboutUs::query()->select('id','funeral_id','title','description','status');

    	return $datatables->eloquent($builder)
    			->addColumn('check', function ($about) {
                    return "<label class=\"chk\"><input type=\"checkbox\" class=\"selectcheckbox\" value=" . $about->id . " name=\"uid[]\"><span class=\"checkmark\"></span></label>";
                })
                ->editColumn('funeral_id',function($about){
    				return $name = Funeral::where('id',$about->funeral_id)->pluck('home_name')->first();
    			})
    			->editColumn('title',function($about){
    				return $about->title;
    			})
    			->editColumn('status',function($about){
					if($about->status == 1){
						return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=" . $about->id . " checked=\"true\" data-off-text=\"Inactive\">";
					}else if($about->status == 0){
						return "<input type=\"checkbox\" class=\"make-switch btn-success switch-small\" data-size=\"small\" data-on-text=\"Active\" value=".$about->id."checked=\"true\" data-off-text=\"Inactive\">";
					}else{
						return '<span class="badge bg-yellow">Delete</span>';
					}
				})
				->addColumn('action', function($about){
                     return "<a href=" . url('admin/about/edit/' . $about->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>                 

                        <a href=" . url('admin/about/destroy/' . $about->id) . " class=\"btn btn-danger btn-sm\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
                })
                ->rawColumns(['id','funeral_id','title','status','action','check'])
                ->toJson();
    }

    public function create()
    {
    	$data['funeral'] = Funeral::orderBy('id')->pluck('home_name','id');
    	return view('Admin.about-us.add',$data);
    }

    public function store(Request $request)
    {
    	$rules = [
    			'funeral_home' => 'required',
    			'title' => 'required',
    		];

    	$message = [
    			'funeral_home.required' => 'Select Funeral Home Name',
    			'title.required' => 'Title Required'
    		];

    	$validator = Validator::make($request->all(),$rules,$message);

    	if($validator->fails()){
    		if(Input::get('id') != null && Input::get('id') > 0){
                return Redirect::to('admin/about/edit/'.Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
            }else{
                return Redirect::to('admin/about/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
            }
    	}else{
    		if($request->status == '' || $about['status'] == 1){
                $about['status'] = 1;
            }else{
                $about['status'] = $request->status;
            }

            $about['funeral_id'] = $request->funeral_home;
            $about['title'] = $request->title;
            $about['description'] = strip_tags($request->about);
            $about['updated_at'] = date('Y-m-d H:i:s');

            if(isset($request->id)){
                AboutUs::where('id',$request->id)->update($about);
                return Redirect('admin/about')->with('success','Content Updated Successfully');
            }else{
                $about1 = new AboutUs;
                $about1->insert($about);
                return Redirect('admin/about')->with('success','Content Created Successfully');
            }
    	}
    }

    public function edit($id)
    {
    	if($id > 0){
    		$data['funeral'] = Funeral::orderBy('id')->pluck('home_name', 'id');
            $data['about'] = AboutUs::find($id);

            if($data){
                return View::make('Admin.about-us.edit')->with($data);
            }else{
                return View::make('Admin.error.404');
            }
        }
    }

    public function destroy($id)
    {
        $about = AboutUs::find($id);
        if($about){
            if(AboutUs::where('id',$id)->delete()){
                return Redirect('admin/about')->with('success','Content deleted successfully');
            }
        
        }else{
            return "false";
        }
    }
}

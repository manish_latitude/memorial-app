<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\ResponseMessage;
use App\Models\Bereavement;
use App\Models\Condolence;
use App\Models\FuneralPlan;
use App\Models\WillTable;
use App\Models\MemorialTag;
use App\Models\FuneralProfile;
use App\Models\FuneralImage;
use App\Models\MemberImage;
use App\Models\AppUser;
use App\Models\Flower;
use App\Models\Funeral;
use App\Models\Member;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use Validator;
use Input;
use Cache;

class VisitorController extends Controller
{
    //
  protected $userId;
    
  	public function __construct() {
        header("Content-Type: application/json");

        $headers = getallheaders();

        $apiToken = isset($headers['apiToken']) ? $headers['apiToken'] : '';
        if ($apiToken != '' && $apiToken != null) {
            $token = AppUser::where('header_token',$apiToken)->orWhere('header_token','L@titude2018')->get()->toArray();
            if (count($token) > 0) {
                $this->userId = $token[0]['id'];
            } else {
                echo json_encode([
                    'status' => false,
                    'error' => 201,
                    'message' => Cache::get('invalid-token'),
                ]);
                exit;
            }
        } else {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => Cache::get('token-req'),
            ]);
            exit;
        }
    }

    /**
    * Condolence List
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: limit,page
    */

    public function condolenceList(Request $request)
    {
      $rules = [
              'limit' => 'required',
              'page' => 'required'
          ];

      $message = [
              'limit.required' => 'Data Limit Required',
              'page.required' => 'Page Number Required'
          ];

      $validator = Validator::make($request->all(),$rules,$message);

      if($validator->fails()){
        echo json_encode([
                      'status' => false,
                      'error' => 401,
                      'message' => $validator->messages()->first()
                  ]);
        exit;
      }

      $limit = $request->get('limit') != null?$request->get('limit'):5;
        $page = $request->get('page') != null?$request->get('page') - 1:'0';
     
      $data = $page * $limit;

      $con = Condolence::query()->orderBy('position')->select('id','name','image','price','description','url')->take($limit)->skip($data)->get();
      
      if($con != '' && count($con) > 0){
        ResponseMessage::success(Cache::get('con-list'),$con);
      }else{
        ResponseMessage::error(Cache::get('con-not-found'));
      }
    }

    /**
    * Flowers List
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: limit,page
    */

    public function flowerList(Request $request)
    {
        $rules = [
              'limit' => 'required',
              'page' => 'required'
          ];

        $message = [
              'limit.required' => 'Data Limit Required',
              'page.required' => 'Page Number Required'
          ];

        $validator = Validator::make($request->all(),$rules,$message);

        if($validator->fails()){
          echo json_encode([
                        'status' => false,
                        'error' => 401,
                        'message' => $validator->messages()->first()
                    ]);
          exit;
        }

        $limit = $request->get('limit') != null?$request->get('limit'):5;
        $page = $request->get('page') != null?$request->get('page') - 1:'0';
     
        $data = $page * $limit;

        $flower = Flower::query()->orderBy('position')->select('id','name','image','price','description','url')->take($limit)->skip($data)->get();

        if($flower != '' && count($flower) > 0){
          ResponseMessage::success(Cache::get('flower-list'),$flower);
        }else{
          ResponseMessage::error(Cache::get('flower-not-found'));
        }
    }

    /**
    * Bereavement List
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: limit,page
    */

    public function bereavementList(Request $request)
    {
      $rules = [
              'limit' => 'required',
              'page' => 'required'
          ];

      $message = [
              'limit.required' => 'Data Limit Required',
              'page.required' => 'Page Number Required'
          ];

      $validator = Validator::make($request->all(),$rules,$message);

      if($validator->fails()){
        echo json_encode([
                    'status' => false,
                    'error' => 401,
                    'message' => $validator->messages()->first()
              ]);
        exit;
      }

      $limit = Input::get('limit') != null?Input::get('limit'):5;
      $page = Input::get('page') != null?Input::get('page') - 1:0;

      $data = $limit * $page;

      $ber = Bereavement::query()->orderBy('position')->select('id','image','name','description','url')->get();

      if($ber != '' && count($ber) > 0){
        ResponseMessage::success(Cache::get('bre-list'),$ber);
      }else{
        ResponseMessage::error(Cache::get('bre-not-found'));
      }
    }

    /**
    * Funeral Plan List
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: limit,page
    */

    public function funeralPlanList(Request $request)
    {
      $rules = [
              'limit' => 'required',
              'page' => 'required'
          ];

      $message = [
              'limit.required' => 'Data Limit Required',
              'page.required' => 'Page Number Required'
          ];

      $validator = Validator::make($request->all(),$rules,$message);

      if($validator->fails()){
        echo json_encode([
                    'status' => false,
                    'error' => 401,
                    'message' => $validator->messages()->first()
              ]);
        exit;
      }

      $limit = Input::get('limit') != null?Input::get('limit'):5;
      $page = Input::get('page') != null?Input::get('page') - 1:0;

      $data = $limit * $page;

      $plan = FuneralPlan::query()->orderBy('position')->select('id','image','name','location')->get();

      if($plan != '' && count($plan) > 0){
        ResponseMessage::success(Cache::get('plan-list'),$plan);
      }else{
        ResponseMessage::error(Cache::get('plan-not-found'));
      }
    }

    /**
    * Will List
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: limit,page
    */

    public function willList(Request $request)
    {
      $rules = [
              'limit' => 'required',
              'page' => 'required'
          ];

      $message = [
              'limit.required' => 'Data Limit Required',
              'page.required' => 'Page Number Required'
          ];

      $validator = Validator::make($request->all(),$rules,$message);

      if($validator->fails()){
        echo json_encode([
                    'status' => false,
                    'error' => 401,
                    'message' => $validator->messages()->first()
              ]);
        exit;
      }

      $limit = Input::get('limit') != null?Input::get('limit'):5;
      $page = Input::get('page') != null?Input::get('page') - 1:0;

      $data = $limit * $page;

      $will = WillTable::query()->orderBy('position')->select('id','image','name','description','url')->get();

      if($will != '' && count($will) > 0){
        ResponseMessage::success(Cache::get('will-list'),$will);
      }else{
        ResponseMessage::error(Cache::get('will-not-found'));
      }
    }

    /**
    * Will List
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: limit,page
    */

    public function memorialTagList(Request $request)
    {
      $rules = [
              'limit' => 'required',
              'page' => 'required'
          ];

      $message = [
              'limit.required' => 'Data Limit Required',
              'page.required' => 'Page Number Required'
          ];

      $validator = Validator::make($request->all(),$rules,$message);

      if($validator->fails()){
        echo json_encode([
                    'status' => false,
                    'error' => 401,
                    'message' => $validator->messages()->first()
              ]);
        exit;
      }

      $limit = Input::get('limit') != null?Input::get('limit'):5;
      $page = Input::get('page') != null?Input::get('page') - 1:0;

      $data = $limit * $page;

      $tag = MemorialTag::query()->orderBy('position')->select('id','image','name','price','description','url')->get();

      if($tag != '' && count($tag) > 0){
        ResponseMessage::success(Cache::get('tag-list'),$tag);
      }else{
        ResponseMessage::error(Cache::get('tag-not-found'));
      }
    }

    /**
    * Search Funeral Home
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: Funeral home name,state,city
    */    

    public function searchFuneral(Request $request)
    {
   		if(Input::get('home_name') == null && Input::get('area') == null && Input::get('country') == null){
   			$f1 = Funeral::query()->select('id','home_name','address','image')->where('home_name','!=',null)->where('approval',1)->where('status',1)->orderBy('updated_at','desc')->orderBy('id','DESC')->get();
   			ResponseMessage::success(Cache::get('funeral-list'),$f1);
   		}else if(Input::get('home_name') && Input::get('area') && Input::get('country')){
	        if(Funeral::where('home_name','like','%'.Input::get('home_name').'%')->where('approval',1)->where('status',1)->exists() && Funeral::where('area','like','%'.Input::get('area').'%')->where('approval',1)->where('status',1)->exists()){
	          $f1 = Funeral::query()->select('id','home_name','address','image')->where('home_name','like','%'.Input::get('home_name').'%')->where('area','like','%'.Input::get('area').'%')->where('country',Input::get('country'))->where('approval',1)->where('status',1)->get();
	          ResponseMessage::success(Cache::get('funeral-list'),$f1);
	        }else{
            ResponseMessage::error(Cache::get('funeral-not-found'));
          }
	    }else if(Input::get('home_name') && Input::get('country')){
	        if(Funeral::where('home_name','like','%'.Input::get('home_name').'%')->where('approval',1)->where('status',1)->exists()){
	          $f1 = Funeral::query()->select('id','home_name','address','image')->where('home_name','like','%'.Input::get('home_name').'%')->where('country',Input::get('country'))->where('approval',1)->where('status',1)->get();
	          ResponseMessage::success(Cache::get('funeral-list'),$f1);
	        }else{
	        	ResponseMessage::error(Cache::get('funeral-not-found'));
	        }
	    }else if(Input::get('home_name') && Input::get('area')){
	        if(Funeral::where('home_name','like','%'.Input::get('home_name').'%')->where('approval',1)->where('status',1)->exists() && Funeral::where('area','like','%'.Input::get('area').'%')->where('approval',1)->where('status',1)->exists()){
	            $f1 = Funeral::query()->select('id','home_name','address','image')->where('home_name','like','%'.Input::get('home_name').'%')->where('area','like','%'.Input::get('area').'%')->where('approval',1)->where('status',1)->get();
	            ResponseMessage::success(Cache::get('funeral-list'),$f1);
	        }else{
	            ResponseMessage::error(Cache::get('funeral-not-found'));
	        }
	    }else if(Input::get('area') && Input::get('country')){
          if(Funeral::where('area','like','%'.Input::get('area').'%')->where('approval',1)->where('status',1)->exists()){
	          $f1 = Funeral::query()->select('id','home_name','address','image')->where('area','like','%'.Input::get('area').'%')->where('country',Input::get('country'))->where('approval',1)->where('status',1)->get();
	          ResponseMessage::success(Cache::get('funeral-list'),$f1);
          }else{
            ResponseMessage::error(Cache::get('funeral-not-found'));  
          }
	    }else if(Input::get('country')){
	        $f1 = Funeral::query()->select('id','home_name','address','image')->where('country',Input::get('country'))->where('approval',1)->where('status',1)->get();
	        ResponseMessage::success(Cache::get('funeral-list'),$f1);
	    }else if(Input::get('home_name')){
          if(Funeral::where('home_name','like','%'.Input::get('home_name').'%')->where('approval',1)->where('status',1)->exists()){
             $f1 = Funeral::query()->select('id','home_name','address','image')->where('home_name','like','%'.Input::get('home_name').'%')->where('approval',1)->where('status',1)->get();
             ResponseMessage::success(Cache::get('funeral-list'),$f1);
          }else{
            ResponseMessage::error(Cache::get('funeral-not-found'));  
          }
      }else if(Input::get('area')){
          if(Funeral::where('area','like','%'.Input::get('area').'%')->where('approval',1)->where('status',1)->exists()){
            $f1 = Funeral::query()->select('id','home_name','address','image')->where('area','like','%'.Input::get('area').'%')->where('approval',1)->where('status',1)->get();
            ResponseMessage::success(Cache::get('funeral-list'),$f1);
          }else{
            ResponseMessage::error(Cache::get('funeral-not-found'));  
          }
      }else{
	        ResponseMessage::error(Cache::get('funeral-not-found'));
	    }
    }

    /*public function searchFuneral(Request $request)
    {
   		if(Input::get('home_name') == null && Input::get('state') == null && Input::get('city') == null){
   			$f1 = Funeral::query()->select('id','home_name','address','image')->where('home_name','!=',null)->where('approval',1)->where('status',1)->orderBy('updated_at','desc')->get();
   			ResponseMessage::success(Cache::get('funeral-list'),$f1);
   		}else if(Input::get('home_name') && Input::get('state') && Input::get('city')){
	        if(Funeral::where('home_name','like','%'.Input::get('home_name').'%')->where('approval',1)->where('status',1)->exists()){
	          $f1 = Funeral::query()->select('id','home_name','address','image')->where('home_name','like','%'.Input::get('home_name').'%')->where('state',Input::get('state'))->where('city',Input::get('city'))->where('approval',1)->where('status',1)->get();
	          ResponseMessage::success(Cache::get('funeral-list'),$f1);
	        }
	    }else if(Input::get('home_name') && Input::get('state')){
	        if(Funeral::where('home_name','like','%'.Input::get('home_name').'%')->where('approval',1)->where('status',1)->exists()){
	          $f1 = Funeral::query()->select('id','home_name','address','image')->where('home_name','like','%'.Input::get('home_name').'%')->where('approval',1)->where('status',1)->where('state',Input::get('state'))->get();
	          ResponseMessage::success(Cache::get('funeral-list'),$f1);
	        }else{
	        	ResponseMessage::error(Cache::get('funeral-not-found'));	
	        }
	    }else if(Input::get('state') && Input::get('city')){
	        $f1 = Funeral::query()->select('id','home_name','address','image')->where('approval',1)->where('status',1)->where('state',Input::get('state'))->where('city',Input::get('city'))->get();
	        ResponseMessage::success(Cache::get('funeral-list'),$f1);
	    }else if(Input::get('home_name')){
	        if(Funeral::where('home_name','like','%'.Input::get('home_name').'%')->where('approval',1)->where('status',1)->exists()){
	          $f1 = Funeral::query()->select('id','home_name','address','image')->where('home_name','like','%'.Input::get('home_name').'%')->where('approval',1)->where('status',1)->get();
	          ResponseMessage::success(Cache::get('funeral-list'),$f1);
	        }else{
	        	ResponseMessage::error(Cache::get('funeral-not-found'));
	        }
	    }else if(Input::get('state') || Input::get('city')){
	        $f1 = Funeral::query()->select('id','home_name','address','image')->where('state',Input::get('state'))->orWhere('city',Input::get('city'))->get();
	        ResponseMessage::success(Cache::get('funeral-list'),$f1);
	    }else{
	        ResponseMessage::error(Cache::get('funeral-not-found'));
	    }
    }*/

    /**
    * Funeral Home Data
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: funeral id
    */ 

    public function funeralProfile(Request $request)
    {
      $rules = [
              'funeral_id' => 'required'
          ];

      $message = [
              'funeral_id.required' => 'Funeral Id is Required'
          ];

      $validator = Validator::make($request->all(),$rules,$message);

      if($validator->fails()){
        echo json_encode([
                      'status' => false,
                      'error' => 401,
                      'message' => $validator->messages()->first()
                ]);
        exit;
      }

      if(Funeral::where('id',Input::get('funeral_id'))->first()){
        $profile = Funeral::with(['profile'=>function($query){
            $query->select(['funeral_id','title','description']);
            }])->with(['obituaries'=>function($query){
              $query->select(['id','funeral_id','name','dateOfbirth','dateOfpass','about']);
            }])->with(['gallery'=>function($query){
              $query->select(['funeral_id','image_video','type']);
            }])->select('id','image','home_name','address','email','phone','facebook','approval')->where('id',Input::get('funeral_id'))->get();

            if(Member::where('funeral_id',Input::get('funeral_id'))->exists()){
              foreach ($profile as $value) {
                foreach ($value->obituaries as $val) {
                  if(MemberImage::where('member_id',$val->id)->exists()){
                    $image[] = MemberImage::query()->select('id','member_id','image_video','type')->where('member_id',$val->id)->get();    
                  }else{
                    $image[] = "";
                  }
                }
              }
            }else{
              $image = null;
            }

              $imageObj = $image;
              $array = array_merge(['profile' => $profile, 'member_profile' => $imageObj]); 
              ResponseMessage::success(Cache::get('funeral-profile'),$array);
      }else{
        ResponseMessage::error(Cache::get('funeral-not-found'));
      }
    }

    /**
    * Country List
    * Author : Pooja Makwana 
    * @param Request ''
    * param list :: ''
    */

    public function countryList()
    {
      $country = Country::query()->select('id','country')->where('status',1)->get();

      if($country != '' && count($country) > 0){
        ResponseMessage::success(Cache::get('country-list'),$country);
      }else{
        ResponseMessage::error(Cache::get('country-not-found'));
      }
    }

    /**
    * State List
    * Author : Pooja Makwana 
    * @param Request ''
    * param list :: ''
    */

    public function stateList(Request $request)
    {
      if(Input::get('country_id')){
        $rules = [
                'country_id' => 'required'
            ];

        $message = [
                'country_id.required' => 'Country Id is required'
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if($validator->fails()){
          echo json_encode([
                          'status' => false,
                          'error' => 401,
                          'message' => $validator->messages()->first()
                  ]);
          exit;
        }
        $state = State::query()->select('id','country_id','state')->where('country_id',Input::get('country_id'))->where('status',1)->get();
      }else{
        $state = State::query()->select('id','state')->where('status',1)->get();
      }

      

      if($state != '' && count($state) > 0){
        ResponseMessage::success(Cache::get('state-list'),$state);
      }else{
        ResponseMessage::error(Cache::get('state-not-found'));
      }
    }

    /**
    * City List
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: state_id
    */

    public function cityList(Request $request)
    {
      if(Input::get('state_id')){
        $rules = [
                'state_id' => 'required',
           ];

        $message = [
                'state_id.required' => 'State Id is required'
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if($validator->fails()){
          echo json_encode([
                          'status' => false,
                          'error' => 401,
                          'message' => $validator->messages()->first()
                  ]);
          exit;
        }
        $city = City::query()->select('id','state_id','city')->where('status',1)->where('state_id',$request->state_id)->get();
      }else{
        $city = City::query()->select('id','city')->where('status',1)->get();
      }
      
      if($city != '' && count($city) > 0){
        ResponseMessage::success(Cache::get('city-list'),$city);
      }else{
        ResponseMessage::error(Cache::get('city-not-found'));
      }
    }

    /**
    * City wise funeral list
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: country id
    */    

    public function countryFuneral(Request $request)
    {
      $rules = [
              'country_id' => 'required'
          ];

      $message = [
              'country_id.required' => 'Country Id is required'
          ];

      $validator = Validator::make($request->all(),$rules,$message);

      if($validator->fails()){
        echo json_encode([
                        'status' => false,
                        'error' => 401,
                        'message' => $validator->messages()->first()
                ]); 
        exit;
      }

      $funeral = Funeral::query()->select('id','home_name')->where('home_name','!=',null)->where('country',Input::get('country_id'))->get();

      if($funeral != '' && count($funeral) > 0){
        ResponseMessage::success(Cache::get('funeral-list'),$funeral);
      }else{
        ResponseMessage::error(Cache::get('funeral-not-found'));
      }
    }

    // public function cityFuneral(Request $request)
    // {
    //   $rules = [
    //           'city_id' => 'required'
    //       ];

    //   $message = [
    //           'city_id.required' => 'City Id is required'
    //       ];

    //   $validator = Validator::make($request->all(),$rules,$message);

    //   if($validator->fails()){
    //     echo json_encode([
    //                     'status' => false,
    //                     'error' => 401,
    //                     'message' => $validator->messages()->first()
    //             ]); 
    //     exit;
    //   }

    //   $funeral = Funeral::query()->select('id','home_name')->where('city',Input::get('city_id'))->get();
    //   if($funeral != '' && count($funeral) > 0){
    //     ResponseMessage::success(Cache::get('funeral-list'),$funeral);
    //   }else{
    //     ResponseMessage::error(Cache::get('funeral-not-found'));
    //   }
    // }

    /**
    * Search Deceased Member
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: member name,area
    */   

    public function searchMember(Request $request)
    {
      if(Input::get('name') == null && Input::get('funeral_home') == null){
        $m1 = Member::with(['gallery' => function($query){
          $query->select(['id','member_id','image_video']);
          $query->where('type',1);
        }])->with(['country' => function($query){
          $query->select(['id','country']);
        }])->select('id','name','dateOfpass','country_id')->where('approval',1)->where('status',1)->orderBy('updated_at','desc')->get();
        ResponseMessage::success(Cache::get('member-list'),$m1);
      }else if(Input::get('name') && Input::get('funeral_home') != null){
        dD('2');
        if(Member::where('name','like','%'.Input::get('name').'%')->where('approval',1)->where('status',1)->exists() && Input::get('funeral_home') == 0){
          $m1 = Member::with(['gallery' => function($query){
                $query->select(['id','member_id','image_video']);
                $query->where('type',1);
            }])->with(['country' => function($query){
                $query->select(['id','country']);
            }])->select('id','name','dateOfpass','country_id')->where('name','like','%'.Input::get('name').'%')->where('approval',1)->where('status',1)->Where('funeral_id',0)->get();
          ResponseMessage::success(Cache::get('member-list'),$m1);
        }else if(Member::where('name','like','%'.Input::get('name').'%')->where('approval',1)->where('status',1)->exists()){
          dd('22');
          $m1 = Member::with(['gallery' => function($query){
                $query->select(['id','member_id','image_video']);
                $query->where('type',1);
            }])->with(['country' => function($query){
                $query->select(['id','country']);
            }])->select('id','name','dateOfpass','country_id')->where('name','like','%'.Input::get('name').'%')->where('approval',1)->where('status',1)->where('funeral_id',Input::get('funeral_home'))->get();
          ResponseMessage::success(Cache::get('member-list'),$m1);
        }else{
        	ResponseMessage::error(Cache::get('member-not-found'));   
        }
      }else if(Input::get('name')){
        dd('3');
        if(Member::where('name','like','%'.Input::get('name').'%')->where('approval',1)->where('status',1)->exists()){
          $m1 = Member::with(['gallery' => function($query){
                $query->select(['id','member_id','image_video']);
                $query->where('type',1);
            }])->with(['country' => function($query){
              $query->select(['id','country']);
            }])->select('id','name','dateOfpass','country_id')->where('name','like','%'.Input::get('name').'%')->where('approval',1)->where('status',1)->get();
          ResponseMessage::success(Cache::get('member-list'),$m1);
        }else{
        	ResponseMessage::error(Cache::get('member-not-found'));   
        }
      }else if(Input::get('funeral_home') != null){
        $m1 = Member::with(['gallery' => function($query){
                $query->select(['id','member_id','image_video']);
                $query->where('type',1);
            }])->with(['country' => function($query){
              $query->select(['id','country']);
            }])->select('id','name','dateOfpass','country_id')->where('approval',1)->where('status',1)->where('funeral_id',Input::get('funeral_home'))->get();
        ResponseMessage::success(Cache::get('member-list'),$m1);
      }else{
        ResponseMessage::error(Cache::get('member-not-found'));   
      }
    }

    /**
    * Deceased Member Profile
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: member id
    */  

    public function memberProfile(Request $request)
    {
      $rules = [
              'member_id' => 'required'
          ];

      $message = [
              'member_id.required' => 'Member Id is required'
          ];

      $validator = Validator::make($request->all(),$rules,$message);

      if($validator->fails()){
        echo json_encode([
                        'status' => 'false',
                        'error' => 401,
                        'message' => $validator->messages()->first()
                ]);
        exit;
      }

      if(Member::where('id',Input::get('member_id'))->first()){
        $member = Member::with(['funeral'=>function($query){
            $query->select(['id','home_name']);
          }])->with(['country' => function($query){
            $query->select(['id','country']);
          }])->with(['gallery'=>function($query){
            $query->select(['id','member_id','image_video','type']);
          }])->with(['orderOfservice'=>function($query){
            $query->select(['id','member_id','image']);
          }])->with(['community' => function($query){
            $query->select(['id','community']);
          }])->select('id','name','dateOfbirth','dateOfpass','about','funeral_id','country_id','community_id')->where('id',Input::get('member_id'))->first();

        ResponseMessage::success(Cache::get('member-profile'),$member);
      }else{
        ResponseMessage::error(Cache::get('member-not-found'));
      }
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\AppUserCode;
use App\Models\Admob;
use App\Models\AppAdmob;
use App\Models\MoreApps;
use App\Models\setconfig;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use DB;
use Cache;

class generalSettingController extends Controller
{
    public function __construct() {
    }

    /**
    * Faqs list
    * Author : Manish Patolia 
    * @param Request $request
    * param list :: user_id=MjM4NDE3MzQ4MDA=,
    */
    
   public function generalSetting(Request $request) {
        $rules = [
            'app_id'  => 'required',
        ];
        $message = [
            'app_id.required'  => "app_id is required.",
        ];
        $validator = Validator::make($_POST, $rules,$message);
        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
        }
        $app_id = $request->get('app_id');
        
        $statusArray = array(
           'status' => true,
           'message' => "success",
        );
        $setconfigArr = setconfig::where('app_id',$app_id)
                                    ->get()->toArray();
        foreach ($setconfigArr as $val)
        {
            $statusArray['content'][$val['key']] = $val['value'];
        }
//        $statusArray['content']['code_timer'] = Cache::get('CodeTimer');
//        $statusArray['content']['click_count'] = Cache::get('ClickCount');
//        $statusArray['content']['click_time'] = Cache::get('ClickTime');
        $contactArr = AppAdmob::select('id','admob_title')
	                            //->whereIn('app_id',$app_id)
                                    ->whereRaw("find_in_set($app_id,app_id)")
	                            ->where('status',"active")
	                            ->with(['admob_get' => function($query){
	                                $query->select('admob_id','ad_key','ad_value','ad_type');
	                            }])
	                            ->get()->toArray();
        if(!empty($contactArr[0]['admob_get']))
        {
            $statusArray['content']['admob'] = $contactArr[0]['admob_get'];
        }else{
            $statusArray['content']['admob'] = array();
        }
//         print_r($statusArray);
//         exit;
        
        echo(json_encode($statusArray));
        exit;
    }
    
   public function moreApp(Request $request) {
        $rules = [
            'app_id'  => 'required',
        ];
        $message = [
            'app_id.required'  => "app_id is required.",
        ];
        $validator = Validator::make($_POST, $rules,$message);
        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
        }
        $app_id = $request->get('app_id');
        
        $statusArray = array(
           'status' => true,
           'message' => "success",
        );
        $appsArr = MoreApps::select('id','app_name','app_link','app_icon')
	                            //->whereRaw("find_in_set($app_id,app_id)")
	                            ->where('status',1)
	                            ->get()->toArray();
//        print_r($appsArr);
//        exit;
        if(!empty($appsArr))
        {
            foreach ($appsArr as $key => $value) {

                $statusArray['apps'][$key]['app_name'] = $value['app_name'];
                $statusArray['apps'][$key]['app_link'] = $value['app_link'];
                //$statusArray['apps'][$key]['app_icon'] = url(APP_ICON.$value['id'].DS.$value['app_icon']);
                $statusArray['apps'][$key]['app_icon'] = url("uploads/app_icon/".$value['app_icon']);
            }
        }else{
            $statusArray['content']['apps'] = array();
        }
//         print_r($statusArray);
//         exit;
        
        echo(json_encode($statusArray));
        exit;
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\ResponseMessage;
use App\Models\FuneralImage;
use App\Models\FuneralProfile;
use App\Models\AppUser;
use App\Models\Funeral;
use App\Models\Member;
use App\Models\CMS;
use App\Models\State;
use App\Models\City;
use App\Models\Faqs;
use Validator;
use Input;
use Cache;
use Mail;
use File;
use Hash;

class FuneralController extends Controller
{
    protected $userId;
    public function __construct(Request $request)
	{
    	header("Content-Type: application/json");

        $headers = getallheaders();

        $apiToken = isset($headers['apiToken'])?$headers['apiToken']:'';

        if($apiToken != '' && $apiToken != null){
            if($request->is('api/funeralList')){
                $token = AppUser::where('header_token',$apiToken)->get()->toArray();
                if(count($token) > 0){
                    $this->userId = $token[0]['id'];
                }else{
                    echo json_encode([
                                'status' => false,
                                'error' => 201,
                                'message' => Cache::get('invalid-token'),
                            ]);
                    exit;
                }
            }else if($request->is('api/getFuneral')){
                $token = Funeral::where('header_token',$apiToken)->get()->toArray();
                if(count($token) > 0){
                    $this->userId = $token[0]['id'];
                }else{
                    echo json_encode([
                                'status' => false,
                                'error' => 201,
                                'message' => Cache::get('invalid-token'),
                            ]);
                    exit;
                }
            }else if ($request->is('api/addFuneral')) {
                $token = Funeral::where('header_token',$apiToken)->get()->toArray();
                if(count($token) > 0){
                    $this->userId = $token[0]['id'];
                }else{
                    echo json_encode([
                                'status' => false,
                                'error' => 201,
                                'message' => Cache::get('invalid-token'),
                            ]);
                    exit;
                }
            }else if($request->is('api/editFuneral')){
                $token = Funeral::where('header_token',$apiToken)->get()->toArray();
                if(count($token) > 0){
                    $this->userId = $token[0]['id'];
                }else{
                    echo json_encode([
                           'status'    => false,
                           'error'     => 201,
                           'message'   => Cache::get('invalid-token'),
                       ]);
                   exit;
                }
            }else if($request->is('api/funeralChangePassword')){
                $token = Funeral::where('header_token',$apiToken)->get()->toArray();
                if(count($token) > 0){
                    $this->userId = $token[0]['id'];
                }else{
                    echo json_encode([
                                'status' => false,
                                'error' => 201,
                                'message' => Cache::get('invalid-token'),
                            ]);
                    exit;
                }
            }else if($request->is('api/obituaryList')){
                $token = Funeral::where('header_token',$apiToken)->get()->toArray();
                if(count($token) > 0){
                    $this->userId = $token[0]['id'];
                }else{
                    echo json_encode([
                                'status' => false,
                                'error' => 201,
                                'message' => Cache::get('invalid-token'),
                            ]);
                    exit;
                }
            }else if($request->is('api/faqList')){
                $token = Funeral::where('header_token',$apiToken)->get()->toArray();
                if(count($token) > 0){
                    $this->userId = $token[0]['id'];
                }else{
                    echo json_encode([
                                'status' => false,
                                'error' => 201,
                                'message' => Cache::get('invalid-token'),
                            ]);
                    exit;
                }
            }else{
                    if($apiToken != 'L@titude2018'){
                        echo json_encode([
                               'status'    => false,
                               'error'     => 201,
                               'message'   => Cache::get('invalid-token'),
                           ]);
                        exit;
                    }
                }
        }else{
            if($request->is('api/cms')){
                if($apiToken == '' && $apiToken == null){

                }
            }else{
                echo json_encode([
                                'status' => false,
                                'error' => 401,
                                'message' => Cache::get('token-req'),
                            ]);
                exit;
            }
        }
    }

    /**
    * Funeral List
    * Author : Pooja Makwana 
    * @param Request ''
    * param list :: 
    */

    public function funeralList()
    {
    	$funeral = Funeral::query()->orderBy('id','DESC')->select('id','home_name')->where('home_name','!=',null)->where('approval',1)->where('status',1)->get();
    	if($funeral != '' && count($funeral) > 0){
       		ResponseMessage::success(Cache::get('funeral-list'),$funeral); 	
    	}else{
    		ResponseMessage::error(Cache::get('funeral-not-found'));
    	}
    	
    }

    /**
    * CMS Details
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: slug
    */

    public function cms(Request $request)
    {
        $rules = [
                'slug' => 'required'
            ];

        $message = [
                'slug.required' => 'Slug is required'
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if($validator->fails()){
            echo json_encode([
                            'status' => false,
                            'error' => 401,
                            'message' => $validator->messages()->first()
                    ]);
        }

        if(CMS::where('slug',Input::get('slug'))->first()){
            $cms = CMS::where('slug',Input::get('slug'))->get();
            return view('web.cms',['data' => $cms]);
        }else{
            ResponseMessage::error(Cache::get('slug-not-found'));
        }
    }

    /**
    * Funeral Registration
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: 
    */    

    public function funeralSignup(Request $request)
    {
        $rules = [
                'email' => 'required_without:phone|nullable|string|email|max:50',
                'phone' => 'required_without:email|nullable|string|min:10|max:12',
                'password' => 'required|min:6',
                'confirm_password' => 'required|same:password'
            ];

        $message = [
                'email.required_without:phone' => "Email Or Mobile is Required",
                'email.email' => "Enter Valid Email",
                'phone.required_without:email' => "Email Or Mobile is Required",
                'phone.min' => 'The :attribute must be at least :min',
                'phone.max' => 'The :attribute must be :max',
                "password.required" => "Password is required",
                "confirm_password.required" => "Confirm Password is required",
                "password.min" => "Password should be six characters long",
                "confirm_password.same" => "Confirmation Password should be like password"
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if ($validator->fails()) {
            echo json_encode([
               'status'    => false,
               'error'      => 401,
               'message'   => $validator->messages()->first(),
           ]);
           exit; 
        }

        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $token = substr(str_shuffle(str_repeat($pool, 5)), 0, 30);

        $name = explode('@',Input::get('email'));
        $funeral['person_name'] = $name[0];
        $funeral['email'] = strtolower(Input::get('email'));
        $funeral['password'] = bcrypt(Input::get('password'));
        $funeral['phone'] = Input::get('phone');
        $funeral['token'] = $token;
        $funeral['header_token'] = "L@titude2018";
        $funeral['register_type'] = 0;
        $funeral['user_type'] = Input::get('user_type');
        $funeral['device_type'] = Input::get('device_type');
        $funeral['device_token'] = Input::get('device_token');
        $funeral['updated_at'] = date('Y-m-d H:i:s');

        if(Funeral::where('email',Input::get('email'))->where('is_verified',1)->exists() && Input::get('reg_type') == 0){
            ResponseMessage::error(Cache::get('exists'));
            exit;
        }else if(Input::get('user_token')){ 
            if(Funeral::where('token',Input::get('user_token'))->first()){
                Funeral::where('token',Input::get('user_token'))->update($funeral);
            }else{
                ResponseMessage::error(Cache::get('reg-fail'));
                exit;
            }
        }else if(Funeral::where('email',Input::get('email'))->where('is_verified',0)->exists()){
            Funeral::where('email',Input::get('email'))->update($funeral);
        }else if(Funeral::where('email',Input::get('email'))->exists() && Input::get('reg_type') == 1){
            $f1['register_type'] = 1;
            $f1['updated_at'] = date('Y-m-d H:i:s');

            Funeral::where('email',Input::get('email'))->update($f1);
        }else if(Funeral::where('email',Input::get('email'))->exists() && Input::get('reg_type') == 2){
            $f1['register_type'] = 2;
            $f1['updated_at'] = date('Y-m-d H:i:s');

            Funeral::where('email',Input::get('email'))->update($f1);
        }else{
            $f1 = new Funeral;
            $f1->insert($funeral);
        }

        $tomail = Input::get('email');
        $subject = "Email Verification";
        $body = "Funeral Created Successfully";

        $data = array('message' => $body, "to" => $tomail, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'));

        $res = Mail::send('emails.otpMail',['data' => $data], function($message) use ($data){
            $message->from($data['from'],$data['emailLabel']);
            $message->to($data['to'],'Name');
            $message->subject($data['subject']);
        });

        $f2 = Funeral::where('email',Input::get('email'))->first();

        $data1 = [
                'id' => $f2->id,
                'email' => $f2->email,
                'token' => $f2->token,
                'header_token' => $f2->header_token,
                'register_type' => $f2->register_type,
                'phone' => $f2->phone,
                'user_type' => $f2->user_type
            ];
        ResponseMessage::success(Cache::get('register'),$data1);
    }

    /**
    * Funeral Mail Varification
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: Email=example@gmail.com, OTP
    */

    public function verifyFuneralMail(Request $request)
    {
        $rules = [
                'otp' => 'required'
            ];

        $message = [
                'otp.required' => "OTP Field is Required"
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if($validator->fails()){
            echo json_encode([
                    'status' => false,
                    'error' => 401,
                    'message' => $validator->message()->first()
                ]);
        exit;
        }else{
            $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $token = substr(str_shuffle(str_repeat($pool, 5)), 0, 30);
            $tokenHeader = substr(str_shuffle(str_repeat($pool, 5)), 0, 50);

            if(Funeral::where('token',Input::get('token'))->where('otp',Input::get('otp'))->first()){

                $id = Funeral::where('token',Input::get('token'))->where('otp',Input::get('otp'))->first()->id;

                $verify['is_verified'] = 1;
                $verify['verified_at'] = date('Y-m-d H:i:s');

                if(Funeral::where('token',Input::get('token'))->update($verify)){
                    // flag = 1 -> Verification for mail
                    // flag = 2 -> Verification for Forgot password 
                    if(Input::get('flag') == 1){
                        $token1['token'] = NUll;
                        $token1['header_token'] = $tokenHeader;
                        $token1['updated_at'] = date('Y-m-d H:i:s');

                        if(Funeral::where('id',$id)->update($token1))
                        {
                            ResponseMessage::success(Cache::get('verify'),$token1);
                        }
                    }else if(Input::get('flag') == 2){
                        $token1['token'] = $token;
                        $token1['updated_at'] = date('Y-m-d H:i:s');

                        if(Funeral::where('id',$id)->update($token1)){
                            ResponseMessage::success(Cache::get('verify'),$token1);
                        }

                    }
                }
            }else if(Funeral::where('token',null)->where('otp',Input::get('otp'))->first()){
                ResponseMessage::error(Cache::get('otp-exp'));
            }else{  
                ResponseMessage::error(Cache::get('invalid-otp'));
            }
        }
    }

    /**
    * Funeral Login
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: Email=example@gmail.com OR MobileNo=8888888888,Password=......
    */

    public function funeralSignIn(Request $request)
    {
        $rules = [
                'username' => 'required',
                'password' => 'required'
            ]; 

        $message = [
                'username.required' => "Username Field is Required",
                'password.required' => "Password Field is Required"
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if($validator->fails()){
            echo json_encode([
                    'status' => false,
                    'error' => 401,
                    'message' => $validator->messages()->first()
                ]);
        exit;
        }

        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $token = substr(str_shuffle(str_repeat($pool, 5)), 0, 50);

        // $token1['header_token'] = $token;
        $token1['login_type'] = 0;
        $token1['device_type'] = Input::get('device_type');
        $token1['device_token'] = Input::get('device_token');
        $token1['updated_at'] = date('Y-m-d H:i:s');

        $funeral = Funeral::where('phone',Input::get('username'))->orWhere('email',Input::get('username'))->first();

        if(is_numeric(Input::get('username'))){
            if($funeral){
                if(Funeral::where('phone',Input::get('username'))->where('is_verified',1)->first()){
                    $pwd = $funeral->password;
                    if(Hash::check(Input::get('password'),$pwd)){
                        Funeral::where('phone',Input::get('username'))->update($token1);
                    }else{
                        ResponseMessage::error(Cache::get('invalid-pwd'));
                        exit;
                    }
                }else{
                    ResponseMessage::error(Cache::get('ver-req'));
                    exit;
                }
            }else{
                ResponseMessage::error(Cache::get('mob-exists'));
                exit;
            }
        }else if(!is_numeric(Input::get('username'))){
            if($funeral){
                if(Funeral::where('email',Input::get('username'))->where('is_verified',1)->first()){
                    $pwd = $funeral->password;
                    if(Hash::check(Input::get('password'),$pwd)){
                        Funeral::where('email',Input::get('username'))->update($token1);
                    }else{
                        ResponseMessage::error(Cache::get('invalid-pwd'));
                        exit;
                    }
                }else{
                    ResponseMessage::error(Cache::get('ver-req'));
                    exit;
                }
            }else{
                ResponseMessage::error(Cache::get('mail-exists'));
                exit;
            }
        }
        
        $data = [
            'id' => $funeral->id,
            'email' => $funeral->email,
            'token' => $funeral->token,
            // 'header_token' => $token,
            'header_token' => $funeral->header_token,
            'register_type' => $funeral->register_type,
            'phone' => $funeral->phone,
            'user_type' => $funeral->user_type
        ];
        
        echo json_encode([      
            'status' => true,
            'error' => 200,
            'message' => "Login Successfully",
            'data' => $data
        ]);
    }

    /**
    * Funeral Social Login
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: Facebook Id OR Email Id 
    */

    public function funeralSocialLogin(Request $request)
    {
        if(Input::get('login_type') == 1){
            $rules = [
                'login_type' => 'required',
                'name' => 'required',
                'facebook_id' => 'required'
            ];

            $message = [
                'login_type.required' => 'Login Type is Required',
                'name.required' => "Name is Required",
                'facebook_id.required' => "Facebook Id is Required"
            ];
        }else if(Input::get('login_type') == 2){
            $rules = [
                'login_type' => 'required',
                'name' => 'required',
                'gplus_id' => 'required'
            ];

            $message = [
                'login_type.required' => 'Login Type is Required',
                'name.required' => "Name is Required",
                'gplus_id.required' => "GPlus Id is Required"
            ];
        }

        $validator = Validator::make($request->all(),$rules,$message);

        if ($validator->fails()) {
            echo json_encode([
               'status'    => false,
               'error'      => 401,
               'message'   => $validator->messages()->first(),
           ]);
        }else{
            $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $token = substr(str_shuffle(str_repeat($pool, 5)), 0, 50);

            $login['person_name'] = Input::get('name');
            $login['login_type'] = Input::get('login_type');
            if(Input::get('facebook_id'))
            {
                $login['social_id'] = Input::get('facebook_id');
            }
            if(Input::get('gplus_id'))
            {
                $login['social_id'] = Input::get('gplus_id');
            }
            $login['device_type'] = Input::get('device_type');
            $login['device_token'] = Input::get('device_token');
            $login['dateOfbirth'] = Input::get('dob');
            if(Input::get('email'))
            {
                $login['email'] = Input::get('email');
            }
            $login['phone'] = Input::get('phone');
            $login['header_token'] = $token;
            $login['register_type'] = Input::get('register_type');
            $login['created_at'] = date('Y-m-d H:i:s');
            $login['updated_at'] = date('Y-m-d H:i:s');

            if(Input::get('login_type') == 1){
                if(Funeral::where('social_id',Input::get('facebook_id'))->exists()){
                    Funeral::where('social_id',Input::get('facebook_id'))->update($login);
                }else if(Funeral::where('email',Input::get('email'))->exists()){
                    Funeral::where('email',Input::get('email'))->update($login);
                }else{
                    $login1 = new Funeral;
                
                    $login1->insert($login);
                }
            }
            if(Input::get('login_type') == 2){
                if(Funeral::where('social_id',Input::get('gplus_id'))->exists()){
                    Funeral::where('social_id',Input::get('gplus_id'))->update($login);
                }else if(Funeral::where('email',Input::get('email'))->exists()){
                    Funeral::where('email',Input::get('email'))->update($login);
                }else{
                    $login1 = new Funeral;
                
                    $login1->insert($login);
                }
            }

        if(Input::get('facebook_id'))
        {
          $data = Funeral::query()->select('id','home_name','person_name','email','phone','dateOfbirth','token','header_token','user_type')->where('social_id',Input::get('facebook_id'))->first();

        }
        if(Input::get('gplus_id'))
        {
        $data = Funeral::query()->select('id','home_name','person_name','email','phone','dateOfbirth','token','header_token','user_type')->where('social_id',Input::get('gplus_id'))->first();

        }

            echo json_encode([
                    'status' => true,
                    'error' => 200,
                    'message' => Cache::get('login'),
                    'data' => $data
                ]);
            exit;
        }
    }

    /**
    * Funeral Forgot Password
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: Email Id 
    */

    public function funeralforgotPassword(Request $request)
    {
        $rules = [  
                'email' => 'required|email'
            ]; 

        $message = [
                'email.required' => "Email Id is Required",
                'email.email' => "Enter Valid Email"
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if ($validator->fails()) {
            echo json_encode([
               'status'    => false,
               'error'      => 401,
               'message'   => $validator->messages()->first(),
           ]);
           //exit; 
        }

        if(Funeral::where('email',Input::get('email'))->exists()){
            if(Funeral::where('email',Input::get('email'))->where('register_type',0)->exists()){
                if(Funeral::where('email',Input::get('email'))->where('is_verified',1)->first()){
                    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $token = substr(str_shuffle(str_repeat($pool, 5)), 0, 30);
        
                    $otp['token'] = $token;
                    $otp['is_verified'] = 0;
        
                    if(Funeral::where('email',Input::get('email'))->update($otp)){
                    
                        $tomail = Input::get('email');
                        $subject = 'Forgot Password';
                        $body = "Funeral created successfully";
            
                        $data = array('message' => $body, "to" => $tomail, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'));
            
                        $res =Mail::send('emails.otpMail',['data'=>$data],function($message)use ($data){
                        
                            $message->from($data['from'],$data['emailLabel']);
                            $message->to($data['to'],'Name');
                            $message->subject($data['subject']);
                        });
            
                        ResponseMessage::success(Cache::get('code-sent'),$otp);
                    }
                }else{
                    ResponseMessage::error(Cache::get('ver-mail'));
                }
            }else{
                ResponseMessage::error(Cache::get('not-normal'));
                exit;
            }
        }else{
            ResponseMessage::error(Cache::get('mail-exists'));
        }
    }

    /**
    * Funeral Reset Password
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: Token,Password,Confirm Password 
    */

    public function funeralResetPassword(Request $request)
    {
        $rules = [
                'token' => 'required',
                'password' => 'required|min:6',
                'confirm_password' => 'required|same:password'
            ];

        $message = [ 
                'token.required' => 'Token is Required',
                "password.required" => "Password is required",
                "confirm_password.required" => "Confirm Password is required",
                "password.min" => "Password should be six characters long",
                "confirm_password.same" => "Confirmation Password should be like password"
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if ($validator->fails()) {
            echo json_encode([
               'status'    => false,
               'error'      => 401,
               'message'   => $validator->messages()->first(),
           ]);
           //exit; 
        }else{
            if(Funeral::where('token',Input::get('token'))->where('is_verified',1)->first()){
                $pwd['password'] = bcrypt(Input::get('password'));

                if(Funeral::where('token',Input::get('token'))->update($pwd)){

                    $id = Funeral::where('token',Input::get('token'))->first()->id;

                    $token['token'] = NUll;
                    $token['updated_at'] = date('Y-m-d H:i:s');

                    Funeral::where('id',$id)->update($token);

                    echo json_encode([
                                'status' => true,
                                'error' => 200,
                                'message' => Cache::get('pwd-reset')
                            ]);
                }
            }else{
                ResponseMessage::error(Cache::get('token-not-found'));
            }
        }
    }
    
    /**
    * Funeral Resend OTP
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: emailId 
    */

    public function funeralResendOTP(Request $request)
    {
      $rules = [
              'email' => 'required|email'
          ];

      $message = [
              'email.required' => 'Email Id is Required',
              'email.email' => 'Enter Valid Email'
          ];

      $validator = Validator::make($request->all(),$rules,$message);

      if($validator->fails()){
        echo json_encode([
                    'status' => false,
                    'code' => 401,
                    'message' => $validator->messages()->first()
                ]);
        exit;
      }else{
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $token = substr(str_shuffle(str_repeat($pool, 5)), 0, 30);

        if(Funeral::where('email',Input::get('email'))->exists()){
          $mail['token'] = $token;
          $mail['updated_at'] = date('Y-m-d H:i:s');

          if(Funeral::where('email',Input::get('email'))->update($mail)){
            $tomail = Input::get('email');
            $subject = 'Resend OTP';
            $body = "User New OTP created successfully";

            $data = array('message' => $body, "to" => $tomail, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'));

            $res =Mail::send('emails.otpMail',['data'=>$data],function($message)use ($data){
          
                  $message->from($data['from'],$data['emailLabel']);
                  $message->to($data['to'],'Name');
                  $message->subject($data['subject']);

                  });

            $user = Funeral::where('email',Input::get('email'))->first();

            $data = [
                'id' => $user->id,
                'token' => $user->token,
                'header_token' => $user->header_token,
                'otp' => $user->otp,
              ];

      ResponseMessage::success(Cache::get('otp-create'),$data);   

          }
        }else{
          ResponseMessage::error(Cache::get('mail-not-found'));
        }
      }
    }

    /**
    * Get Funeral Profile
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: 
    */    

    public function getFuneral(Request $request)
    {
        $rules = [
                'fuser_id' => 'required'
            ];

        $message = [
                'fuser_id.required' => "Funeral Id is required"
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if($validator->fails()){
            echo json_encode([
                            'status' => false,
                            'error' => 401,
                            'message' => $validator->messages()->first()
                    ]);
            exit;
        }else{

            $type = Funeral::where('id',Input::get('fuser_id'))->first()->user_type;
            
            if($type == 1){
                $data = Funeral::with(['state' => function($query){
                    $query->select(['id','state']);
                }])->with(['city' => function($query){
                    $query->select(['id','city']);
                }])->with(['profile' => function($query){
                    $query->select(['id','funeral_id','title','description']);
                }])->with(['gallery' => function($query){
                    $query->select(['id','funeral_id','image_video','type']);
                }])->select('id','home_name','person_name','address','state','city','phone','zip','email','website','facebook','twitter','linkedin')->where('id',Input::get('fuser_id'))->get();
            }else if($type == 2){
                $data = 'Community';
            }

            ResponseMessage::success(Cache::get('funeral-profile'),$data);
        }
    }

    /**
    * Add Funeral 
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: 
    */    

    public function createFuneral(Request $request)
    {
        //dd($request->all());
        $rules = [
                'fuser_id'      => 'required',
                'home_name'     => 'required',
                'person_name'   => 'required',
                'profile_image' => 'required',
                'address'       => 'required',
                // 'state'         => 'required',
                // 'city'          => 'required',
                'phone_number'  => 'required|numeric|min:9',
                'zip_code'      => 'required',
                //'email'         => 'required|email|unique:funeral,email,'.Input::get('id'),'id',
                'website'       => 'nullable|url',
                'facebook'      => 'nullable|url',
                'twitter'       => 'nullable|url',
                'linkedin'      => 'nullable|url'
            ];

        $message = [
                'fuser_id.required'      => 'User Id is required',
                'home_name.required'     => 'Funeral home name is required',
                'person_name.required'   => 'Contact person name is required',
                'profile_image.required' => 'Profile image is required',
                'address.required'       => 'Funeral home address is required',
                // 'state.required'         => 'State is required',
                // 'city.required'          => 'City is required',
                'phone_number.required'  => 'Phone Number is required',
                'phone_number.numeric'   => 'Phone should be numeric',
                'phone_number.min'       => 'Minimum 9 digit required',
                'phone_number.max'       => 'Maximum 15 digit required',
                'zip_code.required'      => 'Zip code is required',
                // 'email.required'         => "Email is Required",
                // 'email.email'            => "Invalide Email Address",
                // 'email.unique'           => "Email Address Already Exist",
                'website.url'            => 'Enter Valid URL',
                'facebook.url'           => 'Enter Valid URL',
                'twitter.url'            => 'Enter Valid URL',
                'linkedin.url'           => 'Enter Valid URL'
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if($validator->fails()){
            echo json_encode([
                            'status' => false,
                            'error' => 401,
                            'message' => $validator->messages()->first()
                    ]);
            exit;
        }else{
            $img = Input::file('profile_image');
            $image = Input::hasfile('image');
            $v = Input::get('video');
            $video = explode(',',$v);
            $profile = Input::get('profile');

            $ui = Input::get('update_image');
            $update_image = explode(',', $ui);

            $p = Input::get('profile_delete');
            $delete_profile = explode(',', $p);

            if($img){
                $imageName = time().'.'.$img->getClientOriginalExtension();
                $img->move(public_path('images/funeral-profile'),$imageName);
            }else{
                $imageName = "";
            }

            if($request->hasfile('image')){
                foreach ($request->file('image') as $img) {
                    $filename = $img->getClientOriginalName();
                    $extension = $img->getClientOriginalExtension();
                    $picture[] = date('His').$filename;
                    $img->move(public_path('images/funeral'),date('His').$filename);
                }
            }else{
                $image = "";
            }

            $funeral['home_name'] = Input::get('home_name');
            $funeral['person_name'] = Input::get('person_name');
            $funeral['image'] = $imageName;
            $funeral['address'] = Input::get('address');
            if(Input::get('country')){
                $funeral['country'] = Input::get('country');
            }else{
                $funeral['country'] = 54;
            }
            // $funeral['state'] = Input::get('state');
            // $funeral['city'] = Input::get('city');
            $funeral['area'] = Input::get('area');
            $funeral['phone'] = Input::get('phone_number');
            $funeral['zip'] = Input::get('zip_code');
            if(Input::get('email')){
                $funeral['email'] = Input::get('email');    
            }
            $funeral['website'] = Input::get('website');
            $funeral['facebook'] = Input::get('facebook');
            $funeral['twitter'] = Input::get('twitter');
            $funeral['linkedin'] = Input::get('linkedin');
            $funeral['updated_at'] = date('Y-m-d H:i:s');
        
            if(Input::file('profile_image')){
                $img = Funeral::where('id',Input::get('fuser_id'))->first()->image;
               File::delete(public_path('/images/funeral-profile/') . $img);
            }

            Funeral::where('id',Input::get('fuser_id'))->update($funeral);

            if($p != ''){
                foreach ($delete_profile as $pro_del) {
                    FuneralProfile::where('id',$pro_del)->delete();
                }
            }

            if(Input::get('profile')){
                foreach ($profile as $value) {
                    foreach (json_decode($value) as $val) {
                        if(Input::get('profile_id')){
                            $FuneralProfile = FuneralProfile::find(Input::get('profile_id'));
                        }else{
                            $FuneralProfile = new FuneralProfile;    
                        }
                        $FuneralProfile->funeral_id = Input::get('fuser_id');
                        $FuneralProfile->title = $val->Title;
                        $FuneralProfile->description = $val->Description;
                        $FuneralProfile->save();
                    }
                }
            }

            if($ui != ''){
                foreach ($update_image as $update_img) {
                    $img = FuneralImage::where('id',$update_img)->first()->image_video;
                    File::delete(public_path('/images/funeral/') . $img);
                    FuneralImage::where('id',$update_img)->delete();
                }
            }
            if($request->hasfile('image')){
                foreach ($picture as $value) {
                    $file = new FuneralImage;

                    $file->funeral_id = Input::get('fuser_id');
                    $file->image_video = $value;
                    $file->type = 1;

                    $file->save();
                }
            }
            if($v){
                foreach ($video as $value) {
                    $file = new FuneralImage;

                    $file->funeral_id = Input::get('fuser_id');
                    $file->image_video = $value;
                    $file->type = 2;

                    $file->save();
                }
            }

            //foreach ($file as $value) {
                $image = FuneralImage::query()->select('id','funeral_id','image_video','type')->where('funeral_id',Input::get('fuser_id'))->get();
            //}

            $profile1 = FuneralProfile::query()->select('id','funeral_id','title','description')->where('funeral_id',Input::get('fuser_id'))->get();

            /*$funeral1 = Funeral::with(['state' => function($query){
                $query->select(['id','state']);
            }])->with(['city' => function($query){
                $query->select(['id','city']);
            }])->select('home_name','person_name','address','zip','email','website','facebook','twitter','linkedin','phone','state','city')->where('id',Input::get('fuser_id'))->get();*/
            
            $funeral1 = Funeral::select('home_name','person_name','address','zip','email','website','facebook','twitter','linkedin','phone')->where('id',Input::get('fuser_id'))->get();

            $image1 = $image;
            $profiles = $profile1; 
            $funerals = array_merge(array('funeral' => $funeral1),array('profile' => $profiles),array('image' => $image1));

            if($funerals){
                ResponseMessage::success(Cache::get('add-funeral'),$funerals);
            }else{
                ResponseMessage::error(Cache::get('funeral-not-add'));
            }
        }
    }

    /**
    * Edit Funeral Profile
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: userId,image,name
    */

    public function editFuneral(Request $request)
    {
        $rules = [
            'fuser_id' => 'required',
            'name' => 'required',
        ];

        $message = [
            'fuser_id.required' => 'Funeral Id Required',
            'name.required' => 'Name is Required',
        ];

        $validator = Validator::make($request->all(),$rules,$message);

        if($validator->fails()){
            echo json_encode([
                        'status' => false,
                        'error' => 401,
                        'message' => $validator->messages()->first()
                    ]);
            exit;
        }else{    
            if($request->file('image')){
                $img = Funeral::where('id',Input::get('fuser_id'))->first()->image;
                File::delete(public_path('/images/funeral-profile/') . $img);
                $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
                $isUpload = $request->file('image')->move(public_path('images/funeral-profile'), $imageName);
            }else{
                $imageName = Funeral::where('id',Input::get('fuser_id'))->first()->image;
            }

            $funeral['home_name'] = Input::get('name');
            $funeral['image'] = $imageName;
            $funeral['updated_at'] = date('Y-m-d H:i:s');

            if(Funeral::where('id',Input::get('fuser_id'))->update($funeral)){
              ResponseMessage::success(Cache::get('edit-profile'),$funeral);
            }else{
              ResponseMessage::error(Cache::get('profile-not-update'));
            }
        }
    }

    /**
    * Change Funeral Password
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: fuser_id,old-password,new-password,confirm-password
    */

    public function funeralChangePassword(Request $request)
    {
        $rules = [
                'fuser_id' => 'required',
                'old_password' => 'required|min:6',
                'new_password' => 'required',
                'confirm_password' => 'required|same:new_password'
            ];

        $message = [
                'fuser_id.required' => 'Funeral Id Required',
                'old_password.required' => 'Please Enter Old Password',
                "old_password.min" => "Password should be six characters long",
                'new_password.required' => 'Please Enter New Password',
                'confirm_password.same' => 'Confirmation Password should be like New Password'
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if($validator->fails()){
            echo json_encode([
                        'status' => true,
                        'error' => 401,
                        'message' => $validator->messages()->first()
                    ]);
            exit;
        }else{
            $funeral = Funeral::where('id',Input::get('fuser_id'))->first();
        
            if($funeral->login_type == 1 || $funeral->login_type == 2){
                ResponseMessage::error(Cache::get('pwd-cant-change'));
            }else{
                $pwd = $funeral->password;
                if(Hash::check(Input::get('old_password'),$pwd)){
                     
                    $tomail = $funeral->email;
                    $subject = 'Change Password';
                    $body = "Password Changed successfully";

                    $data = array('message' => $body, "to" => $tomail, "subject" => $subject, "from" => 'info@keeptheirmemoryalive.com', "emailLabel" => 'Change Password','password' => Input::get('new_password'));

                    $res =Mail::send('emails.changePassword',['data'=>$data],function($message)use ($data){
                        
                          $message->from($data['from'],$data['emailLabel']);
                          $message->to($data['to'],'Name');
                          $message->subject($data['subject']);

                          });
                        
                    $change['password'] = bcrypt(Input::get('new_password'));
                    $change['updated_at'] = date('Y-m-d H:i:s');

                        if(Funeral::where('id',Input::get('fuser_id'))->update($change)){
                            echo json_encode([
                                        'status' => true,
                                        'error' => 200,
                                        'message' => Cache::get('pwd-change')
                                    ]);
                            exit;
                        }else{
                            ResponseMessage::error(Cache::get('pwd-change-fail'));
                            exit;
                        }
                        
                }else{
                    ResponseMessage::error(Cache::get('old-pwd-wrong'));
                    exit;
                }
            }
        }
    }

    /**
    * Obituaries List
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: fuser_id
    */   

    public function obituaryList(Request $request)
    {
        $rules = [
                'fuser_id' => 'required',
            ];

        $message = [
                'fuser_id.required' => 'Funeral Id is required'
            ];

        $validator = Validator::make($request->all(),$rules,$message);

        if($validator->fails()){
            echo json_encode([
                            'status' => false,
                            'error' => 401,
                            'message' => $validator->messages()->first()
                     ]);
            exit;
        }

        if(Funeral::where('id',Input::get('fuser_id'))->first()){
            $obituary = Member::with(['gallery' => function($query){
                $query->select(['id','member_id','image_video']);
                $query->where('type',1);
            }])->select('id','funeral_id','name','dateOfbirth','dateOfpass','about')->where('funeral_id',Input::get('fuser_id'))->get();
            if($obituary != '' && count($obituary) > 0){
                ResponseMessage::success(Cache::get('obituaries-list'),$obituary);
            }else{
                ResponseMessage::error(Cache::get('obituary-not-fount'));
            }
        }else{
            ResponseMessage::error(Cache::get('funeral-not-found'));
        }
    }

    public function faqList()
    {
        $faq = Faqs::query()->select('id','title','description')->get();

        if($faq != null){
            ResponseMessage::success(Cache::get('faq'),$faq);
        }else{
            ResponseMessage::error(Cache::get('faq-not-found'));
        }
    }
}
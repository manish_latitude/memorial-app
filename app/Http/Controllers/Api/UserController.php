<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Helper\ResponseMessage;
use App\Models\AppUser;
use App\Models\Setting;
use App\Models\Contact;
use Validator;
use Input;
use Mail;
use Cache;
use File;

class UserController extends Controller
{
  protected $user_id;
	public function __construct(Request $request){
      //header("Content-Type: application/json");
       $headers = getallheaders();
	    	$apiToken = isset($headers['apiToken'])?$headers['apiToken']:'';
      	if($apiToken != ''){
          if ($request->is('api/getProfile')) {
            $token = AppUser::where('header_token',$apiToken)->get()->toArray();
              if(count($token) > 0){
                $this->user_id = $token[0]['id'];
              }else{
                echo json_encode([
                       'status'    => false,
                       'error'     => 201,
                       'message'   => Cache::get('invalid-token'),
                   ]);
               exit;
              }
          }else if($request->is('api/editProfile')){
              $token = AppUser::where('header_token',$apiToken)->get()->toArray();
              if(count($token) > 0){
                $this->user_id = $token[0]['id'];
              }else{
                echo json_encode([
                       'status'    => false,
                       'error'     => 201,
                       'message'   => Cache::get('invalid-token'),
                   ]);
               exit;
              }
          }else if($request->is('api/generalSetting')){
            $token = AppUser::where('header_token',$apiToken)->orWhere('header_token','L@titude2018')->get()->toArray();
              if(count($token) > 0){
                $this->user_id = $token[0]['id'];
              }else{
                echo json_encode([
                       'status'    => false,
                       'error'     => 201,
                       'message'   => Cache::get('invalid-token'),
                   ]);
               exit;
              }
          }else if($request->is('api/contactMail')){
              $token = AppUser::where('header_token',$apiToken)->orWhere('header_token','L@titude2018')->get()->toArray();
              if(count($token) > 0){
                $this->user_id = $token[0]['id'];
              }else{
                echo json_encode([
                       'status'    => false,
                       'error'     => 201,
                       'message'   => Cache::get('invalid-token'),
                   ]);
               exit;
              }
          }else{

           if($apiToken != 'L@titude2018'){
               echo json_encode([
                       'status'    => false,
                       'error'     => 201,
                       'message'   => Cache::get('invalid-token'),
                   ]);
               exit;
           }
         }
      }else{
           if($request->is('api/contactForm')){
                if($apiToken == '' && $apiToken == null){

                }
              }else{
               echo json_encode([
                           'status'    => false,
                           'error'     => 401,
                           'message'   => Cache::get('token-req'),
                       ]);
               exit; 
              }
      }
   }

	/**
    * User Registration
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: Email=example@gmail.com OR MobileNo=8888888888 OR Both,Password=mko21@,Register Type=(0=simple,1=Facebook,2=GPlus)
    */

    public function signUp(Request $request)
    {
    	$rules = [
    			'email' => 'required_without:phone|nullable|string|email|max:50',
                'phone' => 'required_without:email|nullable|string|min:10|max:12',
                'password' => 'required|min:6',
                'confirm_password' => 'required|same:password'
    		];

    	$message = [
    			'email.required_without:phone' => "Email Or Mobile is Required",
    			'email.email' => "Enter Valid Email",
    			'phone.required_without:email' => "Email Or Mobile is Required",
    			'phone.min' => 'The :attribute must be at least :min',
    			'phone.max' => 'The :attribute must be :max',
    			"password.required" => "Password is required",
	            "confirm_password.required" => "Confirm Password is required",
	            "password.min" => "Password should be six characters long",
	            "confirm_password.same" => "Confirmation Password should be like password"
    		];

    	$validator = Validator::make($request->all(),$rules,$message);

    	if ($validator->fails()) {
       		echo json_encode([
               'status'    => false,
               'error'      => 401,
               'message'   => $validator->messages()->first(),
           ]);
           exit; 
   		}else{
   		  $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
       	$token = substr(str_shuffle(str_repeat($pool, 5)), 0, 30);
          
        $name = explode('@',Input::get('email'));
        $user['name'] = $name[0];
       	$user['password'] = bcrypt($request->get('password'));
	    	$user['email'] = strtolower(Input::get('email'));
	    	$user['phone_number'] = Input::get('phone');
	    	$user['token'] = $token;
	    	$user['header_token'] = "L@titude2018";    	
	    	$user['register_type'] = 0;
	    	$user['device_type'] = Input::get('device_type');
	    	$user['device_token'] = Input::get('device_token');
	    	$user['updated_at'] = date('Y-m-d H:i:s');

       		//if(Input::get('email')){
        if(Input::get('user_token')){
            if(AppUser::where('token',Input::get('user_token'))->first()){
              AppUser::where('token',Input::get('user_token'))->update($user);
            }else{
                ResponseMessage::error(Cache::get('reg-fail'));
                exit;
            }
	    	}else if(AppUser::where('email',Input::get('email'))->where('is_verified',1)->exists() && Input::get('reg_type') == 0){
            ResponseMessage::error(Cache::get('exists'));
            exit; 
	    	}else if(AppUser::where('email',Input::get('email'))->where('is_verified',0)->exists()){
	    		AppUser::where('email',Input::get('email'))->update($user);
	    	}else if(AppUser::where('email',Input::get('email'))->exists() && Input::get('reg_type') == 1){
	    		$user1['register_type'] = 1;
	    		$user1['updated_at'] = date('Y-m-d H:i:s');

	    		AppUser::where('email',Input::get('email'))->update($user1);
	    	}else if(AppUser::where('email',Input::get('email'))->exists() && Input::get('reg_type') == 2){
	    		$user1['register_type'] = 2;
	    		$user1['updated_at'] = date('Y-m-d H:i:s');

	    		AppUser::where('email',Input::get('email'))->update($user1);
	    	}else{
	    		$user1 = new AppUser;
	    	    $user1->insert($user);
	    	}
    	  
    		$tomail = Input::get('email');
    		$subject = 'Email Verification';
    		$body = "User created successfully";

    		$data = array('message' => $body, "to" => $tomail, "subject" => $subject, "from" => 'info@keeptheirmemoryalive.com', "emailLabel" => config('constants.labelMail'));

    		$res =Mail::send('emails.otpMail',['data'=>$data],function($message)use ($data){
    			
                  $message->from($data['from'],$data['emailLabel']);
                  $message->to($data['to'],'Name');
                  $message->subject($data['subject']);

                  });

            $user2 = AppUser::where('email',Input::get('email'))->first();

    		$data1 = [
    			    'id' => $user2->id,
    				'email' => $user2->email,
    				'token' => $user2->token,
    				'header_token' => $user2->header_token,
    				'register_type' => $user2->register_type,
    				'phone_number' => $user2->phone,
    				'created_at' => $user2->created_at
    			];

			ResponseMessage::success(Cache::get('register'),$data1);   
    	}
   	}

    /**
    * Mail Varification
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: Email=example@gmail.com, OTP
    */

    public function verifyMail(Request $request)
    {
    	$rules = [
	    		'otp' => 'required'
    		];

    	$message = [
    			'otp.required' => "OTP Field is Required"
    		];

    	$validator = Validator::make($request->all(),$rules,$message);

    	if($validator->fails()){
    		echo json_encode([
    				'status' => false,
    				'error' => 401,
    				'message' => $validator->message()->first()
    			]);
    	exit;
    	}else{
    		$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    	$token = substr(str_shuffle(str_repeat($pool, 5)), 0, 30);
        $tokenHeader = substr(str_shuffle(str_repeat($pool, 5)), 0, 50);

			if(AppUser::where('token',Input::get('token'))->where('otp',Input::get('otp'))->first()){

				$id = AppUser::where('token',Input::get('token'))->where('otp',Input::get('otp'))->first()->id;

				$verify['is_verified'] = 1;
				$verify['verified_at'] = date('Y-m-d H:i:s');

				if(AppUser::where('token',Input::get('token'))->update($verify)){
					// flag = 1 -> Verification for mail
					// flag = 2 -> Verification for Forgot password 
					if(Input::get('flag') == 1){
						$token1['token'] = NUll;
            $token1['header_token'] = $tokenHeader;
						$token1['updated_at'] = date('Y-m-d H:i:s');

						if(AppUser::where('id',$id)->update($token1))
						{
		    				ResponseMessage::success(Cache::get('verify'),$token1);
						}
					}else if(Input::get('flag') == 2){
						$token1['token'] = $token;
						$token1['updated_at'] = date('Y-m-d H:i:s');

						if(AppUser::where('id',$id)->update($token1)){
							ResponseMessage::success(Cache::get('verify'),$token1);
						}

					}
				}
			}else if(AppUser::where('token',null)->where('otp',Input::get('otp'))->first()){
				ResponseMessage::error(Cache::get('otp-exp'));
			}else{
			    ResponseMessage::error(Cache::get('invalid-otp'));
			}
	    }
    }

    /**
    * User Login
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: Email=example@gmail.com OR MobileNo=8888888888,Password=......
    */

    public function signIn(Request $request)
    {
    	$rules = [
    			'username' => 'required',
    			'password' => 'required'
    		]; 

    	$message = [
    			'username.required' => "Username Field is Required",
    			'password.required' => "Password Field is Required"
    		];

    	$validator = Validator::make($request->all(),$rules,$message);

    	if($validator->fails()){
    		echo json_encode([
    				'status' => false,
    				'error' => 401,
    				'message' => $validator->message()->first()
    			]);
    	exit;
    	}

    	$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	$token = substr(str_shuffle(str_repeat($pool, 5)), 0, 50);

    	$token1['header_token'] = $token;
		$token1['login_type'] = 0;
		$token1['device_type'] = Input::get('device_type');
		$token1['device_token'] = Input::get('device_token');
		$token1['updated_at'] = date('Y-m-d H:i:s');

		$user = AppUser::where('phone_number',Input::get('username'))->orWhere('email',Input::get('username'))->first();

    	if(is_numeric(Input::get('username'))){
    		if($user){
    		    if(AppUser::where('phone_number',Input::get('username'))->where('is_verified',1)->first()){
        			$pwd = $user->password;
        			if(Hash::check(Input::get('password'),$pwd)){
        				AppUser::where('phone_number',Input::get('username'))->update($token1);
        			}else{
        				ResponseMessage::error(Cache::get('invalid-pwd'));
        				exit;
        			}
    			}else{
                	ResponseMessage::error(Cache::get('ver-req'));
                	exit;
            	}
    		}else{
    			ResponseMessage::error(Cache::get('mob-exists'));
    			exit;
    		}
    	}else if(!is_numeric(Input::get('username'))){
    		if($user){
    		    if(AppUser::where('email',Input::get('username'))->where('is_verified',1)->first()){
        			$pwd = $user->password;
        			if(Hash::check(Input::get('password'),$pwd)){
        				AppUser::where('email',Input::get('username'))->update($token1);
        			}else{
        				ResponseMessage::error(Cache::get('invalid-pwd'));
        				exit;
        			}
        		}else{
                    ResponseMessage::error(Cache::get('ver-req'));
                    exit;
                }
        	}else{
        		ResponseMessage::error(Cache::get('mail-exists'));
        		exit;
        	}
    	}


      $data = [
              'id' => $user->id,
              'email' => $user->email,
              'token' => $user->token,
              'is_verified' => $user->is_verified,
              'header_token' => $token,
              'register_type' => $user->register_type,
              'phone_number' => $user->phone
          ];

        echo json_encode([      
            'status' => true,
            'error' => 200,
            'message' => "Login Successfully",
            'data' => $data
        ]);
    }

     /**
    * Social Login
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: Facebook Id OR Email Id 
    */

    public function socialLogin(Request $request)
    {
    	if(Input::get('login_type') == 1){
    		$rules = [
    			'login_type' => 'required',
    			'name' => 'required',
				'facebook_id' => 'required'
    		];

	    	$message = [
	    		'login_type.required' => 'Login Type is Required',
	    		'name.required' => "Name is Required",
    			'facebook_id.required' => "Facebook Id is Required"
    		];
    	}else if(Input::get('login_type') == 2){
    		$rules = [
    			'login_type' => 'required',
    			'name' => 'required',
				'gplus_id' => 'required'
    		];

	    	$message = [
	    		'login_type.required' => 'Login Type is Required',
	    		'name.required' => "Name is Required",
	    		'gplus_id.required' => "GPlus Id is Required"
	    	];
    	}

    	$validator = Validator::make($request->all(),$rules,$message);

    	if ($validator->fails()) {
       		echo json_encode([
               'status'    => false,
               'error'      => 401,
               'message'   => $validator->messages()->first(),
           ]);
   		}else{
	   		$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    	$token = substr(str_shuffle(str_repeat($pool, 5)), 0, 50);

	    	$login['name'] = Input::get('name');
    		$login['login_type'] = Input::get('login_type');
    		if(Input::get('facebook_id'))
    		{
    			$login['social_id'] = Input::get('facebook_id');
    		}
    		if(Input::get('gplus_id'))
    		{
    			$login['social_id'] = Input::get('gplus_id');
    		}
    		$login['device_type'] = Input::get('device_type');
    		$login['device_token'] = Input::get('device_token');
    		$login['dateOfbirth'] = Input::get('dob');
    		if(Input::get('email'))
    		{
    			$login['email'] = Input::get('email');
    		}
    		$login['phone_number'] = Input::get('phone');
    		$login['header_token'] = $token;
        if(Input::get('register_type')){
          $login['register_type'] = Input::get('register_type');
        }else{
          $login['register_type'] = Input::get('login_type');
        }
    		$login['created_at'] = date('Y-m-d H:i:s');
    		$login['updated_at'] = date('Y-m-d H:i:s');

    		if(Input::get('login_type') == 1){
    			if(AppUser::where('social_id',Input::get('facebook_id'))->exists()){
    				AppUser::where('social_id',Input::get('facebook_id'))->update($login);
    			}else if(AppUser::where('email',Input::get('email'))->exists()){
    				AppUser::where('email',Input::get('email'))->update($login);
    			}else{
    				$login1 = new AppUser;
	    		
	    			$login1->insert($login);
    			}
    		}
    		if(Input::get('login_type') == 2){
    			if(AppUser::where('social_id',Input::get('gplus_id'))->exists()){
    				AppUser::where('social_id',Input::get('gplus_id'))->update($login);
    			}else if(AppUser::where('email',Input::get('email'))->exists()){
    				AppUser::where('email',Input::get('email'))->update($login);
    			}else{
    				$login1 = new AppUser;
	    		
	    			$login1->insert($login);
    			}
    		}

        if(Input::get('facebook_id'))
        {
          $data = AppUser::query()->select('id','name','email','phone_number','dateOfbirth','token','header_token')->where('social_id',Input::get('facebook_id'))->first();

        }else if(Input::get('gplus_id')){

        $data = AppUser::query()->select('id','name','email','phone_number','dateOfbirth','token','header_token')->where('social_id',Input::get('gplus_id'))->first();
        }

			echo json_encode([
					'status' => true,
					'error' => 200,
					'message' => Cache::get('login'),
					'data' => $data
				]);
			exit;
    	}
    }
    
    /**
    * Forgot Password
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: Email Id 
    */

    public function forgotPassword(Request $request)
    {
    	$rules = [	
    			'email' => 'required|email'
    		]; 

    	$message = [
    			'email.required' => "Email Id is Required",
    			'email.email' => "Enter Valid Email"
    		];

    	$validator = Validator::make($request->all(),$rules,$message);

    	if ($validator->fails()) {
       		echo json_encode([
               'status'    => false,
               'error'      => 401,
               'message'   => $validator->messages()->first(),
           ]);
           //exit; 
   		}

   		if(AppUser::where('email',Input::get('email'))->exists()){
   		    if(AppUser::where('email',Input::get('email'))->where('register_type',0)->exists()){
       		    if(AppUser::where('email',Input::get('email'))->where('is_verified',1)->first()){
           		    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
           			$token = substr(str_shuffle(str_repeat($pool, 5)), 0, 30);
        
           			$otp['token'] = $token;
        
           			if(AppUser::where('email',Input::get('email'))->update($otp)){
           		    
               			$tomail = Input::get('email');
                		$subject = 'Forgot Password';
                		$body = "User created successfully";
            
                		$data = array('message' => $body, "to" => $tomail, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'));
            
                		$res =Mail::send('emails.otpMail',['data'=>$data],function($message)use ($data){
            			
                          	$message->from($data['from'],$data['emailLabel']);
                          	$message->to($data['to'],'Name');
                          	$message->subject($data['subject']);
                        });
            
            			ResponseMessage::success(Cache::get('code-sent'),$otp);
           			}
       		    }else{
       		        ResponseMessage::error(Cache::get('ver-mail'));
       		    }
   		    }else{
   		        ResponseMessage::error(Cache::get('not-normal'));
   		    	exit;
   		    }
   		}else{
   		    ResponseMessage::error(Cache::get('mail-exists'));
   		}
    }
    
    /**
    * Reset Password
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: Token,Password,Confirm Password 
    */

    public function resetPassword(Request $request)
    {
    	$rules = [
    	        'token' => 'required',
    			'password' => 'required|min:6',
                'confirm_password' => 'required|same:password'
    		];

    	$message = [ 
    	        'token.required' => 'Token is Required',
    			"password.required" => "Password is required",
	            "confirm_password.required" => "Confirm Password is required",
	            "password.min" => "Password should be six characters long",
	            "confirm_password.same" => "Confirmation Password should be like password"
    		];

    	$validator = Validator::make($request->all(),$rules,$message);

    	if ($validator->fails()) {
       		echo json_encode([
               'status'    => false,
               'error'      => 401,
               'message'   => $validator->messages()->first(),
           ]);
           //exit; 
   		}else{
	   		if(AppUser::where('token',Input::get('token'))->where('is_verified',1)->first()){
	   			$pwd['password'] = bcrypt(Input::get('password'));

	   			if(AppUser::where('token',Input::get('token'))->update($pwd)){

	   				$id = AppUser::where('token',Input::get('token'))->first()->id;

	   				$token['token'] = NUll;
					$token['updated_at'] = date('Y-m-d H:i:s');

					AppUser::where('id',$id)->update($token);

	   				echo json_encode([
	   							'status' => true,
	   							'error' => 200,
	   							'message' => Cache::get('pwd-reset')
	   						]);
	   			}
	   		}else{
	   			ResponseMessage::error(Cache::get('token-not-found'));
	   		}
   		}
    }
    
    /**
    * Resend OTP
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: emailId 
    */

    public function resendOTP(Request $request)
    {
      $rules = [
              'email' => 'required|email'
          ];

      $message = [
              'email.required' => 'Email Id is Required',
              'email.email' => 'Enter Valid Email'
          ];

      $validator = Validator::make($request->all(),$rules,$message);

      if($validator->fails()){
        echo json_encode([
                    'status' => false,
                    'code' => 401,
                    'message' => $validator->messages()->first()
                ]);
        exit;
      }else{
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $token = substr(str_shuffle(str_repeat($pool, 5)), 0, 30);

        if(AppUser::where('email',Input::get('email'))->exists()){
          $mail['token'] = $token;
          $mail['updated_at'] = date('Y-m-d H:i:s');

          if(AppUser::where('email',Input::get('email'))->update($mail)){
            $tomail = Input::get('email');
            $subject = 'Resend OTP';
            $body = "User New OTP created successfully";

            $data = array('message' => $body, "to" => $tomail, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'));

            $res =Mail::send('emails.otpMail',['data'=>$data],function($message)use ($data){
          
                  $message->from($data['from'],$data['emailLabel']);
                  $message->to($data['to'],'Name');
                  $message->subject($data['subject']);

                  });

            $user = AppUser::where('email',Input::get('email'))->first();

            $data = [
                'id' => $user->id,
                'token' => $user->token,
                'header_token' => $user->header_token,
                'otp' => $user->otp,
              ];

      ResponseMessage::success(Cache::get('otp-create'),$data);   

          }
        }else{
          ResponseMessage::error(Cache::get('mail-not-found'));
        }
      }
    }

    /**
    * Member Data Display
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: memberId
    */

    public function getProfile(Request $request)
    {
      $rules = [
          'user_id' => 'required'
        ];

      $message = [
          'user_id.required' => 'Member Id is Required'
        ];
      $validator = Validator::make($request->all(),$rules,$message);
      if ($validator->fails()) {
          echo json_encode([
                 'status'    => false,
                 'error'      => 401,
                 'message'   => $validator->messages()->first()
             ]);
          exit;
      }else{
          $user_id = Input::get('user_id');
          $user = AppUser::select('id','name','image','login_type','social_id','dateOfbirth','email','phone_number')->where('id',$user_id)->first();
          
      if($user){
          ResponseMessage::success(Cache::get('member-data'),$user);
      }else{
          ResponseMessage::error(Cache::get('member-no-data'));
      }
    }
    }

    /**
    * Edit User Profile
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: userId,image,name
    */

    public function editProfile(Request $request)
    {
      $rules = [
            'user_id' => 'required',
            'name' => 'required',
        ];

      $message = [
            'user_id.required' => 'User Id Required',
            'name.required' => 'Name is Required',
        ];

      $validator = Validator::make($request->all(),$rules,$message);

      if($validator->fails()){
        echo json_encode([
                    'status' => false,
                    'error' => 401,
                    'message' => $validator->messages()->first()
                ]);
        exit;
      }else{

        if($request->file('image')){
          $img = AppUser::where('id',Input::get('user_id'))->first()->image;
          File::delete(public_path('/images/user-profile/') . $img);
          $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
          $isUpload = $request->file('image')->move(public_path('images/user-profile'), $imageName);
        }else{
            $imageName = AppUser::where('id',Input::get('user_id'))->first()->image;
        }

        $user['name'] = Input::get('name');
        $user['image'] = $imageName;
        $user['updated_at'] = date('Y-m-d H:i:s');

        if(AppUser::where('id',Input::get('user_id'))->update($user)){
          ResponseMessage::success(Cache::get('edit-profile'),$user);
        }else{
          ResponseMessage::error(Cache::get('profile-not-update'));
        }

      }
    }

    /**
    * General Setting
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: 
    */

    public function generalSetting()
    {
      $setting = Setting::query()->select('slug','value','description')->where('status',1)->get();
      
      if($setting){
        ResponseMessage::success(Cache::get('data'),$setting);
      }else{
        ResponseMessage::error(Cache::get('no-data'));
      }
    }

    /**
    * Contact Mail
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: name,email,data,description
    */

    public function contactMail(Request $request)
    {
      $rules = [
              'name' => 'required',
              'email' => 'required|email',
              'areaOfInterest' => 'required', 
          ];

      $message = [
              'name.required' => 'Name is required',
              'email.required' => 'Email Id is required',
              'email.email' => 'Enter valid email',
              'email.unique' => 'Email id is already added',
              'areaOfInterest' => 'Please select your area of interest'
          ];

      $validator = Validator::make($request->all(),$rules,$message);

      if($validator->fails()){
        echo json_encode([
                        'status' => false,
                        'error' => 401,
                        'message' => $validator->messages()->first()
                ]);
        exit;
      }

      $tomail = Input::get('email');
      $subject = 'Contact Details';
      $body = "Contact Details";

      $data = array('message' => $body, "to" => $tomail, "subject" => $subject, "from" => 'info@keeptheirmemoryalive.com', 'name' => Input::get('name'),'email' => Input::get('email'),'note' => Input::get('message'),'emailLabel' => Input::get('name'),'area' => Input::get('areaOfInterest'));

      $res =Mail::send('emails.contact',$data,function($message)use ($data){

        $message->from($data['from']);
        $message->to($data['to'],'Name');
        $message->subject($data['subject']);

        $cntct = new Contact;
        $cntct->name = Input::get('name');
        $cntct->email = Input::get('email');
        $cntct->areaOfinterest = Input::get('areaOfInterest');
        $cntct->description = Input::get('message');
        $cntct->save();

        });
      echo json_encode([
                    'status' => true,
                    'error' => 200,
                    'message' => "Email Sent Successfully"
            ]);
      exit;
    }
}

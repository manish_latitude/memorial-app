<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\ResponseMessage;
use App\Models\AppUser;
use App\Models\Funeral;
use App\Models\Member;
use App\Models\MemberImage;
use App\Models\OrderOfService;
use App\Models\Template;
use App\Models\FuneralImage;
use App\Models\TemplateImage;
use App\Models\Community;
use Validator;
use Input;
use File;
use Cache;
use Hash;
use Mail;

class MemberController extends Controller {

    protected $userId;

    public function __construct(Request $request) {
        header("Content-Type: application/json");

        $headers = getallheaders();

        $apiToken = isset($headers['apiToken']) ? $headers['apiToken'] : '';
        if ($apiToken != '' && $apiToken != null) {
            $token = AppUser::where('header_token', $apiToken)->get()->toArray();
            $tokenFuneral = Funeral::where('header_token',$apiToken)->get()->toArray();
            if($request->is('api/communityList')){ 
                if(count($token) > 0){
                    $this->userId = $token[0]['id'];
                }else if(count($tokenFuneral) > 0){
                    $this->userId = $tokenFuneral[0]['id'];
                }else{
                    echo json_encode([
                        'status' => false,
                        'error' => 201,
                        'message' => Cache::get('invalid-token'),
                    ]);
                    exit;       
                }
            }else if($request->is('api/addMember')){
                if(count($token) > 0){
                    $this->userId = $token[0]['id'];
                }else if(count($tokenFuneral) > 0){
                    $this->userId = $tokenFuneral[0]['id'];
                }else{
                    echo json_encode([
                        'status' => false,
                        'error' => 201,
                        'message' => Cache::get('invalid-token'),
                    ]);
                    exit;       
                }
            }else if($request->is('api/getMember')){
                if(count($token) > 0){
                    $this->userId = $token[0]['id'];
                }else if(count($tokenFuneral) > 0){
                    $this->userId = $tokenFuneral[0]['id'];
                }else{
                    echo json_encode([
                        'status' => false,
                        'error' => 201,
                        'message' => Cache::get('invalid-token'),
                    ]);
                    exit;       
                }
            }else if($request->is('api/editMember')){
                if(count($token) > 0){
                    $this->userId = $token[0]['id'];
                }else if(count($tokenFuneral) > 0){
                    $this->userId = $tokenFuneral[0]['id'];
                }else{
                    echo json_encode([
                        'status' => false,
                        'error' => 201,
                        'message' => Cache::get('invalid-token'),
                    ]);
                    exit;       
                }
            }else if($request->is('api/deleteMember')){
                if(count($token) > 0){
                    $this->userId = $token[0]['id'];
                }else if(count($tokenFuneral) > 0){
                    $this->userId = $tokenFuneral[0]['id'];
                }else{
                    echo json_encode([
                        'status' => false,
                        'error' => 201,
                        'message' => Cache::get('invalid-token'),
                    ]);
                    exit;       
                }
            }else{
                if (count($token) > 0) {
                    $this->userId = $token[0]['id'];
                } else {
                    echo json_encode([
                        'status' => false,
                        'error' => 201,
                        'message' => Cache::get('invalid-token'),
                    ]);
                    exit;
                }
            }
        } else {
            if($request->is('api/orderOfService')){
                if($apiToken == '' && $apiToken == null){

                }
            }else if($request->is('api/imageGallery')){
                if($apiToken == '' && $apiToken == null){

                }
            }else{
                echo json_encode([
                    'status' => false,
                    'error' => 401,
                    'message' => Cache::get('token-req'),
                ]);
                exit;
            }
        }
    }

    /**
     * List Of Deceased Member
     * Author : Pooja Makwana 
     * @param Request $request
     * param list :: memberId
     */
    public function memberList(Request $request) {
        $rules = [
            'user_id' => 'required'
        ];

        $message = [
            'user_id.required' => 'User Id is Required'
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $validator->messages()->first()
            ]);
            exit;
        } else {
            $member = Member::with(['gallery' => function($query){
                $query->select(['member_id','image_video']);
                $query->where('type',1);
            }])->select('id','name','dateOfpass')->where('user_id',Input::get('user_id'))->get();

            if ($member != '' && count($member) > 0) {
                ResponseMessage::success(Cache::get('member-list'), $member);
            } else {
                ResponseMessage::error(Cache::get('member-not-found'));
            }
        }
    }

    /**
     * Add Deceased Member
     * Author : Pooja Makwana 
     * @param Request $request
     * param list :: funeralHome, memberName, Date of Birth, Date of Passing, about, family, orderOfservice
     */
    public function createMember(Request $request) {
        $rules = [
            'user_id' => 'required',
            'funeral_home' => 'required',
            'member_name' => 'required',
            'dateOfbirth' => 'required|date',
            'dateOfpass' => 'required|date|after_or_equal:dateOfbirth',
            'country' => 'required',
            //'flag' => 'required'
        ];

        $message = [
            'user_id.required' => 'User Id is Required',
            'funeral_home.required' => 'Please Select Funeral Home Name',
            'member_name.required' => 'Please Enter Member Name',
            'dateOfbirth.required' => 'Date Of Birth is Required',
            'dateOfbirth.date' => 'Select Valid Date',
            'dateOfpass.required' => 'Date Of Passing is Required',
            'dateOfpass.date' => 'Select Valid Date',
            'dateOfpass.after_or_equal' => 'Passing date not before birthdate',
            'country.required' => 'Please Select Country',
            //'flag.required' => 'Flag is Required'
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $validator->messages()->first()
            ]);
            exit;
        } else {
           // $funeral_home = Funeral::where('home_name', Input::get('funeral_home'))->first()->id;

            $images = $request->file('image');
            $order = $request->file('orderOfservice');
            $v = Input::get('video');
            $video = explode(',', $v);
            $temp = Input::get('temp_id');
            $temp_id = explode(',', $temp);

            if ($images) {
                foreach ($images as $img) {
                    $filename = $img->getClientOriginalName();
                    $extension = $img->getClientOriginalExtension();
                    $picture[] = date('His') . $filename;
                    $img->move(public_path('images/member'), date('His') . $filename);
                }
            } else {
                $images = "";
            }

            if ($order) {
                foreach ($order as $img) {
                    $filename = $img->getClientOriginalName();
                    $extension = $img->getClientOriginalExtension();
                    $picturer[] = date('His') . $filename;
                    $img->move(public_path('images/OrderOfService'), date('His') . $filename);
                }
            } else {
                $order = "";
            }

            $member = new Member;

            $member->user_id = Input::get('user_id');
            $member->funeral_id = Input::get('funeral_home');
            if(Input::get('community')){
                $member->community_id = Input::get('community');
            }else if(Input::get('communityText')){
                if(Community::where('community',Input::get('communityText'))->exists()){
                    echo json_encode([
                                'status' => false,
                                'error' => 401,
                                'message' => 'Community Already Exists'
                            ]); 
                    exit;   
                }
                $com = new Community;
                $com->community = Input::get('communityText');
                if($com->save()){
                    $member->community_id = $com->id;
                }
            }
            $member->country_id = Input::get('country');
            $member->name = Input::get('member_name');
            $member->dateOfbirth = date("Y-m-d", strtotime(Input::get('dateOfbirth')));
            $member->dateOfpass = date("Y-m-d", strtotime(Input::get('dateOfpass')));
            $member->about = Input::get('about');
            $member->area = Input::get('area');
            $member->created_at = date('Y-m-d H:i:s');
            $member->save();

            if ($images) {
                foreach ($picture as $value) {
                    $file = new MemberImage;

                    $file->member_id = $member->id;
                    $file->image_video = $value;
                    $file->type = 1;

                    $file->save();
                }
            }
            if ($v) {
                foreach ($video as $value) {
                    $file = new MemberImage;

                    $file->member_id = $member->id;
                    $file->image_video = $value;
                    $file->type = 2;

                    $file->save();
                }
            }
            if ($order) {
                foreach ($picturer as $value) {
                    $file = new OrderOfService;

                    $file->member_id = $member->id;
                    $file->image = $value;

                    $file->save();
                }
            }

            foreach ($temp_id as $value) {
                $val = TemplateImage::query()->select('id','image')->where('id',$value)->where('user_id',Input::get('user_id'))->get();
                foreach($val as $v){
                    $orderObj = new OrderOfService;
                    $orderObj->member_id = $member->id;
                    $orderObj->image = $v->image;
                    if($orderObj->save()){
                        $file = public_path('images/orderTemplate/').$v->image;
                        $newFile = public_path('images/OrderOfService/').$v->image;
                        copy($file,$newFile);
                        File::delete(public_path('/images/orderTemplate/') . $v->image);
                        TemplateImage::where('id',$v->id)->delete();
                    }
                }
            }

            if (($images || $images == '') || ($order || $order == '')) {
                if ($images || $images == '') {
                    $image = MemberImage::query()->select('id', 'image_video', 'type')->where('member_id', $member->id)->get();
                }

                if ($order || $order == '') {
                    $img = OrderOfService::query()->select('id', 'image')->where('member_id', $member->id)->get();
                }

                $image1 = $image;
                $image2 = $img;
                $member1 = $member;
                $members = array_merge(array('member' => $member1), array('image' => $image1), array('OrderOfService' => $image2));

                if ($members) {
                    if(Input::get('funeral_home') != 0){
                        $funeral = Funeral::where('id',Input::get('funeral_home'))->first()->home_name;
                    }else{
                        $funeral = 'none';
                    }
                    if(Input::get('flag') == 1){
                        $email = AppUser::where('id',Input::get('user_id'))->first()->email;
                    }else if(Input::get('flag') == 2){
                        $email = Funeral::where('id',Input::get('user_id'))->first()->email;
                    }
                    $tomail = "athwani.sagar1@gmail.com";
                    $subject = 'Member Information';
                    $body = "Member Added successfully";

                    $data = array('message' => $body, "to" => $tomail, "subject" => $subject, "from" => $email, "emailLabel" => 'Member Mail',"funeral" => $funeral, 'member' => Input::get('member_name'));

                    $res =Mail::send('emails.adminMail',['data'=>$data],function($message)use ($data){
                        
                        $message->from($data['from'],$data['emailLabel']);
                        $message->to($data['to'],'Name');
                        $message->subject($data['subject']);

                    });
                    ResponseMessage::success(Cache::get('add-member'), $members);
                } else {
                    ResponseMessage::error(Cache::get('member-not-add'));
                }
            }
        }
    }

    /**
     * Update Deceased Member
     * Author : Pooja Makwana 
     * @param Request $request
     * param list :: memberId, updateImage, funeralHome, memberName, Date of Birth, Date of Passing, about, family,orderofservice,updateorder
     */
    public function updateMember(Request $request) {
        $rules = [
            'user_id' => 'required',
            'member_id' => 'required',
            'funeral_home' => 'required',
            'member_name' => 'required',
            'dateOfbirth' => 'required|date',
            'dateOfpass' => 'required|date|after_or_equal:dateOfbirth',
            'country' => 'required',
            // 'community' => 'required_without:communityText',
            // 'communityText' => 'required_without:community|unique:community,community'
        ];

        $message = [
            'user_id.required' => 'User Id is Required',
            'member_id.required' => 'Please Select Member',
            'funeral_home.required' => 'Please Select Funeral Home Name',
            'member_name.required' => 'Please Enter Member Name',
            'dateOfbirth.required' => 'Date Of Birth is Required',
            'dateOfbirth.date' => 'Select Valid Date',
            'dateOfpass.required' => 'Date Of Passing is Required',
            'dateOfpass.date' => 'Select Valid Date',
            'dateOfpass.after' => 'Passing date not before birthdate',
            'country.required' => 'Please Select Country',
            // 'community.required_without:communityText' => 'Please Select or Enter Community',
            // 'communityText.required_without:community' => 'Please Select or Enter Community',
            // 'communityText.unique' => 'Community Already Added'
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $validator->messages()->first()
            ]);
            exit;
        } else {
            //$funeral_home = Funeral::where('home_name', Input::get('funeral_home'))->first()->id;

            $images = $request->file('image');
            $order = $request->file('orderOfservice');
            $v = Input::get('video');
            $video = explode(',', $v);

            $ui = Input::get('update_image');
            $update_image = explode(',', $ui);

            $uo = Input::get('update_order');
            $update_order = explode(',', $uo);

            $temp = Input::get('temp_id');
            $temp_id = explode(',', $temp);

            if ($images) {
                foreach ($images as $img) {
                    $filename = $img->getClientOriginalName();
                    $extension = $img->getClientOriginalExtension();
                    $picture[] = date('His') . $filename;
                    $img->move(public_path('images/member'), date('His') . $filename);
                }
            } else {
                $images = "";
            }

            if ($order) {
                foreach ($order as $img) {
                    $filename = $img->getClientOriginalName();
                    $extension = $img->getClientOriginalExtension();
                    $picturer[] = date('His') . $filename;
                    $img->move(public_path('images/OrderOfService'), date('His') . $filename);
                }
            } else {
                $order = "";
            }

            $member['user_id'] = Input::get('user_id');
            // if(Input::get('funeral_home')){
            //     if(Funeral::where('home_name', Input::get('funeral_home'))->where('approval',1)->where('status',1)->first()){    
            //         $member['funeral_id'] = Funeral::where('home_name', Input::get('funeral_home'))->where('approval',1)->where('status',1)->first()->id;
            //     }
            // }else if(Input::get('other')){
            //     $member['funeral_id'] = Input::get('other');
            // }
            $member['funeral_id'] = Input::get('funeral_home');
            if(Input::get('community')){
                $member['community_id'] = Input::get('community');
            }else if(Input::get('communityText')){
                if(Community::where('community',Input::get('communityText'))->exists()){
                    echo json_encode([
                                'status' => false,
                                'error' => 401,
                                'message' => 'Community Already Exists'
                            ]); 
                    exit;   
                }
                $com = new Community;
                $com->community = Input::get('communityText');
                if($com->save()){
                    $member['community_id'] = $com->id;
                }
            }
            $member['country_id'] = Input::get('country');
            $member['name'] = Input::get('member_name');
            $member['dateOfbirth'] = date("Y-m-d", strtotime(Input::get('dateOfbirth')));
            $member['dateOfpass'] = date("Y-m-d", strtotime(Input::get('dateOfpass')));
            $member['about'] = Input::get('about');
            $member['area'] = Input::get('area');
            $member['updated_at'] = date('Y-m-d H:i:s');

            Member::where('id', Input::get('member_id'))->update($member);

            if($ui != ''){
                foreach ($update_image as $update_img) {
                    $img = MemberImage::where('id', $update_img)->first()->image_video;
                    File::delete(public_path('/images/member/') . $img);
                    MemberImage::where('id', $update_img)->delete();
                }
            }
            if ($images) {
                foreach ($picture as $value) {
                    $file = new MemberImage;

                    $file->member_id = Input::get('member_id');
                    $file->image_video = $value;
                    $file->type = 1;

                    $file->save();
                }
            }
            if ($v) {
                foreach ($video as $value) {
                    $file = new MemberImage;

                    $file->member_id = Input::get('member_id');
                    $file->image_video = $value;
                    $file->type = 2;

                    $file->save();
                }
            }

            if($uo){
                foreach ($update_order as $update_img) {
                    $img = OrderOfService::where('id', $update_img)->first()->image;
                    File::delete(public_path('/images/OrderOfService/') . $img);
                    OrderOfService::where('id', $update_img)->delete();
                }
            }
            if ($order) {
                foreach ($picturer as $value) {
                    $file = new OrderOfService;

                    $file->member_id = Input::get('member_id');
                    $file->image = $value;

                    $file->save();
                }
            }

            foreach ($temp_id as $value) {
                $val = TemplateImage::query()->select('id','image')->where('id',$value)->where('user_id',Input::get('user_id'))->get();
                foreach($val as $v){
                    $orderObj = new OrderOfService;
                    $orderObj->member_id = Input::get('member_id');
                    $orderObj->image = $v->image;
                    if($orderObj->save()){
                        $file = public_path('images/orderTemplate/').$v->image;
                        $newFile = public_path('images/OrderOfService/').$v->image;
                        copy($file,$newFile);
                        File::delete(public_path('/images/orderTemplate/') . $v->image);
                        TemplateImage::where('id',$v->id)->delete();
                    }
                }
            }

            if (($images || $images == '') || ($order || $order == '')) {
                if ($images || $images == '') {
                $image = MemberImage::query()->select('id', 'image_video', 'type')->where('member_id', Input::get('member_id'))->get();
              }

            if ($order || $order == '') {
                $img = OrderOfService::query()->select('id', 'image')->where('member_id', Input::get('member_id'))->get();
            }

        	echo json_encode([
        					'status' => true,
        					'error' => 200,
        					'message' => Cache::get('edit-member')
        			]);
           }
        }
    }

    /**
     * Delete Deceased Member
     * Author : Pooja Makwana 
     * @param Request $request
     * param list :: memberId
     */
    public function deleteMember(Request $request) {
        $rules = [
            'user_id' => 'required',
            'member_id' => 'required'
        ];

        $message = [
            'user_id.required' => 'User Id is Required',
            'member_id.required' => 'Please Select Member'
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $validator->messages()->first()
            ]);
            exit;
        } else {
            $delete = Member::where('id', Input::get('member_id'))->where('user_id', Input::get('user_id'))->delete();

            if ($delete) {
                if (MemberImage::where('member_id', Input::get('member_id'))->exists()) {
                    MemberImage::where('member_id', Input::get('member_id'))->delete();
                }

                if (OrderOfService::where('member_id', Input::get('member_id'))->exists()) {
                    OrderOfService::where('member_id', Input::get('member_id'))->delete();
                }

                echo json_encode([
                    'status' => true,
                    'error' => 200,
                    'message' => Cache::get('delete-member')
                ]);
                exit;
            } else {
                ResponseMessage::error(Cache::get('member-not-delete'));
            }
        }
    }

    /**
     * Member Data Display
     * Author : Pooja Makwana 
     * @param Request $request
     * param list :: memberId
     */
    public function getMember(Request $request) {
        $rules = [
            'member_id' => 'required'
        ];

        $message = [
            'member_id.required' => 'Member Id is Required'
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $validator->messages()->first()
            ]);
            exit;
        } else {
            if(Member::where('id',Input::get('member_id'))->first()){
		        $member = Member::with(['funeral'=>function($query){
		          $query->select(['id','home_name']);
		        }])->with(['country' => function($query){
		        	$query->select(['id','country']);
		        }])->with(['state' => function($query){
		        	$query->select(['id','state']);
		        }])->with(['city' => function($query){
		        	$query->select(['id','city']);
		        }])->with(['community' => function($query){
                    $query->select(['id','community']);
                }])->with(['gallery'=>function($query){
		          $query->select(['id','member_id','image_video','type']);
		        }])->with(['orderOfservice'=>function($query){
		          $query->select(['id','member_id','image']);
		        }])->select('id','name','dateOfbirth','dateOfpass','about','funeral_id','country_id','state_id','city_id','community_id')->where('id',Input::get('member_id'))->first();
                ResponseMessage::success(Cache::get('member-data'), $member);
            } else {
                ResponseMessage::error(Cache::get('member-no-data'));
            }
        }
    }

    /**
     * Change Password
     * Author : Pooja Makwana 
     * @param Request $request
     * param list :: user_id,old-password,new-password,confirm-password
     */
    public function changePassword(Request $request) {
        $rules = [
            'user_id' => 'required',
            'old_password' => 'required|min:6',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password'
        ];

        $message = [
            'user_id.required' => 'User Id Required',
            'old_password.required' => 'Please Enter Old Password',
            "old_password.min" => "Password should be six characters long",
            'new_password.required' => 'Please Enter New Password',
            'confirm_password.same' => 'Confirmation Password should be like New Password'
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                'status' => true,
                'error' => 401,
                'message' => $validator->messages()->first()
            ]);
            exit;
        } else {
            $user = AppUser::where('id', Input::get('user_id'))->first();
            if ($user->login_type == 1 || $user->login_type == 2) {
                ResponseMessage::error(Cache::get('pwd-cant-change'));
            } else {
                $pwd = $user->password;
                if (Hash::check(Input::get('old_password'), $pwd)) {

                    $tomail = $user->email;
                    $subject = 'Change Password';
                    $body = "Password Changed successfully";

                    $data = array('message' => $body, "to" => $tomail, "subject" => $subject, "from" => 'info@keeptheirmemoryalive.com', "emailLabel" => 'Change Password','password' => Input::get('new_password'));

                    $res =Mail::send('emails.changePassword',['data'=>$data],function($message)use ($data){
                        
                          $message->from($data['from'],$data['emailLabel']);
                          $message->to($data['to'],'Name');
                          $message->subject($data['subject']);

                          });

                    $change['password'] = bcrypt(Input::get('new_password'));
                    $change['updated_at'] = date('Y-m-d H:i:s');

                    if (AppUser::where('id', Input::get('user_id'))->update($change)) {
                        echo json_encode([
                            'status' => true,
                            'error' => 200,
                            'message' => Cache::get('pwd-change')
                        ]);
                        exit;
                    } else {
                        ResponseMessage::error(Cache::get('pwd-change-fail'));
                        exit;
                    }
                } else {
                    ResponseMessage::error(Cache::get('old-pwd-wrong'));
                    exit;
                }
            }
        }
    }

    /**
    * Webview Image Gallery
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: member_id | funeral_id
    */

    public function gallery(Request $request)
    {
        if(Input::get('member_id')){
            if(Member::find(Input::get('member_id'))){
                if(MemberImage::where('member_id',Input::get('member_id'))->exists()){
                    $img[] = MemberImage::where('member_id',Input::get('member_id'))->get();
                    $data['mimg'] = $img;
                    $data['user'] = "member";
                    return view('web.gallery',$data);
                }else{
                    return view('web.notFound');
                }
            }else{
                ResponseMessage::error(Cache::get('member-not-found'));
            }
        }
        if(Input::get('funeral_id')){
            if(Funeral::find(Input::get('funeral_id'))){
                if(FuneralImage::where('funeral_id',Input::get('funeral_id'))->exists()){
                    $img[] = FuneralImage::where('funeral_id',Input::get('funeral_id'))->get();
                    $data['mimg'] = $img;
                    $data['user'] = "funeral";
                    return view('web.gallery',$data);
                }else{
                    return view('web.notFound');
                }
            }else{
                ResponseMessage::error(Cache::get('funeral-not-found'));
            }
        }
    }

    /**
    * Order Of Service
    * Author : Pooja Makwana 
    * @param Request $request
    * param list :: member_id
    */    

    public function orderOfService(Request $request)
    {
        if(AppUser::find(Input::get('user_id'))){
            $data['template'] = Template::get();
            $data['user'] = AppUser::where('id',$request->user_id)->get();
            return view('web.orderOfService',$data);
        }else{
            ResponseMessage::error(Cache::get('member-not-found'));
        }
    }

    /**
    * Community List
    * Author : Pooja Makwana
    * @param Request $request
    * param list :: 
    */

    public function communityList()
    {
        $community = Community::query('id','community')->where('status',1)->get();

        if($community != '' && count($community) > 0){
            ResponseMessage::success(Cache::get('community-list'),$community);
        }else{
            ResponseMessage::error(Cache::get('community-not-found'));
        }
    }
}

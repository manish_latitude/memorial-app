<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use Redirect;
use Validator;
use Input;
use Mail;
use App\Http\Requests\ReCaptchataTestFormRequest;
/**
* 
*/
class ContactController extends Controller
{
	//https://m.dotdev.co/google-recaptcha-integration-with-laravel-ad0f30b52d7d
	//https://codeburst.io/simple-integration-google-recaptcha-v2-with-laravel-in-details-2db49d1d8e60
	public function contactForm()
	{
		return view('web.contact');
	}

	public function contact(ReCaptchataTestFormRequest $request)
	{
		$rules = [
				'nm' => 'required',
				'email' => 'required|email',
				'chk' => 'required',
				'g-recaptcha-response' => 'required|recaptcha'
			] ;

		$message = [
				'nm.required' => 'Name is required',
				'email.required' => 'Email id is required',
				'email.email' => 'Enter valid email',
				'chk.required' => 'Select Area of Interest',
				'g-recaptcha-response.required' => 'Please ensure that you are a human!'
			];

		$validator = Validator::make($request->all(),$rules,$message);

		if($validator->fails()){
			return redirect()->back()->withErrors($validator)->withInput();
		}else{
			$tomail = 'developer@latitudetechnolabs.com';
	      	$subject = 'Contact Details';
	      	$body = "Contact Details";

	      	$data = array('message' => $body, "to" => $tomail, "subject" => $subject, "from" => 'info@keeptheirmemoryalive.com', 'name' => 'Admin','email' => Input::get('email'),'note' => Input::get('message'),'emailLabel' => Input::get('name'),'area' => implode(',', Input::get('chk')));

	      	$res =Mail::send('emails.contact',$data,function($message)use ($data){

	        	$message->from($data['from']);
	        	$message->to($data['to'],'Name');
	        	$message->subject($data['subject']);

	        	$cntct['name'] = Input::get('nm');
				$cntct['email'] = Input::get('email');
				//foreach ($request->chk as $value) {
					$cntct['areaOfinterest'] = implode(',', Input::get('chk'));
				//}
				$cntct['description'] = Input::get('msg');
				$cntct['updated_at'] = date('Y-m-d H:i:s');

	        	if(Contact::where('email',Input::get('email'))->exists()){
				$update = Contact::where('email',Input::get('email'))->update($cntct);
				if($update) return Redirect::back()->with('success','Data Update Successfully'); else return Redirect::back()->with('error','Data Updation Fail');
				}else{
					$contact = new Contact;
					$contact = insert($cntct);
					if($contact) return Redirect::back()->with('success','Data Created Successfully'); else return Redirect::back()->with('error','Data Creation Fail');
				}

	        });
	       return Redirect::back()->with('success','Mail Sent Successfully');
	      // echo json_encode([
	      //               'status' => true,
	      //               'error' => 200,
	      //               'message' => "Email Sent Successfully"
	      //       ]);
	      // exit;
		}
	}
}
?>
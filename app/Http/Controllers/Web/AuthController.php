<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\AuthenticateUser;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\Tests\Session\Flash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use App\Models\Country;
use App\Models\User;
use App\Models\Board;
use App\Models\Medium;

use Auth;
use DB;
use Validator;
use Redirect;
use View;
use App\Models\Student;
use Hash;

// use App\Model\AdminRole;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    /* public function __construct() {
      $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
      } */

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    //  public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    protected function validator(array $data) {
        return Validator::make($data, [
                    // 'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|min:6|confirmed',
        ]);
    }

    public function __construct() {
//         $this->middleware('guest:admin', ['except' => 'logout']);
    }

    //return admin login view page
    public function index() {
        return View::make('Login.login');
    }

    //admin login
    public function loginAuth() {

        // echo bcrypt('admin');exit();

        $input = Input::all();
        $validate = Validator::make(Input::all(), [
                    'email' => 'required|max:32',
                    'password' => 'required|min:3',
                        ], [
                    'required' => ': fireld is required.',
                    'email' => 'Enter valid email',
                    'min' => [
                        'numeric' => 'The :attribute must be at least :min.',
                    ],
                        ]
        );

        $auth = Auth::guard('web')->attempt(
                array(
            'email' => Input::get('email'),
            'password' => Input::get('password'),
                ), true);

        if ($auth) {
            if(Auth::user()->institute_type){
              if(Auth::user()->page_trk == '1')
                {
                return Redirect::to('/student-profile');
                }
               elseif(Auth::user()->page_trk == '2'){
                return Redirect::to('/payment');
               }elseif(Auth::user()->page_trk === '3'){
                return Redirect::to('/');
               }elseif(Auth::user()->page_trk === '4'){
                return Redirect::to('/');
               }elseif(Auth::user()->page_trk === '5'){
                return Redirect::to('/');
               }elseif(Auth::user()->page_trk === '6'){
                return Redirect::to('/');
               }
            }else{
              return Redirect::to('/student-profile');
            }
            
        } else {
            return Redirect::to('/')->with('error', 'Invalid Email / Username or Password.')->with('email', Input::get('email'));
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function studentCreate() { 
        $data = Input::all();
       
        // $validate = Validator::make(Input::all(), [
        //             'email' => 'required||unique:student,email',
        //             'password' => 'required|min:5',
        //                 ], ['required' => ': fireld is required.',
        //             'email' => 'Enter valid email',
        //             'min' => ['numeric' => 'The :attribute must be at least :min.'],
        //                 ]
        // ); 
        // if ($validate->fails()) {  
        //     return Redirect::to('/')->withErrors($validate);
        // }  

        $AppUser = new Student;
        $AppUser->email = $data['email'];
        $AppUser->fname = $data['fname'];
        $AppUser->lname = $data['lname'];
        $AppUser->password = Hash::make($data['password']);
        //echo $data['password']; exit;
        $userdata = array(
            'password'  => Input::get('password'),
            'email'     => $data['email']
        );
        
        if ($AppUser->save()) {
            
            if (Auth::attempt($userdata,true)) { 
                // print_r(Auth::user());
                return Redirect::to('/student-profile')->with('success', 'successfully login');
                // dd(Auth::user()->institute_type);
            } else {
               return Redirect::to('/')->with('error', 'Invalid Email / Username or Password.')->with('email', Input::get('email'));
            }
        } else {
        
            return Redirect::to('/')->with('error', 'Invalid Email / Username or Password.')->with('email', Input::get('email'));
        }
          exit;

    }
    protected function studentEdit(Request $data) { 
            // dd(Auth::user());
            if(Auth::user()->page_trk == '1')
                {
                    if($data->individual_board && $data->individual_medium){
                      $board = $data->input('individual_board');
                      $medium = $data->input('individual_medium');
                      $institute_id = $data->input('institute_id');
                    }
                    elseif($data->other_board && $data->other_medium){
                       $board = $data->input('other_board');
                       $medium = $data->input('other_medium');
                       $institute_id = DB::table('user')->insertGetId(['first_name' => $data->i_name,
                                                    'address1' => $data->i_address1,
                                                    'address2' => $data->i_address2,
                                                    'city' => $data->i_city,
                                                    'state' => $data->i_state,
                                                    'country' => $data->i_country,
                                                    'phone_number' => $data->i_contact_no,
                                                    'website' => $data->i_website,
                                                    'board' => $data->other_board,
                                                    'medium' => $data->other_medium,
                                                    'created_at' => date('Y-m-d H:i:s'),
                                                  ]);
                       $institute_id = $institute_id;
                    }
                    elseif($data->institute_id){
                        $board = '';
                        $medium = '';
                        $institute_id = $data->input('institute_id');
                    }
                    
                     // dd($data->institute_type);
                   DB::table('student')->where('id',Auth::user()->id)->update(["fname" => $data->fname,
                                             'mname' => $data->mname,
                                             'lname' => $data->lname,
                                             'page_trk' => '2',
                                             'institute_id' => $institute_id,
                                             'institute_type' => $data->institute_type,
                                             'phone_number' => $data->phone_number,
                                             'email' => $data->email,
                                             'updated_at' => $data->updated_at,
                                             'dob' => $data->dob,
                                             'gender' => $data->gender,
                                             'address1' => $data->address1,
                                             'address2' => $data->address2,
                                             'city' => $data->city,
                                             'country' => $data->country,
                                             'state' => $data->state,
                                             'pincode' => $data->pincode,
                                             'updated_by' => Auth::user()->id,
                                             'updated_at' => date('Y-m-d H:i:s'),
                                             'updated_ip' => \Request::ip(),
                                             'std' => $data->std,
                                             'board' => $board,
                                             'medium' => $medium,
                                           ]);
                   if(Auth::user()->page_trk == 2)
                    {
                        echo "string";
                        // Redirect::to('/profile');
                   }else{
                    echo "sdf";
                   // return Redirect::to('/payment');
                   }
               }
               elseif(Auth::user()->page_trk == 2){
                return Redirect::to('/payment');
               }elseif(Auth::user()->page_trk === '3'){
                return Redirect::to('/');
               }elseif(Auth::user()->page_trk === '4'){
                return Redirect::to('/');
               }elseif(Auth::user()->page_trk === '5'){
                return Redirect::to('/');
               }elseif(Auth::user()->page_trk === '6'){
                return Redirect::to('/');
               }else{
                return Redirect::to('/');
               }
       
    }

    public function InstituteSlecte(){
        echo '<div class="form-group form-row mb-0 text-left">
                                            <div class="col-4"><strong>Address:</strong></div>
                                            <div class="col-8">
                                                <p>123, lorem ipsum, dolor sit amet, consectetur - 123456.</p>
                                            </div>
                                        </div>
                                        <div class="form-group form-row mb-0 text-left">
                                            <div class="col-4"><strong>City:</strong></div>
                                            <div class="col-8">
                                                <p>Gandhinagar</p>
                                            </div>
                                        </div>
                                        <div class="form-group form-row mb-0 text-left">
                                            <div class="col-4"><strong>State:</strong></div>
                                            <div class="col-8">
                                                <p>Gujarat</p>
                                            </div>
                                        </div>
                                        <div class="form-group form-row mb-0 text-left">
                                            <div class="col-4"><strong>Country:</strong></div>
                                            <div class="col-8">
                                                <p>India</p>
                                            </div>
                                        </div>
                                        <div class="form-group form-row mb-0 text-left">
                                            <div class="col-4"><strong>Mobile Number:</strong></div>
                                            <div class="col-8">
                                                <p>1234567890</p>
                                            </div>
                                        </div>
                                        <div class="form-group form-row mb-0 text-left">
                                            <div class="col-4"><strong>Website:</strong></div>
                                            <div class="col-8">
                                                <p>www.unityinfoway.com</p>
                                            </div>
                                        </div>';
    }
    public function profileUpdate() { 
        if(Auth::user()){
         $boardArr = Board::select('board_name', 'board_id')->orderBy('board_id')->get(); 
         $mediumArr = Medium::select('name', 'id')->orderBy('id')->get(); 
         $userList = Student::find(Auth::user()->id);
         $countryArr = Country::pluck('country', 'id')->toArray();
         $instituteArr = User::where('user_role',2)->get();
        // $stateArr = Country::pluck('state', 'id')->toArray();
         // dd($boardArr,$mediumArr);
        return View::make('web.home.student-profile',['data'=>$userList,'country'=>$countryArr,'institute'=>$instituteArr,'board' => $boardArr,'newboard' => $boardArr,'medium' => $mediumArr,'newmedium' => $mediumArr]);
        }
        else{
            return Redirect::to('/');
        }
    }
     
    protected function create(array $data) {

        return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
        ]);
    }

    public function getLogout() {
        //Auth::logout();
        Auth::guard('admin')->logout();
        Session::flush();
        return redirect('admin')->with('success', 'You are logged out.');
    }

    public function studentLogout() {
        Auth::guard('web')->logout();
        Session::flush();
        return redirect('/')->with('success', 'You are logged out.');
    }

    public function adminLogout(Request $request) {
        Auth::logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->guest(route('login'));
    }

}

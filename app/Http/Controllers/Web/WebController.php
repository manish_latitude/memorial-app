<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\CMS;
use App\Models\UserRequest;
use App\Models\Notification;
use App\Models\Setting;
use App\Models\Student;
use App\Models\Country;
use App\Models\Board;
use App\Models\Medium;
use App\Models\TempImage;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use DB;



class WebController extends Controller
{
   	public function __construct()
    {
        // echo "HERE";die;
        // $this->middleware('guest')->except('logout');
    }

    //return admin login view page
    public function index() {
        // echo "HERE";die;
        return view('web.home.home');
    }
    public function about() {
        $pagesData = CMS::where('title','About Us')->get();
        return view('web.staticpage.about',['data'=>$pagesData]);
    }
    public function contact() {
      // echo "HERE";die;
        return view('web.staticpage.contact');
    }
    public function ContactInsert(Request $cotact_data) {
       $this->validate($cotact_data, [
            'name' => 'required',
            'mobile_no' => 'required|max:10|min:10',
            'email' => 'required|email',
            'note' => 'required',
        ],[
            'name.required' => 'The Name field is required.',
            'mobile_no.required' => 'The Mobile Number field is required.',
            'email.required' => 'The E-mail field is required.',
            'note.required' => 'The Note field is required.',          
        ]);
      $data  = array(
                    'name' => $cotact_data->name, 
                    'mobile_no' => $cotact_data->mobile_no, 
                    'email' => $cotact_data->email, 
                    'note' => $cotact_data->note, 
                    'added_ip' => \Request::ip(),
                    'edited_ip' => \Request::ip(),
                    'created_by' => $cotact_data->name, 
                    'updated_by' => $cotact_data->name, 
                );   
       \Mail::send('emails.contact', $data, function($message) use($data){
        $message->from('manish@unityinfoway.in','MIB');
        $message->to($data['email'], $data['name'])->subject('Sunject');
    });
      DB::table('contact')->insert($data);
     return back()->with('message','Thanks for your Inquiry');
    }               

    public function feedback() {
      
        return view('web.staticpage.feedback');
    }
    public function FeedbackInsert(Request $feedback_data) {
      $this->validate($feedback_data, [
            'name' => 'required',
            'mobile_no' => 'required|max:10|min:10',
            'email' => 'required|email',
            'note' => 'required',
        ],[
            'name.required' => 'The Name field is required.',
            'mobile_no.required' => 'The Mobile Number field is required.',
            'email.required' => 'The E-mail field is required.',
            'note.required' => 'The Note field is required.',          
        ]);
      $data  = array(
                    'name' => $feedback_data->name, 
                    'mobile_no' => $feedback_data->mobile_no, 
                    'email' => $feedback_data->email, 
                    'note' => $feedback_data->note,  
                    'added_ip' => \Request::ip(),
                    'edited_ip' => \Request::ip(),
                    'created_by' => $feedback_data->name, 
                    'updated_by' => $feedback_data->name, 
                );   
       \Mail::send('emails.feedback', $data, function($message) use($data){
        $message->from('manish@unityinfoway.in','GUPTA MIB');
        $message->to($data['email'], $data['name'])->subject('Feedback');
    });
      DB::table('feedback')->insert($data);
     return back()->with('message','Thanks for your Feedback');
    }
    public function privacypolicy() {
      // echo "HERE";die;
        $pagesData = CMS::where('title','Privacy Policy')->get();
        return view('web.staticpage.privacy-policy',['data'=>$pagesData]);
    }
    public function termsCondition(){
        $pagesData = CMS::where('title','Terms & Condition')->where('status','1')->get();
        return view('web.staticpage.terms',['data'=>$pagesData]);
    }

    public function expireRequest(Request $request){

        $requestData = UserRequest::orderBy('id','DESC')->get();

        foreach ($requestData as $key => $rdata) {
            $checkService = $rdata->is_service;
            if($checkService == 0){
                $getCreatedDate = $rdata->created_at->format('Y-m-d H:i:s');
                $expire = date("Y-m-d H:i:s");
                $difftime = strtotime($getCreatedDate) - strtotime($expire);
                if(abs($difftime) >= 300){
                    // if($rdata->is_approve == 0){
                    $rdata->is_approve = 3;
                    $rdata->update();
                    $getRequestId = $rdata->id;
                    $notification = Notification::where('type_id',$getRequestId)->first();
                        if(count($notification) > 0){
                            $notification->type = 'request_expired';
                            $notification->save();
                        }
                 // }
                  
                }
            }
        }
        echo json_encode([
                    'status'    => true,
                    'error'   => 200,
                    'message'   => 'Request is expired!!!'
                ]);
            exit;
    }

   
    public function ProfileView(Request $request){ 
         if(Auth::user()){
            
            $institute  = user::find($request->id); 
          //  echo  $cdata = Country::find($institute->country);
            $appdata = Board::find($institute->board);
            $appdata1 = Medium::find($institute->medium);
             $countrydata = Country::find(51);
           // $cdata = Country::where('id',$institute->country)->get();
//print_r($cdata); exit;
           $data['address1'] = $institute->address1;
           $data['city'] = $institute->city;
           $data['state'] = $institute->state;
           $data['country'] = $institute->country;
           $data['phone_number'] = $institute->phone_number;
           $data['website'] = $institute->website;
           $data['board'] = $appdata->board_name;
           $data['medium'] = $appdata1->name;
          // print_r($request);
            echo "<div class='user-profile text-left'>
                            <div class='user-detail px-2 px-md-5'>
                                <div class='row mb-2'>
                                    <div class='col-4 col-md-3'><strong>Address:</strong></div>
                                        <div class='col-8 col-md-9'>
                                            <p>{$data['address1']}</p>
                                        </div>
                                </div>
                                <div class='row mb-2'>
                                    <div class='col-4 col-md-3'><strong>City:</strong></div>
                                        <div class='col-8 col-md-9'>
                                            <p>{$data['city']}</p>
                                        </div>
                                </div>
                                <div class='row mb-2'>
                                    <div class='col-4 col-md-3'><strong>State:</strong></div>
                                        <div class='col-8 col-md-9'>
                                            <p>{$data['state']}</p>
                                        </div>
                                </div>
                                <div class='row mb-2'>
                                    <div class='col-4 col-md-3'><strong>Country:</strong></div>
                                        <div class='col-8 col-md-9'>
                                            <p>{$data['country']}</p>
                                        </div>
                                </div>
                                <div class='row mb-2'>
                                    <div class='col-4 col-md-3'><strong>Contact Number:</strong></div>
                                        <div class='col-8 col-md-9'>
                                            <p>{$data['phone_number']}</p>
                                        </div>
                                </div>
                                <div class='row mb-2'>
                                    <div class='col-4 col-md-3'><strong>Website:</strong></div>
                                        <div class='col-8 col-md-9'>
                                            <p>{$data['website']}</p>
                                        </div>
                                </div>
                                <div class='row mb-2'>
                                    <div class='col-4 col-md-3'><strong>Board:</strong></div>
                                        <div class='col-8 col-md-9'>
                                            <p>{$data['board']}</p>
                                        </div>
                                </div>
                                <div class='row mb-2'>
                                    <div class='col-4 col-md-3'><strong>Medium:</strong></div>
                                        <div class='col-8 col-md-9'>
                                            <p>{$data['medium']}</p>
                                        </div>
                                </div>
                            </div>
                        </div>";
            //return view('web.home.user-profile',compact('profiledata','institute'));
         }else{
             echo "1";
           // return Redirect::to('/');
         }
    }
     public function UserProfile(){
        if(Auth::user()){
         $boardArr = Board::select('board_name', 'board_id')->orderBy('board_id')->get(); 
         $mediumArr = Medium::select('name', 'id')->orderBy('id')->get(); 
         $userList = Student::find(Auth::user()->id);
         $countryArr = Country::pluck('country', 'id')->toArray();
         $instituteArr = User::where('user_role',2)->get();
         $selectinstitute = User::where('id',$userList->institute_id)->get();
         // dd($selectinstitute);
         return View('web.home.user-profile',['data'=>$userList,'country'=>$countryArr,'institute'=>$instituteArr,'board' => $boardArr,'newboard' => $boardArr,'medium' => $mediumArr,'newmedium' => $mediumArr,'selectinstitute' => $selectinstitute]);
     }else{
        return redirect('/');
     }
    }
    public function UserProfileEdit(Request $data)
    {
        if(Auth::user()){

        if($data->individual_board && $data->individual_medium){
                      $board = $data->input('individual_board');
                      $medium = $data->input('individual_medium');
                      $institute_id = $data->input('institute_id');
                    }
                    elseif($data->other_board && $data->other_medium){
                       $board = $data->input('other_board');
                       $medium = $data->input('other_medium');
                       $institute_id = DB::table('user')->insertGetId(['first_name' => $data->i_name,
                                                    'address1' => $data->i_address1,
                                                    'address2' => $data->i_address2,
                                                    'city' => $data->i_city,
                                                    'state' => $data->i_state,
                                                    'country' => $data->i_country,
                                                    'phone_number' => $data->i_contact_no,
                                                    'website' => $data->i_website,
                                                    'board' => $data->other_board,
                                                    'medium' => $data->other_medium,
                                                    'created_at' => date('Y-m-d H:i:s'),
                                                  ]);
                       $institute_id = $institute_id;
                    }
                    elseif($data->institute_id){
                        $board = '';
                        $medium = '';
                        $institute_id = $data->input('institute_id');
                    }
                    
                     // dd($data->institute_type);
                   DB::table('student')->where('id',Auth::user()->id)->update(["fname" => $data->fname,
                                             'mname' => $data->mname,
                                             'lname' => $data->lname,
                                             'page_trk' => '2',
                                             'institute_id' => $institute_id,
                                             'institute_type' => $data->institute_type,
                                             'phone_number' => $data->phone_number,
                                             'email' => $data->email,
                                             'updated_at' => $data->updated_at,
                                             'dob' => $data->dob,
                                             'gender' => $data->gender,
                                             'address1' => $data->address1,
                                             'address2' => $data->address2,
                                             'city' => $data->city,
                                             'country' => $data->country,
                                             'state' => $data->state,
                                             'pincode' => $data->pincode,
                                             'updated_by' => Auth::user()->id,
                                             'updated_at' => date('Y-m-d H:i:s'),
                                             'updated_ip' => \Request::ip(),
                                             'std' => $data->std,
                                             'board' => $board,
                                             'medium' => $medium,
                                           ]);
                   return Redirect::to('/');
                   }else{
        return redirect('/');
     }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class AppUser extends Model
{
    //
    protected $table = "app_user";
    protected $primaryKey = 'id';


    //generate unique string for user token
    public static function generateToken($length=25) {
        $token          =   "";
        $codeAlphabet   =   "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet   .=  "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet   .=  "0123456789";
        $max            =   strlen($codeAlphabet); // edited

        for($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }
        return $token;
    }
}

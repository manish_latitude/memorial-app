<?php
namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Foundation\Auth\User as Authenticatable;
/**
 * Class User
 * 
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \Carbon\Carbon $verified_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $permissions
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Payment extends Authenticatable
{
	
	protected $table = 'payment';
	protected $primaryKey = 'id';
	protected $casts = [
		'status' => 'int'
	];


	protected $fillable = [
		'id',
		'bankname',
		'transactionno',
		'transferdate',
            ];

}

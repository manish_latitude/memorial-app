<?php

namespace App\models;

use Reliese\Database\Eloquent\Model as Eloquent;

class Faqs extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'faqs';
    protected $primaryKey = 'id';
    public $timestamps = false;

   /* protected $casts = [
            'BusinessSector' => 'int',
            'Region' => 'int',
            'Province' => 'int',
            'District' => 'int',
            'City' => 'int',
            'CEOName' => 'int',
            'CFOName' => 'int',
            'MDName' => 'int',
            'FirstAuthorized' => 'int',
            'SecondAuthorized' => 'int',
            'Logo' => 'boolean'
    ];*/

    protected $fillable = [
            'id',
            'app_id',
            'title',
            'answer',
            'status',
            'created_at',
            'updated_at'
    ];
    
    public function getData($order_by = 'id', $sort = 'desc')
    {
        return $this->query()
            ->select('*')
            ->groupBy('faqs.id')
            ->orderBy($order_by,$sort)
            ->get();
    }
    
    public function createFaqs($input)
    {
        $faqs = new Faqs();
        $faqs->title = (isset($input['title'])) ? $input['title'] : '';
        $faqs->title = (isset($input['title'])) ? $input['title'] : '';
        $faqs->answer = (isset($input['answer'])) ? $input['answer'] : '';
        $faqs->status = (isset($input['status'])) ? $input['status'] : 'Active';
        
        $faqs->created_at = Carbon::now();
        $faqs->updated_at = Carbon::now();
        $faqs->save();
        return $faqs->id;
    }
    
    public function updateFaqs($faqs,$input)
    {
        $faqs->updated_at = Carbon::now();
        
        if(isset($input['title'])) {
            $faqs->title= $input['title'];
        }
        if(isset($input['answer'])) {
            $faqs->answer = $input['answer'];
        }
        if(isset($input['status'])) {
            $faqs->status = $input['status'];
        }
        $faqs->save();
        return $faqs->id;
    }
}

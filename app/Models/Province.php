<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 04 Jan 2018 13:11:18 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Province
 * 
 * @property int $id
 * @property int $region
 * @property string $country
 * @property string $province
 *
 * @package App\Models
 */
class Province extends Eloquent
{
	protected $table = 'province';
	public $timestamps = false;

	protected $casts = [
		'region' => 'int'
	];

	protected $fillable = [
		'region',
		'country',
		'province'
	];
}

<?php



/**

 * Created by Reliese Model.

 * Date: Fri, 29 Dec 2017 06:07:32 +0000.

 */



namespace App\Models;



use Illuminate\Database\Eloquent\Model;

use Reliese\Database\Eloquent\Model as Eloquent;

/**

 * Class User

 * 

 * @property int $id

 * @property int $app_id

 * @property string $title

 * @property string $description

 * @property int $status

 * @property \Carbon\Carbon $created_at

 * @property \Carbon\Carbon $updated_at

 * @property string $deleted_at

 

 * 

 * @property \Illuminate\Database\Eloquent\Collection $contacts

 * @property \Illuminate\Database\Eloquent\Collection $languages

 * @property \Illuminate\Database\Eloquent\Collection $language_keywords

 * @property \Illuminate\Database\Eloquent\Collection $language_multis

 * @property \Illuminate\Database\Eloquent\Collection $roles

 * @property \Illuminate\Database\Eloquent\Collection $user_login_histories

 *

 * @package App\Models

 */

class AppContents extends Eloquent

{

	use \Illuminate\Database\Eloquent\SoftDeletes;



	protected $table = 'app_contents';

	protected $primaryKey = 'id';

	protected $casts = [

		'status' => 'int'

	];





	protected $fillable = [

		'app_id',

        'content_type',

		'title',

		'description',
        'image_url',
		'web_url',
		'youtube_url',
		'status'

	];



	



	//generate unique string for user token

	

	public static function imageUpload($fileName = []) {



		$publicPath	= public_path('uploads/tmp/');

        $imagename 	= "";



		if($fileName !== null) {



			$photo 		= $fileName;

	        $imagename 	= time().uniqid().'.'.$photo->getClientOriginalExtension(); 

                

            $s3 = AWS::createClient('s3');

            try {

				$uploadAws = $s3->putObject([

					// s3 bucket name

	   				'Bucket'     => 'contactstorage',

	   				// filename

	   				'Key'        => $imagename,

	   				// path of image $file_path

	   				'SourceFile' => $photo->getRealPath()

			 	]);



				return $imagename;

				// temp image delete

				//return response()->json(['status'=>true,'message'=>'Image uploaded successfully','data'=>["fileName"=>$imagename,"fileUrl"=>$uploadAws['ObjectURL'],"filePath"=>$imagename]],200);



			} catch (Exception $e) {

				return $imagename; 

				// temp image delete

				//return response()->json(['status'=>false,'message'=>'failed to upload image'],500);

			}

		}

		return $imagename;

		// return response()->json(['status'=>false,'message'=>'failed to upload image'],500);

	}





}


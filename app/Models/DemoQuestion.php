<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Reliese\Database\Eloquent\Model as Eloquent;
/**
 * Class User
 * 
 * @property int $id
 * @property int $category_id
 * @property string $question_title
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $roles
 *
 * @package App\Models
 */
class DemoQuestion extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $table = 'demo_question';
	protected $primaryKey = 'id';
	protected $casts = [
		'status' => 'int'
	];


	protected $fillable = [
		'category_id',
		'question_title',
//		'category_id'
            ];

}

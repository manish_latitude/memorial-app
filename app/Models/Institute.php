<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 06:07:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * 
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $token
 * @property int $user_role
 * @property string $phone_number
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \Carbon\Carbon $verified_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $contacts
 * @property \Illuminate\Database\Eloquent\Collection $languages
 * @property \Illuminate\Database\Eloquent\Collection $language_keywords
 * @property \Illuminate\Database\Eloquent\Collection $language_multis
 * @property \Illuminate\Database\Eloquent\Collection $roles
 * @property \Illuminate\Database\Eloquent\Collection $user_login_histories
 *
 * @package App\Models
 */
class Institute extends Authenticatable
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $table = 'institute';
	protected $primaryKey = 'id';
	protected $casts = [
		'status' => 'int'
	];

	/*protected $dates = [
		'verified_at'
	];*/

	protected $hidden = [
		'token'
	];

	protected $fillable = [
		'name',
		'address1',
		'address2',
		'city',
		'country',
		'state',
		'pincode',
		'contact_number',
		'website',
		'board',
                'medium'
	];

	//generate unique string for user token
        public static function generateToken($length=25) {
            $token          =   "";
            $codeAlphabet   =   "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $codeAlphabet   .=  "abcdefghijklmnopqrstuvwxyz";
            $codeAlphabet   .=  "0123456789";
            $max            =   strlen($codeAlphabet); // edited

            for($i=0; $i < $length; $i++) {
                $token .= $codeAlphabet[random_int(0, $max-1)];
            }
            return $token;
        }
}

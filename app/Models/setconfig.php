<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 06:07:32 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Reliese\Database\Eloquent\Model as Eloquent;
/**
 * Class User
 * 
 * @property int $id
 * @property int $admob_id
 * @property string $ad_key
 * @property string $ad_value
 * @property string $ad_type
 * @property string $description
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 
 * 
 * @property \Illuminate\Database\Eloquent\Collection $appadmob
 
 *
 * @package App\Models
 */
class setconfig extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $table = 'site_configs';
	protected $primaryKey = 'id';
	


	protected $fillable = [
		'app_id',
		'key',
        'value',
		'description'
	];

	
        
        
}

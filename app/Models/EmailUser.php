<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Foundation\Auth\User as Authenticatable;

class EmailUser extends Authenticatable
{
	protected $table = 'setting_user';
	protected $primaryKey = 'id';
}
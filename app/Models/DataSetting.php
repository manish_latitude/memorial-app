<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 8 Feb 2018 15:38:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;

/**
 * Class DataSetting
 * @package App\Models
 */
class DataSetting extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'data_property';
	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_on';
    const DELETED_AT = 'deleted_on';
	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int',
		// 'status' => 'int'
	];

	protected $fillable = [
		'slug',
		'title',
		'notes',
		'created_by',
		'created_at',
		'updated_by',
		'updated_on',
		'deleted_on',
		// 'status'
	];

	public function language()
	{
		return $this->belongsTo(\App\Models\Language::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}
}

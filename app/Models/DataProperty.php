<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Feb 2018 05:54:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DataProperty
 * 
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property string $notes
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property int $created_by
 * @property int $updated_by
 * @property \Carbon\Carbon $updated_on
 * @property \Carbon\Carbon $deleted_on
 * 
 * @property \Illuminate\Database\Eloquent\Collection $data_property_children
 *
 * @package App\Models
 */
class DataProperty extends Eloquent
{
	protected $table = 'data_property';
	public $timestamps = false;
	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_on';
    const DELETED_AT = 'deleted_on';

	protected $casts = [
		'status' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'updated_on',
		'deleted_on'
	];

	protected $fillable = [
		'slug',
		'title',
		'notes',
		'status',
		'created_by',
		'updated_by',
		'updated_on',
		'deleted_on'
	];

	public function data_property_children()
	{
		return $this->hasMany(\App\Models\DataPropertyChild::class);
	}

	public function language()
	{
		return $this->belongsTo(\App\Models\Language::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	//return all data setting multilanguage data
	public static function getDataSetting($languageId = 1) {

		$dataSettings 	= DataProperty::select('id', 'slug', 'title', 'notes', 'status', 'created_at', 'updated_on')
                    		->with(['data_property_children' => function($query) {
                            	$query->select('data_property_id', 'id', 'title');
                        	}])
                        	->with(['data_property_children.data_property_sub_children' => function($query) use ($languageId) {
                            	$query->select('data_property_child_id', 'id', 'multilang_title','language_id')->where('language_id',$languageId);
                        	}])
							->get();

		$settingsArr    = [];							
		if(count($dataSettings) > 0) {

            foreach ($dataSettings as $key => $dataSet) {
                
                if($dataSet->slug != "") {
                    if(count($dataSet['data_property_children'])) {
                        foreach ($dataSet['data_property_children'] as $key => $dataPropertyChild) {

                            $title  = isset($dataPropertyChild['data_property_sub_children'][0]['multilang_title'])?$dataPropertyChild['data_property_sub_children'][0]['multilang_title']:"";
                            if($title != "") {
                                $settingsArr[$dataSet->slug][$dataPropertyChild->id] = "$title";
                            }
                        }
                    }      
                }
            }
        }		
        
        // set individual data set for view start 
        $settings['setTitle']                   = (!empty($settingsArr['title']))?$settingsArr['title']:[];
        $settings['setSuffix']                  = (!empty($settingsArr['suffix']))?$settingsArr['suffix']:[];
        $settings['setGender']                  = (!empty($settingsArr['gender']))?$settingsArr['gender']:[];
        $settings['setImportance']              = (!empty($settingsArr['importance']))?$settingsArr['importance']:[];
        $settings['setCaution']                 = (!empty($settingsArr['caution']))?$settingsArr['caution']:[];
        $settings['setSensitivity']             = (!empty($settingsArr['sensitivity']))?$settingsArr['sensitivity']:[];
        $settings['setMaritalStatus']           = (!empty($settingsArr['marital_status']))?$settingsArr['marital_status']:[];
        $settings['setPoliticalParty']          = (!empty($settingsArr['political_party']))?$settingsArr['political_party']:[];
        $settings['setPoliticalPositionHeld']   = (!empty($settingsArr['political_position_held']))?$settingsArr['political_position_held']:[];
        $settings['setPoliticalPositionType']   = (!empty($settingsArr['political_position_type']))?$settingsArr['political_position_type']:[];
        $settings['setPoliticalPositionGreeting'] = (!empty($settingsArr['political_position_greeting']))?$settingsArr['political_position_greeting']:[];
        $settings['setAdminstrativeGroup']      = (!empty($settingsArr['adminstrative_group']))?$settingsArr['adminstrative_group']:[];
        $settings['setRelationshipReference']   = (!empty($settingsArr['relationship_reference']))?$settingsArr['relationship_reference']:[];
        $settings['setSponshorships']           = (!empty($settingsArr['sponshorships']))?$settingsArr['sponshorships']:[];
        $settings['setChartiableOgranization']  = (!empty($settingsArr['chartiable_ogranization']))?$settingsArr['chartiable_ogranization']:[];
        $settings['setInternalOgranization']    = (!empty($settingsArr['internal_ogranization']))?$settingsArr['internal_ogranization']:[];
        $settings['setAddressType']             = (!empty($settingsArr['address_type']))?$settingsArr['address_type']:[];
        $settings['setJobType']                 = (!empty($settingsArr['job_type']))?$settingsArr['job_type']:[];
        $settings['setPhoneNumberType']         = (!empty($settingsArr['phone_number_type']))?$settingsArr['phone_number_type']:[];
        $settings['setEmailType']               = (!empty($settingsArr['email_type']))?$settingsArr['email_type']:[];
        $settings['setBusinessType']            = (!empty($settingsArr['business_type']))?$settingsArr['business_type']:[];
        $settings['setReligion']                = (!empty($settingsArr['religion']))?$settingsArr['religion']:[];
        $settings['setOrganization']            = (!empty($settingsArr['organization']))?$settingsArr['organization']:[];
        $settings['setPhoneNumberCategory']     = (!empty($settingsArr['phone_number_category']))?$settingsArr['phone_number_category']:[];
        $settings['setNoOfEmployee']            = (!empty($settingsArr['no_of_employee']))?$settingsArr['no_of_employee']:[];
        //$settings['setFatherName']              = (!empty($settingsArr['father_name']))?$settingsArr['father_name']:[];
        $settings['setFatherName']             = ['1' => 'Abagnale' , '2' => 'Aaron', '3' => 'Abelson', '4' => 'Hega', '5' => 'huuey', '6' => 'Mike', '7' => 'Maaiz', '8' => 'Sham', '9' => 'Sawin', '10' => 'Sunny'];
        //$settings['setMotherName']              = (!empty($settingsArr['mother_name']))?$settingsArr['mother_name']:[];
        $settings['setMotherName']              = ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross','4' => 'Maddy','5' => 'Harmony','6' => 'Harriot','7' => 'Shimeka','8' => 'Shirenna','9' => 'Madaya','10' => 'Maddie' ];
        //$settings['setFirstName']               = (!empty($settingsArr['first_name']))?$settingsArr['first_name']:[];
        $settings['setFirstName']               = ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross', '4' => 'Sara', '5' => 'Shay', '6' => 'Sofi', '7' => 'Jeshan', '8' => 'Janet', '9' => 'Ruhi', '10' => 'Tianna'];
        $settings['setBloodType']               = (!empty($settingsArr['blood_type']))?$settingsArr['blood_type']:[];
        //$settings['setSurname']                 = (!empty($settingsArr['surname']))?$settingsArr['surname']:[];
        $settings['setSurname']                 = ['1' => 'Hank','2' => 'Frank','3' => 'Edward','4' => 'Reuben','5' => 'Hal','6' => 'James','7' => 'Creighton','8' => 'Jane', '9' => 'Henry', '10' => 'Samuel'];
        
        $settings['setProfession']              = (!empty($settingsArr['profession']))?$settingsArr['profession']:[];
        //Education Data Settings
        $settings['setQualification']           = (!empty($settingsArr['qualification']))?$settingsArr['qualification']:[];
        $settings['setAchievement']             = (!empty($settingsArr['education-achievement']))?$settingsArr['education-achievement']:[];
        $settings['setInstitute']               = (!empty($settingsArr['institute']))?$settingsArr['institute']:[];
        $settings['setCommunicationCategory']   = (!empty($settingsArr['communication_category']))?$settingsArr['communication_category']:[];
        // set individual data set for view end					



		return 	$settings;					
	}

    public static function checkDataSetting($id) {

        //check data in contact tab - start
            $contactObj = Contact::where('title','LIKE',"%$id%")
                                ->orWhere('suffix','LIKE',"%$id%")
                                ->orWhere('blood_type','LIKE',"%$id%")
                                ->orWhere('gender','LIKE',"%$id%")
                                ->orWhere('marital_status','LIKE',"%$id%")
                                ->orWhere('profession','LIKE',"%$id%")
                                ->orWhere('importance','LIKE',"%$id%")
                                ->orWhere('caution','LIKE',"%$id%")
                                ->orWhere('sensitivity','LIKE',"%$id%")
                                ->orWhere('sponsorships','LIKE',"%$id%")
                                ->orWhere('charitable_organizations','LIKE',"%$id%")
                                ->orWhere('political_party','LIKE',"%$id%")
                                ->orWhere('relationship_preference','LIKE',"%$id%")
                                ->orWhere('religion','LIKE',"%$id%")
                                ->orWhere('internal_organizations','LIKE',"%$id%")
                                ->orWhere('administrative_group','LIKE',"%$id%")
                                ->first();

            if(count($contactObj) > 0) {
                return 1;
            }
        //check data in contact tab - end

        //check data in address tab - end
            $addressObj = ContactAddress::where('type','LIKE',"%$id%")
                                ->orWhere('importance','LIKE',"%$id%")
                                ->orWhere('caution','LIKE',"%$id%")
                                ->orWhere('sensitivity','LIKE',"%$id%")
                                ->first();

            if(count($addressObj) > 0) {
                return 1;
            }
        //check data in address tab - end

        //check data in job position tab - start
            $jobPositionObj = ContactJob::where('job_type',$id)
                                ->orWhere('oranization','LIKE',"%$id%")
                                ->first();

            if(count($jobPositionObj) > 0) {
                return 1;
            }
        //check data in job position tab - end

        //check data in contact number tab - start
            $contactNumberObj = ContactNumber::where('phone_type',$id)
                                ->orWhere('category','LIKE',"%$id%")
                                ->orWhere('importance','LIKE',"%$id%")
                                ->orWhere('caution','LIKE',"%$id%")
                                ->orWhere('sensitivity','LIKE',"%$id%")
                                ->first();

            if(count($contactNumberObj) > 0) {
                return 1;
            }
        //check data in contact number tab - end

        //check data in communication tab - start
            $communicationObj = ContactCommunication::where('email_type',$id)
                                ->orWhere('category','LIKE',"%$id%")
                                ->orWhere('importance','LIKE',"%$id%")
                                ->orWhere('caution','LIKE',"%$id%")
                                ->orWhere('sensitivity','LIKE',"%$id%")
                                ->first();

            if(count($communicationObj) > 0) {
                return 1;
            }
        //check data in communication tab - end

        //check data in business tab - start
            $businessObj = ContactBusiness::where('business_type','LIKE',"%$id%")
                                ->orWhere('organization','LIKE',"%$id%")
                                ->orWhere('number_of_employees',$id)
                                ->first();

            if(count($businessObj) > 0) {
                return 1;
            }
        //check data in business tab - end

        //check data in political position tab - start
            $politicalPositionObj = ContactPoliticalPosition::where('type','LIKE',"%$id%")
                                ->orWhere('political_posotion','LIKE',"%$id%")
                                ->orWhere('greeting','LIKE',"%$id%")
                                ->orWhere('political_party','LIKE',"%$id%")
                                ->first();

            if(count($politicalPositionObj) > 0) {
                return 1;
            }
        //check data in political position tab - end

        //check data in representative tab - start
            $representativeObj = ContactRepresentative::where('title','LIKE',"%$id%")
                                ->orWhere('suffix','LIKE',"%$id%")
                                ->orWhere('blood_type','LIKE',"%$id%")
                                ->orWhere('gender',$id)
                                ->orWhere('profession','LIKE',"%$id%")
                                ->orWhere('importance','LIKE',"%$id%")
                                ->orWhere('caution','LIKE',"%$id%")
                                ->orWhere('sensitivity','LIKE',"%$id%")
                                ->orWhere('sponsorships','LIKE',"%$id%")
                                ->orWhere('charitable_organizations','LIKE',"%$id%")
                                ->orWhere('political_party','LIKE',"%$id%")
                                ->orWhere('relationship_preference','LIKE',"%$id%")
                                ->orWhere('religion_id',$id)
                                ->orWhere('internal_organizations','LIKE',"%$id%")
                                ->orWhere('administrative_group','LIKE',"%$id%")
                                ->first();

            if(count($representativeObj) > 0) {
                return 1;
            }
        //check data in representative tab - end

        //check data in referral tab - start
            $referralObj = ContactReferral::where('title','LIKE',"%$id%")
                                ->orWhere('suffix','LIKE',"%$id%")
                                ->orWhere('blood_type','LIKE',"%$id%")
                                ->orWhere('gender',$id)
                                ->orWhere('profession','LIKE',"%$id%")
                                ->orWhere('importance','LIKE',"%$id%")
                                ->orWhere('caution','LIKE',"%$id%")
                                ->orWhere('sensitivity','LIKE',"%$id%")
                                ->orWhere('sponsorships','LIKE',"%$id%")
                                ->orWhere('charitable_organizations','LIKE',"%$id%")
                                ->orWhere('political_party','LIKE',"%$id%")
                                ->orWhere('relationship_preference','LIKE',"%$id%")
                                ->orWhere('religion_id',$id)
                                ->orWhere('internal_organizations','LIKE',"%$id%")
                                ->orWhere('administrative_group','LIKE',"%$id%")
                                ->first();

            if(count($referralObj) > 0) {
                return 1;
            }
        //check data in referral tab - end

        //check data in spouse tab - start
            $spouseObj = ContactSpouse::where('title','LIKE',"%$id%")
                                ->orWhere('suffix','LIKE',"%$id%")
                                ->orWhere('blood_type','LIKE',"%$id%")
                                ->orWhere('gender',$id)
                                ->orWhere('profession','LIKE',"%$id%")
                                ->orWhere('importance','LIKE',"%$id%")
                                ->orWhere('caution','LIKE',"%$id%")
                                ->orWhere('sensitivity','LIKE',"%$id%")
                                ->orWhere('sponsorships','LIKE',"%$id%")
                                ->orWhere('charitable_organizations','LIKE',"%$id%")
                                ->orWhere('political_party','LIKE',"%$id%")
                                ->orWhere('religion_id',$id)
                                ->orWhere('internal_organizations','LIKE',"%$id%")
                                ->orWhere('administrative_group','LIKE',"%$id%")
                                ->first();

            if(count($spouseObj) > 0) {
                return 1;
            }
        //check data in spouse tab - end

        //check data in dependent tab - start
            $dependentObj = ContactDependent::where('title','LIKE',"%$id%")
                                ->orWhere('suffix','LIKE',"%$id%")
                                ->orWhere('blood_type','LIKE',"%$id%")
                                ->orWhere('gender',$id)
                                ->orWhere('profession','LIKE',"%$id%")
                                ->orWhere('importance','LIKE',"%$id%")
                                ->orWhere('caution','LIKE',"%$id%")
                                ->orWhere('sensitivity','LIKE',"%$id%")
                                ->orWhere('religion_id',$id)
                                ->orWhere('internal_organizations','LIKE',"%$id%")
                                ->orWhere('administrative_group','LIKE',"%$id%")
                                ->first();

            if(count($dependentObj) > 0) {
                return 1;
            }
        //check data in dependent tab - end

        //check data in education tab - start
            $educationObj = ContactEducation::where('qualification','LIKE',"%$id%")
                                ->orWhere('institute','LIKE',"%$id%")
                                ->orWhere('achivement','LIKE',"%$id%")
                                ->orWhere('sensitivity','LIKE',"%$id%")
                                ->first();

            if(count($educationObj) > 0) {
                return 1;
            }
        //check data in education tab - end

        //check data in greetings letter tab - start
            $greetingsLetterObj = ContactGreetingsLetter::where('importance','LIKE',"%$id%")
                                ->orWhere('caution','LIKE',"%$id%")
                                ->orWhere('sensitivity','LIKE',"%$id%")
                                ->first();

            if(count($greetingsLetterObj) > 0) {
                return 1;
            }
        //check data in greetings letter tab - end

        return 0;
    }
}

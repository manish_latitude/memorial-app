<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 06:07:32 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Reliese\Database\Eloquent\Model as Eloquent;
/**
 * Class User
 * 
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \Carbon\Carbon $verified_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $roles
 *
 * @package App\Models
 */
class Category extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $table = 'category';
	protected $primaryKey = 'id';
	protected $casts = [
		'status' => 'int'
	];


	protected $fillable = [
		'category_name'
            ];

}

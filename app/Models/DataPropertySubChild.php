<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Feb 2018 05:57:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DataPropertySubChild
 * 
 * @property int $id
 * @property int $data_property_child_id
 * @property int $language_id
 * @property string $multilang_title
 * @property int $created_by
 * @property \Carbon\Carbon $created_at
 * @property int $updated_by
 * @property \Carbon\Carbon $updated_on
 * @property \Carbon\Carbon $deleted_on
 * 
 * @property \App\Models\Language $language
 * @property \App\Models\DataPropertyChild $data_property_child
 *
 * @package App\Models
 */
class DataPropertySubChild extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'data_property_sub_child';
	public $timestamps = false;
	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_on';
    const DELETED_AT = 'deleted_on';

	protected $casts = [
		'data_property_child_id' => 'int',
		'language_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'updated_on',
		'deleted_on'
	];

	protected $fillable = [
		'data_property_child_id',
		'language_id',
		'multilang_title',
		'created_by',
		'updated_by',
		'updated_on',
		'deleted_on'
	];

	public function language()
	{
		return $this->belongsTo(\App\Models\Language::class);
	}

	public function data_property_child()
	{
		return $this->belongsTo(\App\Models\DataPropertyChild::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppMessage extends Model
{
    //
    protected $table = "app-message";
}

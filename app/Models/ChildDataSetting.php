<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 8 Feb 2018 15:38:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;

/**
 * Class ChildDataSetting
 * @package App\Models
 */
class ChildDataSetting extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'data_property_child';
	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_on';
    const DELETED_AT = 'deleted_on';
	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'data_property_id',
		'title',
		'created_by',
		'created_at',
		'updated_by',
		'updated_on',
		'deleted_on',
	];

	public function language()
	{
		return $this->belongsTo(\App\Models\Language::class);
	}

	public function subChild()
	{
		return $this->belongsTo(\App\Models\Language::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}
}

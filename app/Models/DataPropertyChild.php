<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Feb 2018 05:56:39 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DataPropertyChild
 * 
 * @property int $id
 * @property int $data_property_id
 * @property string $title
 * @property int $created_by
 * @property \Carbon\Carbon $created_at
 * @property int $updated_by
 * @property \Carbon\Carbon $updated_on
 * @property \Carbon\Carbon $deleted_on
 * 
 * @property \App\Models\DataProperty $data_property
 * @property \Illuminate\Database\Eloquent\Collection $data_property_sub_children
 *
 * @package App\Models
 */
class DataPropertyChild extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	
	protected $table = 'data_property_child';
	public $timestamps = false;
	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_on';
    const DELETED_AT = 'deleted_on';

	protected $casts = [
		'data_property_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'updated_on',
		'deleted_on'
	];

	protected $fillable = [
		'data_property_id',
		'title',
		'created_by',
		'updated_by',
		'updated_on',
		'deleted_on'
	];

	public function data_property()
	{
		return $this->belongsTo(\App\Models\DataProperty::class);
	}

	public function data_property_sub_children()
	{
		return $this->hasMany(\App\Models\DataPropertySubChild::class);
	}
}

<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 04 Jan 2018 13:11:18 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class District
 * 
 * @property int $id
 * @property int $region
 * @property string $country
 * @property int $province
 * @property string $district
 *
 * @package App\Models
 */
class District extends Eloquent
{
	protected $table = 'district';
	public $timestamps = false;

	protected $casts = [
		'region' => 'int',
		'province' => 'int'
	];

	protected $fillable = [
		'region',
		'country',
		'province',
		'district'
	];
}

<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 06:07:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserLoginHistory
 * 
 * @property int $id
 * @property int $user_id
 * @property string $ip_address
 * @property \Carbon\Carbon $created_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UserLoginHistory extends Eloquent
{
	protected $table = 'user_login_history';
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'ip_address'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    //
    protected $table = "member";
    protected $primarykey = "id";

    public function gallery(){
    	return $this->hasMany('App\Models\MemberImage','member_id','id');
    }

    public function orderOfservice(){
    	return $this->hasMany('App\Models\OrderOfService','member_id','id');
    }

    public function funeral(){
    	return $this->belongsTo('App\Models\Funeral','funeral_id','id');
    }

    public function country(){
        return $this->belongsTo('App\Models\Country','country_id','id');
    }

    public function state(){
        return $this->belongsTo('App\Models\State','state_id','id');
    }

    public function city(){
        return $this->belongsTo('App\Models\City','city_id','id');
    }

    public function community(){
        return $this->belongsTo('App\Models\Community','community_id','id');
    }
}

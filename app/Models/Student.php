<?php
namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Foundation\Auth\User as Authenticatable;
/**
 * Class User
 * 
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \Carbon\Carbon $verified_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $permissions
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Student extends Authenticatable
{
	
	protected $table = 'student';
	protected $primaryKey = 'id';
	protected $casts = [
		'status' => 'int'
	];


	protected $fillable = [
		'institute_id',
		'fname',
		'lname',
		'contact_number',
		'email',
		'password',
		'state',
		'created_at',
		'updated_at',
		'deleted_at',
		'phone_number',
		'dob',
		'gender',
		'address1',
		'address2',
		'city',
		'country',
		'state',
		'pincode',
		'std',
            ];

}

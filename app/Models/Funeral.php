<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Funeral extends Model
{
    //
    protected $table = "funeral";
    protected $primarykey = "id";

    public function profile(){
    	return $this->hasMany('App\Models\FuneralProfile','funeral_id','id');
    }

    public function obituaries(){
    	return $this->hasMany('App\Models\Member','funeral_id','id');	
    }

    public function gallery(){
    	return $this->hasMany('App\Models\FuneralImage','funeral_id','id');
    }

    public function state(){
        return $this->belongsTo('App\Models\State','state','id');
    }

    public function city(){
        return $this->belongsTo('App\Models\City','city','id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemorialTag extends Model
{
    //
    protected $table = "memorial_tag";
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'Web\WebController@index');
Route::post('web/loginauth', 'Web\AuthController@loginAuth');
Route::any('/registration', 'Web\AuthController@studentCreate');
Route::put('/registration/edit', 'Web\AuthController@studentEdit');
Route::get('/web/instituteslecte/{id}', 'Web\AuthController@InstituteSlecte');
Route::any('/student-profile', 'Web\AuthController@profileUpdate');
Route::post('web/registerauth', 'Web\Auth\AuthController@registerAuth');
Route::any('/studentlogout', 'Web\AuthController@studentLogout');

//demo question

Route::get('/member/orderOfService','Admin\MemberController@orderOfService');
Route::post('/member/imageStore','Admin\MemberController@imageStore');
Route::get('/member/imageJson','Admin\MemberController@imageJson');
Route::get('/contactUsForm','Web\ContactController@contactForm');


Route::group(['prefix' => 'web','middleware' => 'web'], function() {
	Route::get('/contact','Web\ContactController@contact');
});

Route::group(['prefix' => 'admin','middleware' => 'guest:admin'], function () {

	Route::get('/', ['as' => 'admin', 'uses' => 'Admin\Auth\AuthController@index']);
	//Admin Login
	Route::get('login', ['as' => 'login', 'uses' => 'Admin\Auth\AuthController@index']);
	Route::post('loginauth', 'Admin\Auth\AuthController@loginAuth');
	Route::get('/user/confirm/{token}', 'Admin\UserController@userConfirm');
	Route::get('/user/account/confirm', 'Admin\UserController@userAccountConfirm');
	Route::post('/user/setPassword', 'Admin\UserController@savePassword');
	Route::post('/user/save-reset-Password', 'Admin\UserController@saveResetPassword');
	Route::get('/user/reset-password/{token}', 'Admin\UserController@resetPassword');
	Route::get('/user/reset-password-confirm', 'Admin\UserController@resetConfirm');
});

    
Route::group(['prefix' => 'admin','middleware' => 'auth:admin'], function () {

	Route::any('/logout', 'Admin\Auth\AuthController@getLogout');
	Route::get('/dashboard','Admin\DashboardController@index');
	Route::get('/add', 'Admin\DashboardController@add');
        
	//User Profile
	Route::get('/profile', 'Admin\ProfileController@index');
	Route::post('/profile/store', 'Admin\ProfileController@store');
	//End User profile
	
	Route::get('/change-password','Admin\ChangePassword@index');
	Route::post('/change-password/store','Admin\ChangePassword@store');

	//////////////////////////////////User Management//////////////////////////////////
	Route::get('/user', 'Admin\UserController@index');
	Route::get('/user/create', 'Admin\UserController@create');
	Route::post('/user/store', 'Admin\UserController@store');
	Route::get('/user/destroy', 'Admin\UserController@destroy');
	Route::get('/user/edit/{id}', 'Admin\UserController@edit');
	Route::any('/confirm-pass', 'Admin\UserController@editConfirmPass');
	Route::get('/user/reset-pass-link', 'Admin\UserController@sendResetPasswordLink');
	Route::any('/user/perfomaction', 'Admin\UserController@perfomaction');
	Route::any('/user/array-data', 'Admin\UserController@arrayData');
       
        Route::get('/role', ['as'=>'role','uses'=>'Admin\RoleController@index']);
	Route::get('/role/list',['as'=>'/role/list','uses'=>'Admin\RoleController@roleList']);
	Route::get('/role/add',['as'=>'/role/add','uses'=>'Admin\RoleController@addEditRole']);
	Route::get('/role/edit/{id}',['as'=>'/role/edit/{id}','uses'=>'Admin\RoleController@addEditRole']);
	Route::post('/role/store',['as'=>'/role/store','uses'=>'Admin\RoleController@storeRole']);
	Route::get('/role/destroy/{id}',['as'=>'/role/destroy/{id}','uses'=>'Admin\RoleController@destroyRole']);
	//////////////////////////////////User Management//////////////////////////////////
	
	/////////////////////////////////App User Management///////////////////////////////
	Route::get('/app-user','Admin\AppUserController@index');

	Route::get('/app-user/export','Admin\AppUserController@export');

	Route::any('/app-user/perfomaction', 'Admin\AppUserController@perfomaction');
	Route::any('/app-user/array-data', 'Admin\AppUserController@arrayData');

	Route::get('/app-user/create','Admin\AppUserController@create');
	Route::post('/app-user/store','Admin\AppUserController@store');

	Route::get('/app-user/edit/{id}', 'Admin\AppUserController@edit');

	Route::get('/app-user/destroy/{id}','Admin\AppUserController@destroy');
    
    Route::get('/app-user/resetPassword/{token}','Admin\AppUserController@resetPasswordView');
    Route::post('/app-user/resetPasswordStore','Admin\AppUserController@resetPassword');
	/////////////////////////////////App User Management///////////////////////////////
	
	/////////////////////////////Funeral Management/////////////////////////////
	Route::get('/funeral','Admin\FuneralController@index');

	Route::get('/funeral/export','Admin\FuneralController@export');

	Route::any('/funeral/perfomaction','Admin\FuneralController@perfomaction');
	Route::any('/funeral/array-data','Admin\FuneralController@arrayData');

	Route::get('/funeral/create','Admin\FuneralController@create');
	Route::post('/funeral/store','Admin\FuneralController@store');

	Route::get('/funeral/edit/{id}','Admin\FuneralController@edit');
	Route::post('/funeral/update','Admin\FuneralController@update');

	Route::get('/funeral/image-update','Admin\FuneralController@updateImage');
	Route::get('/funeral/profile-update','Admin\FuneralController@updateProfile');

	Route::get('/funeral/destroy/{id}','Admin\FuneralController@destroy');

	Route::get('/funeral/stateList','Admin\FuneralController@stateList');
	Route::get('/funeral/cityList','Admin\FuneralController@cityList');

	Route::get('/funeral/getId','Admin\FuneralController@getId');
	Route::get('/funeral/approval','Admin\FuneralController@approval');

	Route::get('/funeral/resetPassword/{token}','Admin\FuneralController@resetPasswordView');
    Route::post('/funeral/resetPasswordStore','Admin\FuneralController@resetPassword');
	/////////////////////////////Funeral Management/////////////////////////////
	
	/////////////////////////////Member Management/////////////////////////////
	Route::get('/member','Admin\MemberController@index');

	Route::get('/member/export','Admin\MemberController@export');

	Route::any('/member/array-data','Admin\MemberController@arrayData');
	Route::any('/member/perfomaction','Admin\MemberController@perfomaction');

	Route::get('/member/create','Admin\MemberController@create');
	Route::post('/member/store','Admin\MemberController@store');

	Route::get('/member/edit/{id}','Admin\MemberController@edit');
	Route::post('/member/update','Admin\MemberController@update');

	Route::get('/member/profile-update','Admin\MemberController@updateProfile');

	Route::get('/member/image-update','Admin\MemberController@updateImage');
	Route::get('/member/order-update','Admin\MemberController@updateOrder');

	Route::get('/member/destroy/{id}','Admin\MemberController@destroy');

	Route::get('/member/stateList','Admin\MemberController@stateList');
	Route::get('/member/cityList','Admin\MemberController@cityList');

	Route::get('/member/getId','Admin\MemberController@getId');
	Route::get('/member/approval','Admin\MemberController@approval');
	/////////////////////////////Member Management/////////////////////////////
	
	/////////////////////////////Community Management/////////////////////////////
	Route::get('/community','Admin\CommunityController@index');

	Route::any('/community/array-data','Admin\CommunityController@arrayData');
	Route::any('/community/perfomaction','Admin\CommunityController@perfomaction');

	Route::get('/community/create','Admin\CommunityController@create');
	Route::post('/community/store','Admin\CommunityController@store');

	Route::get('/community/edit/{id}','Admin\CommunityController@edit');

	Route::get('/community/destroy/{id}','Admin\CommunityController@destroy');
	/////////////////////////////Community Management/////////////////////////////

	/////////////////////////////Condolence Management/////////////////////////////
	Route::get('/condolence','Admin\CondolenceController@index');

	Route::any('/condolence/perfomaction', 'Admin\CondolenceController@perfomaction');
	Route::any('/condolence/array-data', 'Admin\CondolenceController@arrayData');

	Route::get('/condolence/create','Admin\CondolenceController@create');
	Route::post('/condolence/store','Admin\CondolenceController@store');

	Route::get('/condolence/edit/{id}','Admin\CondolenceController@edit');
	Route::get('/condolence/image-update','Admin\CondolenceController@updateImage');

	Route::get('/condolence/destroy/{id}','Admin\CondolenceController@destroy');	

	Route::get('/condolence/updateOrder','Admin\CondolenceController@updateOrder');
	/////////////////////////////Condolence Management/////////////////////////////

	/////////////////////////////Flower Management/////////////////////////////
	Route::get('/flower','Admin\FlowerController@index');

	Route::any('/flower/perfomaction', 'Admin\FlowerController@perfomaction');
	Route::any('/flower/array-data', 'Admin\FlowerController@arrayData');

	Route::get('/flower/create','Admin\FlowerController@create');
	Route::post('/flower/store','Admin\FlowerController@store');

	Route::get('/flower/edit/{id}','Admin\FlowerController@edit');
	Route::get('/flower/image-update','Admin\FlowerController@updateImage');

	Route::get('/flower/destroy/{id}','Admin\FlowerController@destroy');

	Route::get('/flower/updateOrder','Admin\FlowerController@updateOrder');	
	/////////////////////////////Flower Management/////////////////////////////
	
	////////////////////////////Bereavement Management/////////////////////////////
	Route::get('/bereavement','Admin\BereavementController@index');

	Route::any('/bereavement/perfomaction', 'Admin\BereavementController@perfomaction');
	Route::any('/bereavement/array-data','Admin\BereavementController@arrayData');

	Route::get('/bereavement/create','Admin\BereavementController@create');
	Route::post('/bereavement/store','Admin\BereavementController@store');	

	Route::get('/bereavement/edit/{id}','Admin\BereavementController@edit');
	Route::get('/bereavement/image-update','Admin\BereavementController@updateImage');

	Route::get('/bereavement/destroy/{id}','Admin\BereavementController@destroy');

	Route::get('/bereavement/updateOrder','Admin\BereavementController@updateOrder');	
	////////////////////////////Bereavement Management/////////////////////////////
	
	////////////////////////////Funeral Plan Management///////////////////////////
	Route::get('/funeral-plan','Admin\FuneralPlanController@index');

	Route::any('/funeral-plan/perfomaction', 'Admin\FuneralPlanController@perfomaction');
	Route::any('/funeral-plan/array-data','Admin\FuneralPlanController@arrayData');

	Route::get('/funeral-plan/create','Admin\FuneralPlanController@create');
	Route::post('/funeral-plan/store','Admin\FuneralPlanController@store');	

	Route::get('/funeral-plan/edit/{id}','Admin\FuneralPlanController@edit');
	Route::get('/funeral-plan/image-update','Admin\FuneralPlanController@updateImage');

	Route::get('/funeral-plan/destroy/{id}','Admin\FuneralPlanController@destroy');

	Route::get('/funeral-plan/updateOrder','Admin\FuneralPlanController@updateOrder');
	////////////////////////////Funeral Plan Management////////////////////////////
	
	////////////////////////////Will Plan Management///////////////////////////
	Route::get('/will-plan','Admin\WillController@index');

	Route::any('/will-plan/perfomaction', 'Admin\WillController@perfomaction');
	Route::any('/will-plan/array-data','Admin\WillController@arrayData');

	Route::get('/will-plan/create','Admin\WillController@create');
	Route::post('/will-plan/store','Admin\WillController@store');	

	Route::get('/will-plan/edit/{id}','Admin\WillController@edit');
	Route::get('/will-plan/image-update','Admin\WillController@updateImage');

	Route::get('/will-plan/destroy/{id}','Admin\WillController@destroy');

	Route::get('/will-plan/updateOrder','Admin\WillController@updateOrder');
	////////////////////////////Will Plan Management////////////////////////////
	
	////////////////////////////Memorial Tag Management////////////////////////////
	Route::get('/memorial-tag','Admin\MemorialTagController@index');

	Route::any('/memorial-tag/perfomaction','Admin\MemorialTagController@perfomaction');
	Route::any('memorial-tag/array-data','Admin\MemorialTagController@arrayData');

	Route::get('/memorial-tag/create','Admin\MemorialTagController@create');
	Route::post('memorial-tag/store','Admin\MemorialTagController@store');

	Route::get('/memorial-tag/edit/{id}','Admin\MemorialTagController@edit');
	Route::get('/memorial-tag/image-update','Admin\MemorialTagController@updateImage');

	Route::get('/memorial-tag/destroy/{id}','Admin\MemorialTagController@destroy');

	Route::get('/memorial-tag/updateOrder','Admin\MemorialTagController@updateOrder');
	////////////////////////////Memorial Tag Management////////////////////////////
	
	////////////////////////////CMS Management////////////////////////////	
	Route::get('/cms','Admin\CMSController@index');

	Route::any('/cms/perfomaction','Admin\CMSController@perfomaction');
	Route::any('/cms/array-data','Admin\CMSController@arrayData');

	Route::get('/cms/create','Admin\CMSController@create');
	Route::post('/cms/store','Admin\CMSController@store');

	Route::get('/cms/edit/{id}','Admin\CMSController@edit');

	Route::get('/cms/destroy/{id}','Admin\CMSController@destroy');
	////////////////////////////CMS Management////////////////////////////  

	////////////////////////////Country Management////////////////////////////
	Route::get('/country','Admin\CountryController@index');

	Route::any('/country/perfomaction','Admin\CountryController@perfomaction');
	Route::any('/country/array-data','Admin\CountryController@arrayData');

	Route::get('/country/create','Admin\CountryController@create');
	Route::post('/country/store','Admin\CountryController@store');

	Route::get('/country/edit/{id}','Admin\CountryController@edit');

	Route::get('/country/destroy/{id}','Admin\CountryController@destroy');
	////////////////////////////Country Management////////////////////////////

	////////////////////////////State/City Management////////////////////////////
	Route::get('/state','Admin\StateController@index');

	Route::any('/state/perfomaction','Admin\StateController@perfomaction');
	Route::any('/state/array-data','Admin\StateController@arrayData');

	Route::get('/state/create','Admin\StateController@create');
	Route::post('/state/store','Admin\StateController@store');

	Route::get('/state/edit/{id}','Admin\StateController@edit');

	Route::get('/state/destroy/{id}','Admin\StateController@destroy');

	Route::any('/state/city/{id}','Admin\StateController@city');
	Route::get('/state/delete/{id}','Admin\StateController@cityDelete');
	Route::post('/state/update','Admin\StateController@cityUpdate');
	////////////////////////////State/City Management////////////////////////////

	////////////////////////////About Us Management////////////////////////////	
	Route::get('/about','Admin\AboutUsController@index');

	Route::any('/about/perfomaction','Admin\AboutUsController@perfomaction');
	Route::any('/about/array-data','Admin\AboutUsController@arrayData');

	Route::get('/about/create','Admin\AboutUsController@create');
	Route::post('/about/store','Admin\AboutUsController@store');

	Route::get('/about/edit/{id}','Admin\AboutUsController@edit');

	Route::get('/about/destroy/{id}','Admin\AboutUsController@destroy');
	////////////////////////////About Us Management////////////////////////////  

	/////////////////////////////////General Setting///////////////////////////////
	// Route::get('/setting','Admin\GeneralSettingController@index');

	// Route::any('/setting/perfomaction','Admin\GeneralSettingController@perfomaction');
	// Route::any('/setting/array-data','Admin\GeneralSettingController@arrayData');

	Route::get('/setting','Admin\GeneralSettingController@create');
	Route::post('/setting/store','Admin\GeneralSettingController@store');
	/////////////////////////////////General Setting///////////////////////////////

	/////////////////////////////////FAQ///////////////////////////////
	Route::get('/faq','Admin\FaqController@index');

	Route::any('/faq/perfomaction','Admin\FaqController@perfomaction');
	Route::any('/faq/array-data','Admin\FaqController@arrayData');

	Route::get('/faq/create','Admin\FaqController@create');
	Route::post('/faq/store','Admin\FaqController@store');

	Route::get('/faq/edit/{id}','Admin\FaqController@edit');

	Route::get('/faq/destroy/{id}','Admin\FaqController@destroy');
	/////////////////////////////////FAQ///////////////////////////////	
});

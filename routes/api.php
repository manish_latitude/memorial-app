<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//App User Api
Route::post('/signUp','Api\UserController@signUp');
Route::post('/verifyMail','Api\UserController@verifyMail');
Route::post('/signIn','Api\UserController@signIn');
Route::post('/socialLogin','Api\UserController@socialLogin');
Route::post('/forgotPassword','Api\UserController@forgotPassword');
Route::post('/resetPassword','Api\UserController@resetPassword');
Route::post('/resendOTP','Api\UserController@resendOTP');
Route::post('/getProfile','Api\UserController@getProfile');
Route::post('/editProfile','Api\UserController@editProfile');
Route::get('/generalSetting','Api\UserController@generalSetting');
Route::post('/contactMail','Api\UserController@contactMail');

//Funeral Api
Route::post('/funeralSignup','Api\FuneralController@funeralSignup');
Route::post('/verifyFuneralMail','Api\FuneralController@verifyFuneralMail');
Route::post('/funeralSignIn','Api\FuneralController@funeralSignIn');
Route::post('/funeralSocialLogin','Api\FuneralController@funeralSocialLogin');
Route::post('/funeralforgotPassword','Api\FuneralController@funeralforgotPassword');
Route::post('/funeralResetPassword','Api\FuneralController@funeralResetPassword');
Route::post('/funeralResendOTP','Api\FuneralController@funeralResendOTP');
Route::post('/getFuneral','Api\FuneralController@getFuneral');
Route::post('/addFuneral','Api\FuneralController@createFuneral');
Route::post('/editFuneral','Api\FuneralController@editFuneral');
Route::post('/funeralChangePassword','Api\FuneralController@funeralChangePassword');
Route::get('/funeralList','Api\FuneralController@funeralList');
Route::get('/cms','Api\FuneralController@cms');
Route::post('/obituaryList','Api\FuneralController@obituaryList');
Route::post('/faqList','Api\FuneralController@faqList');

//Member Api
Route::post('/memberList','Api\MemberController@memberList');
Route::post('/addMember','Api\MemberController@createMember');
Route::post('/getMember','Api\MemberController@getMember');
Route::post('/editMember','Api\MemberController@updateMember');
Route::post('/deleteMember','Api\MemberController@deleteMember');
Route::post('/changePassword','Api\MemberController@changePassword');
Route::get('/imageGallery','Api\MemberController@gallery');
Route::get('/orderOfService','Api\MemberController@orderOfService');
Route::post('/communityList','Api\MemberController@communityList');

//Visitor Api
Route::post('/condolenceList','Api\VisitorController@condolenceList');
Route::post('/flowerList','Api\VisitorController@flowerList');
Route::post('/bereavementList','Api\VisitorController@bereavementList');
Route::post('/funeralPlanList','Api\VisitorController@funeralPlanList');
Route::post('/willList','Api\VisitorController@willList');
Route::post('/memorialTagList','Api\VisitorController@memorialTagList');
Route::post('/searchFuneral','Api\VisitorController@searchFuneral');
Route::post('/funeralProfile','Api\VisitorController@funeralProfile');
Route::get('/countryList','Api\VisitorController@countryList');
Route::post('/stateList','Api\VisitorController@stateList');
Route::post('/cityList','Api\VisitorController@cityList');
Route::post('/countryFuneral','Api\VisitorController@countryFuneral');
// Route::post('/cityFuneral','Api\VisitorController@cityFuneral');
Route::post('/searchMember','Api\VisitorController@searchMember');
Route::post('/memberProfile','Api\VisitorController@memberProfile');
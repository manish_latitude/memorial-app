<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <!-- Required meta tags -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="#">

        <title>Memorial</title>

        @section('style')
        @include('web.master_layout.style')
        @show
        @stack('styles')

    </head>

    <body>
        @section('header_section')
        @include('web.master_layout.header')

        @show
        @stack('header')
        @section('main-content')  
        @show

        @section('footer_section')
        @include('web.master_layout.footer')
        @show

        @section('script')
        @include('web.master_layout.script')

        @show
        @stack('scripts')
    </body>
</html>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="user-scalable=no">
	<title></title>
	<script type="text/javascript" src="http://codewizinc.com/memorial/public/html5lightbox/jquery.js"></script>
	<script type="text/javascript" src="http://codewizinc.com/memorial/public/html5lightbox/html5lightbox.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- https://codepen.io/maulikdarji/pen/WwqdMO -->
	<!-- https://html5box.com/html5lightbox/ -->
	<style type="text/css">
		.container-fluidd{
			background-image: url("http://codewizinc.com/memorial/public/images/one.png");
			background-repeat: no-repeat;
			background-size: cover;
		}
		#gallery{
		  -webkit-column-count:4;
		  -moz-column-count:4;
		  column-count:4;
		  -webkit-column-gap:20px;
		  -moz-column-gap:20px;
		  column-gap:20px;
		}
		@media screen and (min-width:992px) and (max-width:1200px) {
			  #gallery{
			  -webkit-column-count:3;
			  -moz-column-count:3;
			  column-count:3;
			    
			  -webkit-column-gap:20px;
			  -moz-column-gap:20px;
			  column-gap:20px;
			}
		}
		@media screen and (min-width:768px) and (max-width:991px) {
			#gallery{
			  -webkit-column-count:3;
			  -moz-column-count:3;
			  column-count:3;
			    
			  -webkit-column-gap:20px;
			  -moz-column-gap:20px;
			  column-gap:20px;
			}
		}
		@media screen and (min-width:501px) and (max-width:767px) {
			#gallery{
			  -webkit-column-count:2;
			  -moz-column-count:2;
			  column-count:2;
			    
			  -webkit-column-gap:20px;
			  -moz-column-gap:20px;
			  column-gap:20px;
			}
		}
		@media screen and (min-width:320px) and (max-width:500px) {
			#gallery{
			  -webkit-column-count:2;
			  -moz-column-count:2;
			  column-count:2;
			}  
		}

		.card{
		  width:100%;
		  height:auto;
		  margin: 4% auto;
		  /*box-shadow:-3px 5px 15px #000;*/
		  cursor: pointer;
		  z-index: 9999;
		}
	</style>
</head>
<body class="container-fluidd">
	<div id="gallery" class="container-fluid"> 
		@foreach($mimg as $img)
			@foreach($img as $i)
				@if($user == 'member')
					@if($i->type == 1)
		    			<a href="{{ url('public/images/member/'.$i->image_video) }}" class="html5lightbox" data-group="mygroup" target="_blank"><img src="{{ url('public/images/member/'.$i->image_video) }}" onerror="this.src='{{ url('public/images/videoImg.png') }}'" class="card img-responsive"></a>
		    			<!-- <a href="#" onclick="test(this)" class="html5lightbox" data-group="mygroup" target="_blank"><img src="{{ url('public/images/member/'.$i->image_video) }}" onerror="this.src='{{ url('public/images/videoImg.png') }}'" class="card img-responsive"></a> -->
		    		@endif
		    		@if($i->type == 2)
		    			<a href="{{ url($i->image_video) }}" class="html5lightbox" data-group="mygroup" ><img src="{{ url('public/images/member/'.$i->image_video) }}" onerror="this.src='{{ url('public/images/videoImg.png') }}'" class="card img-responsive"></a>
		    		@endif
				@endif

				@if($user == 'funeral')
					@if($i->type == 1)
		    			<a href="{{ url('public/images/funeral/'.$i->image_video) }}" class="html5lightbox" data-group="mygroup"><img src="{{ url('public/images/funeral/'.$i->image_video) }}" onerror="this.src='{{ url('public/images/videoImg.png') }}'" class="card img-responsive"></a>
		    		@endif
		    		@if($i->type == 2)
		    			<a href="{{ url($i->image_video) }}" class="html5lightbox" data-group="mygroup"><img src="{{ url('public/images/funeral/'.$i->image_video) }}" onerror="this.src='{{ url('public/images/videoImg.png') }}'" class="card img-responsive"></a>
		    		@endif
				@endif
			@endforeach
		@endforeach
	</div>
</body>
<script type="text/javascript">
	function test(element) { 
		var newTab = window.open();
		setTimeout(function(){newTab.document.body.innerHTML = element.innerHTML;
		},500);
		 
		  return false;

	}

</script>
</html>
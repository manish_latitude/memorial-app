<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="user-scalable=no">
	<title></title>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- https://codepen.io/maulikdarji/pen/WwqdMO -->
	<style type="text/css">
		.container-fluidd{
			background-image: url("http://codewizinc.com/memorial/public/images/one.png");
			background-repeat: no-repeat;
			background-size: cover;
		}
		@media screen and (min-width:992px) and (max-width:1200px) {
			  #gallery{
			  -webkit-column-count:3;
			  -moz-column-count:3;
			  column-count:3;
			    
			  -webkit-column-gap:20px;
			  -moz-column-gap:20px;
			  column-gap:20px;
			}
		}
		@media screen and (min-width:768px) and (max-width:991px) {
			#gallery{
			  -webkit-column-count:3;
			  -moz-column-count:3;
			  column-count:3;
			    
			  -webkit-column-gap:20px;
			  -moz-column-gap:20px;
			  column-gap:20px;
			}
		}
		@media screen and (min-width:501px) and (max-width:767px) {
			#gallery{
			  -webkit-column-count:3;
			  -moz-column-count:3;
			  column-count:3;
			    
			  -webkit-column-gap:20px;
			  -moz-column-gap:20px;
			  column-gap:20px;
			}
		}
		@media screen and (min-width:320px) and (max-width:500px) {
			#gallery{
			  -webkit-column-count:3;
			  -moz-column-count:3;
			  column-count:3;
			    
			  -webkit-column-gap:20px;
			  -moz-column-gap:20px;
			  column-gap:20px;
			}  
		}

		.card{
		  width:100%;
		  height:200px;
		  margin: 5% auto;
		  box-shadow:-3px 5px 15px #000;
		  cursor: pointer;
		}
	</style>
</head>
<body class="container-fluidd">
<div id="gallery" class="container-fluid"> 
  	@foreach($user as $m)
	
		@foreach($template as $tmp)
			<input type="hidden" name="member" value="{{ $m->id }}">
			<a href="{{ url('member/orderOfService?user_id='.$m->id.'&tmp_id='.$tmp->id) }}">
				<img src="{{url('public/images/template/'.$tmp->template)}}" class="card img responsive">
			</a>
		@endforeach

		@endforeach
</div>
</body>
</html>
{{--<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		.container-fluidd{
			background-image: url("http://codewizinc.com/memorial/public/images/one.png");
			background-repeat: no-repeat;
			background-size: cover;
		}
		.column {
		    float: left;
		    width: 30%;
		}
		/* Clear floats after the columns */
		.row:after {
		    content: "";
		    display: block;
		    clear: both;
		}
		@media screen and (max-width: 600px) {
		    .column {
		        width: 100%;
		    }
		}
		/*.row {
			width:250px;
		}
		.row img{
		    display:block;
		    float:left;
		}*/
	</style>
</head>
<body class="container-fluidd">
	<div class="row" align="center">
		@foreach($user as $m)
		@foreach($template as $tmp)
			<div class="column">
				<input type="hidden" name="member" value="{{ $m->id }}">
				<a href="{{ url('member/orderOfService?user_id='.$m->id.'&tmp_id='.$tmp->id) }}">
					<img src="{{url('public/images/template/'.$tmp->template)}}" height="350" width="250px">
				</a>
			</div>
		@endforeach
		@endforeach
	</div>
</body>--}}
<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var count = $(".row img").size();
		var columns = Math.sqrt(count);
   		columns = parseInt(columns);        
		$(".row img").css('width', $(".row").width() / columns);
	});
</script> -->
<!-- </html> -->

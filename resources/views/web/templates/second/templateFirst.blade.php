<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Memorial template</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style type="text/css">
.bg_image {
	background-image: url("http://codewizinc.com/memorial/public/images/tem_4.png");
	background-repeat: no-repeat;
	height: 100vh;
	width: 100%;
	background-size: cover;
}
.brd {
	border: 10px solid #d0d0d0;
}
.inner_img_top {
	background-image: url("http://codewizinc.com/memorial/public/images/frame_4.png");
	background-position: center center;
	background-repeat: no-repeat;
	background-size: contain;
	position: absolute;
	/* top: 85px; */
	width: 100%;
	height: inherit;
}
.inner_img_bottom {
	background-image: url("http://codewizinc.com/memorial/public/images/tem_2.png");
	background-position: bottom left;
	background-repeat: no-repeat;
	background-size: 100%;
	position: absolute;
	bottom: 24px;
	width: 100%;
	height: 300px;
}
.center {
	text-align: center;
	position: absolute;
	/*top: 30px;*/
	color: white;
	padding-left: 10px;
	padding-right: 10px;
	z-index: 9999;
	color: #000;
}
.sample {
	display: block;
	margin: auto;
	width: 55%;
	height:40%;
	position: absolute;
	left: 22%;
	top: 36%;
	z-index: 10;
	opacity: 0;
	background-color: #CCCCCC;
}
.txt {

	background-color: transparent;
	/*height: 270px;*/
	margin-top: 17.5rem;
	width: 200px;
	height: 100px;
	border: none;
	outline: none;
	margin-left: 45px;
	color: #000;
	font-weight: 500;
}
.disply_grid {
	display: grid;
	padding-left: 30px;
}
.mg_lft {
	margin-left: 10px;
}
.txt_lft {
	text-align: left;
}
.txt_rgt {
	text-align: right;
}
.txt_cen {
	text-align: center;
}
.wd_100 {
	width: 100%;
}
.fnt_16px {
	font-size:16px;
}
.btnImage {
	position:absolute;
    width:55%;
    margin: auto;
    z-index:10;
    left: 22%;
    bottom: 15%;
}
.avatar-zone {
    background-color:#CCCCCC;
    display: block;
	margin: auto;
	width: 55%;
	height: 40%;
	position: absolute;
	left: 22%;
	top: 36%;
}
.dvPreview {
    filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=image);
   position:absolute;
    width:60%;
    height:40%;
    margin: auto;
    z-index:10;
    left: 22%;
    top: 36%;
    display: none;
}
@media screen and (min-width:992px) and (max-width:1200px) {
	#loader{
		display:    none;
	    position:   fixed;
	    z-index:    1000;
	    height:     100%;
	    width:      100%;
	    background: url('http://ajaxloadingimages.net/Content/Resources/Svg/watermill.svg') 
                50% 50% 
                no-repeat;
	}
}
@media screen and (min-width:768px) and (max-width:991px) {
	#loader{
		display:    none;
	    position:   fixed;
	    z-index:    1000;
	    height:     100%;
	    width:      100%;
	    background: url('http://ajaxloadingimages.net/Content/Resources/Svg/watermill.svg') 
                50% 50% 
                no-repeat;
	}
}
@media screen and (min-width:501px) and (max-width:767px) {
	#loader{
		display:    none;
	    position:   fixed;
	    z-index:    1000;
	    height:     100%;
	    width:      100%;
	    background: url('http://ajaxloadingimages.net/Content/Resources/Svg/watermill.svg') 
                50% 50% 
                no-repeat;
	}	
}
@media screen and (min-width:320px) and (max-width:500px) {
	#loader{
		display:    none;
	    position:   fixed;
	    z-index:    1000;
	    height:     100%;
	    width:      100%;
	    background: url('http://ajaxloadingimages.net/Content/Resources/Svg/watermill.svg') 
                50% 50% 
                no-repeat;
	}	
}
body.loading #loader {
	overflow: hidden;   
}

body.loading #loader {
    display: block;
}
@media only screen and (max-width: 320px) {
.txt {
	margin-top: 10.5rem;
}
</style>
</head>

<body>
	<div id="html-content-holder">
		<div class="bg_image">
			<input type="hidden" name="user_id" id="user_id" value="{{ $user_id }}">
  			<div class="inner_img_top">&nbsp;</div>
  			<span class="center">	
  				<textarea class="txt">Name</textarea>
  			</span> 
  			<span> 
  				<div class="avatar-zone" style="background-image: url(http://codewizinc.com/memorial/public/images/avtar1.png); background-repeat: no-repeat; background-position: center;">
  				</div>  
  				<div class="dvPreview"></div>
  				<input type="file" class="sample" id="fileupload"/> 
  				<div id="loader"></div>
  				<!-- <img class="sample" src="http://codewizinc.com/memorial/public/images/sample.png" alt="image">  -->
  			</span> 
  			<input id="btn-Preview-Image" type="button" value="Save Template" class="btnImage" />
  		</div>
	</div>
</body>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
<script language="javascript" type="text/javascript">
	$(function () {
		$("#fileupload").change(function () {

			var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
			if (regex.test($(this).val().toLowerCase())) {
	            if ($.browser.msie && parseFloat(jQuery.browser.version) <= 9.0) {
	            	$(".avatar-zone").hide();
	                $(".dvPreview").show();
	                $(".dvPreview")[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = $(this).val();
	            }else{
	            	if (typeof (FileReader) != "undefined") {
	            		$(".avatar-zone").hide();
                		$(".dvPreview").show();
                		$(".dvPreview img:last-child").remove();
                		$(".dvPreview").append("<img />");
            
                		var reader = new FileReader();
                		reader.onload = function (e) {
                    		$(".dvPreview img").attr("src", e.target.result);
                    		$(".dvPreview img").attr({ width: '175px', height: '200px' });
                		}
                		reader.readAsDataURL($(this)[0].files[0]);
            		} else {
                		alert("This browser does not support FileReader.");
            		}
        		}
	        }else{
	        	alert("Please upload a valid image file.");
	        }
		});

		var element = $("#html-content-holder"); // global variable
		var getCanvas; // global variable
	    $("#btn-Preview-Image").on('click', function () {
	    	$('#btn-Preview-Image').hide();
	        html2canvas(element, {
	        onrendered: function (canvas) {
	        		var myImage = canvas.toDataURL();
	            	downloadURI(myImage,'image.png');
	            }
			});
	    });

	    function downloadURI(uri, name) {
	    	var link = document.createElement("img");
		    link.name = name;
		    link.src = uri;
		    var uid = $('#user_id').val();
		    $.ajax({
		    	type: "post",
		    	url: "{{ url('member/imageStore') }}",
		    	data: { src: uri, user_id: uid },
		    	dataType: "json",
		    	beforeSend: function(){
		    		$("body").addClass("loading");
		    	},
		    	success: function(response){
		    		window.location.href = "{{ url('member/imageJson') }}?json="+response['json'];
		    	},
		    	complete: function(data){
		    		$("body").removeClass("loading");
		    	}
		    });
	    }
	});
</script>
</html>

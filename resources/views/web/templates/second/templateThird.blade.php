<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Memorial template</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="http://codewizinc.com/memorial/resources/admin-assets/css/template4.css">

<style type="text/css">
.btnImage {
	position:absolute;
    width:55%;
    margin: auto;
    z-index:10;
    left: 22%;
    bottom: 15%;
}
@media screen and (min-width:992px) and (max-width:1200px) {
	#loader{
		display:    none;
	    position:   fixed;
	    z-index:    1000;
	    height:     100%;
	    width:      100%;
	    background: url('http://ajaxloadingimages.net/Content/Resources/Svg/watermill.svg') 
                50% 50% 
                no-repeat;
	}
}
@media screen and (min-width:768px) and (max-width:991px) {
	#loader{
		display:    none;
	    position:   fixed;
	    z-index:    1000;
	    height:     100%;
	    width:      100%;
	    background: url('http://ajaxloadingimages.net/Content/Resources/Svg/watermill.svg') 
                50% 50% 
                no-repeat;
	}
}
@media screen and (min-width:501px) and (max-width:767px) {
	#loader{
		display:    none;
	    position:   fixed;
	    z-index:    1000;
	    height:     100%;
	    width:      100%;
	    background: url('http://ajaxloadingimages.net/Content/Resources/Svg/watermill.svg') 
                50% 50% 
                no-repeat;
	}	
}
@media screen and (min-width:320px) and (max-width:500px) {
	#loader{
		display:    none;
	    position:   fixed;
	    z-index:    1000;
	    height:     100%;
	    width:      100%;
	    background: url('http://ajaxloadingimages.net/Content/Resources/Svg/watermill.svg') 
                50% 50% 
                no-repeat;
	}	
}
body.loading #loader {
	overflow: hidden;   
}

body.loading #loader {
    display: block;
}

</style>
</head>

<body>
	<div id="html-content-holder">
		<input type="hidden" name="user_id" id="user_id" value="{{ $user_id }}">
		<div class="bg_image brd"> 
			<span class="center">
  				<h1><input class="txt" type="text" value="Lorem Ipsum"></h1>
  				<h2><input class="txt" type="text" value="Lorem Ipsum"></h2>
  				<h3><input class="txt" type="text" value="Lorem Ipsum"></h3>
  <textarea class="txt">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</textarea>
  <textarea class="txt">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</textarea>
  </span> 
</div>
<input id="btn-Preview-Image" type="button" value="Save Template" class="btnImage" />
</div>
</body>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
<script language="javascript" type="text/javascript">
	$(function () {
		var element = $("#html-content-holder"); // global variable
		var getCanvas; // global variable
	    $("#btn-Preview-Image").on('click', function () {
	    	$('#btn-Preview-Image').hide();
	        html2canvas(element, {
	        onrendered: function (canvas) {
	        		var myImage = canvas.toDataURL();
	            	downloadURI(myImage,'image.png');
	            }
			});
	    });

	    function downloadURI(uri, name) {
	    	var link = document.createElement("img");
		    link.name = name;
		    link.src = uri;
		    var uid = $('#user_id').val();
		    $.ajax({
		    	type: "post",
		    	url: "{{ url('member/imageStore') }}",
		    	data: { src: uri, user_id: uid },
		    	dataType: "json",
		    	beforeSend: function(){
		    		$("body").addClass("loading");
		    	},
		    	success: function(response){
		    		window.location.href = "{{ url('member/imageJson') }}?json="+response['json'];
		    	},
		    	complete: function(data){
		    		$("body").removeClass("loading");
		    	}
		    });
	    }
	    var textarea = document.querySelector('textarea');

      textarea.addEventListener('keydown', autosize);
                   
      function autosize(){
        var el = this;
        setTimeout(function(){
          el.style.cssText = 'height:auto; padding:0';
          // for box-sizing other than "content-box" use:
          // el.style.cssText = '-moz-box-sizing:content-box';
          el.style.cssText = 'height:' + el.scrollHeight + 'px';
        },0);
}
    });
</script>
</html>

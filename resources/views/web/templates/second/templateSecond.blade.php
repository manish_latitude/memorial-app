<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Memorial template</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="http://codewizinc.com/memorial/resources/admin-assets/css/template4.css">
<style type="text/css">

.btnImage {
  position:absolute;
    width:55%;
    margin: auto;
    z-index:10;
    left: 22%;
}
@media screen and (min-width:992px) and (max-width:1200px) {
  #loader{
    display:    none;
      position:   fixed;
      z-index:    1000;
      height:     100%;
      width:      100%;
      background: url('http://ajaxloadingimages.net/Content/Resources/Svg/watermill.svg') 
                50% 50% 
                no-repeat;
  }
}
@media screen and (min-width:768px) and (max-width:991px) {
  #loader{
    display:    none;
      position:   fixed;
      z-index:    1000;
      height:     100%;
      width:      100%;
      background: url('http://ajaxloadingimages.net/Content/Resources/Svg/watermill.svg') 
                50% 50% 
                no-repeat;
  }
}
@media screen and (min-width:501px) and (max-width:767px) {
  #loader{
    display:    none;
      position:   fixed;
      z-index:    1000;
      height:     100%;
      width:      100%;
      background: url('http://ajaxloadingimages.net/Content/Resources/Svg/watermill.svg') 
                50% 50% 
                no-repeat;
  } 
}
@media screen and (min-width:320px) and (max-width:500px) {
  #loader{
    display:    none;
      position:   fixed;
      z-index:    1000;
      height:     100%;
      width:      100%;
      background: url('http://ajaxloadingimages.net/Content/Resources/Svg/watermill.svg') 
                50% 50% 
                no-repeat;
  } 
}
body.loading #loader {
  overflow: hidden;   
}

body.loading #loader {
    display: block;
}

</style>
</head>

<body>
  <div id="html-content-holder">
    <div id="loader"></div>
    <input type="hidden" name="user_id" id="user_id" value="{{ $user_id }}">
<div class="bg_image brd clr">
  <div class="col-xs-12">
    <h2 class="txt_cen"><input class="txt" type="text" value="Lorem Ipsum"></h2>
    <table class="wd_100">
      <tr>
        <td class="txt_lft"><input class="txt" type="text" value="Alfreds Futterkiste"></td>
        <td class="txt_rgt"><input class="txt" type="text" value="Maria Anders"></td>
      </tr>
      <tr>
       <td class="txt_lft"><input class="txt" type="text" value="Alfreds Futterkiste"></td>
        <td class="txt_rgt"><input class="txt" type="text" value="Maria Anders"></td>
      </tr>
      <tr>
        <td class="txt_lft"><input class="txt" type="text" value="Alfreds Futterkiste"></td>
       <td class="txt_rgt"><input class="txt" type="text" value="Maria Anders"></td>
      </tr>
      <tr>
        <td class="txt_lft"><input class="txt" type="text" value="Alfreds Futterkiste"></td>
       <td class="txt_rgt"><input class="txt" type="text" value="Maria Anders"></td>
      </tr>
      <tr>
        <td class="txt_lft"><input class="txt" type="text" value="Alfreds Futterkiste"></td>
        <td class="txt_rgt"><input class="txt" type="text" value="Maria Anders"></td>
      </tr>
      <tr>
        <td class="txt_lft"><input class="txt" type="text" value="Alfreds Futterkiste"></td>
        <td class="txt_rgt"><input class="txt" type="text" value="Maria Anders"></td>
      </tr>
    </table>
    <h2 class="txt_cen"><input class="txt" type="text" value="Lorem Ipsum"></h2>
    <table class="wd_100">
      <tr>
       <td class="txt_lft"><input class="txt" type="text" value="Alfreds Futterkiste"></td>
       <td class="txt_rgt"><input class="txt" type="text" value="Maria Anders"></td>
      </tr>
      <tr>
        <td class="txt_lft"><input class="txt" type="text" value="Alfreds Futterkiste"></td>
       <td class="txt_rgt"><input class="txt" type="text" value="Maria Anders"></td>
      </tr>
      <tr>
       <td class="txt_lft"><input class="txt" type="text" value="Alfreds Futterkiste"></td>
        <td class="txt_rgt"><input class="txt" type="text" value="Maria Anders"></td>
      </tr>
      <tr>
       <td class="txt_lft"><input class="txt" type="text" value="Alfreds Futterkiste"></td>
        <td class="txt_rgt"><input class="txt" type="text" value="Maria Anders"></td>
      </tr>
      <tr>
        <td class="txt_lft"><input class="txt" type="text" value="Alfreds Futterkiste"></td>
        <td class="txt_rgt"><input class="txt" type="text" value="Maria Anders"></td>
      </tr>
      <tr>
      <td class="txt_lft"><input class="txt" type="text" value="Alfreds Futterkiste"></td>
       <td class="txt_rgt"><input class="txt" type="text" value="Maria Anders"></td>
      </tr>
    </table>
    <input id="btn-Preview-Image" type="button" value="Save template" class="btnImage" />
  </div>
</div>
</div>
</body>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
<script language="javascript" type="text/javascript">
  $(function () {
    var element = $("#html-content-holder"); // global variable
    var getCanvas; // global variable
      $("#btn-Preview-Image").on('click', function () {
        $('#btn-Preview-Image').hide();
          html2canvas(element, {
          onrendered: function (canvas) {
              var myImage = canvas.toDataURL();
                downloadURI(myImage,'image.png');
              }
      });
      });

      function downloadURI(uri, name) {
        var link = document.createElement("img");
        link.name = name;
        link.src = uri;
        var uid = $('#user_id').val();
        $.ajax({
          type: "post",
          url: "{{ url('member/imageStore') }}",
          data: { src: uri, user_id: uid },
          dataType: "json",
          beforeSend: function(){
            $("body").addClass("loading");
          },
          success: function(response){
            window.location.href = "{{ url('member/imageJson') }}?json="+response['json'];
          },
          complete: function(data){
            $("body").removeClass("loading");
          }
        });
      }
  });
</script>
</html>

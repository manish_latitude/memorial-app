<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="user-scalable=no">
	<title></title>
	<style type="text/css">
		body{
			background-image: url(http://codewizinc.com/memorial/public/images/one.png);
			background-repeat: no-repeat;
			background-size: cover;
		}
		h1{
			color: white;
		}	
		p{
			color: white;
		}
	</style>
</head>
<body>
	@foreach($data as $dt)
		<h1>{{ $dt->title }}</h1>
		<p>{!! $dt->description !!}</p>
	@endforeach
</body>
</html>
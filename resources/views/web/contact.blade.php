<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Contact Us</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('http://codewizinc.com/memorial/resources/admin-assets/css/contactStyle.css') }}">
<script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>
<div class="bg_image">
  <div class="img_top">&nbsp;
    @if (Session::has('message'))
      <div class="alert alert-info alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
      <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
      <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('success') !!}</div>
    @endif
    <form action="{{ url('web/contact') }}" method="get">
    <div class="flt_lft">
        <div class="txt_align <?php echo $errors->first('nm') !== '' ? 'has-error' : '';?>">
          <span class="center">
            <input class="lft_txt fnt_16px" type="text" name="nm" placeholder="Name"><br>
            <span class="text-danger">{{ $errors->first('nm') }}</span>
          </span>
            <div class="pd_20px <?php echo $errors->first('email') !== '' ? 'has-error' : '';?>">
              <span class="center">
                <input class="rght_txt fnt_16px" type="text" name="email" placeholder="Email">
              </span>
            </div>
            <span class="text-danger">{{ $errors->first('email') }}</span>
        </div>
        <span>
          <p class="area">Area of Interest</p>
        </span>
        <div class="col-xs-12 text_align_center">

          <div class="col-xs-6 pd_0px">
            <label class="checkbox_label">Family Friend
              <input type="checkbox" name="chk[]" value="Family Friend">
              <span class="checkmark"></span>
            </label>
          </div>
          <div class="col-xs-6 pd_0px">
            <label class="checkbox_label">Affiliates
              <input type="checkbox" name="chk[]" value="Affiliates">
              <span class="checkmark"></span>
            </label>
          </div>
          <div class="col-xs-6 pd_0px">
            <label class="checkbox_label">Funeral Home
              <input type="checkbox" name="chk[]" value="Funeral Home">
              <span class="checkmark"></span>
            </label>
          </div>
          <div class="col-xs-6 pd_0px">
            <label class="checkbox_label">Suppliers
              <input type="checkbox" name="chk[]" value="Suppliers">
              <span class="checkmark"></span>
            </label>
          </div>
          <div class="col-xs-6 pd_0px">
            <label class="checkbox_label">Associations
              <input type="checkbox" name="chk[]" value="Associations">
              <span class="checkmark"></span>
            </label>
          </div>
          <div class="col-xs-6 pd_0px">
            <label class="checkbox_label">Partners
              <input type="checkbox" name="chk[]" value="Partners">
              <span class="checkmark"></span>
            </label>
          </div>
          <div class="col-xs-6 pd_0px">
            <label class="checkbox_label">Other
              <input type="checkbox" name="chk[]" value="Other">
              <span class="checkmark"></span>
            </label>
          </div>
          <div class="col-xs-6 pd_0px">
            <label class="checkbox_label">Media
              <input type="checkbox" name="chk[]" value="Media">
              <span class="checkmark"></span>
            </label>
          </div>
        </div>
  </div>
  <div class="msg">
    <textarea rows="6" cols="50" name="msg" placeholder="   Message"></textarea> 
  </div>
  <center><div class="g-recaptcha captch" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
  </div></center>  
  <div class="sbmt">
    <button type="submit" value="Submit">Submit</button>
  </div>
  </form>
</div>
</div>
</body>
</html>

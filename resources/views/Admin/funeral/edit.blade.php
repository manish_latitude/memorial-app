@extends('Admin.master_layout.master')

@section('title', 'Edit Funeral')

@section('breadcum')
     / Edit Funeral
@endsection

@section('content')

<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>
        {!! Form::open(array('url' => ('admin/funeral/update'),'method'=>'POST', 'files'=>true)) !!}
        <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- <div class="box-primary"> -->
            <div class="x_panel">
                <!-- <div class="x_content"> -->
                <div class="x_title">
                    <h4>Funeral Details</h4>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-6">
                    <input type="hidden" name="id" value="{{ $funeral->id }}">
                    <div class="form-group <?php  echo $errors->first('home_name') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('home_name', 'Funeral home name',array('class'=>'control-label')) !!}
                        <span class="required">*</span>
                        {{ Form::text('home_name', $funeral->home_name,array('class' => 'form-control', 'id' => 'home_name','placeholder' => 'Home Name')) }}
                        <span class="text-danger" >{{ $errors->first('home_name') }}</span>
                    </div>

                    <div class="form-group <?php  echo $errors->first('person_name') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('person_name'   , 'Contact person name',array('class'=>'control-label')) !!}
                        <span class="required">*</span>
                        {{ Form::text('person_name', $funeral->person_name,array('class' => 'form-control', 'id' => 'person_name','placeholder' => 'Person Name')) }}
                        <span class="text-danger" >{{ $errors->first('person_name') }}</span>
                    </div>

                    <div class="form-group <?php  echo $errors->first('address') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('address', 'Funeral home address',array('class'=>'control-label')) !!}
                        <span class="required">*</span>
                        {{ Form::textarea('address', $funeral->address, array('class' => 'form-control', 'id' => 'address','rows'=>'3','placeholder' => 'Home Address')) }}
                        <span class="text-danger" >{{ $errors->first('address') }}</span>
                    </div>

                    <div class="form-group <?php  echo $errors->first('country') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('country', 'Country',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {!! Form::select('country',isset($country)?$country:[],$funeral->country,['placeholder' => '-- Select Country --','class' => 'form-control','id'=>'country']) !!}
                    
                    <span class="text-danger" >{{ $errors->first('country') }}</span>
                    </div>

                    <!-- <div class="form-group <?php  echo $errors->first('state') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('state', 'State',array('class'=>'control-label')) !!}
                        <span class="required">*</span>
                        {!! Form::select('state',isset($state)?$state:[],$funeral->state,['placeholder' => '-- Select State --','class' => 'form-control','id'=>'state']) !!}
                        <span class="text-danger" >{{ $errors->first('state') }}</span>
                    </div>

                    <div class="form-group <?php  echo $errors->first('city') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('city', 'City',array('class'=>'control-label')) !!}
                        <span class="required">*</span>
                        {!! Form::select('city',isset($city)?$city:[],$funeral->city,['placeholder' => '-- Select City --','class' => 'form-control','id'=>'city']) !!}
                        <span class="text-danger" >{{ $errors->first('city') }}</span>
                    </div> -->

                    <div class="form-group <?php  echo $errors->first('phone_number') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('phone', 'Phone',array('class'=>'control-label')) !!}
                        <span class="required">*</span>
                        {{ Form::text('phone_number', $funeral->phone, array('class' => 'form-control', 'id' => 'phone','placeholder' => 'Mobile Number')) }}
                        <span class="text-danger" >{{ $errors->first('phone_number') }}</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group <?php  echo $errors->first('zip') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('zip', 'Zip/Code',array('class'=>'control-label')) !!}
                        <span class="required">*</span>
                        {{ Form::text('zip', $funeral->zip, array('class' => 'form-control', 'id' => 'zip','placeholder' => 'Zip/Code')) }}
                        <span class="text-danger" >{{ $errors->first('zip') }}</span>
                    </div>

                    <div class="form-group <?php  echo $errors->first('email') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('email', 'Email',array('class'=>'control-label')) !!}
                        <span class="required">*</span>
                        {{ Form::text('email', $funeral->email, array('class' => 'form-control', 'id' => 'email','placeholder' => 'Email')) }}
                        <span class="text-danger" >{{ $errors->first('email') }}</span>
                    </div>

                    <div class="form-group <?php  echo $errors->first('website') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('website', 'Website',array('class'=>'control-label')) !!}
                        {{ Form::text('website', $funeral->website, array('class' => 'form-control', 'id' => 'website','placeholder' => 'Website Url')) }}
                        <span class="text-danger" >{{ $errors->first('website') }}</span>
                    </div>

                    <div class="form-group <?php  echo $errors->first('facebook') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('facebook', 'Facebook',array('class'=>'control-label')) !!}
                        {{ Form::text('facebook', $funeral->facebook, array('class' => 'form-control', 'id' => 'facebook','placeholder' => 'Facebook Url')) }}
                        <span class="text-danger" >{{ $errors->first('facebook') }}</span>
                    </div>

                    <div class="form-group <?php  echo $errors->first('twitter') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('twitter', 'Twitter',array('class'=>'control-label')) !!}
                        {{ Form::text('twitter', $funeral->twitter, array('class' => 'form-control', 'id' => 'twitter','placeholder' => 'Twitter Url')) }}
                        <span class="text-danger" >{{ $errors->first('twitter') }}</span>
                    </div>

                    <div class="form-group <?php  echo $errors->first('linkedin') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('linkedin', 'Linkedin',array('class'=>'control-label')) !!}
                        {{ Form::text('linkedin', $funeral->linkedin, array('class' => 'form-control', 'id' => 'linkedin','placeholder' => 'Linkedin Url')) }}
                        <span class="text-danger" >{{ $errors->first('linkedin') }}</span>
                    </div>

                    <div class="form-group <?php  echo $errors->first('profile-image') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('profile_image', 'Profile Image',array('class'=>'control-label')) !!}
                        <span class="required">*</span>
                        {{ Form::file('profile_image', null, array('class' => 'form-control', 'id' => 'profile_image')) }}
                        <br>
                        <!-- <div id="image_div">
                            <p class="img_wrapper"> -->
                                    <center><img id="myImg" width="120px" height="90px" src="{{url('public/images/funeral-profile/'.$funeral->image)}}" name="image"  onerror="this.src='{{ url("public/images/avatar.png") }}'"></center>
                                    <br>
                                    <center><a href="javascript:void(0)" onclick="profile('{{ $funeral->id }}')">Delete</a></center>

                            <!-- </p>
                        </div>
                         -->
                        <span class="text-danger" >{{ $errors->first('profile_image') }}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>Funeral Profile</h4>
                    <div class="clearfix"></div>
                </div>
        
                @foreach($profile as $pro)
                    @foreach($pro as $key => $p)

                    <div id="profile">
                        <div class="form-group <?php  echo $errors->first('title') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('title', 'Title',array('class'=>'control-label')) !!}
                            {{ Form::text('title[]', $p->title, array('class' => 'form-control', 'id' => 'title','placeholder' => 'Title')) }}
                            <span class="text-danger" >{{ $errors->first('title') }}</span>
                        </div>                        

                        <div class="form-group <?php  echo $errors->first('description') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('description', 'Description',array('class'=>'control-label')) !!}
                            {{ Form::textarea('description[]', $p->description, array('class' => 'form-control', 'id' => 'summary-ckeditor0')) }}
                            <span class="text-danger" >{{ $errors->first('description') }}</span>
                        </div>
                    </div>
                    @endforeach
                @endforeach
                <div id="profile1"></div>
                <input type="button" id="add" value="Add More" class="btn btn-sm btn-xs btn-success pull-right"/>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">        
                <div class="x_title">
                    <h4>Funeral Images</h4>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group <?php  echo $errors->first('image') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('image', 'Images',array('class'=>'control-label')) !!}
                    <input type="file" name='image[]' id="files" multiple="">
                    <div id="selectedFiles"></div>
                    <br>
                    @foreach($fimg as $img)
                        @foreach($img as $i)
                            <img width="100px" height="100px" src="{{url('public/images/funeral/'.$i->image_video)}}" name="update_image[]" >
                            <a href="javascript:void(0)" onclick="img('{{ $i->id }}');">Delete</a>&nbsp;&nbsp;
                        @endforeach
                    @endforeach
                    <span class="text-danger" >{{ $errors->first('image') }}</span>
                </div>

                

                <div class="form-group <?php echo $errors->first('video') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('video','Videos',array('class' => 'control-label')) !!}
                    <!-- <input class="form-control" type="text" name="video[]" id="video" placeholder="Video Url"> -->
                    @foreach($fv as $v)
                        @foreach($v as $v1)
                            {{ Form::text('video[]',$v1->image_video, array('class' => 'form-control', 'id' => 'update_video','placeholder' => 'Video Url')) }}<br>
                        @endforeach
                    @endforeach
                    <span class="text-danger" >{{ $errors->first('video') }}</span>
                </div>
                <div id="videoid"></div>
                <input type="button" id="addVideo" value="Add More" class="btn btn-sm btn-xs btn-success pull-right">
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="box-footer">
                    {!! Form::submit('Update', array('class' => 'btn btn-primary submit')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('admin/funeral') }}">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('public/ckeditor/ckeditor.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function(){

       CKEDITOR.replace('summary-ckeditor0');

        $("#add").click(function(){

            var $div = $('textarea[id^="summary-ckeditor"]:last');
            var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;

            $("#profile1").append('<div class="form-group <?php  echo $errors->first('title') !== '' ? 'has-error' : '';?>"><label class="control-label">Title</label><input class="form-control" type="text" name="title[]" id="title"><span class="text-danger">{{ $errors->first('title') }}</span> <div class="form-group <?php  echo $errors->first('description') !== '' ? 'has-error' : '';?>"> <label class="control-label">Description</label> <textarea name="description[]" class="form-control" id="summary-ckeditor'+ num +'"> <span class="text-danger">{{ $errors->first('description') }}</span>');

                CKEDITOR.replace('summary-ckeditor'+num);
        });
        $("#addVideo").click(function(){
            $("#videoid").append('<div class="form-group <?php echo $errors->first('video') !== '' ? 'has-error' : '';?>"><input class="form-control" type="text" name="video[]" id="video" placeholder="Video Url"><span class="text-danger" >{{ $errors->first('video') }}</span>');
        });

        $('select[name="state"]').on('change',function(){
            var stateId = $(this).val();

            if(stateId){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('admin/funeral/cityList') }}?state_id="+stateId,
                    dataType: "json",
                    success: function(data){
                        $('select[name="city"]').html('<option value="">-- Select State --</option>');
                        $.each(data,function(key, value){
                            $('select[name="city"]').append('<option value="'+key+'">'+value+'</option>');
                        });
                    }
                });
            }else{
                $('select[name="city"]').empty();
            }
        });
    });
</script>

<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);

    function img(id){
        //alert(id);
        $.ajax({
            url: "{{ url('admin/funeral/image-update') }}",
            type: 'GET',
            async: false,
            data: {'id':id},
            success: function(data){
                if(data == "success"){
                    location.reload();
                }
            },
            error: function(){
                alert('error');
            }
        })
    }

    function profile(id){
        $.ajax({
            url: "{{ url('admin/funeral/profile-update') }}",
            type: 'GET',
            data: {'id':id},
            success: function(data){
                if(data == "true"){
                    location.reload();
                }
            },
            error: function(){
                alert('error');
            }
        })
    }

    $(document).ready(function(){
        $('select[name="country"]').on('change',function(){
            var countryId = $(this).val();

            if(countryId){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('admin/funeral/stateList') }}?country_id="+countryId,
                    dataType: "json",
                    success: function(res){
                        if(res){
                            $('select[name="state"]').html('<option value="">-- Select State --</option>');
                            $.each(res,function(key,value){
                                $('select[name="state"]').append('<option value="'+key+'">'+value+'</option>');
                            });
                        }else{
                            $('select[name="state"]').empty();
                        }
                    }
                });
            }else{
                $('select[name="state"]').append('<option>-- Select State --</option>');
                $('select[name="city"]').empty();
            }
        });
        $('select[name="state"]').on('change',function(){
            var stateId = $(this).val();

            if(stateId){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('admin/funeral/cityList') }}?state_id="+stateId,
                    dataType: "json",
                    success: function(data){
                        $('select[name="city"]').html('<option value="">-- Select State --</option>');
                        $.each(data,function(key, value){
                            $('select[name="city"]').append('<option value="'+key+'">'+value+'</option>');
                        });
                    }
                });
            }else{
                $('select[name="city"]').empty();
            }
        });
    });

    $('body').append('<div class="product-image-overlay"><span class="product-image-overlay-close">x</span><img src="" /></div>');
    var productImage = $('img');
    var productOverlay = $('.product-image-overlay');
    var productOverlayImage = $('.product-image-overlay img');

    productImage.click(function () {
        var productImageSource = $(this).attr('src');

        productOverlayImage.attr('src', productImageSource);
        productOverlay.fadeIn(100);
        $('body').css('overflow', 'hidden');

        $('.product-image-overlay-close').click(function () {
            productOverlay.fadeOut(100);
            $('body').css('overflow', 'auto');
        });
    });

    var selDiv = "";
    document.addEventListener("DOMContentLoaded", init, false);
    function init(){
        document.querySelector('#files').addEventListener('change',handleFileSelect,false);
        selDiv = document.querySelector('#selectedFiles');
    }

    function handleFileSelect(e){
        if(!e.target.files){
            return;
        } 
        selDiv.innerHTML = "";
        var files = e.target.files;
        for(var i=0; i<files.length; i++){
            var f = files[i];
            selDiv.innerHTML += f.name + "<br/>";
        }
    }
</script>
@endpush('scripts')
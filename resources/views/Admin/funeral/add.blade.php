@extends('Admin.master_layout.master')

@section('title', 'Create Funeral')

@section('breadcum')
     / Create Funeral
@endsection

@section('content')

<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

    {!! Form::open(array('url' => ('admin/funeral/store'),'method'=>'POST', 'files'=>true)) !!}
    <div class="col-md-12 col-sm-12 col-xs-12">
<!--             <div class="box-primary"> -->

        <!-- <div class="x_content"> -->
        <div class="x_panel">
            <div class="x_title">
                <h4>Funeral Details</h4>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-6">
                <div class="form-group <?php  echo $errors->first('home_name') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('home_name', 'Funeral home name',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {{ Form::text('home_name', null,array('class' => 'form-control', 'id' => 'home_name','placeholder' => 'Home Name')) }}
                    <span class="text-danger" >{{ $errors->first('home_name') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('person_name') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('person_name'   , 'Contact person name',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {{ Form::text('person_name', null,array('class' => 'form-control', 'id' => 'person_name','placeholder' => 'Person Name')) }}
                    <span class="text-danger" >{{ $errors->first('person_name') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('address') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('address', 'Funeral home address',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {{ Form::textarea('address', null, array('class' => 'form-control', 'id' => 'address','rows'=>'3','placeholder' => 'Home Address')) }}
                    <span class="text-danger" >{{ $errors->first('address') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('country') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('country', 'Country',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    <select name="country" id="country" class="form-control">
                        <option value="">-- Select Country --</option>
                        @foreach($country as $c)
                            <option value="{{ $c->id }}">{{ $c->country }}</option>
                        @endforeach
                    </select>
                    <span class="text-danger" >{{ $errors->first('country') }}</span>
                </div>

                <!-- <div class="form-group <?php  echo $errors->first('state') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('state', 'State',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    <select name="state" id="state" class="form-control">
                        <option value="">-- Select State --</option>
                    </select>
                    <span class="text-danger" >{{ $errors->first('state') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('city') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('city', 'City',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    <select name="city" id="city" class="form-control">
                        <option value="">-- Select City --</option>
                    </select>
                    <span class="text-danger" >{{ $errors->first('city') }}</span>
                </div> -->

                <div class="form-group <?php  echo $errors->first('phone_number') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('phone', 'Mobile',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {{ Form::text('phone_number', null, array('class' => 'form-control', 'id' => 'phone','placeholder' => 'Mobile Number')) }}
                    <span class="text-danger" >{{ $errors->first('phone_number') }}</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group <?php  echo $errors->first('zip') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('zip', 'Zip/Code',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {{ Form::text('zip', null, array('class' => 'form-control', 'id' => 'zip','placeholder' => 'Zip/Code')) }}
                    <span class="text-danger" >{{ $errors->first('zip') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('email') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('email', 'Email',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {{ Form::text('email', null, array('class' => 'form-control', 'id' => 'email','placeholder' => 'Email')) }}
                    <span class="text-danger" >{{ $errors->first('email') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('website') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('website', 'Website',array('class'=>'control-label')) !!}
                    {{ Form::text('website', null, array('class' => 'form-control', 'id' => 'website','placeholder' => 'Website Url')) }}
                    <span class="text-danger" >{{ $errors->first('website') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('facebook') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('facebook', 'Facebook',array('class'=>'control-label')) !!}
                    {{ Form::text('facebook', null, array('class' => 'form-control', 'id' => 'facebook','placeholder' => 'Facebook Url')) }}
                    <span class="text-danger" >{{ $errors->first('facebook') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('twitter') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('twitter', 'Twitter',array('class'=>'control-label')) !!}
                    {{ Form::text('twitter', null, array('class' => 'form-control', 'id' => 'twitter','placeholder' => 'Twitter Url')) }}
                    <span class="text-danger" >{{ $errors->first('twitter') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('linkedin') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('linkedin', 'Linkedin',array('class'=>'control-label')) !!}
                    {{ Form::text('linkedin', null, array('class' => 'form-control', 'id' => 'linkedin','placeholder' => 'Linkedin Url')) }}
                    <span class="text-danger" >{{ $errors->first('linkedin') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('profile-image') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('profile_image', 'Profile Image',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {{ Form::file('profile_image', null, array('class' => 'form-control', 'id' => 'profile_image','placeholder' => 'Select Profile Photo')) }}
                    <span class="text-danger" >{{ $errors->first('profile_image') }}</span>
                </div>
            </div>
        </div>
    <!-- </div>
</div> -->
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h4>Funeral Profile</h4>
                <div class="clearfix"></div>
            </div>

            <div id="profile">
                <div class="form-group <?php  echo $errors->first('title') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('title', 'Title',array('class'=>'control-label')) !!}
                    {{ Form::text('title[]', null, array('class' => 'form-control', 'id' => 'title','placeholder' => 'Title')) }}
                    <span class="text-danger" >{{ $errors->first('title') }}</span>
                </div>                        

                <div class="form-group <?php  echo $errors->first('description') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('description', 'Description',array('class'=>'control-label')) !!}
                    {{ Form::textarea('description[]', null, array('class' => 'form-control', 'id' => 'summary-ckeditor0','placeholder' => 'Description')) }}
                    <span class="text-danger" >{{ $errors->first('description') }}</span>
                </div>
                <div id="profile1"></div>
                <input type="button" id="add" value="Add More" class="btn btn-sm btn-xs btn-success pull-right"/>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h4>Funeral Images</h4>
                <div class="clearfix"></div>
            </div>

            <div class="form-group <?php  echo $errors->first('image') !== '' ? 'has-error' : '';?>">
                {!! Form::label('image', 'Images',array('class'=>'control-label')) !!}
                <input type="file" name='image[]' multiple="" id="files">
                <div id="selectedFiles"></div>
                <span class="text-danger" >{{ $errors->first('image') }}</span>
            </div>

            <div class="form-group <?php echo $errors->first('video') !== '' ? 'has-error' : '';?>">
                {!! Form::label('video','Videos',array('class' => 'control-label')) !!}
                
                <input class="form-control" type="text" name="video[]" id="video" placeholder="Video Url">
                <span class="text-danger" >{{ $errors->first('video') }}</span>
            </div>
            <div id="videoid"></div>
            <input type="button" id="addVideo" value="Add More" class="btn btn-sm btn-xs btn-success pull-right">
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="box-footer">
                {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                <a class="btn btn-default btn-close" href="{{ URL::to('admin/funeral') }}">Cancel</a>
            </div>
        </div>
    </div>
{!! Form::close() !!}
                <!-- </div>
            </div> -->
        </div> 
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')<!-- 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="{{ asset('public\ckeditor\ckeditor.js') }}" type="text/javascript"> </script>

<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);

    $(document).ready(function(){
        $("#add").click(function(){

            var $div = $('textarea[id^="summary-ckeditor"]:last');
            var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;

            $("#profile1").append('<div class="form-group <?php  echo $errors->first('title') !== '' ? 'has-error' : '';?>"><label class="control-label">Title</label><input class="form-control" type="text" name="title[]" id="title"><span class="text-danger">{{ $errors->first('title') }}</span> <div class="form-group <?php  echo $errors->first('description') !== '' ? 'has-error' : '';?>"> <label class="control-label">Description</label> <textarea name="description[]" class="form-control" id="summary-ckeditor'+ num +'"> <span class="text-danger">{{ $errors->first('description') }}</span>');

                CKEDITOR.replace('summary-ckeditor'+num);
        });
        $("#addVideo").click(function(){
            $("#videoid").append('<div class="form-group <?php echo $errors->first('video') !== '' ? 'has-error' : '';?>"><input class="form-control" type="text" name="video[]" id="video" placeholder="Video Url"><span class="text-danger" >{{ $errors->first('video') }}</span>');
        });

        CKEDITOR.replace('summary-ckeditor0');

        $('select[name="country"]').on('change',function(){
            var countryId = $(this).val();

            if(countryId){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('admin/funeral/stateList') }}?country_id="+countryId,
                    dataType: "json",
                    success: function(res){
                        if(res){
                            $('select[name="state"]').html('<option value="">-- Select State --</option>');
                            $.each(res,function(key,value){
                                $('select[name="state"]').append('<option value="'+key+'">'+value+'</option>');
                            });
                        }else{
                            $('select[name="state"]').empty();
                        }
                    }
                });
            }else{
                $('select[name="state"]').empty();
                $('select[name="city"]').empty();
            }
        });
        $('select[name="state"]').on('change',function(){
            var stateId = $(this).val();

            if(stateId){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('admin/funeral/cityList') }}?state_id="+stateId,
                    dataType: "json",
                    success: function(data){
                        $('select[name="city"]').html('<option value="">-- Select City --</option>');
                        $.each(data,function(key, value){
                            $('select[name="city"]').append('<option value="'+key+'">'+value+'</option>');
                        });
                    }
                });
            }else{
                $('select[name="city"]').empty();
            }
        });


    });
    //https://www.raymondcamden.com/2013/09/10/Adding-a-file-display-list-to-a-multifile-upload-HTML-control
    var selDiv = "";
    document.addEventListener("DOMContentLoaded", init, false);
    function init(){
        document.querySelector('#files').addEventListener('change',handleFileSelect,false);
        selDiv = document.querySelector('#selectedFiles');
    }

    function handleFileSelect(e){
        if(!e.target.files){
            return;
        } 
        selDiv.innerHTML = "";
        var files = e.target.files;
        for(var i=0; i<files.length; i++){
            var f = files[i];
            selDiv.innerHTML += f.name + "<br/>";
        }
    }
</script>

@endpush('scripts')
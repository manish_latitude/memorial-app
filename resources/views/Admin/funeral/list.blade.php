@extends('Admin.master_layout.master')

@section('title', 'Funeral List')

@section('breadcum')
	/ Funeral List
@endsection

@section('content')
	<div class="">

		<div class="clearfix"></div>
	    @if (Session::has('message'))
	    <div class="alert alert-info alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('message') !!}</div>
	    @endif
	    @if (Session::has('error'))
	    <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('error') !!}</div>
	    @endif
	    @if (Session::has('success'))
	    <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('success') !!}</div>
	    @endif
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h4>Funeral</h4>
                        <div class="col-sm-2 pull-right">
                            <a href="{{ url('admin/funeral/export') }}" class="btn btn-sm btn-xs btn-success pull-right" id="Export to excel">Export</a>
    						<a href="{{ url('admin/funeral/create') }}" class="btn btn-sm btn-xs btn-success pull-right">
                            	<i class="fa fa-plus-circle" aria-hidden="true"></i>
                            	Add Funeral
                        	</a>
                        </div>
                        <div class="col-sm-2 pull-right" id="tableactiondiv" style="display:none;">
                            <select name="actions" id="tableaction" class="form-control" >
                                <option value="">Select </option>
                                <option value="approve"> Approve </option>
                                <option value="reject"> Reject </option>
                                <option value="active"> Active </option>
                                <option value="deactive"> Deactive </option>
                                <option value="delete"> Delete </option>
                            </select>
                        </div>
                    	<div class="clearfix"></div>
					</div>
					<table id="users-table" class="table table-bordered">
						<thead>
							<tr>
                <th style="width: 3% !important;"><label class="chk">
                    <input name="select_all" value="1" id="example-select-all" type="checkbox" />
                    <span class="checkmark"></span> </label>
                </th>
                <th>ID</th>
                <th>Image</th>
							  <th>Funeral Home</th>
								<th>Person Name</th>
                <th>Country</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Website</th>
                <th>Status <i class="fa fa-sort"></th>
								<th>Action</th>
							</tr>
						</thead>
					</table>

                	<div id="myModal" class="modal fade users-table-1">
                    	<div class="modal-dialog modal-confirm">
                        	<div class="modal-content">
                            	<div class="modal-header">
                                	<div class="icon-box">
                                    	<i class="fa fa-close"></i>
                                	</div>              
                                	<h4 class="modal-title">Are you sure?</h4>  
                                	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            	</div>
	                            <div class="modal-body">
	                                <p>Do you really want to delete these records? This process cannot be undone.</p>
	                            </div>
	                            <div class="modal-footer">
	                                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
	                                <a href="" class="btn btn-danger" id="deleteurl">Delete</a>
	                            </div>
                        	</div>
                    	</div>
                	</div>
                	<div id="approvalModal" class="modal fade in" tabindex="-1">
  						<div class="modal-dialog modal-lg" role="document" style="width: 400px;">
    						<div class="modal-content">
      							<div class="modal-header">
        							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      							</div>
      							<div class="modal-body">
      								<h5 align="center">Approve or Reject Funeral</h5>
      								<center>
		      							<input type="hidden" name="id" id="id" value="">
		      							<button class="btn btn-primary" id="approval" name="approve" value="Approve">Approve</button>
		      							<button class="btn btn-danger" id="approval" name="reject" value="Reject">Reject</button>
		      						</center>
      							</div>
    						</div>
  						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@push('scripts')


<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js" data-turbolinks-track="true"></script>
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>

<script type="text/javascript">
	setTimeout(function(){
		$(".alert-success").hide()
	}, 10000);

	$(document).on("click", '.delete-btn', function(event) { 
        var id = $(this).attr('data-id');
        $('#select-delete').val(id);
    });

    $('#delete-submit').click(function(){
    var id = $('#select-delete').val();
    $.ajax({
    	url: "{{ url('admin/funeral/destroy') }}",
    	type: 'GET',
    	async: false,
    	data: {'password': password, 'id': id},
    	success: function(data){
    		if(data == "true"){
    			$('.delete-close').click();
    			$('.delete-' + id).parent().parent().remove();
    			$(document).ready(function(){
    				$.notify({
    					message: 'Funeral Deleted Successfully'
    				},{
    					type: 'success'
    				});
    			});
    		}else{
    			$('#deletepasswordError').html('please enter valid password');
                return false;
    		}
    	},
    	error: function(){
    		$('#deletepasswordError').html("something went wrong please try later");
            return false;
    	}
    });
});

table = $('#users-table').DataTable({
	serverSide: true,
	processing: true,
  autoWidth: false,
  "order" : [[9,'asc']],
	ajax: "{{ url('admin/funeral/array-data') }}",
	columns: [
            {data: 'check', orderable: false, searchable: false},
            {data: 'id', name:'funeral.id', orderable: false, searchable: true},
	        {data: 'image', name: 'funeral.image', orderable: false, searchable:true},
			{data: 'home_name', name: 'funeral.home_name', orderable: false, searchable: true},
            {data: 'person_name', name: 'funeral.person_name', orderable: false, searchable: true},
            {data: 'country', name: 'country.country', orderable: false, searchable: true},
            // {data: 'state', name: 'state.state', orderable: false, searchable: true},
            //{data: 'city', name : 'city.city',orderable: false, searchable: true},
            {data: 'phone', name: 'funeral.phone', orderable: false, searchable: true},
			{data: 'email' , name: 'funeral.email', orderable: false, searchable: true},
            {data: 'website', name: 'funeral.website', orderable: false, searchable: true},
            {data: 'approval', name: 'funeral.approval',orderable:true},
			{data: 'action', name: 'action', searchable: false, orderable: false}
		],
		"language":{
			"paginate" :{
				next:'<i class="fa fa-angle-right"></i>',
				previous: '<i class="fa fa-angle-left"></i>'
			}
		},"fnDrawCallback": function() {

         $('.make-switch').bootstrapSwitch();
            $('.make-switch').on('switchChange.bootstrapSwitch', function (event, state) {
          
             action=state == true?'active':'deactive';
             var ids=[this.value];
             
             $.ajax({
                 url: "{{ url('admin/funeral/perfomaction') }}",
                 type: 'GET',
                 async: false,
                 data: {'action':action,'ids':ids},
                 success: function (data) {
                     if(data == "true") {
                        table.ajax.reload(); 
                       
                     }else{
                       alert('something went wrong');
                     }
                 },
                 error: function () {
        
                     alert('something went wrong');
                 }
             });
         });

        }
});

table.columns().every(function () {
        var that = this;

        $('input', this.header()).on('keyup change', function () {
            var colid = that[0][0];
            activearray = ['a', 'ac', 'act', 'acti', 'activ', 'active'];
            deactivearray = ['i', 'in', 'ina', 'inac', 'inact', 'inacti', 'inactiv', 'inactive'];
             var newval = '';
            if (colid == 10) {
                if ($.inArray(this.value, activearray) != -1) {
                    newval = 1;
                }
                if ($.inArray(this.value, deactivearray) != -1) {
                    newval = 0;
                }
            }
            if (that.search() !== this.value) {
                if (newval == '') {
                    newval = this.value;
                }
                that
                        .search(newval)
                        .draw();
            }
        });
    });

    $('#example-select-all').on('click', function () {
        if (!this.checked) {
            $('#tableactiondiv').css({'display': 'none'});
        } else {
            $('#tableactiondiv').css({'display': 'block'});
        }
        var rows = table.rows({'search': 'applied'}).nodes();
        $('.selectcheckbox', rows).prop('checked', this.checked);
    });

    $('#users-table tbody').on('change', '.selectcheckbox', function () {
        var rowsa = $(table.$('.selectcheckbox').map(function () {

            return $(this).prop("checked") ? 'true' : 'false';
        }));
        var array = $.map(rowsa, function (value, index) {
            return [value];
        });

        if (array.indexOf("true") == -1) {
            $('#tableactiondiv').css({'display': 'none'});
        }

        if (!this.checked) {
            var el = $('#example-select-all').get(0);
            if (el && el.checked && ('indeterminate' in el)) {
                el.indeterminate = true;
            }

        } else {
            $('#tableactiondiv').css({'display': 'block'});
        }
    });
    
    $('#tableaction').on('change', function (e) {
        var form = this;
        var ids = [];
    
        table.$('.selectcheckbox').each(function () {
            if (this.checked) {
                ids.push(this.value);
            }
        });
        action = this.value;
        $.ajax({
            url: "{{ url('admin/funeral/perfomaction') }}",
            type: 'GET',
            async: false,
            data: {'action': action, 'ids': ids},
            success: function (data) {
                if (data == "true") {
                    table.ajax.reload();
                    $('#tableactiondiv').css({'display': 'none'});
                    $('#tableaction').val("");
                } else {
                    alert('something went wrong');
                }
            },
            error: function () {
                alert('something went wrong');
            }
        });
    });
    $(document).ready(function(){
    	$('#approvalModal').on('show.bs.modal', function (e) {
        	var rowid = $(e.relatedTarget).data('id');
   		
	        $.ajax({
	            type : 'GET',
	            url:  "{{ url('admin/funeral/getId') }}",
	            data :  'id='+ rowid, 
	            success : function(data){
	                $('#id').val(data.id);
	            }
	        });
     	});

     	$('button').on('click',function(){
     		var id = $('#id').val();
     		var val = $(this).val();

     		$.ajax({
     			type: 'GET',
     			url: "{{ url('admin/funeral/approval') }}",
     			data: {'id':id,'val':val},
     			success: function(res){
     				if(res == 1){
     					table.ajax.reload(null, false);
                		$('#approvalModal').modal('hide');
                		alert('Funeral Approved Successfully');
     				}else if(res == 2){
     					table.ajax.reload(null, false);
                		$('#approvalModal').modal('hide');
                		alert('Funeral Rejected Successfully')
     				}
     			}
     		})
     	})
	});
</script>
@endpush('scripts')
@extends('Admin.master_layout.master')

@section('title', 'Student List')

@section('breadcum')
     / Student List 
@endsection

@section('content')
<div class="">
    <?php
    /*
     <div class="page-title">
        <div class="title_left">
            <h3>Users</h3>
            <p>You can manage users here.</p>
            </p>
        </div>
    </div>
    */
    ?>

    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>Payment</h4>
<!--                    <a href="{{ url('admin/student/create') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Add user
                    </a>-->
                     <div class="clearfix"></div>
                </div>
                <table id="users-table" class="table table-bordered">
                    <thead>
                    <tr>
                        <?php
                        /*
                        <th>#</th>
                        */
                        ?>
                         <th>Student Name</th>
                        <th>Bank Name</th>
                        <th>Payment Mode</th>
                        <th>Transaction No.</th>
                        <th>Transfer Date</th>
                        <th>Cash Paydate</th>
                       <th> Amount</th>
                        <th>Payment Status</th>
                       <th>Created On</th>
                        <th>Updated On</th>
                       
                        <th>Action</th>
                        <?php
                        /*
                        <th>Created On</th>
                        <th>Updated On</th>
                       
                        */
                        ?>
                    </tr>
                    </thead>
                </table>

                <div id="myModal" class="modal fade">
                    <div class="modal-dialog modal-confirm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="icon-box">
                                    <i class="fa fa-close"></i>
                                </div>				
                                <h4 class="modal-title">Are you sure?</h4>	
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <p>Do you really want to delete these records? This process cannot be undone.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                                <a href="" class="btn btn-danger" id="deleteurl">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.css" rel="stylesheet" />
		  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js" data-turbolinks-track="true"></script>
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
<script type="text/javascript">

setTimeout(function() {
        $(".alert-success").hide()
}, 10000);

$(document).on("click", '.delete-btn', function(event) { 
    var id = $(this).attr('data-id');
    $('#select-delete').val(id);
});
$('#delete-submit').click(function(){
    var id = $('#select-delete').val();

    var password = $(".delete-password").val();
    if (password == "") {
        $('#deletepasswordError').html('please enter password');
        return false;
    }
    $.ajax({
        url: "{{ url('admin/payment/destroy') }}",
        type: 'GET',
        async: false,
        data: {'password':password,'id':id},
        success: function (data) {
            if(data == "true") {
                $('.delete-close').click();
                $('.delete-'+id).parent().parent().remove();
                $(document).ready(function(){
                    $.notify({
                        message: 'User deleted successfully.'
                    },{
                        type: 'success'
                    });
                });
            }else{
              $('#deletepasswordError').html('please enter valid password');
              return false;
            }
        },
        error: function () {
            $('#deletepasswordError').html("something went wrong please try later");
            return false;
        }
    });
});
 
    
    table=    $('#users-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: "{{ url('admin/payment/array-data') }}",
            columns: [
                {data: 'st_id', orderable: true, searchable: true },
                {data: 'bank_name', orderable: true, searchable: true },
                {data: 'payment_mode',orderable: true, searchable: true},
                {data: 'transactionno',orderable: true, searchable: true},
                {data: 'transferdate',orderable: true, searchable: true},
                {data: 'cash_paydate', orderable: true, searchable: true},
                 {data: 'amount', orderable: true, searchable: true},
                {data: 'paymentstatus', orderable: true},
                {data: 'created_at', searchable: false },
                {data: 'updated_at', searchable: false },
                {data: 'action', name:'action', searchable: false, orderable: false }
            ],
            "language": {
                "paginate": {
                     next: '<i class="fa fa-angle-right"></i>',
                    previous: '<i class="fa fa-angle-left"></i>'
                }
            },"fnDrawCallback": function() {
			 //iCheck for checkbox and radio inputs
    
           $('.make-switch').bootstrapSwitch();
		   $('.make-switch').on('switchChange.bootstrapSwitch', function (event, state) {
		  
 action=state == true?'done':'pending'; alert(action);
 var ids=[this.value];
	  $.ajax({
            url: "{{ url('admin/payment/perfomaction') }}",
            type: 'GET',
            async: false,
            data: {'action':action,'ids':ids},
            success: function (data) {
                if(data == "true") {
                   table.ajax.reload(); 
				   
                }else{
                  alert('something went wrong');
                }
            },
            error: function () {
                alert('something went wrong');
            }
        });
});
        }
        });
   
</script>
@endpush('scripts')

@extends('Admin.master_layout.master')

@section('title', 'Create Student')

@section('breadcum')
     / Create student
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create Student</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/student/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">
                    
                    <?php if(Auth::user()->user_role == 1): ?>
                    <div class="form-group <?php echo $errors->first('institute_id') !== ''?'has-error': '';?>">
                        {!! Form::label('Institute', 'Type',array('class'=>'control-label')) !!}
                        {{ Form::select('institute_id',isset($user)?$user:[],['id' => 'institute_id'],['placeholder' => 'Select Institute','class' => 'form-control institute_id']) }}
                        <span class="text-danger" >{{ $errors->first('institute_id') }}</span>
                    </div>
                    <?php endif ?>
                        <div class="form-group <?php  echo $errors->first('fname') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('First Name', 'First Name',array('class'=>'control-label')) !!}
                            {{ Form::text('fname', null,array('class' => 'form-control', 'id' => 'fname')) }}
                            <span class="text-danger" >{{ $errors->first('fname') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('lname') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Last Name', 'Last Name',array('class'=>'control-label')) !!}
                            {{ Form::text('lname', null, array('class' => 'form-control', 'id' => 'lname')) }}
                            <span class="text-danger" >{{ $errors->first('lname') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('email') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('email', 'Email',array('class'=>'control-label')) !!}
                            {{ Form::text('email', null, array('class' => 'form-control', 'id' => 'email')) }}
                            <span class="text-danger" >{{ $errors->first('email') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('phone_number') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Phone', 'Phone',array('class'=>'control-label')) !!}
                            {{ Form::text('phone_number', null, array('class' => 'form-control', 'id' => 'phone')) }}
                            <span class="text-danger" >{{ $errors->first('phone_number') }}</span>
                        </div>
                        
                        <div class="box-footer">
                            {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('admin/student') }}">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);
</script>
@endpush('scripts')
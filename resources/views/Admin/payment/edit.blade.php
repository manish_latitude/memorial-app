@extends('Admin.master_layout.master')

@section('title', 'Edit Student')

@section('breadcum')
     / Edit Student 
@endsection

@section('content')

<!-- @foreach ($errors->all() as $error)
<p class="alert alert-warning">{{ $error }}</p>
@endforeach
<span class="errormessage"></span> -->
<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(count($errors))
                <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>Whoops!</strong> There were some problems with your request.
                    <br/>
                </div>
            @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Edit Student</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/student/store'),'method'=>'POST', 'id'=>'signup','files'=>true)) !!}
                <div class="x_content">
                    <input type="hidden" name="id" value="{{ $data->id }} " >
                    <div class="form-group">
                        <div class="form-group <?php echo $errors->first('institute_id') !== ''?'has-error': '';?>">
                        {!! Form::label('Institute', 'Type',array('class'=>'control-label')) !!}
                        {{ Form::select('institute_id',isset($user)?$user:[],$data->institute_id,['placeholder' => 'Select Institute','class' => 'form-control institute_id']) }}
                        <span class="text-danger" >{{ $errors->first('institute_id') }}</span>
                    </div>
                        <?php if(Auth::user()->user_role == 1): ?>
                        <div class="form-group <?php echo $errors->first('fname') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('First Name', 'First Name',array('class'=>'control-label')) !!}
                            {{ Form::text('fname', $data->fname,array('class' => 'form-control', 'id' => 'fname')) }}
                            <span class="text-danger" >{{ $errors->first('fname') }}</span>
                        </div>
                           <?php endif ?>
                        <div class="form-group <?php echo $errors->first('lname') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Last Name', 'Last Name',array('class'=>'control-label')) !!}
                            {{ Form::text('lname', $data->lname, array('class' => 'form-control', 'id' => 'lname')) }}
                            <span class="text-danger" >{{ $errors->first('lname') }}</span>
                        </div>
                        <div class="form-group <?php  echo $errors->first('email') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('email', 'Email',array('class'=>'control-label')) !!}
                            {{ Form::text('email', $data->email, array('class' => 'form-control', 'id' => 'email')) }}
                            <span class="text-danger" >{{ $errors->first('email') }}</span>
                        </div>
                        <div class="form-group <?php  echo $errors->first('phone_number') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Phone', 'Phone',array('class'=>'control-label')) !!}
                            {{ Form::text('phone_number', $data->phone_number, array('class' => 'form-control', 'id' => 'phone')) }}
                            <span class="text-danger" >{{ $errors->first('phone_number') }}</span>
                        </div>

                        <div class="form-group">
                            <label for="status">{!! Form::label('status', 'Status:')   !!}</label>
                            @if ($data->status == 1)
                            {!! Form::radio('status', '1',['checked' => 'checked'],['class' => 'minimal']) !!} Active
                            {!! Form::radio('status', '0','',['class' => 'minimal']) !!} InActive
                            @else
                            {!! Form::radio('status', '1','',['class' => 'minimal']) !!} Active
                            {!! Form::radio('status', '0',['checked' => 'checked'],['class' => 'minimal']) !!} InActive
                            @endif
                        </div>

                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-danger submit" data-toggle="modal" data-target=".bs-example-modal-reset">Reset Password</button>    
                        </div><!-- 'id'=>'reset-password', -->

                    </div>
                    <div class="box-footer">
                        {!! Form::submit('Save', array('class' => 'btn btn-primary submit')) !!}
<!--                        <button type="button" class="btn btn-primary submit" data-toggle="modal" data-target=".bs-example-modal-edit">Save</button>-->

                        <a class="btn btn-default btn-close" href="{{ URL::to('admin/student') }}">Cancel</a>
                    </div>
                {!! Form::close() !!}
                </div>
                <div class="modal fade bs-example-modal-edit" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <h5 class="modal-title" id="myModalLabel2">Verification</h5>
                            </div>
                            <div class="modal-body">
                                <p>Please enter your password to update user details.</p>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control">
                                    <span class="text-danger" id="passwordError"></span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" id="submit">Submit</button>
                                <button type="button" class="btn btn-default edit-close" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade bs-example-modal-reset" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <h5 class="modal-title" id="myModalLabel2">Verification</h5>
                            </div>
                            <div class="modal-body">
                                <p>Please enter your password to send reset link to user.</p>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control reset-password">
                                    <span class="text-danger" id="resetpasswordError"></span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" id="reset-submit">Submit</button>
                                <button type="button" class="btn btn-default reset-close" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
<script type="text/javascript">
    
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);

    $('#reset-submit').click(function(){
        var user_id = "{{$data->id}}";
        var password = $(".reset-password").val();
        if (password == "") {
            $('#resetpasswordError').html('please enter password');
            return false;
        }

        $.ajax({
            url: "{{ url('admin/student/reset-pass-link') }}",
            type: 'GET',
            async: false,
            data: {'password':password,'user_id':user_id},
            success: function (data) {
                if(data == "true") {
                    $('.reset-close').click();
                    $(document).ready(function(){
                        $.notify({
                            message: 'Reset password link send successfully to user.'
                        },{
                            type: 'success'
                        });
                    });

                }else{
                  $('#resetpasswordError').html('please enter valid password');
                  return false;
                }
            },
            error: function () {
                $('#resetpasswordError').html("something went wrong please try later");
                return false;
            }
        });
    });
</script>
@endpush('scripts')
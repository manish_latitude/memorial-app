@extends('Admin.master_layout.master')
<link rel="stylesheet" href="{{ url('public/ckeditor/samples/css/samples.css')}}">
<script src="{{ url('public/ckeditor/ckeditor.js')}}"></script>
<script src="{{ url('public/ckeditor/samples/js/sample.js')}}"></script>
@section('title', 'Create Category')

@section('breadcum')
     / Create User
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create Category</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/category/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">

                        <div class="form-group <?php  echo $errors->first('category_name') !== '' ? 'has-error' : '';?>">
                             <label class="label-category">{{ Form::label('Intelligence Type', 'Intelligence Type:') }}</label>
                           {{--  {{ Form::select('category_name',['Spatial' => 'Spatial',
                                                             'Linguistic' => 'Linguistic',
                                                             'Naturalist' => 'Naturalist',
                                                             'Logical-Mathematical' => 'Logical-Mathematical',
                                                             'Bodily-Kinesthetic' => 'Bodily-Kinesthetic',
                                                             'Interpersonal' => 'Interpersonal',
                                                             'Intra Personal' => 'Intra Personal',
                                                             'Musical' => 'Musical'],['id' => 'category_name'],['class' => 'form-control']) }} --}}
                            {{ Form::text('category_name',null,['class' => 'form-control'])}}
                            <span class="text-danger" >{{ $errors->first('category_name') }}</span>
                        </div>
                        <div class="form-group">
                            <label class="label-category">{{ Form::label('label', 'Nature of work then link:') }}</label>
                            {{-- {{ Form::textarea('nature_of_work_then_link',null,['class' => 'form-control'],['id' => 'editor']) }} --}}
                            <textarea name="nature_of_work_then_link" class="form-control" id="editor"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="label-category">{{ Form::label('label', 'Career preference:') }}</label>
                            {{-- {{ Form::textarea('career_preference',null,['class' => 'form-control']) }} --}}
                            <textarea name="career_preference" class="form-control" id="editor1"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="label-category">{{ Form::label('label', 'Preferred Teaching Learning Strategies:') }}</label>
                            {{-- {{ Form::textarea('preferred_teaching_learning_strategies',null,['class' => 'form-control']) }} --}}
                            <textarea name="preferred_teaching_learning_strategies" class="form-control" id="editor2"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="label-category">{{ Form::label('label', 'Teaching Materials:') }}</label>
                            {{-- {{ Form::textarea('teaching_materials',null,['class' => 'form-control']) }} --}}
                            <textarea name="teaching_materials" class="form-control" id="editor3"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="label-category">{{ Form::label('status', 'Status:') }}</label>
                            {{ Form::select('status', ['1' => 'Active','0' => 'InActive'],['id' => 'status'],['class' => 'form-control']) }}
                        </div>
                        <div class="box-footer">
                            {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('admin/category') }}">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);
</script>
@endpush('scripts')
@extends('Admin.master_layout.master')
<link rel="stylesheet" href="{{ url('public/ckeditor/samples/css/samples.css')}}">
<script src="{{ url('public/ckeditor/ckeditor.js')}}"></script>
<script src="{{ url('public/ckeditor/samples/js/sample.js')}}"></script>
@section('title', 'Edit Category')

@section('breadcum')
     / Edit App 
@endsection

@section('content')

<!-- @foreach ($errors->all() as $error)
<p class="alert alert-warning">{{ $error }}</p>
@endforeach
<span class="errormessage"></span> -->
<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(count($errors))
                <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>Whoops!</strong> There were some problems with your request.
                    <br/>
                </div>
            @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Edit Category</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/category/store'),'method'=>'POST', 'id'=>'signup','files'=>true)) !!}
                <div class="x_content">
                    <input type="hidden" name="id" value="{{ $data->id }} " >
                    <div class="form-group">

                        <div class="form-group <?php  echo $errors->first('category_name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Category Name', 'Category Name',array('class'=>'control-label')) !!}
                            {{ Form::text('category_name', $data->category_name,array('class' => 'form-control', 'id' => 'category_name')) }}
                            <span class="text-danger" >{{ $errors->first('category_name') }}</span>
                        </div>
                        <div class="form-group">
                            <label>{{ Form::label('label', 'Nature of work then link:') }}</label>
                            {{-- {{ Form::textarea('nature_of_work_then_link',null,['class' => 'form-control'],['id' => 'editor']) }} --}}
                            <textarea name="nature_of_work_then_link" class="form-control" id="editor">{{$data->nature_of_work_then_link}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>{{ Form::label('label', 'Career preference:') }}</label>
                            {{-- {{ Form::textarea('career_preference',null,['class' => 'form-control']) }} --}}
                            <textarea name="career_preference" class="form-control" id="editor1">{{$data->career_preference}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>{{ Form::label('label', 'Preferred Teaching Learning Strategies:') }}</label>
                            {{-- {{ Form::textarea('preferred_teaching_learning_strategies',null,['class' => 'form-control']) }} --}}
                            <textarea name="preferred_teaching_learning_strategies" class="form-control" id="editor2">{{$data->preferred_teaching_learning_strategies}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>{{ Form::label('label', 'Teaching Materials:') }}</label>
                            {{-- {{ Form::textarea('teaching_materials',null,['class' => 'form-control']) }} --}}
                            <textarea name="teaching_materials" class="form-control" id="editor3">{{$data->teaching_materials}}</textarea>
                        </div>
                        <div class="form-group <?php  echo $errors->first('status') !== '' ? 'has-error' : '';?>">
                            {{ Form::label('status', 'Status:',array('class'=>'control-label')) }}
                            {{ Form::select('status', ['1' => 'Active','0' => 'InActive'],$data->status,['id' => 'status','class' => 'form-control']) }}
                            <span class="text-danger" >{{ $errors->first('status') }}</span>
                        </div>
                    </div>
                    <div class="box-footer">
                    
                        {!! Form::submit('Save', array('class' => 'btn btn-primary edit-submit')) !!}
                      
                        <a class="btn btn-default btn-close" href="{{ URL::to('admin/category') }}">Cancel</a>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
<script type="text/javascript">
    
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);

    $('#submit').click(function(){
        var password = $("input[name='app_name']").val();
        if (password == "") {
            $('#passwordError').html('please enter App name');
            return false;
        }
        $.ajax({
            url: "{{ url('admin/confirm-pass') }}",
            type: 'GET',
            async: false,
            data: {'password':password},
            success: function (data) {
                if(data == "true") {
                    // $('#passwordError').html('password match');
                    $('.edit-close').click();
                    $('.edit-submit').click();
                }else{
                  $('#passwordError').html('please enter valid password');
                  return false;
                }
            },
            error: function () {
                $('#passwordError').html("something went wrong please try later");
            }
        });
    });

</script>
@endpush('scripts')
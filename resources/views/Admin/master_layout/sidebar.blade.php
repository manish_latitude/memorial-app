<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <?php
            /*
            <!-- <li>
                <a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="index.html">Dashboard 1</a></li>
                    <li><a href="dashboard_2.html">Dashboard 2</a></li>
                </ul>
            </li> -->
            */
            ?>
            <li>
                <a href="{{ url('admin/dashboard') }}"><i class="fa fa-tachometer"></i> Dashboard </a>
            </li>
            @if(Auth::user())
            <?php if(Auth::user()->user_role == 1): ?>
            <!-- users     -->
            <!-- <li>
                <a ><i class="fa fa-users"></i> Users Management</a>
                <ul class="nav child_menu" style="display: none;">
                    <li> <a href="{{ url('admin/user') }}"> Users </a></li>
                    
                    <li> <a href="{{ url('admin/role') }}">User Roles</a></li>
                    
                </ul>
            </li> -->
            <?php endif ?>
            @endif
            <li>
                <a><i class="fa fa-users"></i> App Users Management</a>
                <ul class="nav child_menu" style="display: none;">
                    <li><a href="{{ url('admin/app-user') }}">Users List</a></li>
                    <li><a href="{{ url('admin/app-user/create') }}">Add User</a></li>
                </ul>
            </li>
            
            <li>
                <a><i class="fa fa-users"></i> Funeral Management</a>
                <ul class="nav child_menu" style="display: none;">
                    <li><a href="{{ url('admin/funeral') }}">Funeral List</a></li>
                    <li><a href="{{ url('admin/funeral/create') }}">Add Funeral</a></li>
                </ul>
            </li>
            
            <li>
                <a><i class="fa fa-users"></i> Member Management</a>
                <ul class="nav child_menu" style="display: none;">
                    <li><a href="{{ url('admin/member') }}">Member List</a></li>
                    <li><a href="{{ url('admin/member/create') }}">Add Member</a></li>
                </ul>
            </li>

            <li>
                <a><i class="fa fa-pagelines"></i> Community</a>
                <ul class="nav child_menu" style="display: none;">
                    <li><a href="{{ url('admin/community') }}">Community List</a></li>
                    <li><a href="{{ url('admin/community/create') }}">Add Community</a></li>
                </ul>
            </li> 
            <!-- <li> -->
            	<!-- <a><i class="fa fa-gift"></i> Menu Management</a> -->
            	<!-- <ul class="nav child_menu" style="display: none;"> -->
        	<li>
                <a><i class="fa fa-pagelines"></i> Condolence</a>
                <ul class="nav child_menu" style="display: none;">
                    <li><a href="{{ url('admin/condolence') }}">Condolence List</a></li>
                    <li><a href="{{ url('admin/condolence/create') }}">Add Condolence</a></li>
                </ul>
            </li>
            <li>
                <a><i class="fa fa-pagelines"></i> Flowers</a>
                <ul class="nav child_menu" style="display: none;">
                    <li><a href="{{ url('admin/flower') }}">Flowers List</a></li>
                    <li><a href="{{ url('admin/flower/create') }}">Add Flowers</a></li>
                </ul>
            </li>
            <li>
                <a><i class="fa fa-pagelines"></i> Bereavement</a>
                <ul class="nav child_menu" style="display: none;">
                    <li><a href="{{ url('admin/bereavement') }}">Bereavement List</a></li>
                    <li><a href="{{ url('admin/bereavement/create') }}">Add Bereavement</a></li>
                </ul>
            </li>
            <li>
                <a><i class="fa fa-pagelines"></i> Funeral Plan</a>
                <ul class="nav child_menu" style="display: none;">
                    <li><a href="{{ url('admin/funeral-plan') }}">Plan List</a></li>
                    <li><a href="{{ url('admin/funeral-plan/create') }}">Add Funeral Plan</a></li>
                </ul>
            </li>
            <li>
                <a><i class="fa fa-pagelines"></i> Will Plan</a>
                <ul class="nav child_menu" style="display: none;">
                    <li><a href="{{ url('admin/will-plan') }}">Plan List</a></li>
                    <li><a href="{{ url('admin/will-plan/create') }}">Add Will Plan</a></li>
                </ul>
            </li>
            <li>
                <a><i class="fa fa-pagelines"></i> Memorial Tag</a>
                <ul class="nav child_menu" style="display: none;">
                    <li><a href="{{ url('admin/memorial-tag') }}">Memorial Tag List</a></li>
                    <li><a href="{{ url('admin/memorial-tag/create') }}">Add Memorial Tag</a></li>
                </ul>
            </li>
		        <!-- </ul> -->
	        <!-- </li> -->
            <li>
                <a><i class="fa fa-pagelines"></i> Content Management</a>
                <ul class="nav child_menu" style="display: none;">
                    <li><a href="{{ url('admin/cms') }}">CMS List</a></li>
                    {{--<li><a href="{{ url('admin/cms/create') }}">Add CMS</a></li>--}}
                </ul>
            </li>
            <li>
                <a><i class="fa fa-pagelines"></i> Area</a>
                <ul class="nav child_menu" style="display: none;">
                    <li><a href="{{ url('admin/country') }}">Country</a></li>
                    <!-- <li><a href="{{ url('admin/state') }}">State</a></li> -->
                    <!-- <li><a href="{{ url('admin/country/create') }}">Add Country</a></li> -->
                </ul>
            </li>
            <li>
                <a href="{{ url('admin/setting') }}"><i class="fa fa-pagelines"></i> General Setting</a>
            </li>
            <li>
                <a><i class="fa fa-pagelines"></i> FAQ</a>
                <ul class="nav child_menu" style="display: none;">
                    <li><a href="{{ url('admin/faq') }}">FAQs</a></li>
                    <li><a href="{{ url('admin/faq/create') }}">Add FAQ</a></li>
                </ul>
            </li>

<!--             <li>
                <a><i class="fa fa-pagelines"></i> About Us</a>
                <ul class="nav child_menu" style="display: none;">
                    <li><a href="{{ url('admin/about') }}">List</a></li>
                    <li><a href="{{ url('admin/about/create') }}">Add About</a></li>
                </ul>
            </li> -->
        </div>
    </div>
<!-- /sidebar menu
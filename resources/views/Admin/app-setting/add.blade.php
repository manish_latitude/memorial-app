@extends('Admin.master_layout.master')

@section('title', 'Create User')

@section('breadcum')
     / Create User
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create App</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('app-setting/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">

                        <div class="form-group <?php  echo $errors->first('app_name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('App Name', 'App Name',array('class'=>'control-label')) !!}
                            {{ Form::text('app_name', null,array('class' => 'form-control', 'id' => 'app_name')) }}
                            <span class="text-danger" >{{ $errors->first('app_name') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('app_slug') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('App slug', 'App slug',array('class'=>'control-label')) !!}
                            {{ Form::text('app_slug', null, array('class' => 'form-control', 'id' => 'app_slug')) }}
                            <span class="text-danger" >{{ $errors->first('app_slug') }}</span>
                        </div>

<!--                        <div class="form-group <?php  echo $errors->first('app_type') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Type', 'Type',array('class'=>'control-label')) !!}
                            {{ Form::select('app_type', ['image' => 'Image','content' => 'Content','youtubeurl' => 'Youtube Url','wesiteurl' => 'Wesite Url'],['id' => 'app_type'],['class' => 'form-control']) }}
                            <span class="text-danger" >{{ $errors->first('app_type') }}</span>
                        </div>-->
<!--                        <div class="form-group">
                            <label for="status">{{ Form::label('status', 'Status:') }}</label>
                            {{ Form::radio('status', '1',['checked' => 'checked'],['class' => 'minimal']) }} Active
                            {{ Form::radio('status', '0','',['class' => 'minimal']) }} InActive
                            
                        </div>-->
                        <div class="form-group">
                            <label>{{ Form::label('status', 'Status:') }}</label>
                            {{ Form::select('status', ['1' => 'Active','0' => 'InActive'],['id' => 'status'],['class' => 'form-control']) }}
                        </div>
                        <div class="box-footer">
                            {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('/app-setting') }}">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);
</script>
@endpush('scripts')
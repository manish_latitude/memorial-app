@extends('Admin.master_layout.master')

@section('title', 'Edit App')

@section('breadcum')
     / Edit App 
@endsection

@section('content')

<!-- @foreach ($errors->all() as $error)
<p class="alert alert-warning">{{ $error }}</p>
@endforeach
<span class="errormessage"></span> -->
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Edit App</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('app-setting/store'),'method'=>'POST', 'id'=>'signup','files'=>true)) !!}
                <div class="x_content">
                    <input type="hidden" name="id" value="{{ $data->id }} " >
                    <div class="form-group">

                        <div class="form-group <?php  echo $errors->first('app_name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('App Name', 'App Name',array('class'=>'control-label')) !!}
                            {{ Form::text('app_name', $data->app_name,array('class' => 'form-control', 'id' => 'app_name')) }}
                            <span class="text-danger" >{{ $errors->first('app_name') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('app_slug') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('App slug', 'App slug',array('class'=>'control-label')) !!}
                            {{ Form::text('app_slug', $data->app_slug, array('class' => 'form-control', 'id' => 'app_slug')) }}
                            <span class="text-danger" >{{ $errors->first('app_slug') }}</span>
                        </div>

<!--                        <div class="form-group <?php  echo $errors->first('type') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('App Type', 'App Type',array('class'=>'control-label')) !!}
                            {{ Form::select('app_type',  ['image' => 'Image','content' => 'Content','youtubeurl' => 'Youtube Url','wesiteurl' => 'Wesite Url'] ,$data->app_type,['id' => 'app_type','class' => 'form-control','placeholder' => 'Select App Type']) }}
                            <span class="text-danger" >{{ $errors->first('type') }}</span>
                        </div>-->
                        <div class="form-group <?php  echo $errors->first('status') !== '' ? 'has-error' : '';?>">
                            {{ Form::label('status', 'Status:',array('class'=>'control-label')) }}
                            {{ Form::select('status', ['1' => 'Active','0' => 'InActive'],$data->status,['id' => 'status','class' => 'form-control']) }}
                            <span class="text-danger" >{{ $errors->first('status') }}</span>
                        </div>
<!--                        <div class="form-group">
                            <label for="status">{{ Form::label('status', 'Status:') }}</label>
                            @if ($data->status == 1)
                            {{ Form::radio('status', '1',['checked' => 'checked'],['class' => 'minimal']) }} Active
                            {{ Form::radio('status', '0','',['class' => 'minimal']) }} InActive
                            @else
                            {{ Form::radio('status', '1','',['class' => 'minimal']) }} Active
                            {{ Form::radio('status', '0',['checked' => 'checked'],['class' => 'minimal']) }} InActive
                            @endif
                        </div>-->
                        
<!--                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-danger submit" data-toggle="modal" data-target=".bs-example-modal-reset">Reset Password</button>    
                        </div>-->
<!-- 'id'=>'reset-password', -->

                    </div>
                    <div class="box-footer">
					
                        {!! Form::submit('Save', array('class' => 'btn btn-primary edit-submit')) !!}
                       <!-- <button type="button" class="btn btn-primary submit" data-toggle="modal" data-target=".bs-example-modal-edit">Save</button>-->
                        <a class="btn btn-default btn-close" href="{{ URL::to('/app-setting') }}">Cancel</a>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
<script type="text/javascript">
    
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);

    $('#submit').click(function(){
        var password = $("input[name='app_name']").val();
        if (password == "") {
            $('#passwordError').html('please enter App name');
            return false;
        }
        $.ajax({
            url: "{{ url('confirm-pass') }}",
            type: 'GET',
            async: false,
            data: {'password':password},
            success: function (data) {
                if(data == "true") {
                    // $('#passwordError').html('password match');
                    $('.edit-close').click();
                    $('.edit-submit').click();
                }else{
                  $('#passwordError').html('please enter valid password');
                  return false;
                }
            },
            error: function () {
                $('#passwordError').html("something went wrong please try later");
            }
        });
    });

</script>
@endpush('scripts')
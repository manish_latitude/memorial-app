@extends('Admin.master_layout.master')

@section('title', 'Create Funeral Plan')

@section('breadcum')
     / Create Funeral Plan
@endsection

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    @if(count($errors))
        <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your request.
            <br/>
        </div>
    @endif
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create Funeral Plan</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/funeral-plan/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">

                        <div class="form-group <?php  echo $errors->first('name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('name', 'Name',array('class'=>'control-label')) !!}
                            <span class="required">*</span>
                            {{ Form::text('name', null,array('class' => 'form-control', 'id' => 'name','placeholder' => 'Name')) }}
                            <span class="text-danger" >{{ $errors->first('name') }}</span>
                        </div>

                        <div class="form-group <?php echo $errors->first('location') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('location','Location',array('class' => 'control-label')) !!}
                            <span class="required">*</span>
                            {{ Form::text('location', null, array('class' => 'form-control', 'id' => 'location','placeholder' => 'Location')) }}
                            <span class="text-danger" >{{ $errors->first('location') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('image') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('image', 'Image',array('class'=>'control-label')) !!}
                            {{ Form::file('image', null, array('class' => 'form-control', 'id' => 'image')) }}
                            <span class="text-danger" >{{ $errors->first('image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="box-footer">
                    {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('admin/funeral-plan') }}">Cancel</a>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);
</script>
@endpush('scripts')
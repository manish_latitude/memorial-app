@extends('Admin.master_layout.master')

@section('title', 'Edit Funeral Plan')

@section('breadcum')

     / Edit Funeral Paln

@endsection

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    @if(count($errors))
        <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your request.
            <br/>
        </div>
    @endif
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Edit Funeral Plan</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/funeral-plan/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">
                        <input type="hidden" name="id" value="{{ $data->id }}">
                        <div class="form-group <?php  echo $errors->first('name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('name', 'Name',array('class'=>'control-label')) !!}
                            <span class="required">*</span>
                            {{ Form::text('name', $data->name,array('class' => 'form-control', 'id' => 'name','placeholder' => 'Name')) }}
                            <span class="text-danger" >{{ $errors->first('name') }}</span>
                        </div>
                        
                        <div class="form-group <?php  echo $errors->first('image') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('image', 'Image',array('class'=>'control-label')) !!}
                            {{ Form::file('image', null, array('class' => 'form-control', 'id' => 'image')) }}
                            <input type="hidden" name="old_image" value="{{ $data->image }}">
                            <br>
                            <img width="100px" height="100px" src="{{url('public/images/funeralplan/'.$data->image)}}" onerror="this.src='{{ url("public/images/avatar.png") }}'" name="image">
                            <a href="javascript:void(0)" onclick="profile('{{ $data->id }}')">Delete</a>
                            <span class="text-danger" >{{ $errors->first('image') }}</span>
                        </div>
                        <div class="form-group <?php echo $errors->first('location') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('location','Location',array('class' => 'control-label')) !!}
                            <span class="required">*</span>
                            {{ Form::text('location', $data->location, array('class' => 'form-control', 'id' => 'location','placeholder' => 'Location')) }}
                            <span class="text-danger" >{{ $errors->first('location') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="box-footer">
                    {!! Form::submit('Update', array('class' => 'btn btn-primary submit')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('admin/funeral-plan') }}">Cancel</a>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript">

    setTimeout(function() {

            $(".alert-danger").hide()

    }, 10000);

    // blank value disabled in dropdown

    $("select option[value='']").attr("disabled", true);

    function profile(id){
        $.ajax({
            url: "{{ url('admin/funeral-plan/image-update') }}",
            type: 'GET',
            data: {'id':id},
            success: function(data){
                if(data == "true"){
                    alert('true');
                    location.reload();
                }
            },
            error: function(){
                alert('error');
            }
        });
    }

    $('body').append('<div class="product-image-overlay"><span class="product-image-overlay-close">x</span><img src="" /></div>');
    var productImage = $('img');
    var productOverlay = $('.product-image-overlay');
    var productOverlayImage = $('.product-image-overlay img');

    productImage.click(function () {
        var productImageSource = $(this).attr('src');

        productOverlayImage.attr('src', productImageSource);
        productOverlay.fadeIn(100);
        $('body').css('overflow', 'hidden');

        $('.product-image-overlay-close').click(function () {
            productOverlay.fadeOut(100);
            $('body').css('overflow', 'auto');
        });
    });

</script>

@endpush('scripts')
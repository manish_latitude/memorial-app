@extends('Admin.master_layout.master')

@section('title', 'Create Medium')

@section('breadcum')
     / Create Medium
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create Medium</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/medium/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">

                        <div class="form-group <?php  echo $errors->first('name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Medium Name', 'Medium Name',array('class'=>'control-label')) !!}
                            {{ Form::text('name', null,array('class' => 'form-control', 'id' => 'name')) }}
                            <span class="text-danger" >{{ $errors->first('name') }}</span>
                        </div>
                        
			 <div class="form-group">
                        {!! Form::label('status', 'Status:')   !!}
                        {!! Form::radio('status', '1',true,['class' => 'minimal']) !!}  Active &nbsp;
                        {!! Form::radio('status', '0','',['class' => 'minimal']) !!}  InActive &nbsp;
                    </div>						
                        <div class="box-footer">
                            {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('admin/medium') }}">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);
</script>
@endpush('scripts')
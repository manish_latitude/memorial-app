@extends('Admin.master_layout.master')

@section('title', 'Edit About')

@section('breadcum')
     / Edit About
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <!-- <div class="x_title">
                    <h4>Create Funeral</h4>
                    <div class="clearfix"></div>
                </div> -->
                {!! Form::open(array('url' => ('admin/about/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">
                        <div class="x_title">
                            <h4>About Us</h4>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="id" value="{{ $about->id }}">
                         <div class="form-group <?php echo $errors->first('funeral_home') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('funeral_home','Funeral home',array('class'=>'control-label')) !!} 
                            {!! Form::select('funeral_home',isset($funeral)?$funeral:[],'',['placeholder' => 'Select Funeral','class' => 'form-control']) !!}  
                            <span class="text-danger" >{{ $errors->first('funeral_home') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('title') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('title' , 'Title',array('class'=>'control-label')) !!}
                            {{ Form::text('title', $about->title,array('class' => 'form-control', 'id' => 'title')) }}
                            <span class="text-danger" >{{ $errors->first('title') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('about') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('about', 'About',array('class'=>'control-label')) !!}
                            {{ Form::textarea('about', $about->description, array('class' => 'form-control', 'id' => 'summary-ckeditor')) }}
                            <span class="text-danger" >{{ $errors->first('about') }}</span>
                        </div>

                        <div class="box-footer">
                            {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('admin/about') }}">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('public\ckeditor\ckeditor.js') }}" type="text/javascript"> </script>

<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);    

    CKEDITOR.replace('summary-ckeditor');
</script>

@endpush('scripts')
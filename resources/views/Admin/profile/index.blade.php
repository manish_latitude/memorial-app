@extends('Admin.master_layout.master')

@section('title', 'Profile')

@section('breadcum')
     / Profile
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>
         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Profile</h4>
                    <div class="clearfix"></div>
                </div>
               
                <div class="x_content">
                         {!! Form::open(array('url' => ('admin/profile/store'),'method'=>'POST', 'files'=>true)) !!}
                      
                       <!--  <div class="form-group <?php  echo $errors->first('first_name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Name', 'Name',array('class'=>'control-label')) !!}
                            {{ Form::text('first_name', Auth::user()->first_name,array('class' => 'form-control', 'id' => 'first_name')) }}
                            <span class="text-danger" >{{ $errors->first('first_name') }}</span>
                        </div> -->
                        
                        
                        @if(Auth::user()->user_role != 2)
                        <div class="form-group <?php  echo $errors->first('first_name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('First Name', 'First Name',array('class'=>'control-label')) !!}
                            <span style="color: red">*</span> 
                            {{ Form::text('first_name', Auth::user()->first_name,array('class' => 'form-control', 'id' => 'first_name')) }}
                            <span class="text-danger" >{{ $errors->first('first_name') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('last_name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Last Name', 'Last Name',array('class'=>'control-label')) !!}
                            <span style="color: red">*</span>
                            {{ Form::text('last_name', Auth::user()->last_name, array('class' => 'form-control', 'id' => 'last_name')) }}
                            <span class="text-danger" >{{ $errors->first('last_name') }}</span>
                        </div>
                        @endif

                        <div class="form-group <?php  echo $errors->first('email') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('email', 'Email',array('class'=>'control-label')) !!}
                            <span style="color: red">*</span>
                            {{ Form::text('email', Auth::user()->email, array('class' => 'form-control', 'id' => 'email')) }}
                            <span class="text-danger" >{{ $errors->first('email') }}</span>
                        </div>
                        <!-- <div class="form-group <?php echo $errors->first('password') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('password', 'Password',array('class'=>'control-label')) !!}
                            {{ Form::password('password', array('class' => 'form-control', 'id' => 'password')) }}
                            <span class="text-danger" >{{ $errors->first('password') }}</span>
                        </div> -->

                        @if(Auth::user()->user_role == 2)
                        <div class="form-group <?php  echo $errors->first('address1') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Address 1', 'Address 1',array('class'=>'control-label')) !!}
                            {{ Form::text('address1', Auth::user()->address1,array('class' => 'form-control', 'id' => 'address1')) }}
                            <span class="text-danger" >{{ $errors->first('address1') }}</span>
                        </div>
                        
                        <div class="form-group <?php  echo $errors->first('address2') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Address 2', 'Address 2',array('class'=>'control-label')) !!}
                            {{ Form::text('address2', Auth::user()->address2,array('class' => 'form-control', 'id' => 'address2')) }}
                            <span class="text-danger" >{{ $errors->first('address2') }}</span>
                        </div>
                        
                        <div class="form-group <?php  echo $errors->first('city') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('City', 'City',array('class'=>'control-label')) !!}
                            {{ Form::text('city', Auth::user()->city,array('class' => 'form-control', 'id' => 'city')) }}
                            <span class="text-danger" >{{ $errors->first('city') }}</span>
                        </div>
                        
                        <div class="form-group <?php  echo $errors->first('country') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('country', 'Country',array('class'=>'control-label')) !!}
                            {!! Form::select('country[]', $country, Auth::user()->country, array('class' => 'form-control','placeholder'=>'Select country')) !!}
                            <span class="text-danger" >{{ $errors->first('country') }}</span>
                        </div>
                        
                        <div class="form-group <?php  echo $errors->first('state') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('State', 'State',array('class'=>'control-label')) !!}
                            {{ Form::text('state', Auth::user()->state,array('class' => 'form-control', 'id' => 'state')) }}
                            <span class="text-danger" >{{ $errors->first('state') }}</span>
                        </div>
                    
                        <div class="form-group <?php  echo $errors->first('pincode') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Pincode', 'Pincode',array('class'=>'control-label')) !!}
                            {{ Form::text('pincode', Auth::user()->pincode,array('class' => 'form-control', 'id' => 'pincode')) }}
                            <span class="text-danger" >{{ $errors->first('pincode') }}</span>
                        </div>                       
                        
                        @endif
                        
                        <div class="form-group <?php  echo $errors->first('phone_number') !== '' ? 'has-error' : '';?>">
                            @if(Auth::user()->user_role == 2)
                            {!! Form::label('Contact Number', 'Contact Number',array('class'=>'control-label')) !!}
                            @else
                            {!! Form::label('Phone', 'Phone',array('class'=>'control-label')) !!}
                            @endif
                            {{ Form::text('phone_number', Auth::user()->phone_number, array('class' => 'form-control', 'id' => 'phone')) }}
                            <span class="text-danger" >{{ $errors->first('phone_number') }}</span>
                        </div>
                        
                        @if(Auth::user()->user_role == 2)
                        <div class="form-group <?php  echo $errors->first('website') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Website', 'Website',array('class'=>'control-label')) !!}
                            {{ Form::text('website', Auth::user()->website,array('class' => 'form-control', 'id' => 'website')) }}
                            <span class="text-danger" >{{ $errors->first('website') }}</span>
                        </div>
                    
                        <div class="form-group <?php  echo $errors->first('board') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Board', 'Board',array('class'=>'control-label')) !!}
                            {{ Form::text('board', Auth::user()->board,array('class' => 'form-control', 'id' => 'board')) }}
                            <span class="text-danger" >{{ $errors->first('board') }}</span>
                        </div>
                    
                        <div class="form-group <?php  echo $errors->first('medium') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Medium', 'Medium',array('class'=>'control-label')) !!}
                            {{ Form::text('medium', Auth::user()->medium,array('class' => 'form-control', 'id' => 'medium')) }}
                            <span class="text-danger" >{{ $errors->first('medium') }}</span>
                        </div>
                        @endif                        
                        
                        <div class="box-footer">
                            {!! Form::submit('Update', array('class' => 'btn btn-primary submit')) !!}
                            <!-- <a class="btn btn-default btn-close" href="{{ URL::to('/user') }}">Cancel</a> -->
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);
</script>
@endpush('scripts')
@extends('Admin.master_layout.master')

@section('title', 'Edit Community')

@section('breadcum')
     / Edit Community
@endsection

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    @if(count($errors))
        <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your request.
            <br/>
        </div>
    @endif
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Edit Community</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/community/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">
                        <input type="hidden" name="id" value="{{ $data->id }}">
                        <div class="form-group <?php  echo $errors->first('community') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('community', 'Community',array('class'=>'control-label')) !!}
                            <span class="required">*</span>
                            {{ Form::text('community', $data->community,array('class' => 'form-control', 'id' => 'community','placeholder' => 'Community')) }}
                            <span class="text-danger" >{{ $errors->first('community') }}</span>
                        </div>

                        <div class="form-group <?php echo $errors->first('status') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('status','Status',array('class'=>'control-label')) !!} 
                            <span class="required">*</span>
                            {{ Form::select('status', ['1' => 'Active', '0' => 'In-active'],$data->status,['id' => 'status','class' => 'form-control']) }}
                            <span class="text-danger" >{{ $errors->first('status') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="box-footer">
                    {!! Form::submit('Update', array('class' => 'btn btn-primary submit')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('admin/country') }}">Cancel</a>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);
</script>
@endpush('scripts')
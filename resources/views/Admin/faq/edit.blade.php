@extends('Admin.master_layout.master')

@section('title', 'Edit faq')

@section('breadcum')
     / Edit faq 
@endsection

@section('content')

<!-- @foreach ($errors->all() as $error)
<p class="alert alert-warning">{{ $error }}</p>
@endforeach
<span class="errormessage"></span> -->
<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(count($errors))
                <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>Whoops!</strong> There were some problems with your request.
                    <br/>
                </div>
            @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Edit Content</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/faq/store'),'method'=>'POST', 'id'=>'signup','files'=>true)) !!}
                <div class="x_content">
                    <input type="hidden" name="id" value="{{ $data->id }} " >
                    <div class="form-group <?php  echo $errors->first('title') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('Title', 'Title',array('class'=>'control-label')) !!}
                        {{ Form::text('title', $data->title,array('class' => 'form-control ', 'id' => 'title')) }}
                        <span class="text-danger" >{{ $errors->first('title') }}</span>
                    </div>

                        <div class="form-group <?php  echo $errors->first('description') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('description', 'Description',array('class'=>'control-label')) !!}
                            {{ Form::textarea('description', $data->description, array('class' => 'form-control', 'id' => 'summary-ckeditor')) }}
                            <span class="text-danger" >{{ $errors->first('description') }}</span>
                        </div>
                        
                        <div class="form-group <?php  echo $errors->first('status') !== '' ? 'has-error' : '';?>">
                            {{ Form::label('status', 'Status:',array('class'=>'control-label')) }}
                            {{ Form::select('status', ['1' => 'Active','0' => 'InActive'],$data->status,['id' => 'status','class' => 'form-control']) }}
                            <span class="text-danger" >{{ $errors->first('status') }}</span>
                        </div>

                    </div>
                    <div class="box-footer">
					
                        {!! Form::submit('Update', array('class' => 'btn btn-primary edit-submit')) !!}
                       <!-- <button type="button" class="btn btn-primary submit" data-toggle="modal" data-target=".bs-example-modal-edit">Save</button>-->
                        <a class="btn btn-default btn-close" href="{{ URL::to('admin/faq') }}">Cancel</a>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('public\ckeditor\ckeditor.js') }}" type="text/javascript"> </script>

<script>
    CKEDITOR.replace('summary-ckeditor');
</script>
<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);
</script>
@endpush('scripts')
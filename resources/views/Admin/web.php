<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'Auth\AuthController@index');
Route::post('web/loginauth', 'Auth\AuthController@loginAuth');

Route::group(['prefix' => 'admin','middleware' => 'guest:admin'], function () {

	Route::get('/', ['as' => 'admin', 'uses' => 'Admin\Auth\AuthController@index']);
	//Admin Logind
	Route::get('login', ['as' => 'login', 'uses' => 'Admin\Auth\AuthController@index']);
	Route::post('loginauth', 'Admin\Auth\AuthController@loginAuth');
	Route::get('/user/confirm/{token}', 'Admin\UserController@userConfirm');
	Route::get('/user/account/confirm', 'Admin\UserController@userAccountConfirm');
	Route::get('admin/user/setPassword', 'Admin\UserController@savePassword');
	Route::post('/user/save-reset-Password', 'Admin\UserController@saveResetPassword');
	Route::get('/user/reset-password/{token}', 'Admin\UserController@resetPassword');
	Route::get('/user/reset-password-confirm', 'Admin\UserController@resetConfirm');
});


Route::group(['prefix' => 'admin','middleware' => 'auth:admin'], function () {

	Route::any('/logout', 'Admin\Auth\AuthController@getLogout');
	Route::get('/dashboard','Admin\DashboardController@index');
	Route::get('/add', 'Admin\DashboardController@add');

	//////////////////////////////////User Management//////////////////////////////////
	Route::get('/user', 'Admin\UserController@index');
	Route::get('/user/create', 'Admin\UserController@create');
	Route::post('/user/store', 'Admin\UserController@store');
	Route::get('/user/destroy', 'Admin\UserController@destroy');
	Route::get('/user/edit/{id}', 'Admin\UserController@edit');
	Route::get('/confirm-pass', 'Admin\UserController@editConfirmPass');
	Route::get('/user/reset-pass-link', 'Admin\UserController@sendResetPasswordLink');
	Route::any('/user/perfomaction', 'Admin\UserController@perfomaction');
	Route::any('/user/array-data', 'Admin\UserController@arrayData');
       
        Route::get('/role', ['as'=>'role','uses'=>'Admin\RoleController@index']);
	Route::get('/role/list',['as'=>'/role/list','uses'=>'Admin\RoleController@roleList']);
	Route::get('/role/add',['as'=>'/role/add','uses'=>'Admin\RoleController@addEditRole']);
	Route::get('/role/edit/{id}',['as'=>'/role/edit/{id}','uses'=>'Admin\RoleController@addEditRole']);
	Route::post('/role/store',['as'=>'/role/store','uses'=>'Admin\RoleController@storeRole']);
	Route::get('/role/destroy/{id}',['as'=>'/role/destroy/{id}','uses'=>'Admin\RoleController@destroyRole']);
	//////////////////////////////////User Management//////////////////////////////////
	 //////////////////////////////////board Management//////////////////////////////////
        Route::get('/board',['as'=>'/board','uses'=>'Admin\BoardController@index','middleware' => ['permission:list']]);
        Route::get('/board/create',['as'=>'/board/create','uses'=>'Admin\BoardController@create','middleware' => ['permission:create']]);
        Route::post('/board/store', 'Admin\BoardController@store');
        Route::get('/board/edit/{id}',['as'=>'/board/edit/{id}','uses'=>'Admin\BoardController@edit','middleware' => ['permission:edit']]);
	Route::get('/board/destroy/{id}',['as'=>'/board/destroy/{id}','uses'=>'Admin\BoardController@destroy','middleware' => ['permission:delete']]);
	Route::any('/board/array-data', 'Admin\BoardController@arrayData');
        Route::any('/board/perfomaction', 'Admin\BoardController@perfomaction');
        //////////////////////////////////Medium Management//////////////////////////////////
        Route::get('/medium',['as'=>'/medium','uses'=>'Admin\MediumController@index','middleware' => ['permission:list']]);
        Route::get('/medium/create',['as'=>'/medium/create','uses'=>'Admin\MediumController@create','middleware' => ['permission:create']]);
        Route::post('/medium/store', 'Admin\MediumController@store');
        Route::get('/medium/edit/{id}',['as'=>'/medium/edit/{id}','uses'=>'Admin\MediumController@edit','middleware' => ['permission:edit']]);
        Route::get('/medium/destroy/{id}',['as'=>'/medium/destroy/{id}','uses'=>'Admin\MediumController@destroy','middleware' => ['permission:delete']]);
	//Route::get('/medium/edit/{id}', 'Admin\MediumController@edit');
        Route::any('/medium/array-data', 'Admin\MediumController@arrayData');
        
	
        //////////////////////////////////Institute Management//////////////////////////////////
	Route::get('/institute',['as'=>'/institute','uses'=>'Admin\InstituteController@index','middleware' => ['permission:list']]);
	Route::get('/institute/create',['as'=>'/institute/create','uses'=>'Admin\InstituteController@create','middleware' => ['permission:create']]);
	Route::post('/institute/store', 'Admin\InstituteController@store');
	Route::get('/institute/edit/{id}',['as'=>'/institute/edit/{id}','uses'=>'Admin\InstituteController@edit','middleware' => ['permission:edit']]);
        Route::get('/institute/destroy/{id}',['as'=>'/institute/destroy/{id}','uses'=>'Admin\InstituteController@destroy','middleware' => ['permission:delete']]);
	Route::get('/institute/confirm-pass', 'Admin\InstituteController@editConfirmPass');
	Route::any('/institute/perfomaction', 'Admin\InstituteController@perfomaction');
	// Datatables
	Route::any('/institute/array-data', 'Admin\InstituteController@arrayData');
        //////////////////////////////////Profile Page //////////////////////////////////
        Route::get('/profile', 'Admin\ProfileController@index');
	Route::post('/profile/store', 'Admin\ProfileController@store');
        Route::post('/profile/storeInstitute', 'Admin\ProfileController@storeInstitute');
        /////////////////////// Category Management start ///////////////////////
//	Route::get('/category', 'Admin\CategoryController@index');
        Route::get('/category',['as'=>'/category','uses'=>'Admin\CategoryController@index','middleware' => ['permission:list']]);
	Route::any('/category/array-data', 'Admin\CategoryController@arrayData');
	//Route::get('/category/create', 'Admin\CategoryController@create');
        Route::get('/category/create',['as'=>'/category/create','uses'=>'Admin\CategoryController@create','middleware' => ['permission:create']]);
	Route::post('/category/store', 'Admin\CategoryController@store');
	Route::get('/category/edit/{id}',['as'=>'/category/edit/{id}','uses'=>'Admin\CategoryController@edit','middleware' => ['permission:edit']]);
	Route::get('/category/delete/{id}',['as'=>'/category/delete/{id}','uses'=>'Admin\CategoryController@delete','middleware' => ['permission:delete']]);
	Route::any('/category/perfomaction', 'Admin\CategoryController@perfomaction');
        ///////////////////////  category Management end ///////////////////////
        
        
        /////////////////////// Question Management start ///////////////////////
	Route::get('/question',['as'=>'/question','uses'=>'Admin\QuestionController@index','middleware' => ['permission:list']]);
	Route::any('/question/array-data', 'Admin\QuestionController@arrayData');
	Route::get('/question/create',['as'=>'/question/create','uses'=>'Admin\QuestionController@create','middleware' => ['permission:create']]);
	Route::post('/question/store', 'Admin\QuestionController@store');
	Route::get('/question/edit/{id}', 'Admin\QuestionController@edit');
        Route::get('/question/edit/{id}',['as'=>'/question/edit/{id}','uses'=>'Admin\QuestionController@edit','middleware' => ['permission:edit']]);
	Route::get('/question/delete/{id}',['as'=>'/question/delete/{id}','uses'=>'Admin\QuestionController@delete','middleware' => ['permission:delete']]);
	Route::any('/question/perfomaction', 'Admin\QuestionController@perfomaction');
        /////////////////////// Question Management end ///////////////////////
        
        /////////////////////// Demo Question Management start ///////////////////////
	Route::get('/demo-question', 'Admin\DemoQuestionController@index');
        Route::get('/demo-question',['as'=>'/demo-question','uses'=>'Admin\DemoQuestionController@index','middleware' => ['permission:list']]);
	Route::any('/demo-question/array-data', 'Admin\DemoQuestionController@arrayData');
	Route::get('/demo-question/create',['as'=>'/demo-question/create','uses'=>'Admin\DemoQuestionController@create','middleware' => ['permission:create']]);
	Route::post('/demo-question/store', 'Admin\DemoQuestionController@store');
	Route::get('/demo-question/edit/{id}',['as'=>'/demo-question/edit/{id}','uses'=>'Admin\DemoQuestionController@edit','middleware' => ['permission:edit']]);
	Route::get('/demo-question/delete/{id}',['as'=>'/demo-question/delete/{id}','uses'=>'Admin\DemoQuestionController@delete','middleware' => ['permission:delete']]);
	Route::any('/demo-question/perfomaction', 'Admin\DemoQuestionController@perfomaction');
        /////////////////////// Question Management end ///////////////////////
        
        /////////////////////// Student Management start ///////////////////////
	Route::get('/student',['as'=>'/student','uses'=>'Admin\StudentController@index','middleware' => ['permission:list']]);
	Route::any('/student/array-data', 'Admin\StudentController@arrayData');
	Route::get('/student/create',['as'=>'/student/create','uses'=>'Admin\StudentController@create','middleware' => ['permission:create']]);
	Route::post('/student/store', 'Admin\StudentController@store');
	Route::get('/student/edit/{id}',['as'=>'/student/edit/{id}','uses'=>'Admin\StudentController@edit','middleware' => ['permission:edit']]);
	Route::get('/student/delete/{id}',['as'=>'/student/delete/{id}','uses'=>'Admin\StudentController@delete','middleware' => ['permission:delete']]);
	Route::any('/student/perfomaction', 'Admin\StudentController@perfomaction');
        /////////////////////// Question Management end ///////////////////////
        
        /////////////////////// Result Management start ///////////////////////
	Route::get('/result',['as'=>'/result','uses'=>'Admin\ResultController@index','middleware' => ['permission:list']]);
	Route::any('/result/array-data', 'Admin\ResultController@arrayData');
	Route::get('/result/create',['as'=>'/result/create','uses'=>'Admin\ResultController@create','middleware' => ['permission:create']]);
	Route::post('/result/store', 'Admin\ResultController@store');
	Route::get('/result/edit/{id}',['as'=>'/result/edit/{id}','uses'=>'Admin\ResultController@edit','middleware' => ['permission:edit']]);
	Route::get('/result/delete/{id}',['as'=>'/result/delete/{id}','uses'=>'Admin\ResultController@delete','middleware' => ['permission:delete']]);
	Route::any('/result/perfomaction', 'Admin\ResultController@perfomaction');
        /////////////////////// Question Management end ///////////////////////
        
         /////////////////////// faqs Management start ///////////////////////
	Route::get('/faq', 'Admin\faqController@index');
	Route::get('/faq/create', 'Admin\faqController@create');
	Route::post('/faq/store', 'Admin\faqController@store');
	Route::get('/faq/edit/{id}', 'Admin\faqController@edit');
	Route::get('/faq/delete/{id}', 'Admin\faqController@delete');
	Route::any('/faq/array-data', 'Admin\faqController@arrayData');
	Route::any('/faq/perfomaction', 'Admin\faqController@perfomaction');
	/////////////////////// faqs Management end ///////////////////////
        /////////////////////// app Management start ///////////////////////
	Route::get('/app-setting', 'Admin\AppController@index');
	Route::any('/app-setting/array-data', 'Admin\AppController@arrayData');
	Route::get('/app-setting/create', 'Admin\AppController@create');
	Route::post('/app-setting/store', 'Admin\AppController@store');
	Route::get('/app-setting/edit/{id}', 'Admin\AppController@edit');
	Route::get('/app-setting/delete/{id}', 'Admin\AppController@delete');
	Route::any('/app-setting/perfomaction', 'Admin\AppController@perfomaction');
        /////////////////////// app Management end ///////////////////////
        
        /////////////////////// CMS pages start ///////////////////////
	Route::get('/cms', 'Admin\CMSController@index');
        Route::any('/cms/array-data', 'Admin\CMSController@arrayData');
	Route::get('/cms/create', 'Admin\CMSController@create');
	Route::post('/cms/store', 'Admin\CMSController@store');
	Route::get('/cms/edit/{id}', 'Admin\CMSController@edit');
	Route::get('/cms/delete/{id}', 'Admin\CMSController@delete');
	Route::any('/cms/perfomaction', 'Admin\CMSController@perfomaction');
        /////////////////////// CMS pages end ///////////////////////
        
});

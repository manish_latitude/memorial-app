@extends('Admin.master_layout.master')

@section('title', 'Edit User')

@section('breadcum')
     / Edit App User 
@endsection

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Edit App User</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/app-user/store'),'method'=>'POST', 'id'=>'signup','files'=>true)) !!}
                <div class="x_content">
                    <input type="hidden" name="id" value="{{ $data->id }} " >
                    <div class="form-group">

                        <div class="form-group <?php  echo $errors->first('name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('id', 'ID',array('class'=>'control-label')) !!}
                            {{ Form::text('id', $data->id ,array('class' => 'form-control', 'id' => 'uid')) }}
                            <span class="text-danger" >{{ $errors->first('id') }}</span>
                        </div>

                        <div class="form-group <?php echo $errors->first('user_type') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('user_type','User Type',array('class'=>'control-label')) !!}
                            {{ Form::select('user_type', ['0' => 'Normal User','1' => 'Facebook User', '2' => 'GPlusUser'],$data->register_type,['id' => 'user_type','class' => 'form-control']) }}
                            <span class="text-danger" >{{ $errors->first('user_type') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('name', 'Name',array('class'=>'control-label')) !!}
                            <span class="required">*</span>
                            {{ Form::text('name', $data->name ,array('class' => 'form-control', 'id' => 'name','placeholder' => 'Name')) }}
                            <span class="text-danger" >{{ $errors->first('name') }}</span>
                        </div>

                            <div class="form-group <?php  echo $errors->first('email') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('email', 'Email',array('class'=>'control-label')) !!}
                            <span class="required">*</span>
                            {{ Form::text('email', $data->email, array('class' => 'form-control', 'id' => 'email','placeholder' => 'Email')) }}
                            <span class="text-danger" >{{ $errors->first('email') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('phone_number') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Phone', 'Phone',array('class'=>'control-label')) !!}
                            <span class="required">*</span>
                            {{ Form::text('phone_number', $data->phone_number, array('class' => 'form-control', 'id' => 'phone','placeholder' => 'Mobile Number')) }}
                            <span class="text-danger" >{{ $errors->first('phone_number') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('social_id') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Social ID', 'Social ID',array('class'=>'control-label')) !!}
                            {{ Form::text('social_id', $data->social_id, array('class' => 'form-control', 'id' => 'facebook_id')) }}
                            <span class="text-danger" >{{ $errors->first('social_id') }}</span>
                        </div>                        

                        <div class="form-group <?php  echo $errors->first('dateOfbirth') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Date Of Birth', 'Date Of Birth',array('class'=>'control-label')) !!}
                            {{ Form::date('dateOfbirth', $data->dateOfbirth, array('class' => 'form-control', 'id' => 'dateOfbirth')) }}
                            <span class="text-danger" >{{ $errors->first('dateOfbirth') }}</span>
                        </div>

                        <!--<div class="form-group pull-right">
                            <button type="button" class="btn btn-danger submit" data-toggle="modal" data-target=".bs-example-modal-reset">Reset Password</button>    
                        </div>--><!-- 'id'=>'reset-password', -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="box-footer">
                {!! Form::submit('Update', array('class' => 'btn btn-primary submit')) !!}
                
                <!-- <button type="button" class="btn btn-primary submit" data-toggle="modal" data-target=".bs-example-modal-edit">Save</button> -->

                <a class="btn btn-default btn-close" href="{{ URL::to('admin/app-user') }}">Cancel</a>
            </div>
        </div>
    </div>
        {!! Form::close() !!}
                </div>
                <div class="modal fade bs-example-modal-edit" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <h5 class="modal-title" id="myModalLabel2">Verification</h5>
                            </div>
                            <div class="modal-body">
                                <p>Please enter your password to update user details.</p>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control">
                                    <span class="text-danger" id="passwordError"></span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" id="submit">Submit</button>
                                <button type="button" class="btn btn-default edit-close" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade bs-example-modal-reset" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <h5 class="modal-title" id="myModalLabel2">Verification</h5>
                            </div>
                            <div class="modal-body">
                                <p>Please enter your password to send reset link to user.</p>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control reset-password">
                                    <span class="text-danger" id="resetpasswordError"></span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" id="reset-submit">Submit</button>
                                <button type="button" class="btn btn-default reset-close" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
<!-- <script type="text/javascript">
    
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);

    $('#submit').click(function(){
        var password = $("input[name='password']").val();
        if (password == "") {
            $('#passwordError').html('please enter password');
            return false;
        }
        $.ajax({
            url: "{{ url('admin/confirm-pass') }}",
            type: 'GET',
            async: false,
            data: {'password':password},
            success: function (data) {
                if(data == "true") {
                    // $('#passwordError').html('password match');
                    $('.edit-close').click();
                    $('.edit-submit').click();
                }else{
                  $('#passwordError').html('please enter valid password');
                  return false;
                }
            },
            error: function () {
                $('#passwordError').html("something went wrong please try later");
            }
        });
    });

    $('#reset-submit').click(function(){
        var user_id = "{{$data->id}}";
        var password = $(".reset-password").val();
        if (password == "") {
            $('#resetpasswordError').html('please enter password');
            return false;
        }

        $.ajax({
            url: "{{ url('admin/user/reset-pass-link') }}",
            type: 'GET',
            async: false,
            data: {'password':password,'user_id':user_id},
            success: function (data) { //alert(data);
                if(data == "true") {
                    $('.reset-close').click();
                    $(document).ready(function(){
                        $.notify({
                            message: 'Reset password link send successfully to user.'
                        },{
                            type: 'success'
                        });
                    });

                }else{
                  $('#resetpasswordError').html('please enter valid password');
                  return false;
                }
            },
            error: function () {
                $('#resetpasswordError').html("something went wrong please try later");
                return false;
            }
        });
    });
</script> -->
@endpush('scripts')
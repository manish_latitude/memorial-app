@extends('Admin.master_layout.master')

@section('title', 'Create User')

@section('breadcum')
     / Create App User
@endsection

@section('content')

<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create App User</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/app-user/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">

                        <div class="form-group <?php  echo $errors->first('name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('name', 'Name',array('class'=>'control-label')) !!}
                            <span class="required">*</span>
                            {{ Form::text('name', null,array('class' => 'form-control', 'id' => 'name','placeholder' => 'Name')) }}
                            <span class="text-danger" >{{ $errors->first('name') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('email') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('email', 'Email',array('class'=>'control-label')) !!}
                            <span class="required">*</span>
                            {{ Form::text('email', null, array('class' => 'form-control', 'id' => 'email','placeholder' => 'Email')) }}
                            <span class="text-danger" >{{ $errors->first('email') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('phone_number') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('phone_number', 'Mobile Number',array('class'=>'control-label')) !!}
                            <span style="color: red">*</span>
                            {{ Form::text('phone_number', null, array('class' => 'form-control', 'id' => 'phone','placeholder' => 'Mobile Number')) }}
                            <span class="text-danger" >{{ $errors->first('phone_number') }}</span>
                        </div>

                        <!-- <div class="form-group <?php  echo $errors->first('password') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('password', 'Password',array('class'=>'control-label')) !!}
                            <span style="color: red">*</span>
                            {{ Form::password('phone_number', null, array('class' => 'form-control', 'id' => 'phone','placeholder' => 'Mobile Number')) }}
                            {{ Form::password('password', null, array('class' => 'form-control', 'id' => 'password','placeholder' => 'Password')) }}
                            <span class="text-danger" >{{ $errors->first('password') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('confirm_password') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('confirm_password', 'Confirm Password',array('class'=>'control-label')) !!}
                            <span style="color: red">*</span>
                            {{ Form::password('confirm_password', null, array('class' => 'form-control', 'id' => 'confirm_password','placeholder' => 'Confirm Password')) }}
                            <span class="text-danger" >{{ $errors->first('confirm_password') }}</span>
                        </div>
 -->
                        <div class="form-group <?php  echo $errors->first('dateOfbirth') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('dateOfbirth', 'Date Of Birth',array('class'=>'control-label')) !!}
                            <div class='input-group date' >
                                <input type='text' class="form-control" name="dateOfbirth" id='datetimepicker1' placeholder="Date Of Birth" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            <span class="text-danger" >{{ $errors->first('dateOfbirth') }}</span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="box-footer">
                    {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('admin/app-user') }}">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);

    $(document).ready(function(){
        $('#datetimepicker1').on('click',function(){
            $('#datetimepicker1').datepicker().datepicker('setDate',new Date());     
        }); 
    });
</script>
@endpush('scripts')
@extends('Admin.master_layout.master')

@section('title', 'User List')

@section('breadcum')

     / App User List 

@endsection

@section('content')

<div class="">


    <div class="clearfix"></div>

    @if (Session::has('message'))

    <div class="alert alert-info alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('message') !!}</div>

    @endif

    @if (Session::has('error'))

    <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('error') !!}</div>

    @endif

    @if (Session::has('success'))

    <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('success') !!}</div>

    @endif

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">
                <div class="x_title">
                    <h4>Users</h4>
                    <div class="col-sm-2 pull-right">
                        <a href="{{ url('admin/app-user/export') }}" class="btn btn-sm btn-xs btn-success pull-right" id="Export to excel">Export</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ url('admin/app-user/create') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            Add user
                        </a>
                    </div>
                    <div class="col-sm-2 pull-right" id="tableactiondiv" style="display:none;">
                        <select name="actions" id="tableaction" class="form-control" >
                            <option value="">Select </option>
                            <option value="active"> Active </option>
                            <option value="deactive"> Deactive </option>
                            <option value="delete"> Delete </option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <table id="users-table" class="table table-bordered">

                    <thead>

                    <tr>
                        <th style="width: 3% !important;"><label class="chk">
                           <input name="select_all" value="1" id="example-select-all" type="checkbox" />
                            <span class="checkmark"></span>
                        </label>
                        </th>
                        <th>Id</th>
                        <th>Login Type</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Social Id</th>
                        <th>Created At</th>
                        <th>Last Login</th>
                        <th>Status <i class="fa fa-sort"></i></th>
                        <th>Action</th>
                    </tr>                    
                </thead>
                </table>
                <div id="myModal" class="modal fade users-table-1">
                    <div class="modal-dialog modal-confirm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="icon-box">
                                    <i class="fa fa-close"></i>
                                </div>              
                                <h4 class="modal-title">Are you sure?</h4>  
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <p>Do you really want to delete these records? This process cannot be undone.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                                <a href="" class="btn btn-danger" id="deleteurl">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal fade bs-example-modal-delete" tabindex="-1" role="dialog" aria-hidden="true">

                    <div class="modal-dialog modal-sm">

                        <div class="modal-content">

                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>

                                </button>

                                <h5 class="modal-title" id="myModalLabel2">Verify</h5>

                            </div>

                            <div class="modal-body">

                                <p>Please enter your password to delete the user.</p>

                                <div class="form-group">

                                    <input type="hidden" name="" id="select-delete" >

                                    <label>Password</label>

                                    <input type="password" name="password" class="form-control delete-password">

                                    <span class="text-danger" id="deletepasswordError"></span>

                                </div>

                            </div>

                            <div class="modal-footer">

                                <button type="button" class="btn btn-primary" id="delete-submit">Submit</button>

                                <button type="button" class="btn btn-default delete-close" data-dismiss="modal">Close</button>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection



@section('footer')

    @parent

@endsection



@push('scripts')

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.css" rel="stylesheet" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js" data-turbolinks-track="true"></script>

<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>

<script type="text/javascript"> 

    setTimeout(function() {

        $(".alert-success").hide()

    }, 10000);



    $(document).on("click", '.delete-btn', function(event) { 

        var id = $(this).attr('data-id');

        $('#select-delete').val(id);

    });

    $('#delete-submit').click(function(){
    var id = $('#select-delete').val();
        $.ajax({
            url: "{{ url('admin/app-user/destroy') }}",
            type: 'GET',
            async: false,
            data: {'password':password,'id':id},
            success: function (data) {
                if(data == "true") {
                    $('.delete-close').click();
                    $('.delete-'+id).parent().parent().remove();
                    $(document).ready(function(){
                        $.notify({
                            message: 'User deleted successfully.'
                        },{
                            type: 'success'
                        });
                    });
                }else{
                  $('#deletepasswordError').html('please enter valid password');
                  return false;
                }
            },
            error: function () {
                $('#deletepasswordError').html("something went wrong please try later");
                return false;
            }
        });
    });
    table = $('#users-table').DataTable({
        serverSide: true,
        processing: true,
        autoWidth: false,
        "order":[[9,'asc']],
        ajax: "{{ url('admin/app-user/array-data') }}",
        columns: [
                {data: 'check', orderable: false, searchable: false},
                {data: 'id', orderable: false, searchable: true},
                {data: 'register_type', orderable:false},
                {data: 'name', orderable: false, searchable: true},
                {data: 'email',orderable: false, searchable: true},
                {data: 'phone_number', orderable:false, searchable: true},
                {data: 'social_id',orderable: false, searchable: true},
                {data: 'created_at',orderable: false, searchable: true},
                {data: 'updated_at', orderable:false, searchable: true},
                {data: 'status',orderable:true},
                {data: 'action', name:'action', searchable: false, orderable:false}
        ],
        "language": {

            "paginate": {

                 next: '<i class="fa fa-angle-right"></i>',

                previous: '<i class="fa fa-angle-left"></i>'

            }

        },"fnDrawCallback": function() {



        $('.make-switch').bootstrapSwitch();

           $('.make-switch').on('switchChange.bootstrapSwitch', function (event, state) {

          

            action=state == true?'active':'deactive';

            var ids=[this.value];



            $.ajax({

                url: "{{ url('admin/app-user/perfomaction') }}",

                type: 'GET',

                async: false,

                data: {'action':action,'ids':ids},

                success: function (data) {

                    if(data == "true") {

                       table.ajax.reload(); 

                       

                    }else{

                      alert('something went wrong');

                    }

                },

                error: function () {

                    alert('something went wrong');

                }

            });

        });



        }

    });

table.columns().every(function (){
    var that = this;

    $('input', this.header()).on('keyup change', function (){

        var colid = that[0][0];

        normal = ['n','no','nor','norm','norma','normal'];
        normalObj = ['N','No','Nor','Norm','Norma','Normal'];

        fb = ['f','fa','fac','face','faceb','facebo','faceboo','facebook'];
        fbObj = ['F','Fa','Fac','Face','Faceb','Facebo','Faceboo','Facebook'];

        gplus = ['g','gp','gpl','gplu','gplus'];
        gplusObj = ['G','Gp','Gpl','Gplu','Gplus'];

        var newVal = '';

        if(colid == 2){
            if($.inArray(this.value, normal) != -1 || $.inArray(this.value, normalObj) != -1)
            {
                newVal = '0';
            }
            if($.inArray(this.value, fb) != -1 || $.inArray(this.value, fbObj) != -1){
                newVal = 1;
            }
            if($.inArray(this.value, gplus) != -1 || $.inArray(this.value, gplusObj) != -1){
                newVal = 2;
            }
        }
        if(that.search() !== this.value){
            if(newVal == ''){
                newVal = this.value;
            }
            that
                .search(newVal)
                .draw();
        }
    });
});

    $('#example-select-all').on('click', function () {
        if (!this.checked) {
            $('#tableactiondiv').css({'display': 'none'});
        } else {
            $('#tableactiondiv').css({'display': 'block'});
        }
        // Get all rows with search applied
        var rows = table.rows({'search': 'applied'}).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('.selectcheckbox', rows).prop('checked', this.checked);
    });

    $('#users-table tbody').on('change', '.selectcheckbox', function () {
        // If checkbox is not checked
        var rowsa = $(table.$('.selectcheckbox').map(function () {

            return $(this).prop("checked") ? 'true' : 'false';
        }));
        var array = $.map(rowsa, function (value, index) {
            return [value];
        });

        if (array.indexOf("true") == -1) {
            $('#tableactiondiv').css({'display': 'none'});
        }

        if (!this.checked) {
            var el = $('#example-select-all').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if (el && el.checked && ('indeterminate' in el)) {
                // Set visual state of "Select all" control
                // as 'indeterminate'
                el.indeterminate = true;
            }

        } else {
            $('#tableactiondiv').css({'display': 'block'});
        }
    });
    // Handle form submission event
    $('#tableaction').on('change', function (e) {
        var form = this;
        var ids = [];
        // Iterate over all checkboxes in the table
        table.$('.selectcheckbox').each(function () {

            // If checkbox is checked
            if (this.checked) {
                // Create a hidden element
                ids.push(this.value);

            }

        });
        action = this.value;
        $.ajax({
            url: "{{ url('admin/app-user/perfomaction') }}",
            type: 'GET',
            async: false,
            data: {'action': action, 'ids': ids},
            success: function (data) {
                if (data == "true") {
                    table.ajax.reload();
                    $('#tableactiondiv').css({'display': 'none'});
                    $('#tableaction').val("");
                } else {
                    alert('something went wrong');
                }
            },
            error: function () {
                alert('something went wrong');
            }
        });
    });
</script>

@endpush('scripts')

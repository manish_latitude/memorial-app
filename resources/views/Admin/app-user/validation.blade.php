<!-- @push('scripts') -->
<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
<script type="text/javascript">
    $('.submit').click(function(){
        var url         = "<?=url('/public')?>"
        var first_name      = $('input[name="name"]').val().trim();
        var phone2          = $('input[name="phone"]').val().trim();
        var email           = $('input[name="email"]').val().trim();
        
        if(name == ""){
            $('#name').html('pleas enter name');
            return false;
        }

        var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;  
        if(phone.length > 0) {
          if(!phone.match(phoneno)) {  
              $('#phone').html('please enter valid phone number');
              return false;  
          }
        }

        if(email == ""){
            $('#email').html('please enter email');
            return false;
        }

        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
        if(!email.match(mailformat)) { 
          $('#email').html('please enter valid email');
          return false;
        }
    });

    $("input[name=name]").keyup(function(){
        var name = $(this).val().trim();
        if(name == "") {
            $('#name').html('please enter first name');
            $(this).val('');
            return false;
        }
        $('#name').html('');
    });

    $("input[name=phone]").keyup(function(){
        var phone = $(this).val().trim();
        var numbers = /^[0-9]+$/;  
        if(phone.length == 0) {
          $('#phone').html('');
          return false;
        }
        if(!phone.match(numbers))  {  
          $('#phone').html('please enter valid phone number');
          return false;
        }

        if(phone.length > 12) {
          $('#phone1Error').html('please enter valid phone number');
          return false;
        }
        $('#phone').html('');
    });

    $("input[name=email]").keyup(function(){
        var email = $(this).val().trim();
        if(email == "") {
            $('#email').html('please enter email');
            $(this).val('');
            return false;
        }
        $('#email').html('');
    });
</script>
<!-- @endpush -->
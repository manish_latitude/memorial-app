@extends('Admin.master_layout.master')



@section('title', 'Dashboard')



@section('breadcum')

     / Dashboard 

@endsection



@section('content')



<!--Action boxes-->

<!-- <div class="container-fluid">

    <div class="quick-actions_homepage">

      <h1>Welcome,  {{ ucfirst(Auth::user()->first_name) }} </h1>

      	<img width="100%" src="{{ url('img/banner.png') }}" alt="" />

    </div>

</div> -->

<!--End-Action boxes-->    



<div class="container-fluid">

	<div class="row">

		<div class="col-lg-4 col-xs-6">

			<div class="small-box bg-aqua">

	            <div class="inner">

	            	<h3>{{ $appUser }}</h3>

	            	<p>App Users</p>

	            </div>

	            <div class="icon">

	            	<i class="ion ion-bag"></i>

	            </div>

	            <a href="{{ url('admin/app-user') }}" class="small-box-footer">

	            	More Info

	            	<i class="fa fa-arrow-circle-right"></i>

	            </a>

          	</div>

		</div>

		<div class="col-lg-4 col-xs-6">

			<div class="small-box bg-green">

				<div class="inner">

					<h3>{{ $funeral }}</h3>

					<p>Funeral</p>

				</div>

				<div class="icon">

	            	<i class="ion ion-bag"></i>

	            </div>

	            <a href="{{ url('admin/funeral') }}" class="small-box-footer">

	            	More Info

	            	<i class="fa fa-arrow-circle-right"></i>

	            </a>

			</div>

		</div>

		<div class="col-lg-4 col-xs-6">

			<div class="small-box bg-yellow">

				<div class="inner">

					<h3>{{ $member }}</h3>

					<p>Members</p>

				</div>

				<div class="icon">

	            	<i class="ion ion-bag"></i>

	            </div>

	            <a href="{{ url('admin/member') }}" class="small-box-footer">

	            	More Info

	            	<i class="fa fa-arrow-circle-right"></i>

	            </a>

			</div>

		</div>

		<!-- <div class="col-lg-3 col-xs-6">

			<div class="small-box bg-red">

				<div class="inner">

					<h3>65</h3>

					<p>Unique Visitors</p>

				</div>

				<div class="icon">

	            	<i class="ion ion-bag"></i>

	            </div>

	            <a href="#" class="small-box-footer">

	            	More Info

	            	<i class="fa fa-arrow-circle-right"></i>

	            </a>

			</div>

		</div> -->

	</div>

</div>



@endsection



@section('footer')

    @parent

@endsection


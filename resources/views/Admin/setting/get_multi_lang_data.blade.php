<?php $firstid = 0; 
if(empty($configkeys)){ echo ' <div class="col-md-12 col-sm-12 col-xs-12 text-primary">No Record Found</div>'; } ?>
@foreach($configkeys as $key=>$configkey)
    <?php 
        $firstid++;
        if($firstid == 1) {
            $deleteCss = "padding-top: 15%";
        } else {
            $deleteCss = "padding-top: 5%";
        }
    ?>
    @if($firstid == 1)
    <div class="form-group col-md-12 col-sm-12 col-xs-12">
        <h4>Value list for <b>{{ $data_app->app_name }}</b></h4>
    </div>
    @endif
   
    
    
    <div class="form-group col-md-12 col-sm-12 col-xs-12 entity_multi_data">
        <div class="row">
             <div class="form-group col-md-3 col-sm-3 col-xs-12 childRow_{{$configkey['id']}}">
               
              @if($firstid == 1)   <label class="control-label"><b>Key</b></label> @endif
                
                
                {{ Form::text('key[]', $configkey['key'], array('class' => 'form-control', 'id' => 'default_text')) }}
               
            </div>
           <div class="form-group col-md-3 col-sm-3 col-xs-12 childRow_{{$configkey['id']}}">
               
               @if($firstid == 1)  <label class="control-label"><b>value</b></label> @endif
                
                
                {{ Form::text('value[]', $configkey['value'], array('class' => 'form-control', 'id' => 'default_text')) }}
                
            </div>
			<div class="form-group col-md-3 col-sm-3 col-xs-12 childRow_{{$configkey['id']}}">
               
               @if($firstid == 1)  <label class="control-label"><b>Description</b></label> @endif
                
                
                {{ Form::text('description[]', $configkey['description'], array('class' => 'form-control', 'id' => 'default_text')) }}
                {{ Form::hidden('cid[]', $configkey['id']) }}
            </div>
            <div class="form-group col-md-3 col-sm-3 col-xs-12" style="display:none;">
                <span class="trashRow trashRow_{{$configkey['id']}} remove" trashid="{{$configkey['id']}}" dataId="1">
                    <i class="fa fa-trash" style="font-size: 18px;{{ $deleteCss }}"></i>
                </span>
            </div>
        </div>
    </div>
@endforeach
{{ Form::hidden('total', $firstid) }}
<?php if(!empty($configkeys)){ ?>
<div class="col-md-12 col-sm-12 col-xs-12 submit" style="display: none;">
                                    <div class="box-footer">
                                        {!! Form::submit('submit', array('class' => 'btn btn-primary ','id'=>'submit')) !!}
                                    </div>
                                </div> <?php } ?>
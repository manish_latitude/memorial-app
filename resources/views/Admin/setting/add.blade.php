@extends('Admin.master_layout.master')

@section('title', ' Setting')

@section('breadcum')
     / Setting
@endsection

@section('content')
 
               @if(Session::has('message')) 
                <div class="alert alert-success" role="alert">
                        <strong>{{Session::get('message')}}</strong>
                </div> <!-- alert ends -->
                @endif
                 
    <form action="{{ url('admin/setting/user-email-insert')}}" method="post">
        <div class="row">
            {{-- {{ method_field('PUT') }} --}}
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <div class="col-sm-6 form-group">
                <label>Email Id</label>
                <input type="email" name="email" placeholder="E-mail" value="{{ $email_user->email }}" class="form-control{{ $errors->has('email') ? ' has-error' : '' }}">
                <small class="text-danger">{{ $errors->first('email') }}</small>
            </div>
           {{--  <div class="col-sm-6 form-group">
                <label>Password</label>
                <input type="password" name="password" placeholder="Password" value="{{ $email_user->password }}" class="form-control{{ $errors->has('password') ? ' has-error' : '' }}">
                 <small class="text-danger">{{ $errors->first('password') }}</small>
            </div> --}}
            
        </div>
        <div class="row">
            <div class="col-sm-6 form-group">
                <label>Institutes Amount</label>
                <input type="text" name="institutes_amount" placeholder="Institutes Amount" value="{{$email_user->institutes_amount}}" class="form-control{{ $errors->has('amount') ? ' has-error' : '' }}">
                 <small class="text-danger">{{ $errors->first('amount') }}</small>
            </div>
            <div class="col-sm-6 form-group">
                <label>Student Amount</label>
                <input type="text" name="student_amount" placeholder="Student Amount" value="{{$email_user->student_amount}}" class="form-control{{ $errors->has('amount') ? ' has-error' : '' }}">
                 <small class="text-danger">{{ $errors->first('amount') }}</small>
            </div>
        </div>
        <div class="text-center">
            <input type="submit" class="btn btn-primary" value="Submit">
        </div>
    </form>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
 
@endpush
@extends('Admin.master_layout.master')

@section('title', 'App Content List')

@section('breadcum')
     / App Content List 
@endsection

@section('content')
<div class="">
    <?php
    /*
     <div class="page-title">
        <div class="title_left">
            <h3>Users</h3>
            <p>You can manage users here.</p>
            </p>
        </div>
    </div>
    */
    ?>

    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>Content</h4>
					<div class="col-sm-3 pull-right">
                    <a href="{{ url('app-content/create') }}" class="btn btn-l btn-xl btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Add  Content
                    </a>
                     <a href="{{ url('app-content/download/excel') }}" style="display:none;" class="btn btn-l btn-xl btn-warning pull-right">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        Export
                    </a>
					</div>
					<div class="col-sm-3 pull-right" id="tableactiondiv" style="display:none;">
					<select name="actions" id="tableaction" class="form-control" >
					<option value="">Select </option>
					  <option value="delete"> Delete </option>
					   <option value="active"> Active </option>
					    <option value="deactive"> Deactive </option>
					</select>
					</div>
					
					
                     <div class="clearfix"></div>
                </div>
                <table id="users-table" class="table table-bordered">
                    <thead>
                    <tr>
                        <?php
                        /*
                        <th>#</th>
                        */
                        ?>
						 <th><label class="chk"><input name="select_all" value="1" id="example-select-all" type="checkbox" />
					 <span class="checkmark"></span> </label></th>
                        <th>App id</th>
						<th>Type</th>
						<th>Title</th>
                      
                        
                        <th>Status</th>
                        <th>Created On</th>
                 
                        <th>Action</th>
                        
                    </tr>
					 <tr class="filters">
						 <th></th>
                            <th>ID</th>
                            <th>type</th>
                            <th>Title</th>
<!--                            <th>Type</th>-->
                            <th class="statuscolumn">Status</th>
                            <th>Date</th>
<!--                            <th>Updated On</th>-->
                            <th></th>
                            
                        </tr>
                    </thead>
                </table>

               <div id="myModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="fa fa-close"></i>
				</div>				
				<h4 class="modal-title">Are you sure?</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p>Do you really want to delete these records? This process cannot be undone.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
				<a href="" class="btn btn-danger" id="deleteurl">Delete</a>
			</div>
		</div>
	</div>
</div>  
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<link href="{{ url('public/iCheck/all.css') }}" rel="stylesheet" />
<script type="text/javascript" src="{{ url('public/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.css" rel="stylesheet" />
		  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js" data-turbolinks-track="true"></script>
<script type="text/javascript">

setTimeout(function() {
        $(".alert-success").hide()
}, 10000);

  
     var table =   $('#users-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: "{{ url('/app-content/array-data') }}",
            columns: [
            //  {data: 'id', orderable: true, searchable: true },
			  {data: 'check', orderable: false, searchable: false },
               {data: 'app_id', searchable: true, orderable: false},
                {data: 'content_type',orderable: true, searchable: true},
				{data: 'title',orderable: true, searchable: true},
				
                {data: 'status', orderable: true},
                {data: 'created_at', searchable: false },
                
               {data: 'action', name:'action', searchable: false, orderable: false }
            ],
			"ordering": false,
            "language": {
                "paginate": {
                     next: '<i class="fa fa-angle-right"></i>',
                    previous: '<i class="fa fa-angle-left"></i>'
                }
            },"fnDrawCallback": function() {
			 //iCheck for checkbox and radio inputs
    
           $('.make-switch').bootstrapSwitch();
		   $('.make-switch').on('switchChange.bootstrapSwitch', function (event, state) {
		  
 action=state == true?'active':'deactive';
 var ids=[this.value];
	  $.ajax({
            url: "{{ url('app-content/perfomaction') }}",
            type: 'GET',
            async: false,
            data: {'action':action,'ids':ids},
            success: function (data) {
                if(data == "true") {
                   table.ajax.reload(); 
				   
                }else{
                  alert('something went wrong');
                }
            },
            error: function () {
                alert('something went wrong');
            }
        });
});
        }
			
        });
   $("#users-table thead .filters th").each( function ( i ) {
		
		if ($(this).text() !== '') { if($(this).text() == 'ID'){
	        var isStatusColumn = (($(this).text() == 'Status') ? true : false);
			dd='';
			$.ajax({
        url: "{{ url('app-content/getappdropdown') }}",
        type: 'GET',
        async: false,
        success: function (data) {
		dd=data;
		}
    });
			
		 var select = $(dd).appendTo( $(this).empty() ).on( 'change', function () {
	                var val = $(this).val();
					table.column( i )
	                    .search( val ? '^'+$(this).val()+'$' : val, true, false )
	                    .draw();
	            } );
				if (isStatusColumn) {
				var statusItems = [];
				
                /* ### IS THERE A BETTER/SIMPLER WAY TO GET A UNIQUE ARRAY OF <TD> data-filter ATTRIBUTES? ### */
				table.column( i ).nodes().to$().each( function(d, j){
					var thisStatus = $(j).attr("data-filter");
					if($.inArray(thisStatus, statusItems) === -1) statusItems.push(thisStatus);
				} );
				
				statusItems.sort();
								
				$.each( statusItems, function(i, item){
				    select.append( '<option value="'+item+'">'+item+'</option>' );
				});

			}
            // All other non-Status columns (like the example)
			else {
				table.column( i ).data().unique().sort().each( function ( d, j ) {  
					select.append( '<option value="'+d+'">'+d+'</option>' );
		        } );	
			}
            /* var val = $("#filterapp").val();        
	 		table.column( i )
	                    .search( val ? '^'+val+'$' : val, true, false )
	                    .draw();*/
			// Get the Status values a specific way since the status is a anchor/image
			
	        
		}else if($(this).text() == 'type'){
		
		 var select = $('<select class="form-control"><option value=""> Search Type</option><option value="image"> Image</option><option value="content"> Content</option><option value="youtubeurl"> Youtube Url</option><option value="wesiteurl"> Wesite Url</option></select>').appendTo( $(this).empty() ).on( 'change', function () {
	                var val = $(this).val();
					table.column( i )
	                    .search( val ? '^'+$(this).val()+'$' : val, true, false )
	                    .draw();
	            } );
				
				table.column( i ).data().unique().sort().each( function ( d, j ) {  
					select.append( '<option value="'+d+'">'+d+'</option>' );
		        } );	
			
            /* var val = $("#filterapp").val();        
	 		table.column( i )
	                    .search( val ? '^'+val+'$' : val, true, false )
	                    .draw();*/
			// Get the Status values a specific way since the status is a anchor/image
			
	        
		
		} else{
		if(i == 0 || i == 6){ 
		
		  }else{
        var title = $(this).text();
        $(this).html( '<input type="text" class="form-control" style="width:126px;" placeholder="Search '+title+'" />' ); }
		
		} }
    } );
	
	 // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input',this.header() ).on( 'keyup change', function () {   
		     var colid=that[0][0]; 
			activearray=['a', 'ac', 'act', 'acti', 'activ', 'active'];
			deactivearray=['i', 'in', 'ina', 'inac', 'inact', 'inacti', 'inactiv', 'inactive'];
			var newval='NULL';
			
			if(colid == 4){
			if($.inArray(this.value,activearray) != -1){newval=1; }
			if($.inArray(this.value,deactivearray) != -1){newval=0; }
			}
			
            if ( that.search() !== this.value ) {
			if(newval == 'NULL'){ newval=this.value;  }
                that
                    .search( newval )
                    .draw();
            }
        } );
 } );
  // Handle click on "Select all" control
   $('#example-select-all').on('click', function(){ 
   if(!this.checked){$('#tableactiondiv').css({'display':'none'});
   }else{   
   $('#tableactiondiv').css({'display':'block'});}
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('.selectcheckbox', rows).prop('checked', this.checked);
   });

   // Handle click on checkbox to set state of "Select all" control
   $('#users-table tbody').on('change', '.selectcheckbox', function(){
      // If checkbox is not checked
	  var rowsa = $(table.$('.selectcheckbox').map(function () {
	 
        return $(this).prop("checked") ? 'true' : 'false';
} ) ); 
var array = $.map(rowsa, function(value, index) {
    return [value];
});

 if(array.indexOf("true") == -1){$('#tableactiondiv').css({'display':'none'}); }     
	  
	  if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control
            // as 'indeterminate'
            el.indeterminate = true;
         }
		 
      }else{ $('#tableactiondiv').css({'display':'block'});  }
   });
 // Handle form submission event
   $('#tableaction').on('change', function(e){
      var form = this;
var ids=[];
      // Iterate over all checkboxes in the table
      table.$('.selectcheckbox').each(function(){
       
            // If checkbox is checked
            if(this.checked){
               // Create a hidden element
			   ids.push(this.value);
              
            }
        
      }); 
	  action=this.value;
	  $.ajax({
            url: "{{ url('app-content/perfomaction') }}",
            type: 'GET',
            async: false,
            data: {'action':action,'ids':ids},
            success: function (data) {
                if(data == "true") {
                   table.ajax.reload(); 
				   $('#tableactiondiv').css({'display':'none'});
				   $('#tableaction').val("");
                }else{
                  alert('something went wrong');
                }
            },
            error: function () {
                alert('something went wrong');
            }
        });
   });
   $( document ).ready(function() {
   
    
});


</script>
@endpush('scripts')
<style>
.modal-confirm {		
		color: #636363;
		width: 400px;
	}
	.modal-confirm .modal-content {
		padding: 20px;
		border-radius: 5px;
		border: none;
        text-align: center;
		font-size: 14px;
	}
	.modal-confirm .modal-header {
		border-bottom: none;   
        position: relative;
	}
	.modal-confirm h4 {
		text-align: center;
		font-size: 26px;
		margin: 30px 0 -10px;
	}
	.modal-confirm .close {
        position: absolute;
		top: -5px;
		right: -2px;
	}
	.modal-confirm .modal-body {
		color: #999;
	}
	.modal-confirm .modal-footer {
		border: none;
		text-align: center;		
		border-radius: 5px;
		font-size: 13px;
		padding: 10px 15px 25px;
	}
	.modal-confirm .modal-footer a {
		color: #999;
	}		
	.modal-confirm .icon-box {
		width: 80px;
		height: 80px;
		margin: 0 auto;
		border-radius: 50%;
		z-index: 9;
		text-align: center;
		border: 3px solid #f15e5e;
	}
	.modal-confirm .icon-box i {
		color: #f15e5e;
		font-size: 46px;
		display: inline-block;
		margin-top: 13px;
	}
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
		background: #60c7c1;
		text-decoration: none;
		transition: all 0.4s;
        line-height: normal;
		min-width: 120px;
        border: none;
		min-height: 40px;
		border-radius: 3px;
		margin: 0 5px;
		outline: none !important;
    }
	.modal-confirm .btn-info {
        background: #c1c1c1;
    }
    .modal-confirm .btn-info:hover, .modal-confirm .btn-info:focus {
        background: #a8a8a8;
    }
    .modal-confirm .btn-danger {
        background: #f15e5e;
    }
    .modal-confirm .btn-danger:hover, .modal-confirm .btn-danger:focus {
        background: #ee3535;
    }
	a#deleteurl {
    color: #fff;
    line-height: 28px;
}
	.trigger-btn {
		display: inline-block;
		margin: 100px auto;
	}
	span.bootstrap-switch-handle-on.bootstrap-switch-primary {
    background: #26B99A !important;
    border-radius: 0 !important;
}
#users-table th {
    vertical-align: middle;
    font-size: 13px;
}
</style>

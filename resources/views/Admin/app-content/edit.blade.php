@extends('Admin.master_layout.master')

@section('title', 'Edit App')

@section('breadcum')
     / Edit App 
@endsection

@section('content')

<!-- @foreach ($errors->all() as $error)
<p class="alert alert-warning">{{ $error }}</p>
@endforeach
<span class="errormessage"></span> -->
<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(count($errors))
                <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>Whoops!</strong> There were some problems with your request.
                    <br/>
                </div>
            @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Edit Content</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('app-content/store'),'method'=>'POST', 'id'=>'signup','files'=>true)) !!}
                <div class="x_content">
                    <input type="hidden" name="id" value="{{ $data->id }} " >
                    <div class="form-group <?php  echo $errors->first('app_name') !== '' ? 'has-error' : '';?>">
                            <span class="text-danger" >{{ $errors->first('app_name') }}</span>
                              {!! Form::label('APP Name', 'App name',array('class'=>'control-label')) !!}
                            {!! Form::select('app_id',isset($appdata)?$appdata:[],$data->app_id,['placeholder' => 'Select App ','class' => 'form-control']) !!}
                            <span class="text-danger" >{{ $errors->first('app_id') }}</span>
                        </div>
                        <div class="form-group <?php  echo $errors->first('app_type') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Type', 'Type',array('class'=>'control-label')) !!}
                            {{ Form::select('app_type', ['image' => '','image' => 'Image','content' => 'Content','youtubeurl' => 'Youtube Url','wesiteurl' => 'Wesite Url','all' => 'All'],$data->content_type,['placeholder' => 'Select Type','class' => 'form-control app_type','id' => 'app_type']) }}
                            <span class="text-danger" >{{ $errors->first('app_type') }}</span>
                        </div>
						<div class="form-group <?php  echo $errors->first('title') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Title', 'Title',array('class'=>'control-label ')) !!}
                            {{ Form::text('title', $data->title,array('class' => 'form-control ', 'id' => 'title')) }}
                            <span class="text-danger" >{{ $errors->first('title') }}</span>
                        </div>
						
<div class="form-group"> <?php if($data->image != "") { $iclass='visiblec';}else {$iclass='notvisible';} ?>
                            {!! Form::label('Image', 'Image',array('class'=>'control-label image urls '.$iclass)) !!}
                            {{ Form::file('image',array('class' => 'image urls '.$iclass, 'id' => 'image')) }}
                            
                        </div>
						 <?php
                                if(isset($data->image) && $data->image != "") {
                                ?>
                                    <img src="{{ url('/uploads/content').'/'.$data->image }}" class="contact_photo img-thumbnail image <?php echo $iclass; ?>" >
                                <?php
                                } 
                                ?>
                                <input type="hidden" name="old_content_image" value="{{ isset($data->image)?$data->image:'' }}">
								<?php if($data->content != "") { $cclass='visiblec';}else {$cclass='notvisible'; ?> <style> .mce-tinymce{ display:none; } </style> <?php } ?>
						<div class="form-group <?php  echo $errors->first('content') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Content', 'Content',array('class'=>'control-label content '.$cclass)) !!}
                            {!! Form::textarea('content',$data->content,['class'=>'form-control content textEditor non-editable ', 'placeholder'=>'content','contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}
                            <span class="text-danger" >{{ $errors->first('app_slug') }}</span>
                        </div>
						<?php if($data->url != "") { $uclass='visiblec';}else {$uclass='notvisible';} ?>
						<div class="form-group">
                            {!! Form::label('Url', 'Url',array('class'=>'control-label youtubeurl wesiteurl '.$uclass)) !!}
                            {{ Form::text('url', $data->url,array('class' => 'form-control youtubeurl wesiteurl '.$uclass, 'id' => 'url')) }}
                            
                        </div>
						<?php if($data->yurl != "") { $uclass='visiblec';}else {$uclass='notvisible';} ?>
						<div class="form-group">
                            {!! Form::label('Youtube Url', 'Youtube Url',array('class'=>'control-label urls '.$uclass)) !!}
                            {{ Form::text('yurl', $data->yurl,array('class' => 'form-control urls '.$uclass, 'id' => 'yurl')) }}
                            
                        </div>
						<?php if($data->wurl != "") { $uclass='visiblec';}else {$uclass='notvisible';} ?>
						<div class="form-group">
                            {!! Form::label('Website Url', 'Website Url',array('class'=>'control-label urls '.$uclass)) !!}
                            {{ Form::text('wurl', $data->wurl,array('class' => 'form-control urls '.$uclass, 'id' => 'wurl')) }}
                            
                        </div>
                        <div class="form-group <?php  echo $errors->first('status') !== '' ? 'has-error' : '';?>">
                            {{ Form::label('status', 'Status:',array('class'=>'control-label')) }}
                            {{ Form::select('status', ['1' => 'Active','0' => 'InActive'],$data->status,['id' => 'status','class' => 'form-control']) }}
                            <span class="text-danger" >{{ $errors->first('status') }}</span>
                        </div>
<!--                        <div class="form-group">
                            <label for="status">{{ Form::label('status', 'Status:') }}</label>
                            @if ($data->status == 1)
                            {{ Form::radio('status', '1',['checked' => 'checked'],['class' => 'minimal']) }} Active
                            {{ Form::radio('status', '0','',['class' => 'minimal']) }} InActive
                            @else
                            {{ Form::radio('status', '1','',['class' => 'minimal']) }} Active
                            {{ Form::radio('status', '0',['checked' => 'checked'],['class' => 'minimal']) }} InActive
                            @endif
                        </div>-->
                        
<!--                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-danger submit" data-toggle="modal" data-target=".bs-example-modal-reset">Reset Password</button>    
                        </div>-->
<!-- 'id'=>'reset-password', -->

                    </div>
                    <div class="box-footer">
					
                        {!! Form::submit('Save', array('class' => 'btn btn-primary edit-submit')) !!}
                       <!-- <button type="button" class="btn btn-primary submit" data-toggle="modal" data-target=".bs-example-modal-edit">Save</button>-->
                        <a class="btn btn-default btn-close" href="{{ URL::to('/app-content') }}">Cancel</a>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('resources/admin-assets/js/tinymce/tinymce.min.js') }}" type="text/javascript"> </script>
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
<script type="text/javascript">
    $(function() { 
    tinymce.init({ selector:'.textEditor' }); 
}); 
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);

    $('#submit').click(function(){
        var password = $("input[name='app_name']").val();
        if (password == "") {
            $('#passwordError').html('please enter App name');
            return false;
        }
        $.ajax({
            url: "{{ url('confirm-pass') }}",
            type: 'GET',
            async: false,
            data: {'password':password},
            success: function (data) {
                if(data == "true") {
                    // $('#passwordError').html('password match');
                    $('.edit-close').click();
                    $('.edit-submit').click();
                }else{
                  $('#passwordError').html('please enter valid password');
                  return false;
                }
            },
            error: function () {
                $('#passwordError').html("something went wrong please try later");
            }
        });
    });

$('.app_type').on('change',function(){
 val=this.value;
  $('.urls').addClass('notvisible');
$('.mce-tinymce').addClass('notvisible');
 $('.mce-tinymce').removeClass('visiblec');
 $('.visiblec').addClass('notvisible');
  $('.visiblec').removeClass('visiblec');
 $('.'+val).addClass('visiblec');
 $('.'+val).removeClass('notvisible');
  if(val == 'content'){
 $('.content').addClass('notvisible');
 $('.content').removeClass('visiblec');
  $('.mce-tinymce').addClass('visiblec');
  $('.mce-tinymce').removeClass('notvisible');}
  if(val == 'all'){
 $('.urls').removeClass('notvisible');
 $('.image').removeClass('notvisible');
  $('.content').removeClass('notvisible');
  $('.mce-tinymce').addClass('visiblec');
  $('.mce-tinymce').removeClass('notvisible');
 }
});
</script>
<style>

.notvisible{ display:none !important; }
.visiblec{ display:block !important; }
</style>
@endpush('scripts')
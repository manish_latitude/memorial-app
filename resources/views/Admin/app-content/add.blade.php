@extends('Admin.master_layout.master')

@section('title', 'Create Content')

@section('breadcum')
     / Create Content
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create Content</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('app-content/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">
<div class="form-group <?php  echo $errors->first('app_id') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('APP Name', 'App name',array('class'=>'control-label')) !!}
                            {!! Form::select('app_id',isset($appdata)?$appdata:[],'',['placeholder' => 'Select App ','class' => 'form-control']) !!}
                            <span class="text-danger" >{{ $errors->first('app_id') }}</span>
                        </div>
                        <div class="form-group <?php  echo $errors->first('app_type') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Type', 'Type',array('class'=>'control-label')) !!}
                            {{ Form::select('app_type', ['image' => '','image' => 'Image','content' => 'Content','youtubeurl' => 'Youtube Url','wesiteurl' => 'Wesite Url','all' => 'All'],['id' => 'app_type'],['placeholder' => 'Select Type','class' => 'form-control app_type']) }}
                            <span class="text-danger" >{{ $errors->first('app_type') }}</span>
                        </div>
						<div class="form-group <?php  echo $errors->first('title') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Title', 'Title',array('class'=>'control-label')) !!}
                            {{ Form::text('title', null,array('class' => 'form-control ', 'id' => 'title')) }}
                            <span class="text-danger" >{{ $errors->first('title') }}</span>
                        </div>
<div class="form-group <?php  echo $errors->first('image') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Image', 'Image',array('class'=>'control-label image notvisible')) !!}
                            {{ Form::file('image[]',array('class' => ' image notvisible', 'id' => 'image','multiple'=>'multiple')) }}
                            <span class="text-danger" >{{ $errors->first('image') }}</span>
                        </div>
						<div class="form-group <?php  echo $errors->first('content') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Content', 'Content',array('class'=>'control-label content notvisible')) !!}
                            {!! Form::textarea('content','',['class'=>'form-control notvisible content textEditor non-editable', 'placeholder'=>'content','contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}
                            <span class="text-danger" >{{ $errors->first('content') }}</span>
                        </div>
						<div class="form-group <?php  echo $errors->first('url') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Url', 'Url',array('class'=>'control-label youtubeurl wesiteurl notvisible')) !!}
                            {{ Form::text('url', null,array('class' => 'form-control youtubeurl wesiteurl notvisible', 'id' => 'url')) }}
                            <span class="text-danger" >{{ $errors->first('url') }}</span>
                        </div>
						
						<div class="form-group <?php  echo $errors->first('wurl') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Website Url', 'Website Url',array('class'=>'control-label urls notvisible')) !!}
                            {{ Form::text('wurl', null,array('class' => 'form-control notvisible urls', 'id' => 'wurl')) }}
                            <span class="text-danger" >{{ $errors->first('wurl') }}</span>
                        </div>
						<div class="form-group <?php  echo $errors->first('url') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Youtube Url', 'Youtube Url',array('class'=>'control-label urls notvisible')) !!}
                            {{ Form::text('yurl', null,array('class' => 'form-control notvisible urls', 'id' => 'yurl')) }}
                            <span class="text-danger" >{{ $errors->first('url') }}</span>
                        </div>
                        <div class="form-group">
                            <label>{{ Form::label('status', 'Status:') }}</label>
                            {{ Form::select('status', ['1' => 'Active','0' => 'InActive'],['id' => 'status'],['class' => 'form-control']) }}
                        </div>
                        <div class="box-footer">
                            {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('/app-content') }}">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('resources/admin-assets/js/tinymce/tinymce.min.js') }}" type="text/javascript"> </script>
<script type="text/javascript">
$(function() { 
    tinymce.init({ selector:'.textEditor' }); 
}); 
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);
	
	$('.app_type').on('change',function(){
 val=this.value;

 $('.mce-tinymce').addClass('notvisible');
  $('.urls').addClass('notvisible');
 $('.mce-tinymce').removeClass('visiblec');
 $('.visiblec').addClass('notvisible');
  $('.visiblec').removeClass('visiblec');
 $('.'+val).addClass('visiblec');
 $('.'+val).removeClass('notvisible');
  if(val == 'content'){
 $('.content').addClass('notvisible');
 $('.content').removeClass('notvisible');
  $('.mce-tinymce').addClass('visiblec');
  $('.mce-tinymce').removeClass('notvisible');
 
 }
 if(val == 'all'){
 $('.urls').removeClass('notvisible');
 $('.image').removeClass('notvisible');
  $('.content').removeClass('notvisible');
  $('.mce-tinymce').addClass('visiblec');
  $('.mce-tinymce').removeClass('notvisible');
 }
});
</script>
<style>
.mce-tinymce{ display:none; }
.notvisible{ display:none !important; }
.visiblec{ display:block !important; }
</style>
@endpush('scripts')
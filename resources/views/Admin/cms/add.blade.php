@extends('Admin.master_layout.master')

@section('title', 'Create CMS')

@section('breadcum')
     / Create CMS
@endsection

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    @if(count($errors))
        <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your request.
            <br/>
        </div>
    @endif
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create CMS</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/cms/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">

                        <div class="form-group <?php  echo $errors->first('title') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('title', 'Title',array('class'=>'control-label')) !!}
                            <span class="required">*</span>
                            {{ Form::text('title', null,array('class' => 'form-control', 'id' => 'name','placeholder' => 'Title')) }}
                            <span class="text-danger" >{{ $errors->first('title') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('slug') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('slug', 'Slug',array('class'=>'control-label')) !!}
                            <span class="required">*</span>
                            {{ Form::text('slug', null, array('class' => 'form-control', 'id' => 'price','placeholder' => 'Slug')) }}
                            <span class="text-danger" >{{ $errors->first('slug') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('description') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('description', 'Description',array('class'=>'control-label')) !!}
                            {{ Form::textarea('description', null, array('class' => 'form-control', 'id' => 'summary-ckeditor')) }}
                            <span class="text-danger" >{{ $errors->first('description') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="box-footer">
                    {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('admin/cms') }}">Cancel</a>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('public\ckeditor\ckeditor.js') }}" type="text/javascript"> </script>

<script>
    CKEDITOR.replace('summary-ckeditor');
</script>
<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);
</script>
@endpush('scripts')
@extends('Admin.master_layout.master')

@section('title', 'State List')

@section('breadcum')
	/ State List
@endsection

@section('content')
	<div class="">
		<div class="clearfix"></div>
	    @if (Session::has('message'))
	    <div class="alert alert-info alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('message') !!}</div>
	    @endif
	    @if (Session::has('error'))
	    <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('error') !!}</div>
	    @endif
	    @if (Session::has('success'))
	    <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('success') !!}</div>
	    @endif

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
                        <h4>State</h4>
                        <div class="col-sm-2 pull-right">
                            <a href="{{ url('admin/state/create') }}" class="btn btn-sm btn-xs btn-success pull-right">
                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                              Add State
                            </a>
                        </div>
                        <div class="col-sm-2 pull-right" id="tableactiondiv" style="display:none;">
                            <select name="actions" id="tableaction" class="form-control" >
                                <option value="">Select </option>
                                <option value="active"> Active </option>
                                <option value="deactive"> Deactive </option>
                                <option value="delete"> Delete </option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
					<table id="users-table" class="table table-bordered">
						<thead>
							<tr>
                                <th><label class="chk">
                                    <input name="select_all" value="1" id="example-select-all" type="checkbox" />
                                    <span class="checkmark"></span> </label>
                                </th>
                                <th>ID</th>
                                <th>State</th>
                                <th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>

                    <div id="myModal" class="modal fade users-table-1">
                          <div class="modal-dialog modal-confirm">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <div class="icon-box">
                                          <i class="fa fa-close"></i>
                                      </div>              
                                      <h4 class="modal-title">Are you sure?</h4>  
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  </div>
                                  <div class="modal-body">
                                      <p>Do you really want to delete these records? This process cannot be undone.</p>
                                  </div>
                                  <div class="modal-footer">
                                      <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                                      <a href="" class="btn btn-danger" id="deleteurl">Delete</a>
                                  </div>
                              </div>
                          </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@push('scripts')


<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js" data-turbolinks-track="true"></script>
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>

<script type="text/javascript">
	setTimeout(function(){
		$(".alert-success").hide()
	}, 10000);

	$(document).on("click", '.delete-btn', function(event) { 
        var id = $(this).attr('data-id');
        $('#select-delete').val(id);
    });

    $('#delete-submit').click(function(){
    var id = $('#select-delete').val();
    alert(id);
    $.ajax({
    	url: "{{ url('admin/state/destroy') }}",
    	type: 'GET',
    	async: false,
    	data: {'password': password, 'id': id},
    	success: function(data){
    		if(data == "true"){
    			$('.delete-close').click();
    			$('.delete-' + id).parent().parent().remove();
    			$(document).ready(function(){
    				$.notify({
    					message: 'State Deleted Successfully'
    				},{
    					type: 'success'
    				});
    			});
    		}else{
    			$('#deletepasswordError').html('please enter valid password');
                return false;
    		}
    	},
    	error: function(){
    		$('#deletepasswordError').html("something went wrong please try later");
            return false;
    	}
    });
});

table = $('#users-table').DataTable({
	serverSide: true,
	processing: true,
  "order": [[3,'asc']],
	ajax: "{{ url('admin/state/array-data') }}",
	columns: [
            {data: 'check', orderable: false, searchable: false},
	        {data: 'id',orderable: false, searchable: true},
            {data: 'state', orderable:false, searchable: true},
            {data: 'status',orderable:true},
			{data: 'action', name: 'action', searchable: false, orderable: false}
		],
		"language":{
			"paginate" :{
				next:'<i class="fa fa-angle-right"></i>',
				previous: '<i class="fa fa-angle-left"></i>'
			}
		},"fnDrawCallback": function() {

         $('.make-switch').bootstrapSwitch();
            $('.make-switch').on('switchChange.bootstrapSwitch', function (event, state) {
          
             action=state == true?'active':'deactive';
             var ids=[this.value];
             
             $.ajax({
                 url: "{{ url('admin/state/perfomaction') }}",
                 type: 'GET',
                 async: false,
                 data: {'action':action,'ids':ids},
                 success: function (data) {
                     if(data == "true") {
                        table.ajax.reload(); 
                       
                     }else{
                       alert('something went wrong');
                     }
                 },
                 error: function () {
        
                     alert('something went wrong');
                 }
             });
         });

        }
});

table.columns().every(function () {
        var that = this;

        $('input', this.header()).on('keyup change', function () {
            var colid = that[0][0];
            activearray = ['a', 'ac', 'act', 'acti', 'activ', 'active'];
            deactivearray = ['i', 'in', 'ina', 'inac', 'inact', 'inacti', 'inactiv', 'inactive'];
             var newval = '';
            if (colid == 3) {
                if ($.inArray(this.value, activearray) != -1) {
                    newval = 1;
                }
                if ($.inArray(this.value, deactivearray) != -1) {
                    newval = 0;
                }
            }
            if (that.search() !== this.value) {
                if (newval == '') {
                    newval = this.value;
                }
                that
                        .search(newval)
                        .draw();
            }
        });
    });
</script>
@endpush('scripts')
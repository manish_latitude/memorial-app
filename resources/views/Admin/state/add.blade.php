@extends('Admin.master_layout.master')

@section('title', 'Create State')

@section('breadcum')
     / Create State
@endsection

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    @if(count($errors))
        <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your request.
            <br/>
        </div>
    @endif
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create State</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/state/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">

                        <div class="form-group <?php  echo $errors->first('country') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('country', 'Country',array('class'=>'control-label')) !!}
                            <span class="required">*</span>
                            {!! Form::select('country',isset($country)?$country:[],'',['placeholder' => '-- Select Country --','class' => 'form-control','id'=>'country']) !!}
                            <span class="text-danger" >{{ $errors->first('country') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('state', 'State',array('class'=>'control-label')) !!}
                            <span class="required">*</span>
                            {{ Form::text('state', null,array('class' => 'form-control', 'id' => 'state','placeholder' => 'State')) }}
                            <span class="text-danger" >{{ $errors->first('state') }}</span>
                        </div>

                        <div class="form-group <?php echo $errors->first('status') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('status','Status',array('class'=>'control-label')) !!} 
                            <span class="required">*</span>
                            <select class="form-control" name="status">
                                <option readonly>-- Select Status --</option>
                                <option value="1">Active</option>
                                <option value="0">In-active</option>
                            </select>
                            <span class="text-danger" >{{ $errors->first('status') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="box-footer">
                    {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('admin/state') }}">Cancel</a>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('public\ckeditor\ckeditor.js') }}" type="text/javascript"> </script>

<script>
    CKEDITOR.replace('summary-ckeditor');
</script>
<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);
</script>
@endpush('scripts')
@extends('Admin.master_layout.master')

@section('title', 'Create State')

@section('breadcum')
     / Create State
@endsection

@section('content')
<div class="clearfix"></div>
        @if (Session::has('message'))
        <div class="alert alert-info alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('message') !!}</div>
        @endif
        @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('error') !!}</div>
        @endif
        @if (Session::has('success'))
        <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('success') !!}</div>
        @endif
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    @if(count($errors))
        <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your request.
            <br/>
        </div>
    @endif
    </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">

                <div class="x_title">
                    <h4>Create City</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/state/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">
                    <input type="hidden" name="id" value="{{ $state->id }}">
                    <input type="hidden" name="city1" value="add">

                        <div class="form-group <?php  echo $errors->first('city') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('city', 'City',array('class'=>'control-label')) !!}
                            <span class="required">*</span>
                            {{ Form::text('city', null,array('class' => 'form-control', 'id' => 'city','placeholder' => 'City')) }}
                            <span class="text-danger" >{{ $errors->first('City') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="box-footer">
                        {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                        <a class="btn btn-default btn-close" href="{{ URL::to('admin/state') }}">Cancel</a>
                    </div>
                </div>
            </div>
    {!! Form::close() !!}
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>City List</h4>
                    <div class="clearfix"></div>
                </div>

                    {!! Form::open(array('url' => ('admin/state/update'),'method'=>'POST', 'files'=>true)) !!}

                    <input type="hidden" name="id" value="{{ $state->id }}">

                    @foreach($City as $key => $cityKey)
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="box-primary">
                                <div class="form-group col-md-3 col-sm-3 col-xs-12">
                                    <input type="text" name="city[{{ $cityKey->id }}][{{ $cityKey->state_id }}]" placeholder="City" value="{{ $cityKey->city }}" required="" class="form-control">
                                    <span class="text-danger">{{ $errors->first('city') }}</span>
                                </div>

                                <div class="form-group col-md-3 col-sm-3 col-xs-12">
                                    <span class="trashRow trashRow_{{ $cityKey->id }} remove" trashid="{{ $cityKey->id }}" dataid="{{ $cityKey->id }}">
                                        <i class="fa fa-trash" style="font-size: 18px;padding-top: 2%"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="box-footer">
                        {!! Form::submit('Update', array('class' => 'btn btn-primary submit')) !!}
                        <a class="btn btn-default btn-close" href="{{ URL::to('admin/state') }}">Cancel</a>
                    </div>
                </div>
            </div>
    {!! Form::close() !!}                    
        
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('public\ckeditor\ckeditor.js') }}" type="text/javascript"> </script>

<script>
    CKEDITOR.replace('summary-ckeditor');
</script>
<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);

    $(document).on("click", '.trashRow', function(event) {
            var rowId  = $(this).attr('trashId');
            var dataId = $(this).attr('dataId');
            // console.log(dataId);
            var answer=confirm("Are you sure you want to delete this record?");
            if(answer==true) {
                if(dataId != undefined){
                    $.ajax({
                        type: "GET",
                        url: base_url+"/admin/state/delete/"+rowId,
                        async:false,
                        data: { 'id' : rowId, '_token' : "{{ csrf_token() }}" },
                        success:function(data) {
                            
                            if(data == "true"){
                                $('.trashRow_'+rowId).parent().parent().parent().remove();
                                $('.childRow_'+rowId).remove();
                                $('.show_message').html("<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>Item removed successfully.</div>");
                                setTimeout(function() {
                                    $(".alert-success").hide()
                                }, 10000);
                            } else {
                                alert("This property can not be deleted now.");
                            }
                        }
                    });
                }
            } else {
                return false;
            }
        });
</script>
@endpush('scripts')
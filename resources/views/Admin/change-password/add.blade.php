@extends('Admin.master_layout.master')

@section('title', 'Change Password')

@section('breadcum')
     / Change Password
@endsection

@section('content')

<div class="row">
    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('success') !!}</div>
    @endif

    <div class="col-md-12 col-sm-12 col-xs-12">
    @if(count($errors))
        <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your request.
            <br/>
        </div>
    @endif
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Change Password</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/change-password/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">
                    <input type="hidden" name="id" value="{{ $data }}">

                    <div class="form-group <?php  echo $errors->first('new_password') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('old_password', 'Old Password',array('class'=>'control-label')) !!}
                        <span class="required">*</span>
                        <!-- {{ Form::password('new_password', null,array('class' => 'form-control', 'id' => 'new_password')) }} -->
                        <input type="password" name="old_password" id="old_password" class="form-control">
                        <span class="text-danger" >{{ $errors->first('old_password') }}</span>
                    </div>

                    <div class="form-group <?php  echo $errors->first('new_password') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('new_password', 'New Password',array('class'=>'control-label')) !!}
                        <span class="required">*</span>
                        <!-- {{ Form::password('new_password', null,array('class' => 'form-control', 'id' => 'new_password')) }} -->
                        <input type="password" name="new_password" id="new_password" class="form-control">
                        <span class="text-danger" >{{ $errors->first('new_password') }}</span>
                    </div>

                    <div class="form-group <?php  echo $errors->first('conf_password') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('conf_password', 'Confirm Password',array('class'=>'control-label')) !!}
                        <span class="required">*</span>
                        <!-- {{ Form::password('conf_password', null, array('class' => 'form-control', 'id' => 'conf_password')) }} -->
                        <input type="password" name="conf_password" id="conf_password" class="form-control">
                        <span class="text-danger" >{{ $errors->first('conf_password') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="box-footer">
                {!! Form::submit('Change Password', array('class' => 'btn btn-primary submit')) !!}
                <a class="btn btn-default btn-close" href="{{ URL::to('/admin') }}">Cancel</a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);
</script>
@endpush('scripts')
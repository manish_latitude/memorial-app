@extends('Admin.master_layout.master')

@section('title', 'Create Member')

@section('breadcum')
     / Create Member
@endsection

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    @if(count($errors))
        <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your request.
            <br/>
        </div>
    @endif
    </div>
    {!! Form::open(array('url' => ('admin/member/store'),'method'=>'POST', 'files'=>true)) !!}
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h4>Member Details</h4>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-6">
                <div class="form-group <?php echo $errors->first('funeral_home') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('funeral_home','Funeral home',array('class'=>'control-label')) !!} 
                    <span class="required">*</span>
                    <select name="funeral_home" class="form-control">
                        <option value="0">None</option>
                        @foreach($funeral as $key => $value)
                        <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>

                    <!-- {!! Form::select('funeral_home',isset($funeral)?$funeral:[],'',['class' => 'form-control']) !!}   -->
                    <span class="text-danger" >{{ $errors->first('funeral_home') }}</span>
                </div>

                <div class="form-group <?php echo $errors->first('community') !== '' ? 'has-error' : ''; ?>">
                    {!! Form::label('community','Community',array('class' => 'control-label')) !!}
                    <span class="required">*</span>
                    <select name="community" id="community" class="form-control">
                        <option value="">-- Select Community or Add Community --</option>
                        @foreach($community as $key => $value)
                        <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                        <option value="Other">Other</option>
                    </select>
                    <input type="text" name="other" id="other" style="display: none;" class="form-control">
                    <span class="text-danger" >{{ $errors->first('community') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('member_name') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('member_name' , 'Deceased member name',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {{ Form::text('member_name', null,array('class' => 'form-control', 'id' => 'member_name','placeholder' => 'Name')) }}
                    <span class="text-danger" >{{ $errors->first('member_name') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('country') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('country', 'Country',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    <select name="country" id="country" class="form-control">
                        <option value="">-- Select Country --</option>
                        @foreach($country as $c)
                            <option value="{{ $c->id }}">{{ $c->country }}</option>
                        @endforeach
                    </select>
                    <span class="text-danger" >{{ $errors->first('country') }}</span>
                </div>

               <!--  <div class="form-group <?php  echo $errors->first('state') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('state', 'State',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    <select name="state" id="state" class="form-control">
                        <option value="">-- Select State --</option>
                    </select>
                    <span class="text-danger" >{{ $errors->first('state') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('city') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('city', 'City',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    <select name="city" id="city" class="form-control">
                        <option value="">-- Select City --</option>
                    </select>
                    <span class="text-danger" >{{ $errors->first('city') }}</span>
                </div> -->
            </div>
            <div class="col-md-6">
               <!--  <div class="form-group <?php  echo $errors->first('area') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('area' , 'Area',array('class'=>'control-label')) !!}
                    {{ Form::text('area', null,array('class' => 'form-control', 'id' => 'area','placeholder' => 'Area')) }}
                    <span class="text-danger" >{{ $errors->first('area') }}</span>
                </div> -->

                <div class="form-group <?php  echo $errors->first('dateOfbirth') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('dateOfbirth', 'Date Of Birth',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    <div class='input-group date' >
                        <input type='text' class="form-control" name="dateOfbirth" id='datetimepicker1' placeholder="Date Of Birth" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span class="text-danger" >{{ $errors->first('dateOfbirth') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('dateOfpass') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('dateOfpass', 'Date Of Passing',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    <div class='input-group date'>
                        <input type='text' class="form-control" name="dateOfpass"  id='datetimepicker2' placeholder="Date Of Pass" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <!-- {!! Form::date('dateOfpass', null, array('class' => 'form-control', 'id' => 'dateOfpass')) !!} -->
                    <span class="text-danger" >{{ $errors->first('dateOfpass') }}</span>
                </div>
<!-- 
                <div class="form-group <?php  echo $errors->first('profile_image') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('profile_image', 'Profile Image',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {!! Form::file('profile_image', null, array('class' => 'form-control', 'id' => 'profile_image')) !!}
                    <span class="text-danger" >{{ $errors->first('profile_image') }}</span>
                </div> -->
            </div>        
            <div class="col-md-12 col-sm-12 col-xs-12">        
                <div class="form-group <?php  echo $errors->first('about') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('about', 'About',array('class'=>'control-label')) !!}
                    {!! Form::textarea('about', null, array('class' => 'form-control', 'id' => 'summary-ckeditor','placeholder' => 'Member Information')) !!}
                    <span class="text-danger" >{{ $errors->first('about') }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h4>Member Images</h4>
                <div class="clearfix"></div>
            </div>

            <div class="form-group <?php  echo $errors->first('image') !== '' ? 'has-error' : '';?>">
                {!! Form::label('image', 'Images',array('class'=>'control-label')) !!}
                <input type="file" name='image[]' id="files" multiple="">
                <div id="selectedFiles"></div>
                <span class="text-danger" >{{ $errors->first('image') }}</span>
            </div>

            <div class="form-group <?php echo $errors->first('video') !== '' ? 'has-error' : '';?>">
                {!! Form::label('video','Videos',array('class' => 'control-label')) !!}
                <input class="form-control" type="text" name="video[]" id="video" placeholder="Video Url">
                <span class="text-danger" >{{ $errors->first('video') }}</span>
            </div>
            <div id="videoid"></div>
            <input type="button" id="addVideo" value="Add More" class="btn btn-sm btn-xs btn-success pull-right">
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_title">
                <h4>Order Of Service</h4>
                <div class="clearfix"></div>
            </div>

            <div class="form-group <?php  echo $errors->first('order') !== '' ? 'has-error' : '';?>">
                {!! Form::label('order', 'Order of service pictures',array('class'=>'control-label')) !!}
                <input type="file" name='order[]' id="order" multiple="">
                <div id="selectedOrder"></div>
                <span class="text-danger" >{{ $errors->first('order') }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="box-footer">
                {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                <a class="btn btn-default btn-close" href="{{ URL::to('admin/member') }}">Cancel</a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
    
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('public\ckeditor\ckeditor.js') }}" type="text/javascript"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
<script type="text/javascript">
    $(document).ready(function(){
        $("#addVideo").click(function(){
            $("#videoid").append('<div class="form-group <?php echo $errors->first('video') !== '' ? 'has-error' : '';?>"><input class="form-control" type="text" name="video[]" id="video" placeholder="Video Url"><span class="text-danger" >{{ $errors->first('video') }}</span>');
        });
        $('#datetimepicker1').datepicker();
        $('#datetimepicker2').datepicker();
    });
    $('#datetimepicker1').on('click',function(){
        $('#datetimepicker1').datepicker().datepicker('setDate',new Date());     
    });      
    $('#datetimepicker2').on('click',function(){
       $('#datetimepicker2').datepicker().datepicker('setDate',new Date()); 
    });
</script>

<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);    

    CKEDITOR.replace('summary-ckeditor');

    $(document).ready(function(){
        $('select[name="country"]').on('change',function(){
            var countryId = $(this).val();

            if(countryId){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('admin/member/stateList') }}?country_id="+countryId,
                    dataType: "json",
                    success: function(res){
                        if(res){
                            $('select[name="state"]').html('<option value="">-- Select State --</option>');
                            $.each(res,function(key,value){
                                $('select[name="state"]').append('<option value="'+key+'">'+value+'</option>');
                            });
                        }else{
                            $('select[name="state"]').empty();
                        }
                    }
                });
            }else{
                $('select[name="state"]').empty();
                $('select[name="city"]').empty();
            }
        });
        $('select[name="state"]').on('change',function(){
            var stateId = $(this).val();

            if(stateId){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('admin/member/cityList') }}?state_id="+stateId,
                    dataType: "json",
                    success: function(data){
                        $('select[name="city"]').html('<option value="">-- Select City --</option>');
                        $.each(data,function(key, value){
                            $('select[name="city"]').append('<option value="'+key+'">'+value+'</option>');
                        });
                    }
                });
            }else{
                $('select[name="city"]').empty();
            }
        });

        $('select#community').change(function(){
            var selectObj = $('#community option:selected').val();
            if(selectObj == 'Other'){
                $('select#community').after("<br/>");
                $('#other').show();
            }else{
                $('br').remove();
                $('#other').hide();
            }
        });
    });

    var selDiv = "";
    document.addEventListener("DOMContentLoaded", init, false);
    function init(){
        document.querySelector('#files').addEventListener('change',handleFileSelect,false);
        selDiv = document.querySelector('#selectedFiles');
    }

    function handleFileSelect(e){
        if(!e.target.files){
            return;
        } 
        // if(!e.target.order){
        //     return;
        // }
        selDiv.innerHTML = "";
        var files = e.target.files;
        for(var i=0; i<files.length; i++){
            var f = files[i];
            selDiv.innerHTML += f.name + "<br/>";
        }

        // selOrder.innerHTML = "";
        // var order = e.target.order;
        // for(var i=0; i<order.length; i++){
        //     var f = order[i];
        //     selOrder.innerHTML += f.name + "<br/>";
        // }
    }

    var selOrder = "";

    document.addEventListener("DOMContentLoaded", initObj, false);
    function initObj(){
        document.querySelector('#order').addEventListener('change',handleFileSelects,false);
       selOrder = document.querySelector('#selectedOrder');
    }

    function handleFileSelects(e1){
        if(!e1.target.files){
            return;
        } 

        selOrder.innerHTML = "";
        var order = e.target.files;
        for(var i=0; i<order.length; i++){
            var f = order[i];
            selOrder.innerHTML += f.name + "<br/>";
        }
    }
</script>

@endpush('scripts')
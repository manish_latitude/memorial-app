@extends('Admin.master_layout.master')

@section('title', 'Edit Member')

@section('breadcum')
     / Edit Member
@endsection

@section('content')

<style>

</style>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    @if(count($errors))
        <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your request.
            <br/>
        </div>
    @endif
    </div>

    {!! Form::open(array('url' => ('admin/member/update'),'method'=>'POST', 'files'=>true)) !!}
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h4>Member Details</h4>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-6">
                <input type="hidden" name="id" value="{{ $member->id }}">
                 <div class="form-group <?php echo $errors->first('funeral_home') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('funeral_home','Funeral home',array('class'=>'control-label')) !!} 
                    <span class="required">*</span>
                    {!! Form::select('funeral_home',isset($funeral)?$funeral:[],$member->funeral_id,['placeholder' => 'Select Funeral','class' => 'form-control']) !!}  
                    <span class="text-danger" >{{ $errors->first('funeral_home') }}</span>
                </div>
                <div class="form-group <?php echo $errors->first('community') !== '' ? 'has-error' : ''; ?>">
                    {!! Form::label('community','Community',array('class' => 'control-label')) !!}
                    <span class="required">*</span>
                    <select name="community" id="community" class="form-control">
                        <!-- <option value="">-- Select Community or Add Community --</option> -->
                        {{--<option value="{{ $member->community_id }}">{{ $member->community->community_id }}</option>--}}
                        @foreach($community as $c)
                        <option value="{{ $c->id }}">{{ $c->community }}</option>
                        @endforeach
                        <option value="Other">Other</option>
                    </select>
                    <input type="text" name="other" id="other" style="display: none;" class="form-control">
                    <span class="text-danger" >{{ $errors->first('community') }}</span>
                </div>
                <div class="form-group <?php  echo $errors->first('member_name') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('member_name' , 'Deceased member name',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {{ Form::text('member_name', $member->name,array('class' => 'form-control', 'id' => 'member_name','placeholder' => 'Name')) }}
                    <span class="text-danger" >{{ $errors->first('member_name') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('country') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('country', 'Country',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {!! Form::select('country',isset($country)?$country:[],$member->country_id,['placeholder' => '-- Select Country --','class' => 'form-control','id'=>'country']) !!}
                    
                    <span class="text-danger" >{{ $errors->first('country') }}</span>
                </div>

               <!--  <div class="form-group <?php  echo $errors->first('state') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('state', 'State',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {!! Form::select('state',isset($state)?$state:[],$member->state_id,['placeholder' => '-- Select State --','class' => 'form-control','id'=>'state']) !!}
                    <span class="text-danger" >{{ $errors->first('state') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('city') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('city', 'City',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {!! Form::select('city',isset($city)?$city:[],$member->city_id,['placeholder' => '-- Select City --','class' => 'form-control','id'=>'city']) !!}
                    <span class="text-danger" >{{ $errors->first('city') }}</span>
                </div> -->
            </div>
            <div class="col-md-6">
               <!--  <div class="form-group <?php  echo $errors->first('area') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('area' , 'Area',array('class'=>'control-label')) !!}
                    {{ Form::text('area', $member->area,array('class' => 'form-control', 'id' => 'area','placeholder' => 'Area')) }}
                    <span class="text-danger" >{{ $errors->first('area') }}</span>
                </div> -->

                <div class="form-group <?php  echo $errors->first('dateOfbirth') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('dateOfbirth', 'Date Of Birth',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    <div class='input-group date'>
                        <input type='text' class="form-control" name="dateOfbirth" value="{{ date('m/d/Y',strtotime($member->dateOfbirth)) }}" id='datetimepicker1'/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span class="text-danger" >{{ $errors->first('dateOfbirth') }}</span>
                </div>

                <div class="form-group <?php  echo $errors->first('dateOfpass') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('dateOfpass', 'Date Of Passing',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    <div class='input-group date'>
                        <input type='text' class="form-control" name="dateOfpass" value="{{ date('m/d/Y',strtotime($member->dateOfpass)) }}"  id='datetimepicker2'/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <!-- {!! Form::date('dateOfpass', null, array('class' => 'form-control', 'id' => 'dateOfpass')) !!} -->
                    <span class="text-danger" >{{ $errors->first('dateOfpass') }}</span>
                </div>

                <!-- <div class="form-group <?php  echo $errors->first('profile_image') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('profile_image', 'Profile Image',array('class'=>'control-label')) !!}
                    <span class="required">*</span>
                    {{ Form::file('profile_image', null, array('class' => 'form-control', 'id' => 'profile_image')) }}
                    <br>
                    <center><img id="myImg" width="100px" height="100px" src="{{url('public/images/member-profile/'.$member->image)}}" onerror="this.src='{{ url("public/images/avatar.png") }}'" name="image"></center>
                    <br>
                    <center><a href="javascript:void(0)" onclick="profile('{{ $member->id }}')">Delete</a></center>
                    <span class="text-danger" >{{ $errors->first('profile_image') }}</span>
                </div> -->
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group <?php  echo $errors->first('about') !== '' ? 'has-error' : '';?>">
                    {!! Form::label('about', 'About',array('class'=>'control-label')) !!}
                    {{ Form::textarea('about', $member->about, array('class' => 'form-control', 'id' => 'summary-ckeditor','placeholder' => 'Member Information')) }}
                    <span class="text-danger" >{{ $errors->first('about') }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h4>Member Images</h4>
                <div class="clearfix"></div>
            </div>

            <div class="form-group <?php  echo $errors->first('image') !== '' ? 'has-error' : '';?>">
                {!! Form::label('image', 'Images',array('class'=>'control-label')) !!}
                <input type="file" name='image[]' id="files" multiple="">
                <div id="selectedFiles"></div>
                <br>
                @foreach($mimg as $img)
                    @foreach($img as $i)
                        <img width="100px" height="100px" src="{{url('public/images/member/'.$i->image_video)}}" name="update_image[]" >
                        <a href="javascript:void(0)" onclick="img('{{ $i->id }}');">Delete</a>&nbsp;&nbsp;
                    @endforeach
                @endforeach
                <span class="text-danger" >{{ $errors->first('image') }}</span>
            </div>

            <div class="form-group <?php echo $errors->first('video') !== '' ? 'has-error' : '';?>">
                {!! Form::label('video','Videos',array('class' => 'control-label')) !!}
                <input class="form-control" type="text" name="video[]" id="video" placeholder="Video Url">
                <br>
                @foreach($mv as $v)
                    @foreach($v as $v1)
                        {{ Form::text('video[]',$v1->image_video, array('class' => 'form-control', 'id' => 'update_video','placeholder' => 'Video Url')) }}<br>
                    @endforeach
                @endforeach
                <span class="text-danger" >{{ $errors->first('video') }}</span>
            </div>
            <div id="videoid"></div>
            <input type="button" id="addVideo" value="Add More" class="btn btn-sm btn-xs btn-success pull-right">
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h4>Order Of Service</h4>
                <div class="clearfix"></div>
            </div>

            <div class="form-group <?php  echo $errors->first('order') !== '' ? 'has-error' : '';?>">
                {!! Form::label('order', 'Order of service pictures',array('class'=>'control-label')) !!}
                <input type="file" name='order[]' multiple="">
                <br>
                @foreach($order as $img)
                    @foreach($img as $i)
                        <img width="100px" height="100px" src="{{url('public/images/OrderOfService/'.$i->image)}}" name="update_order[]" >
                        <a href="javascript:void(0)" onclick="order('{{ $i->id }}');">Delete</a>&nbsp;&nbsp;
                     @endforeach
                @endforeach   
                <span class="text-danger" >{{ $errors->first('order') }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="box-footer">
                {!! Form::submit('Update', array('class' => 'btn btn-primary submit')) !!}
                <a class="btn btn-default btn-close" href="{{ URL::to('admin/member') }}">Cancel</a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('public\ckeditor\ckeditor.js') }}" type="text/javascript"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
<script type="text/javascript">
    $(document).ready(function(){
        $("#addVideo").click(function(){
            $("#videoid").append('<div class="form-group <?php echo $errors->first('video') !== '' ? 'has-error' : '';?>"><input class="form-control" type="text" name="video[]" id="video" placeholder="Video Url"><span class="text-danger" >{{ $errors->first('video') }}</span>');
        });  
        $('#datetimepicker1').datepicker();
        $('#datetimepicker2').datepicker();
    });
</script>

<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);    

    CKEDITOR.replace('summary-ckeditor');

    function img(id){
        //alert(id);
        $.ajax({
            url: "{{ url('admin/member/image-update') }}",
            type: 'GET',
            async: false,
            data: {'id':id},
            success: function(data){
                if(data == "success"){
                    location.reload();
                }
            },
            error: function(){
                alert('error');
            }
        })
    }

    function order(id){
        //alert(id);
        $.ajax({
            url: "{{ url('admin/member/order-update') }}",
            type: 'GET',
            async: false,
            data: {'id':id},
            success: function(data){
                if(data == "success"){
                    location.reload();
                }
            },
            error: function(){
                alert('error');
            }
        })
    }

    function profile(id){
        $.ajax({
            url: "{{ url('admin/member/profile-update') }}",
            type: 'GET',
            data: {'id':id},
            success: function(data){
                if(data == "true"){
                    location.reload();
                }
            },
            error: function(){
                alert('error');
            }
        })
    }

    $('body').append('<div class="product-image-overlay"><span class="product-image-overlay-close">x</span><img src="" /></div>');
    var productImage = $('img');
    var productOverlay = $('.product-image-overlay');
    var productOverlayImage = $('.product-image-overlay img');

    productImage.click(function () {
        var productImageSource = $(this).attr('src');

        productOverlayImage.attr('src', productImageSource);
        productOverlay.fadeIn(100);
        $('body').css('overflow', 'hidden');

        $('.product-image-overlay-close').click(function () {
            productOverlay.fadeOut(100);
            $('body').css('overflow', 'auto');
        });
    });

    $(document).ready(function(){
        $('select[name="country"]').on('change',function(){
            var countryId = $(this).val();

            if(countryId){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('admin/member/stateList') }}?country_id="+countryId,
                    dataType: "json",
                    success: function(res){
                        if(res){
                            $('select[name="state"]').html('<option value="">-- Select State --</option>');
                            $.each(res,function(key,value){
                                $('select[name="state"]').append('<option value="'+key+'">'+value+'</option>');
                            });
                        }else{
                            $('select[name="state"]').empty();
                        }
                    }
                });
            }else{
                $('select[name="state"]').append('<option>-- Select State --</option>');
                $('select[name="city"]').empty();
            }
        });
        $('select[name="state"]').on('change',function(){
            var stateId = $(this).val();

            if(stateId){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('admin/member/cityList') }}?state_id="+stateId,
                    dataType: "json",
                    success: function(data){
                        $('select[name="city"]').html('<option value="">-- Select State --</option>');
                        $.each(data,function(key, value){
                            $('select[name="city"]').append('<option value="'+key+'">'+value+'</option>');
                        });
                    }
                });
            }else{
                $('select[name="city"]').empty();
            }
        });
        $('select#community').change(function(){
            var selectObj = $('#community option:selected').val();
            if(selectObj == 'Other'){
                $('select#community').after("<br/>");
                $('#other').show();
            }else{
                $('br').remove();
                $('#other').hide();
            }
        });
    }); 

    var selDiv = "";
    document.addEventListener("DOMContentLoaded", init, false);
    function init(){
        document.querySelector('#files').addEventListener('change',handleFileSelect,false);
        selDiv = document.querySelector('#selectedFiles');
    }

    function handleFileSelect(e){
        if(!e.target.files){
            return;
        } 
        selDiv.innerHTML = "";
        var files = e.target.files;
        for(var i=0; i<files.length; i++){
            var f = files[i];
            selDiv.innerHTML += f.name + "<br/>";
        }
    }
</script>

@endpush('scripts')
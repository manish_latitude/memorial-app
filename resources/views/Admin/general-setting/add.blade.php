@extends('Admin.master_layout.master')

@section('title', 'Create Setting')

@section('breadcum')
     / Create Setting
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create Setting</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('admin/setting/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">

                        @foreach($data as $d)
                            @if($d->slug == "appname")
                                <div class="form-group">
                                    {!! Form::label($d->title, $d->title,array('class'=>'control-label')) !!}
                                    {{ Form::text('setting['.$d->id.']['.$d->slug.']',!empty($d->value)?$d->value:'',array('class' => 'form-control', 'id' => $d->slug)) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif

                            @if($d->slug == "HomeDesc")
                                <div class="form-group">
                                    {!! Form::label($d->title,$d->title,array('class'=>'control-label')) !!}
                                    {{ Form::textarea('setting['.$d->id.']['.$d->slug.']',!empty($d->value)?$d->value:'',array('class' => 'form-control', 'id' => 'summary-ckeditor')) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif

                            @if($d->slug == "contactMail")
                                <div class="form-group">
                                    {!! Form::label($d->title, $d->title, array('class' => 'control-label')) !!}
                                    {!! Form::text('setting['.$d->id.']['.$d->slug.']',!empty($d->value)?$d->value:'',array('class' => 'form-control','id' => $d->slug)) !!}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif

                        @endforeach
                        <!-- <div class="form-group <?php  echo $errors->first('price') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('slug', 'Slug*',array('class'=>'control-label')) !!}
                            {{ Form::text('slug', null, array('class' => 'form-control', 'id' => 'price','placeholder' => 'Price')) }}
                            <span class="text-danger" >{{ $errors->first('price') }}</span>
                        </div> -->

                        <!-- <div class="form-group <?php  echo $errors->first('description') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('description', 'Description',array('class'=>'control-label')) !!}
                            {{ Form::textarea('description', null, array('class' => 'form-control', 'id' => 'summary-ckeditor')) }}
                            <span class="text-danger" >{{ $errors->first('description') }}</span>
                        </div> -->

                        <div class="box-footer">
                            {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('admin/setting') }}">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('public\ckeditor\ckeditor.js') }}" type="text/javascript"> </script>

<script>
    CKEDITOR.replace('summary-ckeditor');
</script>
<script type="text/javascript">
    setTimeout(function() {
            $(".alert-danger").hide()
    }, 10000);
    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);
</script>
@endpush('scripts')
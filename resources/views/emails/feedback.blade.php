<!DOCTYPE html>

<html>

  <head>

    <meta charset="UTF-8">

    <title></title>

    

  </head>

  <body style="padding:0px;margin:0px;">

    <div class="email-background" style="width:100%;background-color:#189b65;padding:15px 0px;font-family:'HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;">

      <div class="email-container" style="max-width:600px;border-radius:5px;margin:0 auto;padding-bottom:5px;background-color:white;">

        <table width="100%" style="border:0px;">

          <tr class="logo" align="center" cellpadding="20" style="border:0px;">

            <td style="border:0px;"><a href="#"><a href=""><div style="width: 100px; margin: 10px 0px;"><h3>GUPTA MIB</h3></div>{{-- <img src="{{ $message->embed('front/images/logo.png') }}" width="100px" style="margin:10px 0px;"> --}}</a></td>

          </tr>

          <tr style="border:0px;">

            <table class="follow-us" width="100%" style="border:0px;">

              <tr align="center" class="follow-us-text" style="border:0px;font-size:14px;font-weight:600;">

                <td style="border:0px;">

                  <p style="margin:0;">Follow us on:</p>

                </td>

              </tr>

              <tr style="border:0px;">

                <table class="follow-us-icons" width="100%" style="border:0px;height:15px;">

                  <td class="white-space" style="border:0px;width:35%;"></td>

                  <td align="center" style="border:0px;"><a href="#"><img src="http://prasaddining.com/public/img/facebook-green-01.png" style="margin:0px;width:80%;"></a></td>

                  <td align="center" style="border:0px;"><a href="#"><img src="http://prasaddining.com/public/img/instagram-green-01.png" style="margin:0px;width:80%;"></a></td>

                  <td align="center" style="border:0px;"><a href="#"><img src="http://prasaddining.com/public/img/twitter-green-01.png" style="margin:0px;width:80%;"></a></td>

                  <td class="white-space" style="border:0px;width:35%;"></td>

                </table>

              </tr>

            </table>

          </tr>

          <tr style="border:0px;">

            <table class="thank-you" width="100%" style="border:0px;margin:5px 0px;">

              <tr style="border:0px;">

                <td align="center" style="border:0px;">

                  <h3 style="margin:0;font-weight:300;font-size:20px;">Hello, <span style="font-weight:600;">{{$name}}</span>

                  </h3>

                </td>

              </tr>

               <tr style="border:0px;">

                <td align="center" style="border:0px;">for inquiry @ GUPTA MIB</td>

              </tr>

            </table>

          </tr>

          <tr style="border:0px;">

            <table align="center" class="note" width="100%" style="border:0px;background-color:#AB5236;">

              <tr style="border:0px;">

                <td align="center" style="border:0px;padding:5px;font-size:14px;color:#FFFFFF;">

                  <p style="margin:0;"><span style="font-weight:600;font-size:16px !important;">Thanks for your Feedback</span></p>

                </td>

              </tr>

             

            </table>

          </tr>

          <tr style="border:0px;">

            <table class="customer-details" width="100%" cellspacing="10" style="border:0px;margin:10px 0px;">

              <tr style="border:0px;">

                <td class="heading" align="center" colspan="2" style="border:0px;font-weight:500;padding-bottom:5px;width:50%;font-size:15px;font-size:18px !important;">Please verify your details.</td>

              </tr>

              <tr style="border:0px;">

                <td valign="top" style="border:0px;width:50%;font-size:15px;"><span style="float:right;text-align:right;padding-right:5px;font-weight:600;">Name:</span></td>

                <td valign="top" style="border:0px;width:50%;font-size:15px;">

                  <p style="margin:0;">{{$name}}</p>

                </td>

              </tr>

              <tr style="border:0px;">

                <td valign="top" style="border:0px;width:50%;font-size:15px;"><span style="float:right;text-align:right;padding-right:5px;font-weight:600;">E-mail:</span></td>

                <td valign="top" style="border:0px;width:50%;font-size:15px;">

                  <p style="margin:0;">{{$email}}</p>

                </td>

              </tr>

              <tr style="border:0px;">

                <td valign="top" style="border:0px;width:50%;font-size:15px;"><span style="float:right;text-align:right;padding-right:5px;font-weight:600;">Mobile No:</span></td>

                <td valign="top" style="border:0px;width:50%;font-size:15px;">

                  <p style="margin:0;">{{$mobile_no}}</p>

                </td>

              </tr>

              <tr style="border:0px;">

                <td valign="top" style="border:0px;width:50%;font-size:15px;"><span style="float:right;text-align:right;padding-right:5px;font-weight:600;">Inquiry</span></td>

                <td valign="top" style="border:0px;width:50%;font-size:15px;">

                  <p style="margin:0;">{{$note}}</p>

                </td>

              </tr>

               

            </table>

          </tr>

          

          <tr style="border:0px;">

            <table class="footer-signature" width="100%" style="border:0px;background-color:#E1E8ED;margin:20px auto 0px;padding:10px;text-align:center;font-size:14px;">

              <tr style="border:0px;">

                <td style="border:0px;">••• ••• •••<br>

                  Cheers,<br>

                  Team GUPTA MIB<br>

                  Call: +91 ----------<br>

                  Email: care@mib.Com</td>

              </tr>

            </table>

          </tr>

          <tr style="border:0px;">

            <table class="made-by-unity" width="100%" style="border:0px;font-size:14px;text-align:center;">

              <tr style="border:0px;">

                <td valign="top" style="border:0px;"><a href="http://www.unityinfoway.com/" style="text-decoration:none;color:black;">Made with <span style="font-size:20px;color:red;font-weight:600;">❤</span> by <b>UNITY</b></a></td>

              </tr>

            </table>

          </tr>

        </table>

      </div>

    </div>

  </body>

</html>
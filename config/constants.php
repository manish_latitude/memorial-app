<?php
return [
    'site_name' 	=> 'Keep Their Memory Alive',
    'fromMail' 		=> 'info@keeptheirmemoryalive.com',
    'labelMail' 	=> 'Email Verification',
    'sender_email'  => 'no-reply@memorial.com',
];

